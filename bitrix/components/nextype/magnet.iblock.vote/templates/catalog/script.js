(function (window){
	'use strict';

	if (window.JMagnetVotes)
		return;
            
        window.JMagnetVotes = function (arParams)
	{
            var _ = this;
            this.container = false;
            this.params = arParams;
            
            if (!!this.params.CONTAINER_ID)
                this.container = $('#' + this.params.CONTAINER_ID);
            
            this.container.find('.star-active').on('click', function (event) {
                event.preventDefault();
                var voteValue = parseInt($(this).data('value'))
                
                if (voteValue <= 0)
                    return false;
                
                BX.ajax.post(
			_.params.AJAX_URL,
                        {
                            vote: 'Y',
                            vote_id: _.params.ITEM_ID,
                            rating: voteValue,
                            AJAX_CALL: 'Y',
                            PAGE_PARAMS: {
                                ELEMENT_ID: _.params.ITEM_ID
                            },
                            SESSION_PARAMS: _.params.SESSION_PARAMS,
                            sessid: _.params.SESSION_ID
                        },
			function (result) {
                            if (!!result && result.length > 0)
                            {
                                var ajaxContent = $(result);
                                _.container.html(ajaxContent.html());
                            }
                        }
		);
                
                return false;
            });
            
            this.container.find('.star-active').on('mouseover', function () {
                var current = parseInt($(this).data('value'));
                $(this).closest('tr').find('.star-active').each(function (index, value) {
                    if (current >= index)
                        $(this).addClass('star-over');
                });
                
                
            });
            
            this.container.find('.star-active').on('mouseout', function () {
                $(this).closest('tr').find('.star-active').removeClass('star-over');
            });
            
        }
        
})(window);