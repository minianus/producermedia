<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

CJSCore::Init(array("ajax"));
CSolution::getInstance(SITE_ID);

$areaId = 'vote' . $arResult["ID"] . randString(4);
$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);

$jsParams = Array (
    'ITEM_ID' => $arResult["ID"],
    'CONTAINER_ID' => $areaId,
    'AJAX_URL' => CUtil::JSEscape($component->getPath().'/component.php'),
    'SESSION_PARAMS' => $arResult['~AJAX_PARAMS']['SESSION_PARAMS'],
    'SESSION_ID' => $arResult['~AJAX_PARAMS']['sessid'],
);

//Let's determine what value to display: rating or average ?
if($arParams["DISPLAY_AS_RATING"] == "vote_avg")
{
	if($arResult["PROPERTIES"]["vote_count"]["VALUE"])
		$DISPLAY_VALUE = round($arResult["PROPERTIES"]["vote_sum"]["VALUE"]/$arResult["PROPERTIES"]["vote_count"]["VALUE"], 2);
	else
		$DISPLAY_VALUE = 0;
}
else
	$DISPLAY_VALUE = $arResult["PROPERTIES"]["rating"]["VALUE"];

?>

<? if (CSolution::$options['CATALOG_ENABLED_EXTENDED_REVIEWS'] != "Y"): ?>
<div class="iblock-vote" id="<?=$areaId?>">
    <table class="vote-table">
            <tr>
            <?if($arResult["VOTED"] || $arParams["READ_ONLY"]==="Y"):?>
                    <?if($DISPLAY_VALUE):?>
                            <?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
                                    <?if(round($DISPLAY_VALUE) > $i):?>
                                            <td><div class="star-voted" title="<?echo $name?>"></div></td>
                                    <?else:?>
                                            <td><div class="star-empty" title="<?echo $name?>"></div></td>
                                    <?endif?>
                            <?endforeach?>
                    <?else:?>
                            <?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
                                    <td><div class="star" title="<?echo $name?>"></div></td>
                            <?endforeach?>
                    <?endif?>
            <?else: ?>
                    <?if($DISPLAY_VALUE):?>
                            <?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
                                    <?if(round($DISPLAY_VALUE) > $i):?>
                                            <td><div data-value="<?=$i?>" class="star-active star-voted" title="<?echo $name?>"></div></td>
                                    <?else:?>
                                            <td><div data-value="<?=$i?>" class="star-active star-empty" title="<?echo $name?>"></div></td>
                                    <?endif?>
                            <?endforeach?>
                    <?else:?>
                            <?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
                                    <td><div data-value="<?=$i?>" class="star-active star-empty" title="<?echo $name?>"></div></td>
                            <?endforeach?>
                    <?endif?>
            <?endif?>
            <?if($arResult["PROPERTIES"]["vote_count"]["VALUE"]):?>
                    <td class="vote-result"><div><?echo GetMessage("T_IBLOCK_VOTE_RESULTS", array("#VOTES#"=>$arResult["PROPERTIES"]["vote_count"]["VALUE"] , "#RATING#"=>$DISPLAY_VALUE))?></div></td>
            <?else:?>
                    <td class="vote-result"><div><?echo GetMessage("T_IBLOCK_VOTE_NO_RESULTS")?></div></td>
            <?endif?>
            </tr>
    </table>
</div>

<script>
    var <?=$obName?> = new JMagnetVotes(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>

<? else: ?>

<div class="iblock-vote readonly">
<table class="vote-table">
	<tr>
	
		<?if($DISPLAY_VALUE):?>
			<?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
				<?if(round($DISPLAY_VALUE) > $i):?>
					<td><div class="star-voted" title="<?echo $name?>"></div></td>
				<?else:?>
					<td><div class="star-empty" title="<?echo $name?>"></div></td>
				<?endif?>
			<?endforeach?>
		<?else:?>
			<?foreach($arResult["VOTE_NAMES"] as $i=>$name):?>
				<td><div class="star" title="<?echo $name?>"></div></td>
			<?endforeach?>
		<?endif?>
	
	<?if($arResult["PROPERTIES"]["vote_count"]["VALUE"]):?>
		<td class="vote-result"><div id="wait_vote_<?echo $arResult["ID"]?>"><?echo GetMessage("T_IBLOCK_VOTE_RESULTS", array("#VOTES#"=>$arResult["PROPERTIES"]["vote_count"]["VALUE"] , "#RATING#"=>$DISPLAY_VALUE))?></div></td>
	<?else:?>
		<td class="vote-result"><div id="wait_vote_<?echo $arResult["ID"]?>"><?echo GetMessage("T_IBLOCK_VOTE_NO_RESULTS")?></div></td>
	<?endif?>
	</tr>
</table>
</div>

<? endif; ?>
