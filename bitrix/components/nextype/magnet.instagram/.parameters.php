<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"LAZY_LOAD" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CT_LAZY_LOAD"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		
	),
);