<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль \"Информационные блоки\" не установлен";
$MESS["MAGNET_MODULE_NOT_INSTALLED"] = "Модуль \"nextype.magnet\" не установлен";
$MESS["CATALOG_SECTION_NOT_FOUND"] = "Раздел не найден";
$MESS["CATALOG_EMPTY_SECTIONS_IDS"] = "Не указаны разделы каталога для отображения";
$MESS["CATALOG_EMPTY_IBLOCK_ID"] = "Не указан информационный блок для отображения";