<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component;
use \Nextype\Magnet\CCache;
use \Nextype\Magnet\CSolution;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

if (!\Bitrix\Main\Loader::includeModule('nextype.magnet'))
{
	ShowError(Loc::getMessage('MAGNET_MODULE_NOT_INSTALLED'));
	return;
}

class ProductsSectionComponent extends \Bitrix\Iblock\Component\ElementList
{
	public function __construct($component = null)
	{
		parent::__construct($component);
		
	}

	public function onPrepareComponentParams($params)
	{
		$params = parent::onPrepareComponentParams($params);
        $params["SECTION_PICTURE_SOURCE"] = (empty($params["SECTION_PICTURE_SOURCE"])) ? "PICTURE" : $params["SECTION_PICTURE_SOURCE"];
                        
		return $params;
	}

	

	
	protected function initAdminIconsPanel()
	{
		global $APPLICATION, $INTRANET_TOOLBAR, $USER;

		if (!$USER->IsAuthorized())
		{
			return;
		}

		$arResult =& $this->arResult;

		if (
			$APPLICATION->GetShowIncludeAreas()
			|| (is_object($INTRANET_TOOLBAR) && $this->arParams['INTRANET_TOOLBAR'] !== 'N')
			|| $this->arParams['SET_TITLE']
			|| isset($arResult[$this->arParams['BROWSER_TITLE']])
		)
		{
			if (Loader::includeModule('iblock'))
			{
				$urlDeleteSectionButton = '';

				if ($arResult['IBLOCK_SECTION_ID'] > 0)
				{
					$sectionIterator = CIBlockSection::GetList(
						array(),
						array('=ID' => $arResult['IBLOCK_SECTION_ID']),
						false,
						array('SECTION_PAGE_URL')
					);
					$sectionIterator->SetUrlTemplates('', $this->arParams['SECTION_URL']);
					$section = $sectionIterator->GetNext();
					$urlDeleteSectionButton = $section['SECTION_PAGE_URL'];
				}

				if (empty($urlDeleteSectionButton))
				{
					$urlTemplate = CIBlock::GetArrayByID($this->arParams['IBLOCK_ID'], 'LIST_PAGE_URL');
					$iblock = CIBlock::GetArrayByID($this->arParams['IBLOCK_ID']);
					$iblock['IBLOCK_CODE'] = $iblock['CODE'];
					$urlDeleteSectionButton = CIBlock::ReplaceDetailUrl($urlTemplate, $iblock, true, false);
				}

				$returnUrl = array(
					'add_section' => (
					strlen($this->arParams['SECTION_URL'])
						? $this->arParams['SECTION_URL']
						: CIBlock::GetArrayByID($this->arParams['IBLOCK_ID'], 'SECTION_PAGE_URL')
					),
					'delete_section' => $urlDeleteSectionButton,
				);
				$buttonParams = array(
					'RETURN_URL' => $returnUrl,
					'CATALOG' => true
				);

				if (isset($arResult['USE_CATALOG_BUTTONS']))
				{
					$buttonParams['USE_CATALOG_BUTTONS'] = $arResult['USE_CATALOG_BUTTONS'];
				}

				$buttons = CIBlock::GetPanelButtons(
					$this->arParams['IBLOCK_ID'],
					0,
					$arResult['ID'],
					$buttonParams
				);
				unset($buttonParams);

				if ($APPLICATION->GetShowIncludeAreas())
				{
					$this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $buttons));
				}

				if (
					is_array($buttons['intranet'])
					&& is_object($INTRANET_TOOLBAR)
					&& $this->arParams['INTRANET_TOOLBAR'] !== 'N'
				)
				{
					Main\Page\Asset::getInstance()->addJs('/bitrix/js/main/utils.js');

					foreach ($buttons['intranet'] as $button)
					{
						$INTRANET_TOOLBAR->AddButton($button);
					}
				}

				if ($this->arParams['SET_TITLE'] || isset($arResult[$this->arParams['BROWSER_TITLE']]))
				{
					$this->storage['TITLE_OPTIONS'] = array(
						'ADMIN_EDIT_LINK' => $buttons['submenu']['edit_section']['ACTION'],
						'PUBLIC_EDIT_LINK' => $buttons['edit']['edit_section']['ACTION'],
						'COMPONENT_NAME' => $this->getName(),
					);
				}
			}
		}
	}
        
        
        
        public function executeComponent()
	{
            if (!is_array($this->arParams['SECTIONS_ID']) || empty($this->arParams['SECTIONS_ID']))
            {
                $this->__showError(GetMessage('CATALOG_EMPTY_SECTIONS_IDS'));
                return;
            }
            
            if (empty($this->arParams['IBLOCK_ID']))
            {
                $this->__showError(GetMessage('CATALOG_EMPTY_IBLOCK_ID'));
                return;
            }
            
            try
            {
                $arSectionsFilter = Array (
                    'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                    'ACTIVE' => 'Y',
                    '=ID' => Array ()
                );
                foreach ($this->arParams['SECTIONS_ID'] as $sectionId)
                {
                    if (!empty($sectionId))
                        $arSectionsFilter['=ID'][] = $sectionId;
                }

                if (empty($arSectionsFilter['=ID']))
                {
                    $this->__showError(GetMessage('CATALOG_EMPTY_SECTIONS_IDS'));
                    return;
                }

                $obCache = new \CPHPCache();
                $cacheDir = "/" . SITE_ID . "/magnet.products.section/";
                $cacheTag = "iblock_id_" . $this->arParams['IBLOCK_ID'];
                $cacheId = md5(serialize($arSectionsFilter));
                
                $arSelect = array("*");
                if (!in_array($this->arParams["SECTION_PICTURE_SOURCE"], array("PICTURE", "DETAIL_PICTURE")))
                    $arSelect[] = $this->arParams["SECTION_PICTURE_SOURCE"];
        
                if ($_REQUEST['clear_cache'] != "Y" && $obCache->InitCache($this->arParams['CACHE_TIME'], $cacheId, $cacheDir))
                {
                    $vars = $obCache->GetVars();
                    $this->arResult = $vars['arResult'];
                }
                else
                {
                    $rsSections = \CIBlockSection::GetList(Array ('SORT' => 'ASC'), $arSectionsFilter, false, $arSelect);
                    while ($arSection = $rsSections->GetNext())
                    {
                        $this->arResult['SECTIONS'][] = $arSection;
                    }
                    
                    if ($_REQUEST['clear_cache'] != "Y")
                    {
                        $obCache->StartDataCache($this->arParams['CACHE_TIME'], $cacheId, $cacheDir);
        
                        if ($cacheTag)
                        {
                            $GLOBALS['CACHE_MANAGER']->StartTagCache($cacheDir);
                            $GLOBALS['CACHE_MANAGER']->RegisterTag($cacheTag);
                            $GLOBALS['CACHE_MANAGER']->EndTagCache();
                        }

                        $obCache->EndDataCache(array("arResult" => $this->arResult));
                        
                    }
                }
                
                $this->includeComponentTemplate();

            }
            catch (Exception $exc)
            {
                if ($this->set404)
                {
                    @define("ERROR_404", "Y");
                }
                elseif ($this->showError)
                {
                    $this->__showError($exc->getMessage());
                }
                $this->AbortResultCache();
            }


        }
}