<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

$arResult['TABS'] = Array ();
$arResult['arComponentSectionParams'] = array_merge($arParams, Array (
    'FILTER_NAME' => 'arrTabsFilter',
    'SHOW_ALL_WO_SECTION' => 'Y',
    'SET_TITLE' => 'N',
    'SET_BROWSER_TITLE' => 'N',
    'SET_META_KEYWORDS' => 'N',
    'SET_META_DESCRIPTION' => 'N',
    'SET_LAST_MODIFIED' => 'N'
));

if (is_array($arParams['TAGS_CODE']))
{
    foreach ($arParams['TAGS_CODE'] as $key => $val)
    {
        if (empty($val))
            unset($arParams['TAGS_CODE'][$key]);
    }
}

if (!empty($arParams['TAGS_CODE']))
{
        $propertyIterator = Iblock\PropertyTable::getList(array(
		'select' => array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_TYPE', 'MULTIPLE', 'LINK_IBLOCK_ID', 'USER_TYPE', 'SORT'),
		'filter' => array('=IBLOCK_ID' => $arParams['IBLOCK_ID'], '=ACTIVE' => 'Y', 'CODE' => $arParams['TAGS_CODE']),
		'order' => array('SORT' => 'ASC', 'NAME' => 'ASC')
	));
	while ($property = $propertyIterator->fetch())
	{
            $arResult['TABS'][] = Array (
                'NAME' => $property['NAME'],
                'CODE' => $property['CODE'],
                'FILTER' => Array (
                    '!PROPERTY_' . $property['CODE'] => false
                ),
            );
        }
}



$this->IncludeComponentTemplate();
