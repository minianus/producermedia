<?php
$MESS['MODULE_NOT_INSTALLED'] = "Модуль #MODULE# не установлен";
$MESS['FIELD_REQUIRED'] = "Поле #FIELD# обязательно для заполнения";
$MESS['FIELD_POSITIVE'] = "Достоинства";
$MESS['FIELD_NEGATIVE'] = "Недостатки";
$MESS['FIELD_NAME'] = "Ваше имя";

$MESS['UF_FIELD_UF_ELEMENT_ID'] = "ИД элемента";
$MESS['UF_FIELD_UF_USER_ID'] = "ИД пользователя";
$MESS['UF_FIELD_UF_CREATED'] = "Дата добавления";
$MESS['UF_FIELD_UF_USER_NAME'] = "Имя пользователя";
$MESS['UF_FIELD_UF_RATING'] = "Рейтинг";
$MESS['UF_FIELD_UF_POSITIVE'] = "Достоинства";
$MESS['UF_FIELD_UF_NEGATIVE'] = "Недостатки";
$MESS['UF_FIELD_UF_COMMENT'] = "Комментарий";
$MESS['UF_FIELD_UF_USE_EXPERIENCE'] = "Опыт использования";
$MESS['UF_FIELD_UF_LIKE'] = "Количество лайков";
$MESS['UF_FIELD_UF_DISLIKE'] = "Количество дизлайков";
$MESS['UF_FIELD_UF_XML_ID'] = "Внешний код";
$MESS['UF_FIELD_UF_IS_MODERATED'] = "Промодерировано";
$MESS['UF_FIELD_UF_RECOMMEND'] = "Рекомендует";