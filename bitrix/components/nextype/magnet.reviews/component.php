<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;
use \Bitrix\Highloadblock as HL;
use Nextype\Magnet\CReviews;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
    ShowError(Loc::getMessage('MODULE_NOT_INSTALLED', Array ('#MODULE#' => 'iblock')));
    return;
}

if (!\Bitrix\Main\Loader::includeModule('nextype.magnet'))
{
    ShowError(Loc::getMessage('MODULE_NOT_INSTALLED', Array ('#MODULE#' => 'nextype.magnet')));
    return;
}

if (!\Bitrix\Main\Loader::includeModule('highloadblock'))
{
    ShowError(Loc::getMessage('MODULE_NOT_INSTALLED', Array ('#MODULE#' => 'highloadblock')));
    return;
}


if (!empty($arParams['CODE']))
{
    $arResult['TABLE_ID'] = CReviews::getHlBlock($arParams['CODE']);
    
    if (!$arResult['TABLE_ID'])
    {
        $arResult['TABLE_ID'] = CReviews::addHlBlock($arParams['CODE']);
    }
    
    $arResult['VOTES_TABLE_ID'] = CReviews::getHlBlock($arParams['CODE'] . "Votes");
    
    
    if (intval($arParams['ELEMENT_ID']) > 0)
    {
        
        
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && is_array($_REQUEST['REVIEW']))
        {
            $arResult['VALUES'] = Array (
                'RATING' => intval($_REQUEST['REVIEW']['RATING']),
                'RECOMMEND' => $_REQUEST['REVIEW']['RECOMMEND'] == "Y" ? "Y" : "N",
                'USE_EXPERIENCE' => trim(strip_tags($_REQUEST['REVIEW']['USE_EXPERIENCE'])),
                'POSITIVE' => trim(strip_tags($_REQUEST['REVIEW']['POSITIVE'])),
                'NEGATIVE' => trim(strip_tags($_REQUEST['REVIEW']['NEGATIVE'])),
                'COMMENT' => trim(strip_tags($_REQUEST['REVIEW']['COMMENT'])),
                'NAME' => trim(strip_tags($_REQUEST['REVIEW']['NAME'])),
            );
            
            foreach (Array ('POSITIVE', 'NEGATIVE', 'NAME') as $field)
            {
                if (empty($arResult['VALUES'][$field]))
                    $arResult['ERRORS'][$field] = GetMessage('FIELD_REQUIRED', Array ('#FIELD#' => GetMessage('FIELD_' . $field)));
            }
            
            if (empty($arResult['ERRORS']))
            {
                $resAddReview = CReviews::addItem($arResult['TABLE_ID'], Array (
                    'UF_ELEMENT_ID' => intval($arParams['ELEMENT_ID']),
                    'UF_USER_ID' => $USER->GetID(),
                    'UF_CREATED' => date("d.m.Y H:i:s"),
                    'UF_USER_NAME' => $arResult['VALUES']['NAME'],
                    'UF_RATING' => $arResult['VALUES']['RATING'],
                    'UF_POSITIVE' => $arResult['VALUES']['POSITIVE'],
                    'UF_NEGATIVE' => $arResult['VALUES']['NEGATIVE'],
                    'UF_COMMENT' => $arResult['VALUES']['COMMENT'],
                    'UF_USE_EXPERIENCE' => $arResult['VALUES']['USE_EXPERIENCE'],
                    'UF_RECOMMEND' => $arResult['VALUES']['RECOMMEND'],
                    'UF_LIKE' => 0,
                    'UF_DISLIKE' => 0,
                    'UF_IS_MODERATED' => false
                ));
                
                if (!isset($resAddReview['error']))
                {
                    $arResult['SUCCESS'] = "Y";
                    
                    CReviews::refreshElementVotes(intval($arParams['ELEMENT_ID']), $arResult['TABLE_ID']);
                }
                else
                {
                    $arResult['ERRORS'][] = $resAddReview['error'];
                }
            }
        }
        
        $sumRating = 0;
        $avgByStars = Array ();
        $arFilter = Array (
            'UF_ELEMENT_ID' => intval($arParams['ELEMENT_ID']),
        );
        
        if (!$USER->IsAdmin())
        {
            $arFilter['UF_IS_MODERATED'] = true;
        }
        
        $arResult['ITEMS'] = CReviews::getItems($arResult['TABLE_ID'], $arFilter);
        $arResult['VOTES'] = CReviews::getItems($arResult['VOTES_TABLE_ID'], Array (
            'UF_USER_IP' => CReviews::getRealIP()
        ));
        
        if (isset($_REQUEST['review_vote']) && isset($_REQUEST['review_id']) && !empty($_REQUEST['review_vote']) && intval($_REQUEST['review_id']) > 0)
        {
            $APPLICATION->RestartBuffer();
            
            $reviewID = intval($_REQUEST['review_id']);
            $vote = intval($_REQUEST['review_vote']);
            
            $arReview = CReviews::getItems($arResult['TABLE_ID'], Array (
                'ID' => $reviewID
            ));
            
            if (!empty($arReview[0]))
                $arReview = $arReview[0];
            
            if (!empty($arReview))
            {
                
                if (!empty($arResult['VOTES']))
                {
                    foreach ($arResult['VOTES'] as $arVote)
                    {
                        if ($arVote['UF_REVIEW_ID'] == $arReview['ID'])
                        {
                            return;
                        }
                    }
                }
                
                $resAddVote = CReviews::addItem($arResult['VOTES_TABLE_ID'], Array (
                    'UF_REVIEW_ID' => $arReview['ID'],
                    'UF_USER_ID' => $USER->GetID(),
                    'UF_CREATED' => date("d.m.Y H:i:s"),
                    'UF_USER_IP' => CReviews::getRealIP(),
                    'UF_VOTE' => $vote
                ));

                if (!isset($resAddVote['error']))
                {
                    $arFields = Array ();
                    if ($vote < 0)
                        $arFields['UF_DISLIKE'] = $arReview['UF_DISLIKE'] + 1;
                    else
                        $arFields['UF_LIKE'] = $arReview['UF_LIKE'] + 1;
                    
                    CReviews::updateItem($arResult['TABLE_ID'], $arReview['ID'], $arFields);
                    
                    if (!empty($arParams['IBLOCK_ID']))
                        $GLOBALS['CACHE_MANAGER']->ClearByTag("iblock_id_" . $arParams['IBLOCK_ID']);
                }
            }
            
            exit();
        }
        
        if (is_array($arResult['ITEMS']))
        {
            foreach ($arResult['ITEMS'] as $key => $arItem)
            {
                $sumRating += intval($arItem['UF_RATING']);
                $avgByStars[intval($arItem['UF_RATING'])]['COUNT'] += 1;
                
                if (!empty($arResult['VOTES']))
                {
                    foreach ($arResult['VOTES'] as $arVote)
                    {
                        if ($arVote['UF_REVIEW_ID'] == $arItem['ID'])
                        {
                            $arResult['ITEMS'][$key]['HAS_VOTE'] = "Y";
                            $arResult['ITEMS'][$key]['VOTE'] = $arVote['UF_VOTE'];
                            break;
                        }
                    }
                }
            }
        }
        
        foreach ($avgByStars as $key => $arItem)
        {
            $avgByStars[$key]['PERCENT'] = intval($arItem['COUNT'] / count($arResult['ITEMS']) * 100);
            if ($avgByStars[$key]['PERCENT'] >= 99)
                $avgByStars[$key]['PERCENT'] = 100;
        }
        
        $arResult['TOTAL'] = Array (
            'ITEMS' => count($arResult['ITEMS']),
            'AVG_RATING' => $sumRating > 0 ? ceil(($sumRating / count($arResult['ITEMS'])) / 0.5) * 0.5 : '',
            'AVG_BY_STARS' => $avgByStars
        );
        
    }
}

$this->IncludeComponentTemplate();
