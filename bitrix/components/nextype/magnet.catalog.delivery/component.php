<?
use \Bitrix\Main,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Error,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Iblock,
    \Bitrix\Iblock\Component\ElementList,
    \Bitrix\Main\Service\GeoIp,
    \Nextype\Magnet\CSolution,
    \Nextype\Magnet\CGeo,
    \Nextype\Magnet\CLocations;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock') || !\Bitrix\Main\Loader::includeModule('sale'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}
    
if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
{
    ShowError("Module nextype.magnet not installed");
    return;
}

class CNextypeCatalogDelivery
{
    protected $productID = false;
    protected $productQty = 1;
    protected $includeBasket = false;
    protected $handlers = array ();
    
    public $totalWeight = 0;
    public $arUserResult = Array ();
    public $arDeliveryServiceAll = Array ();
    public $problemDeliveries = array();
    public $arResult = Array ();
    
    public function __construct($arConfig = array ())
    {
        if ($arConfig['handlers'])
            $this->handlers = $arConfig['handlers'];
    }
    
    protected function getOrderClone($order)
    {
        /** @var Order $orderClone */
        $orderClone = $order->createClone();

        $clonedShipment = $this->getCurrentShipment($orderClone);
        if (!empty($clonedShipment))
        {
            $clonedShipment->setField('CUSTOM_PRICE_DELIVERY', 'N');
        }

        return $orderClone;
    }
    
    public function getCurrentShipment($order)
    {
        /** @var Shipment $shipment */
        foreach ($order->getShipmentCollection() as $shipment)
        {
            if (!$shipment->isSystem())
                return $shipment;
        }

        return null;
    }
    
    protected function initBasket($order)
    {
        $arItem = \CIBlockElement::GetByID($this->productID)->fetch();
        if ($arItem)
        {
            $arPrice = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1, $GLOBALS['USER']->GetUserGroupArray());
            $arProduct = CCatalogProduct::GetByID($arItem['ID']);

            $obBasket = \Bitrix\Sale\Basket::create(SITE_ID);
            $obItem = $obBasket->createItem("catalog", $arItem['ID']);
            $obItem->setFields(Array(
                'PRODUCT_ID' => $arItem['ID'],
                'NAME' => $arItem['NAME'],
                'PRICE' => $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'],
                'CURRENCY' => $arPrice['RESULT_PRICE']['CURRENCY'],
                'QUANTITY' => $this->productQty,
                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                'WEIGHT' => (float)$arProduct['WEIGHT'],
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider'
            ));
            $this->totalWeight = (float)$arProduct['WEIGHT'];
        }
        
        if ($this->includeBasket)
        {
            $basketStorage = \Bitrix\Sale\Basket\Storage::getInstance(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
            $basket = $basketStorage->getBasket();
            
            foreach ($basket as $basketItem)
            {
                if ($basketItem->canBuy() && !$basketItem->isDelay())
                {
                    $arFields = $basketItem->getFields()->getValues();
                    if (!empty($arFields))
                    {
                        $obItem = $obBasket->createItem("catalog", $arFields['PRODUCT_ID']);
                        $obItem->setFields(Array (
                            'NAME' => $arFields['NAME'],
                            'PRICE' => $arFields['PRICE'],
                            'CURRENCY' => $arFields['CURRENCY'],
                            'QUANTITY' => $arFields['QUANTITY'],
                            'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                            'WEIGHT' => (float)$arFields['WEIGHT'],
                            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider'
                        ));
                        
                        $this->totalWeight += (float)$arFields['WEIGHT'];
                    }
                    
                    //$quantityList[$basketItem->getBasketCode()] = $basketItem->getQuantity();
                }
            }
        }
        
        $order->setBasket($obBasket);
        $order->setField('ORDER_WEIGHT', $this->totalWeight);
    }
    
    public function getPersonTypeId($siteId)
    {
        $obRes = \Bitrix\Sale\Internals\PersonTypeTable::getList(array(
            'select' => array('ID'),
            'filter' => array('LID' => $siteId, 'ACTIVE' => 'Y'),
            'limit' => 1
        ));
        if ($arRes = $obRes->fetch())
            return intval($arRes['ID']);
        else
            return 1;
    }

    public function initShipment($order)
    {
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $order->getCurrency());

        /** @var Sale\BasketItem $item */
        foreach ($order->getBasket() as $item)
        {
            /** @var Sale\ShipmentItem $shipmentItem */
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }

        return $shipment;
    }
    
    protected function initDelivery($shipment)
    {
        $deliveryId = intval($this->arUserResult['DELIVERY_ID']);
        $this->arDeliveryServiceAll = \Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($shipment);
        /** @var Sale\ShipmentCollection $shipmentCollection */
        $shipmentCollection = $shipment->getCollection();
        $order = $shipmentCollection->getOrder();
        
        if (!empty($this->arDeliveryServiceAll))
        {
            if (isset($this->arDeliveryServiceAll[$deliveryId]))
            {
                $deliveryObj = $this->arDeliveryServiceAll[$deliveryId];
            }
            else
            {
                $deliveryObj = reset($this->arDeliveryServiceAll);

                if (!empty($deliveryId))
                {
                    //$this->addWarning(Loc::getMessage("DELIVERY_CHANGE_WARNING"), self::DELIVERY_BLOCK);
                }

                $deliveryId = $deliveryObj->getId();
            }

            if ($deliveryObj->isProfile())
            {
                $name = $deliveryObj->getNameWithParent();
            }
            else
            {
                $name = $deliveryObj->getName();
            }

            $order->isStartField();

            $shipment->setFields(array(
                'DELIVERY_ID' => $deliveryId,
                'DELIVERY_NAME' => $name,
                'CURRENCY' => $order->getCurrency()
            ));
            $this->arUserResult['DELIVERY_ID'] = $deliveryId;

            /*$deliveryStoreList = \Bitrix\Sale\Delivery\ExtraServices\Manager::getStoresList($deliveryId);
            if (!empty($deliveryStoreList))
            {
                if ($this->arUserResult['BUYER_STORE'] <= 0 || !in_array($this->arUserResult['BUYER_STORE'], $deliveryStoreList))
                {
                    $this->arUserResult['BUYER_STORE'] = current($deliveryStoreList);
                }

                $shipment->setStoreId($this->arUserResult['BUYER_STORE']);
            }*/

            $deliveryExtraServices = $this->arUserResult['DELIVERY_EXTRA_SERVICES'];
            if (is_array($deliveryExtraServices) && !empty($deliveryExtraServices[$deliveryId]))
            {
                $shipment->setExtraServices($deliveryExtraServices[$deliveryId]);
                $deliveryObj->getExtraServices()->setValues($deliveryExtraServices[$deliveryId]);
            }
            
            $shipmentCollection->calculateDelivery();
            
            //$order->doFinalAction(true);
        }
        else
        {
            $service = \Bitrix\Sale\Delivery\Services\Manager::getById(\Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
            $shipment->setFields(array(
                'DELIVERY_ID' => $service['ID'],
                'DELIVERY_NAME' => $service['NAME'],
                'CURRENCY' => $order->getCurrency()
            ));
        }
    }
    
    protected function calculateDeliveries($order)
    {
        $this->arResult['DELIVERY'] = array();
        $problemDeliveries = array();
        
        if (!empty($this->arDeliveryServiceAll))
        {
            /** @var Order $orderClone */
            $orderClone = null;
            $anotherDeliveryCalculated = false;
            /** @var Shipment $shipment */
            $shipment = $this->getCurrentShipment($order);
            
            foreach ($this->arDeliveryServiceAll as $deliveryId => $deliveryObj)
            {
                if (!in_array($deliveryObj->getId(), $this->handlers))
                    continue;
                
                $calcResult = false;
                $calcOrder = false;
                
                if ($deliveryObj->isProfile())
                    $name = $deliveryObj->getNameWithParent();
                else
                    $name = $deliveryObj->getName();
                
                $this->arResult['DELIVERY'][$deliveryId] = $arDelivery = array(
                    'NAME' => $name,
                    'PARENT_ID' => $deliveryObj->getParentId(),
                    'DESCRIPTION' => $deliveryObj->getDescription(),
                    'CURRENCY' => $deliveryObj->getCurrency()
                );
                
                if (intval($deliveryObj->getLogotip()) > 0)
                    $arDelivery["LOGOTIP"] = CFile::GetPath($deliveryObj->getLogotip());

                if ((int) $shipment->getDeliveryId() === $deliveryId)
                {
                    $arDelivery['CHECKED'] = 'Y';
                    $mustBeCalculated = true;
                    $calcResult = $deliveryObj->calculate($shipment);
                    $calcOrder = $order;
                    $arDiscount = $order->getDiscount()->getApplyResult();
                    if (isset($arDiscount["DISCOUNT_LIST"]))
                    {
                        $this->arResult['DELIVERY'][$deliveryId]['DISCOUNT_VALUE'] = $arDiscount["PRICES"]['DELIVERY']['DISCOUNT'];
                    }
                }
                else
                {
                    //$mustBeCalculated = $deliveryObj->isCalculatePriceImmediately();
                    $mustBeCalculated = true;
                    
                    if ($mustBeCalculated)
                    {
                        $anotherDeliveryCalculated = true;

                        if (empty($orderClone))
                        {
                            $orderClone = $this->getOrderClone($order);
                        }

                        $orderClone->isStartField();

                        $clonedShipment = $this->getCurrentShipment($orderClone);
                        $clonedShipment->setField('DELIVERY_ID', $deliveryId);

                        $calculationResult = $orderClone->getShipmentCollection()->calculateDelivery();
                        
                        if ($calculationResult->isSuccess())
                        {
                            $calcDeliveries = $calculationResult->get('CALCULATED_DELIVERIES');
                            $calcResult = reset($calcDeliveries);
                        }

                        if (empty($calcResult))
                        {
                            $calcResult = new \Bitrix\Sale\Delivery\CalculationResult();
                        }

                        $orderClone->doFinalAction(true);
                        
                        $arDiscount = $orderClone->getDiscount()->getApplyResult();
                        if (!empty($arDiscount["DISCOUNT_LIST"]))
                        {
                            $this->arResult['DELIVERY'][$deliveryId]['DISCOUNT_VALUE'] = $arDiscount["PRICES"]['DELIVERY']['DISCOUNT'];
                        }

                        $calcOrder = $orderClone;
                    }
                }
                
                if ($mustBeCalculated)
                {
                    if ($calcResult->isSuccess())
                    {
                        $calcPrice = $calcResult->getPrice();
                        $bCalcProblem = (is_float($calcPrice)) ? false : true;
                        
                        if (isset($this->arResult['DELIVERY'][$deliveryId]['DISCOUNT_VALUE']))
                            $arDelivery['PRICE'] = \Bitrix\Sale\PriceMaths::roundPrecision($calcPrice - $this->arResult['DELIVERY'][$deliveryId]['DISCOUNT_VALUE']);
                        else
                            $arDelivery['PRICE'] = \Bitrix\Sale\PriceMaths::roundPrecision($calcPrice);
                        
                        $arDelivery['PRICE_FORMATED'] = SaleFormatCurrency($arDelivery['PRICE'], $calcOrder->getCurrency());

                        $currentCalcDeliveryPrice = \Bitrix\Sale\PriceMaths::roundPrecision($calcOrder->getDeliveryPrice());
                        if ($currentCalcDeliveryPrice >= 0 && $arDelivery['PRICE'] != $currentCalcDeliveryPrice)
                        {
                            $arDelivery['PRICE'] = $currentCalcDeliveryPrice;
                            $arDelivery['PRICE_FORMATED'] = SaleFormatCurrency($arDelivery['DELIVERY_DISCOUNT_PRICE'], $calcOrder->getCurrency());
                        }
                        
                        if (strlen($calcResult->getPeriodDescription()) > 0)
                        {
                            $arDelivery['PERIOD_TEXT'] = $calcResult->getPeriodDescription();
                        }
                        
                        //if (empty($arDelivery['PRICE']))
                        //    continue;
                    }
                    else
                    {
                        if (count($calcResult->getErrorMessages()) > 0)
                        {
                            foreach ($calcResult->getErrorMessages() as $message)
                            {
                                $arDelivery['CALCULATE_ERRORS'] .= $message . '<br>';
                            }
                        }
                        else
                        {
                            $arDelivery['CALCULATE_ERRORS'] = Loc::getMessage('SOA_DELIVERY_CALCULATE_ERROR');
                        }

                        if ($arDelivery['CHECKED'] !== 'Y')
                        {
                            if ($this->arParams['SHOW_NOT_CALCULATED_DELIVERIES'] === 'N')
                            {
                                unset($this->arDeliveryServiceAll[$deliveryId]);
                                continue;
                            }
                            elseif ($this->arParams['SHOW_NOT_CALCULATED_DELIVERIES'] === 'L')
                            {
                                $problemDeliveries[$deliveryId] = $arDelivery;
                                continue;
                            }
                        }
                    }

                    $arDelivery['CALCULATE_DESCRIPTION'] = $calcResult->getDescription();
                } 
                
                if (isset($arDelivery['PRICE']) && !$bCalcProblem)
                    $this->arResult['DELIVERY'][$deliveryId] = $arDelivery;
            }

            // for discounts: last delivery calculation need to be on real order with selected delivery
            if ($anotherDeliveryCalculated)
            {
                $order->doFinalAction(true);
            }
        }

        if (!empty($problemDeliveries))
        {
            $this->arResult['DELIVERY'] += $problemDeliveries;
        }
        
        unset($deliveryId, $arDelivery);
        foreach ($this->arResult['DELIVERY'] as $deliveryId => $arDelivery)
        {
            if (!isset($arDelivery["PRICE"]))
            {
                $this->problemDeliveries[$deliveryId] = $arDelivery;
                unset($this->arResult['DELIVERY'][$deliveryId]);
            }
        }
    }
    
    protected function getActualQuantityList($basket)
	{
		$quantityList = array();

		if (!$basket->isEmpty())
		{
			/** @var Sale\BasketItemBase $basketItem */
			foreach ($basket as $basketItem)
			{
				if ($basketItem->canBuy() && !$basketItem->isDelay())
				{
					$quantityList[$basketItem->getBasketCode()] = $basketItem->getQuantity();
				}
			}
		}

		return $quantityList;
	}
    
    protected function useCurrentBasket($order)
	{
		$basketStorage = \Bitrix\Sale\Basket\Storage::getInstance(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
		$basket = $basketStorage->getBasket();
                
		$this->arUserResult['QUANTITY_LIST'] = $this->getActualQuantityList($basket);

		$result = $basket->refresh();
		if ($result->isSuccess())
		{
			$basket->save();
		}

		// right NOW we decide to work only with available basket
		// full basket won't update anymore
		$availableBasket = $basketStorage->getOrderableBasket();
		
		$order->appendBasket($availableBasket);
	}

    public function getResult($productID, $qty = 1, $includeBasket = false, $arLocation = false)
    {
        if (empty($productID))
            return false;
        
        $this->productID = intval($productID);
        $this->productQty = floatval($qty);
        $this->includeBasket = $includeBasket;
        
        $registry = \Bitrix\Sale\Registry::getInstance(\Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER);
        /** @var Order $orderClassName */
        $orderClassName = $registry->getOrderClassName();

        $order = $orderClassName::create(SITE_ID, $GLOBALS['USER']->GetID());
       
        $order->isStartField();
        $this->initBasket($order);
        
        //$ipAddress = GeoIp\Manager::getRealIp();
        
        $order->setPersonTypeId($this->getPersonTypeId($order->getSiteId()));
        
        $propertyCollection = $order->getPropertyCollection();
        foreach ($propertyCollection as $property)
        {
            if ($property->isUtil())
                continue;
            
            $arProperty = $property->getProperty();
            
            if ($arProperty['USER_PROPS'] === 'Y')
            {
                if ($isProfileChanged && !$haveProfileId)
                {
                    $curVal = '';
                }
                elseif(
                        $willUseProfile || (!isset($orderProperties[$arProperty['ID']]) && isset($profileProperties[$arProperty['ID']]))
                )
                {
                    $curVal = $profileProperties[$arProperty['ID']];
                }
                elseif (isset($orderProperties[$arProperty['ID']]))
                {
                    $curVal = $orderProperties[$arProperty['ID']];
                }
                else
                {
                    $curVal = '';
                }
            }
            else
            {
                $curVal = isset($orderProperties[$arProperty['ID']]) ? $orderProperties[$arProperty['ID']] : '';
            }

            if ($arProperty['TYPE'] === 'LOCATION' && empty($curVal))
            {
                if (!$arLocation)
                {
                    $locCode = \Bitrix\Sale\Location\GeoIp::getLocationCode("", LANGUAGE_ID);

                    if (!empty($locCode))
                    {
                        $curVal = $locCode;
                        $this->arResult['LOCATION']['CODE'] = $curVal;
                    }
                }
                else
                {
                    $curVal = $arLocation['CODE'];
                    $this->arResult['LOCATION']['CODE'] = $arLocation['CODE'];
                }
            }
            elseif ($arProperty['IS_ZIP'] === 'Y' && empty($curVal))
            {
                if (!$arLocation)
                {
                    $zip = \Bitrix\Sale\Location\GeoIp::getZipCode("");

                    if (!empty($zip))
                    {
                        $curVal = $zip;
                        $this->arResult['LOCATION']['ZIP'] = $curVal;
                    }
                }
            }

            $this->arUserResult['ORDER_PROP'][$arProperty['ID']] = $curVal;
        }
        
        $res = $propertyCollection->setValuesFromPost(array('PROPERTIES' => $this->arUserResult['ORDER_PROP']), array());

        $shipment = $this->initShipment($order);
        
        $this->initDelivery($shipment);
        
        $order->doFinalAction(true);
        
        $this->calculateDeliveries($order);
        
        return $this->arResult;
    }

}


\Nextype\Magnet\CSolution::getInstance(SITE_ID);

$deliveryHandlers = @unserialize(\Nextype\Magnet\CSolution::$options['CATALOG_DELIVERY_HANDLERS']);

if (!empty($arParams['PRODUCT_ID']) && \Nextype\Magnet\CSolution::$options['CATALOG_DELIVERY_ENABLED'] == "Y" && is_array($deliveryHandlers) && !empty($deliveryHandlers))
{
    $ob = new CNextypeCatalogDelivery(Array (
        'handlers' => $deliveryHandlers
    ));
    $arLocation = false;
    if ($arParams['LOCATION_CODE'] && !$arParams['LOCATION'])
    {
        $arLocation = \Bitrix\Sale\Location\LocationTable::getByCode($arParams['LOCATION_CODE'])->fetch();
    }
    elseif ($arParams['LOCATION'])
    {
        $arLocation = \Bitrix\Sale\Location\LocationTable::getById($arParams['LOCATION'])->fetch();
    }
    
    if ($arLocation && !$arLocation["NAME_RU"])
        $arLocation["NAME_RU"] = CLocations::getCityGenitive(CGeo::getLocationNameById($arLocation["ID"]));
    
    $arResult = $ob->getResult($arParams['PRODUCT_ID'], $arParams['QUANTITY'], $arParams['INCLUDE_BASKET'] == 'Y' ? true : false, $arLocation);
    
    if (!empty($arResult['DELIVERY']))
    {
        usort($arResult['DELIVERY'], function ($a, $b) {
            if ($a['PRICE'] == $b['PRICE'])
                return 0;
            
            return ($a['PRICE'] < $b['PRICE']) ? -1 : 1;
        });
    }
    
    if (!empty($ob->problemDeliveries) && CSolution::$options["CATALOG_DELIVERY_HANDLERS_SHOW_ALWAYS"] == "Y")
    {
        $arResult['DELIVERY'] = array_merge($arResult['DELIVERY'], $ob->problemDeliveries);
    }
    
    if (!$arLocation && !empty($arResult['LOCATION']['CODE']))
    {
        $arLocation = \Bitrix\Sale\Location\LocationTable::getByCode($arResult['LOCATION']['CODE'], array(
            'filter' => ['=NAME.LANGUAGE_ID' => LANGUAGE_ID],
            'select' => array('*', 'NAME_RU' => 'NAME.NAME')
        ))->fetch();
        if ($arLocation)
        {
            $arLocation["NAME_RU"] = CLocations::getCityGenitive($arLocation["NAME_RU"]);
            $arResult['LOCATION'] = $arLocation;
        }
    }
    elseif ($arLocation)
    {
        $arResult['LOCATION'] = $arLocation;
    }

}

if ($arParams['RETURN_PRICE_FROM'] == 'N')
    $this->IncludeComponentTemplate();
else
{
    $arMinDelivery = $arJsonResult = Array ();
    if (is_array($arResult['DELIVERY']))
    {
        foreach ($arResult['DELIVERY'] as $arDelivery)
        {
                $arJsonResult = Array (
                    'PRICE' => $arDelivery['PRICE'],
                    'PRICE_FORMATED' => $arDelivery['PRICE_FORMATED'],
                    'LOCATION' => $arResult['LOCATION']['NAME_RU']
                );
                
                break;
        }
    }
    
    $APPLICATION->RestartBuffer();
    print(\Bitrix\Main\Web\Json::encode($arJsonResult));
    exit();
}
