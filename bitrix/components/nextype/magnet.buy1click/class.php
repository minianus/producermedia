<?php
use \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!Loader::includeModule('nextype.magnet')
    || !Loader::includeModule('sale')
    || !Loader::includeModule('catalog')
    || !Loader::includeModule('iblock')
)
    die('Modules not installed');

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

class CMagnetBuy1Click extends CBitrixComponent
{
    const COMMENT_FIELD_NAME = "USER_DESCRIPTION";
    
    public function onPrepareComponentParams($arParams)
    {
        $arParams["EMAIL_REGISTER"] = Option::get("main", "new_user_email_auth", "N");
        $arParams["EMAIL_CONFIRM"] = Option::get("main", "new_user_registration_email_confirmation", "N");
        $arParams["CAPTCHA_REGISTER"] = Option::get("main", "captcha_registration", "N");
        $arParams["PHONE_REGISTER"] = Option::get("main", "new_user_phone_auth", "N");
        $arParams["PHONE_CONFIRMATION"] = Option::get("main", "new_user_phone_required", "N");
        $arParams["DEFAULT_REGISTER_GROUPS"] = explode(",", Option::get("main", "new_user_registration_def_group", "N"));
        return $arParams;
    }
    
    public function executeComponent()
    {
        $this->initFields();
        
        $this->checkSessid();
        
        if (!empty($this->request->get("BUY1CLICK")))
            $this->processData();
        
        $this->includeComponentTemplate();
    }
    
    private function initFields()
    {
        $this->initResult();
        
        $context = \Bitrix\Main\Context::getCurrent();
        $this->siteId = $context->getSite();
        $this->request = $context->getRequest();
        
        $CSolution = \Nextype\Magnet\CSolution::getInstance(SITE_ID);
        
        $this->useCaptcha = ($CSolution::$options["BUY1CLICK_USE_CAPTCHA"] == "Y");
        $this->recaptchaEnabled = ($CSolution::$options["RECAPTCHA_ENABLED"] == "Y");
        $this->recaptchaType = $CSolution::$options["RECAPTCHA_TYPE"];
        if ($this->useCaptcha)
        {
            $this->initCaptcha();
        }
        
        $this->displayProps = (!empty($CSolution::$options['BUY1CLICK_DISPLAY_ORDER_PROPS'])) ? unserialize($CSolution::$options['BUY1CLICK_DISPLAY_ORDER_PROPS']) : array();
        $this->requiredProps = (!empty($CSolution::$options['BUY1CLICK_REQUIRED_ORDER_PROPS'])) ? unserialize($CSolution::$options['BUY1CLICK_REQUIRED_ORDER_PROPS']) : array();
        if (!empty($this->displayProps))
        {
            $this->fillFormFields();
        }
        
        $this->personTypeId = (int)$CSolution::$options['BUY1CLICK_PERSON_TYPE'];
        if ($this->personTypeId <= 0)
        {
            $this->getPersonTypeId();
        }
        
        $this->defaultPaysystem = (int)$CSolution::$options['BUY1CLICK_DEFAULT_PAYSYSTEM'];
        $this->defaultDelivery = (int)$CSolution::$options['BUY1CLICK_DEFAULT_DELIVERY'];
        
        $this->usePhoneAsLogin = ($CSolution::$options['USER_USE_PHONE_AS_LOGIN'] == "Y");
        $this->phoneMask = $CSolution::$options["PHONE_MASK"];
        
        $this->personalPage = str_ireplace("#SITE_DIR#", SITE_DIR, $CSolution::$options["LINK_PERSONAL"]);
        
        $this->initUserData();
    }
    
    private function initResult()
    {
        $this->arResult = array(
            'PRODUCT' => (!empty($this->arParams['PRODUCT_ID'])) ? \CIBlockElement::GetByID($this->arParams['PRODUCT_ID'])->fetch() : false,
            'DISPLAY_PROPS' => array(),
            'ERRORS' => array(),
            'DEFAULT_CURRENCY' => Option::get('sale', 'default_currency', 'RUB'),
            'BASKET_USER_ID' => \Bitrix\Sale\FUser::getId(),
            'SHOW_'.self::COMMENT_FIELD_NAME => "N",
            "extMessages" => array()
        );
    }
    
    private function initUserData()
    {
        global $USER;
        
        $this->userData = array();
        
        if (is_object($USER) && $USER->IsAuthorized())
        {
            $this->userData["ID"] = $USER->getId();
            $this->userData["FULL_NAME"] = $USER->getFullName();
            $this->userData["EMAIL"] = $USER->getEmail();
            $this->userData["PHONE_NUMBER"] = $this->getCurrentUserPhone();
        }
        
    }
    
    private function initCaptcha()
    {
        if (!class_exists("\CCaptcha"))
            include(Loader::getDocumentRoot() . "/bitrix/modules/main/classes/general/captcha.php");

        $obCaptcha = new \CCaptcha();
        $captchaPass = Option::get("main", "captcha_password", "");
        if(strlen($captchaPass) <= 0)
        {
            $captchaPass = randString(10);
            Option::set("main", "captcha_password", $captchaPass);
        }
        $obCaptcha->SetCodeCrypt($captchaPass);
        
        $this->arResult["CAPTCHA_CODE"] = htmlspecialchars($obCaptcha->GetCodeCrypt());
        $this->arResult["USE_CAPTCHA"] = true;
    }
    
    private function fillFormFields()
    {
        global $USER;
        
        $this->arResult['DISPLAY_PROPS'] = array();
        $arSaveValues = $this->request->get("BUY1CLICK");
        
        $isAuth = $USER->IsAuthorized();
        
        foreach ($this->displayProps as $id)
        {
            if ($id == self::COMMENT_FIELD_NAME)
            {
                $this->arResult['SHOW_'.self::COMMENT_FIELD_NAME] = (in_array($id, $this->requiredProps)) ? 'R' : 'Y';
            }
            elseif ($arProperty = \CSaleOrderProps::GetByID($id))
            {
                if ($isAuth)
                {
                    if ($arProperty['IS_PAYER'] == "Y")
                    {
                        $bHasPayerName = true;
                        $arProperty['VALUE'] = $USER->GetFullName();
                    }

                    if ($arProperty['IS_EMAIL'] == "Y")
                    {
                        $bHasEmail = true;
                        $arProperty['VALUE'] = $USER->GetEmail();
                    }
                }

                $arProperty['REQUIED'] = (in_array($id, $this->requiredProps)) ? 'Y' : 'N';
                if (!empty($arSaveValues[$arProperty["CODE"]]))
                {
                    $arProperty["VALUE"] = (LANG_CHARSET != 'utf-8') ? mb_convert_encoding(htmlspecialcharsbx($arSaveValues[$arProperty["CODE"]]), LANG_CHARSET, 'UTF-8') : htmlspecialcharsbx($arSaveValues[$arProperty["CODE"]]);
                }
                $this->arResult['DISPLAY_PROPS'][$arProperty['CODE']] = $arProperty;
            }
        }
    }
    
    private function getPersonTypeId()
    {
        $arPersonType = \Bitrix\Sale\PersonTypeTable::getList(array(
            "limit" => 1,
            "filter" => array("LID" => $this->siteId),
            "select" => array("ID")
        ))->fetch();
        $this->personTypeId = ((int)$arPersonType["ID"] > 0) ? (int)$arPersonType["ID"] : 1;
    }
    
    private function processData()
    {
        if ($this->hasError())
            return;
        
        $arData = $this->request->get("BUY1CLICK");
        if ($this->useCaptcha)
        {
            $this->processCaptcha($arData);
        }
        
        if ($this->hasError())
            return;
        
        if (is_array($this->arResult['PRODUCT']))
        {
            $this->checkProduct();
        }
        
        if ($this->hasError())
            return;
        
        $this->checkFields($arData);
        
        if ($this->hasError())
            return;
        
        if (empty($this->userData["ID"]))
            $this->processUser($arData);
        
        if ($this->hasError())
            return;
        
        $this->processOrder($arData);
        
    }
    
    private function processCaptcha($arData)
    {
        $capWord = ($this->recaptchaEnabled) ? htmlspecialcharsbx($this->request->get("captcha_word")) : htmlspecialcharsbx($arData["captcha_word"]);
        $capWord = ($capWord) ?: $_REQUEST["captcha_word"];
        $capWord = ($capWord) ?: $_REQUEST["BUY1CLICK"]["captcha_word"];
        $capCode = htmlspecialcharsbx($arData["captcha_code"]);

        if (!$GLOBALS["APPLICATION"]->CaptchaCheckCode($capWord, $capCode))
        {
            $errMess = GetMessage('CAPTCHA_CHECK_FAIL');
            if ($this->recaptchaEnabled)
            {
                $errMess = ($this->recaptchaType == "checkbox") ? GetMessage('RECAPTCHA_CHECK_FAIL') : GetMessage('RECAPTCHA_SCORE_FAIL');
            }
            $this->setError($errMess);
        }
    }
    
    private function checkProduct()
    {
        $arProductData = \Bitrix\Catalog\ProductTable::getList(array(
            "limit" => 1,
            "filter" => array("ID" => $this->arResult['PRODUCT']['ID']),
            "select" => array("AVAILABLE", "CAN_BUY_ZERO")
        ))->fetch();
        if ($arProductData['AVAILABLE'] == 'N' && $arProductData["CAN_BUY_ZERO"] != "Y")
            $this->setError(GetMessage("ZERO_EXIST_PRODUCT"));
    }
    
    private function checkFields($arData)
    {
        foreach ($this->arResult["DISPLAY_PROPS"] as $code => $arProp)
        {
            if (isset($arData[$code]))
            {
                $arData[$code] = htmlspecialcharsbx($arData[$code]);
                
                if (LANG_CHARSET != 'utf-8')
                    $arData[$code] = mb_convert_encoding($arData[$code], LANG_CHARSET, 'UTF-8');

                $this->arResult['DISPLAY_PROPS'][$code]['VALUE'] = strip_tags($arData[$code]);
            }

            if ($arProp['REQUIED'] == "Y" && empty($this->arResult['DISPLAY_PROPS'][$code]['VALUE']))
            {
                $this->arResult['DISPLAY_PROPS'][$code]['ERROR'] = "Y";
                $this->setError(GetMessage('REQUIED_PROPERTY', array("#PROP_NAME#" => $arProp['NAME'])));
            }

            if ($arProp['IS_PAYER'] == 'Y')
                $this->userData["NAME"] = $this->arResult['DISPLAY_PROPS'][$code]['VALUE'];

            if ($arProp['IS_EMAIL'] == 'Y')
                $this->userData["EMAIL"] = trim($this->arResult['DISPLAY_PROPS'][$code]['VALUE']);

            if ($arProp['IS_PHONE'] == "Y" || $arProp['CODE'] == "PHONE")
                $this->userData["PHONE_NUMBER"] = trim($this->arResult['DISPLAY_PROPS'][$code]['VALUE']);
        }
    }
    
    private function processUser($arData)
    {
        $this->userData["LOGIN"] = false;
        $needRegister = true;
        
        if ($this->usePhoneAsLogin || $this->arParams["PHONE_REGISTER"] == "Y")
        {
            if (empty($this->userData["PHONE_NUMBER"]))
            {
                $this->setError(Loc::getMessage("REGISTER_BY_PHONE_ERROR"));
            }
            else
            {
                $this->userData["LOGIN"] = $this->userData["PHONE_NUMBER"];
            }
        }
        else
        {
            $this->userData["LOGIN"] = $this->userData["EMAIL"];
        }

        if (!empty($this->userData["LOGIN"]))
        {
            if ($this->usePhoneAsLogin || $this->arParams["PHONE_REGISTER"] == "Y")
            {
                $rawPhone = $this->formatPhoneNumber($this->userData["LOGIN"], true);
                $arUser = \Bitrix\Main\UserPhoneAuthTable::getList(array(
                    "filter" => array("PHONE_NUMBER" => $rawPhone),
                    "select" => array("USER_ID"),
                    "limit" => 1
                ))->fetch();
            }
            else
            {
                $arUser = \Bitrix\Main\UserTable::getList(array(
                    "filter" => array("EMAIL" => htmlspecialcharsbx($this->userData["LOGIN"])),
                    "select" => array("ID"),
                    "limit" => 1
                ))->fetch();
            }
            
            if (!$arUser)
            {
                $arUser = \Bitrix\Main\UserTable::getList(array(
                    "filter" => array("LOGIN" => htmlspecialcharsbx($this->userData["LOGIN"])),
                    "select" => array("ID"),
                    "limit" => 1
                ))->fetch();
            }
            
            if ($arUser)
            {
                $needRegister = false;
                $this->userData["ID"] = ($arUser["USER_ID"]) ?: $arUser["ID"];
            }
        }

        if ($needRegister)
        {
            $this->registerUser();
        }
    }
    
    private function registerUser()
    {
        global $USER;
        
        if ($this->usePhoneAsLogin || $this->arParams["PHONE_REGISTER"] == "Y")
        {
            if ($this->arParams["PHONE_REGISTER"] != "Y")
            {
                $this->setError(GetMessage("REGISTER_BY_PHONE_NOT_ALLOWED"));
                return;
            }
            $formattedLogin = "";
            $rawPhone = $this->formatPhoneNumber($this->userData["LOGIN"], true);
            $sLen = 1;
            foreach (str_split($this->phoneMask) as $inMask)
            {
                if ((int)$inMask > 0)
                {
                    $formattedLogin .= mb_substr($rawPhone, $sLen, 1);
                    $sLen++;
                }
                else
                {
                    $formattedLogin .= $inMask;
                }
            }
        }
        else
        {
            $formattedLogin = $this->userData["LOGIN"];
        }
        
        $userPassword = \Bitrix\Main\Authentication\ApplicationPasswordTable::generatePassword();
        //$arNewUser = $USER->Register($formattedLogin, $arUserData["NAME"], "", $userPassword, $userPassword, $arUserData["EMAIL"]);
        $arAddData = array(
            "LOGIN" => $formattedLogin,
            "PASSWORD" => $userPassword,
            "CONFIRM_PASSWORD" => $userPassword,
            "ACTIVE" => "Y",
            "GROUP_ID" => $this->arParams["DEFAULT_REGISTER_GROUPS"],
            "SITE_ID" => $this->siteId,
            "CHECKWORD" => md5(\CMain::GetServerUniqID().uniqid()),
            "~CHECKWORD_TIME" => $GLOBALS["DB"]->CurrentTimeFunction(),
            "USER_IP" => $_SERVER["REMOTE_ADDR"],
            "USER_HOST" => @gethostbyaddr($_SERVER["REMOTE_ADDR"]),
        );
        if ($this->usePhoneAsLogin || $this->arParams["PHONE_REGISTER"] == "Y")
        {
            $arAddData["PHONE_NUMBER"] = $formattedLogin;
            
            if ($this->arParams["PHONE_CONFIRMATION"] == "Y")
            {
                $arAddData["ACTIVE"] = "N";
            }
        }
        else
        {
            $arAddData["PERSONAL_PHONE"] = ($this->userData["PHONE_NUMBER"]) ?: "";
        }
        
        if ($this->userData["EMAIL"])
        {
            $arAddData["EMAIL"] = $this->userData["EMAIL"];
        }
        
        if ($this->arParams["EMAIL_CONFIRM"] == "Y")
        {
            $arAddData["ACTIVE"] = "N";
            $arAddData["EMAIL"] = $this->userData["EMAIL"];
            $arAddData["CONFIRM_CODE"] = \randString(8);
        }

        $bOk = true;
        foreach(\GetModuleEvents("main", "OnBeforeUserAdd", true) as $arEvent)
        {
            if (\ExecuteModuleEventEx($arEvent, array(&$arAddData)) === false)
            {
                if ($err = $APPLICATION->GetException())
                {
                    $result_message = array("MESSAGE" => $err->GetString() . "<br>", "TYPE" => "ERROR");
                }
                else
                {
                    $APPLICATION->ThrowException("Unknown error");
                    $result_message = array("MESSAGE" => "Unknown error" . "<br>", "TYPE" => "ERROR");
                }

                $bOk = false;
                break;
            }
        }
        
        if (!$bOk)
        {
            $this->setError($result_message["MESSAGE"]);
            return;
        }
        
        $arAddData["LID"] = $arAddData["SITE_ID"];
        
        
        $this->userData["ID"] = $arAddData["RESULT"] = $USER->Add($arAddData);
        
        if (!$this->userData["ID"])
        {
            $this->setError($USER->LAST_ERROR);
            return;
        }
        
        if ($this->arParams["PHONE_REGISTER"] == "Y" && $this->arParams["PHONE_CONFIRMATION"] == "Y")
        {
            $this->setExtMessages("TPL_NEED_CONFIRM_PHONE_NUMBER", array("#PHONE#" => $formattedLogin, "#PERSONAL#" => $this->personalPage));
        }
        if ($this->arParams["PHONE_REGISTER"] == "Y" && $this->arParams["EMAIL_CONFIRM"] == "Y")
        {
            $this->setExtMessages("TPL_NEED_CONFIRM_EMAIL_ADDRESS", array("#EMAIL#" => $formattedLogin, "#PERSONAL#" => $this->personalPage));
        }
        
        $arAddData["USER_ID"] = $this->userData["ID"];
        
        $arMessages = $arAddData;
        unset($arMessages["CONFIRM_PASSWORD"]);
        unset($arMessages["~CHECKWORD_TIME"]);
        
        $event = new \CEvent;
        $event->Send("NEW_USER", $arMessages["SITE_ID"], $arMessages);
        if ($arAddData["CONFIRM_CODE"])
        {
            $event->Send("NEW_USER_CONFIRM", $arMessages["SITE_ID"], $arMessages);
        }
        
        if (($this->arParams["PHONE_REGISTER"] == "Y" && $this->arParams["PHONE_CONFIRMATION"] != "Y")
            || ($this->arParams["PHONE_REGISTER"] != "Y" && $this->arParams["EMAIL_CONFIRM"] != "Y"))
            $USER->Authorize($this->userData["ID"]);
    }
    
    private function processOrder($arData)
    {
        $result = false;
        
        $order = \Bitrix\Sale\Order::create($this->siteId, $this->userData["ID"]);
        
        $order->setPersonTypeId($this->personTypeId);
        
        $order->setField("USER_DESCRIPTION", strip_tags($arData['COMMENT']) . PHP_EOL . "#" . GetMessage("ORDER_COMMENT_BUY_ONE_CLICK"));
        
        $order->setBasket($this->getBasket());
        
        $this->initShipment($order);
        $this->initPaySystem($order);
        $this->initOrderProperties($order);

        $order->doFinalAction(true);
        
        $bOk = true;
        foreach(\GetModuleEvents("nextype.magnet", "OnBeforeBuy1ClickOrderSave", true) as $arEvent)
        {
            if (\ExecuteModuleEventEx($arEvent, Array(&$order)) === false)
            {
                $bOk = false;
                break;
            }
        }
            
        if ($bOk)
        {
            $result = $order->save();
            foreach(\GetModuleEvents("nextype.magnet", "OnAfterBuy1ClickOrderSave", true) as $arEvent)
                \ExecuteModuleEventEx($arEvent, Array(&$order));
        }
        else
        {
            if ($err = $APPLICATION->GetException())
            {
                $this->setError($err->GetString());
            }
            else
            {
                $this->setError($GLOBALS["strError"]);
            }
        }
        
        if ($result && $order->getId() > 0)
        {
            $this->arResult["ORDER"] = array(
                "ID" => $order->getId(),
                "ACCOUNT_NUMBER" => $order->getField("ACCOUNT_NUMBER")
            );
            if (!empty($emailProp = $order->getPropertyCollection()->getUserEmail()))
            {
                $this->arResult["ORDER"]["USER_EMAIL"] = $emailProp->getValue();
            }
            
            $this->sendEmail($order);
        }
    }
    
    private function getBasket()
    {
        if (is_array($this->arResult["PRODUCT"]))
        {
            $basket = \Bitrix\Sale\Basket::create($this->siteId);
            $item = $basket->createItem("catalog", $this->arResult["PRODUCT"]["ID"]);
            $item->setFields(array(
                'QUANTITY' => $this->getMinQuantityByProduct($this->arResult["PRODUCT"]["ID"]),
                'CURRENCY' => $this->getCurrency(),
                'LID' => $this->siteId,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
            return $basket;
        }
        else
        {
            return \Bitrix\Sale\Basket::loadItemsForFUser($this->arResult["BASKET_USER_ID"], $this->siteId);
        }
    }
    
    private function getBasketClone()
    {
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser($this->arResult["BASKET_USER_ID"], $this->siteId);
        foreach($basket->getItems() as $item)
            $this->basketItems[] = $item;
        unset($basket);
    }
    
    private function clearCurrentBasket()
    {
        
    }
    
    private function initShipment(&$order)
    {
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = \Bitrix\Sale\Delivery\Services\Manager::getById(($this->defaultDelivery) ? $this->defaultDelivery : \Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        foreach ($order->getBasket()->getOrderableItems() as $item)
        {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }
    }
    
    private function initPaySystem(&$order)
    {
        if ($this->defaultPaysystem)
        {
            $paymentCollection = $order->getPaymentCollection();
            $payment = $paymentCollection->createItem();
            $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById($this->defaultPaysystem);
            $payment->setFields(array(
                'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
                'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            ));
        }
    }
    
    private function initOrderProperties(&$order)
    {
        foreach ($order->getPropertyCollection() as $prop)
        {
            $code = $prop->getField("CODE");
            if ($this->arResult['DISPLAY_PROPS'][$code]['VALUE'])
                $prop->setValue($this->arResult['DISPLAY_PROPS'][$code]['VALUE']);
        }
    }
    
    private function sendEmail($order)
    {
        $arMessages = array(
            'ORDER_REAL_ID' => $order->getId(),
            'ORDER_ID' => $order->getField("ACCOUNT_NUMBER"),
            'ORDER_DATE' => $order->getField("DATE_INSERT"),
            'USER_NAME' => $this->userData["NAME"],
            'ORDER_LIST' => '',
            'USER_EMAIL' => $this->userData["EMAIL"],
            'USER_PHONE' => $this->userData["PHONE_NUMBER"],
            'SALE_EMAIL' => Option::get('sale', 'order_email', ''),
            'PRICE' => \CurrencyFormat($order->getPrice(), $order->getCurrency()),
            'ORDER_COMMENT' => $order->getField("USER_DESCRIPTION"),
        );

        foreach ($this->arResult['DISPLAY_PROPS'] as $code => $arProp)
        {
            $arMessages['PROP_' . $code] = $arProp['VALUE'];
        }
        
        foreach ($order->getBasket()->getOrderableItems() as $item)
        {
            $listRow = GetMessage('ORDER_LIST_ROW', array(
                "#NAME#" => $item->getField("NAME"),
                "#QUANTITY#" => $item->getQuantity(),
                "#PRICE#" => \CurrencyFormat($item->getPrice(), $item->getCurrency())
            ));
            $arMessages['ORDER_LIST'] .= $listRow . '\n\r';
        }

        foreach(\GetModuleEvents("sale", "OnOrderNewSendEmail", true) as $arEvent)
            \ExecuteModuleEventEx($arEvent, Array($orderID, 'NEW_ORDER_BUY_1_CLICK', &$arMessages));

        // send mail 
        \CEvent::Send("NEW_ORDER_BUY_1_CLICK", $this->siteId, $arMessages);
    }
    
    private function setError($msg)
    {
        $this->arResult['ERRORS'][] = $msg;
    }
    
    private function hasError()
    {
        return !empty($this->arResult['ERRORS']);
    }
    
    private function getCurrentUserPhone()
    {
        if (empty($this->userData["ID"]))
            return "";
        return \Bitrix\Main\UserPhoneAuthTable::getList(array(
            "filter" => array("USER_ID" => $this->userData["ID"]),
            "select" => array("PHONE_NUMBER"),
            "limit" => 1
        ))->fetch()["PHONE_NUMBER"];
    }
    
    private function formatPhoneNumber($phone = '', $addPlus = true)
    {
        return ($addPlus) ? "+" . preg_replace("/[^0-9]/", '', $phone) : preg_replace("/[^0-9]/", '', $phone);
    }
    
    private function getMinQuantityByProduct($id)
    {
        $id = ($id) ?: $this->arResult["PRODUCT"]["ID"];
        return \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($id)[$id]["RATIO"];
    }
    
    private function getCurrency()
    {
        return \Bitrix\Currency\CurrencyManager::getBaseCurrency();
    }
    
    private function checkSessid()
    {
        if (!empty($this->request->get("sessid")) && ! \check_bitrix_sessid())
        {
            $this->setError(GetMessage("BUY1CLICK_SESSID_FAIL"));
        }
    }
    
    private function setExtMessages($code = "", $replace = array())
    {
        if (!($code && $replace))
            return;
        $this->arResult["extMessages"][] = array(
            "CODE" => $code,
            "REPLACE" => $replace
        );
    }
}

