<?php

$MESS['REQUIED_PROPERTY'] = "Поле #PROP_NAME# обязательно для заполнения";
$MESS['USER_REGISTER_FAIL'] = "Ошибка регистрации пользователя: #MESSAGE#";
$MESS['ORDER_COMMENT_BUY_ONE_CLICK'] = "Заказ оформлен в 1 клик";
$MESS['ZERO_EXIST_PRODUCT'] = "Нет товара в наличии. Оформление заказа невозможно";
$MESS['ORDER_LIST_ROW'] = "#NAME# x #QUANTITY# x #PRICE#";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Вы были распознаны как робот. Повторите попытку позже";
$MESS['CAPTCHA_CHECK_FAIL'] = "Символы с картинки введены неверно";
$MESS['RECAPTCHA_CHECK_FAIL'] = "Подтвердите, что вы не робот";
$MESS['BUY1CLICK_SESSID_FAIL'] = "Ваша сессия истекла, обновите страницу";

$MESS["REGISTER_BY_PHONE_ERROR"] = "Ошибка регистрации пользователя. Номер телефона отсутствует или некорректен";
$MESS["REGISTER_BY_PHONE_NOT_ALLOWED"] = "Не удалось задать логин по номеру телефона. Регистрация по телефону отключена в Главном модуле.";
