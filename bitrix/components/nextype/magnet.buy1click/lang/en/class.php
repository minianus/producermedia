<?php

$MESS['REQUIED_PROPERTY'] = "Field #PROP_NAME# required";
$MESS['USER_REGISTER_FAIL'] = "User registration failed: #MESSAGE#";
$MESS['ORDER_COMMENT_BUY_ONE_CLICK'] = "Order issued in 1 click";
$MESS['ZERO_EXIST_PRODUCT'] = "No product in stock. Ordering is not possible";
$MESS['ORDER_LIST_ROW'] = "#NAME# x #QUANTITY# x #PRICE#";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Robot check fail. Please try again later";
$MESS['CAPTCHA_CHECK_FAIL'] = "Captcha word is incorrect";
$MESS['RECAPTCHA_CHECK_FAIL'] = "Confirm that you are not a robot";
$MESS['BUY1CLICK_SESSID_FAIL'] = "Your session is expired. Please, refresh the page";

