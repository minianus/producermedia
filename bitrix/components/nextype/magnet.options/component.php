<?
use Bitrix\Main\Application;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
$arPageSpeedAgents = Array (
    'Chrome-Lighthouse',
    'Google Page Speed Insights'
);

if (!empty($userAgent))
{
    foreach ($arPageSpeedAgents as $agent)
    {
        if (strpos($userAgent, strtolower($agent)) !== false)
        {
            return;
        }
    }
}

if (!$USER->IsAdmin() && \Bitrix\Main\Config\Option::get("nextype.magnet", "IS_DEMO", 'N', SITE_ID) != "Y")
    return;

if (\Bitrix\Main\Config\Option::get("nextype.magnet", "PUBLIC_OPTIONS", 'Y', SITE_ID) == 'N')
    return;

if (isset($_POST) && !empty($_POST))
{
    foreach ($_POST as $key => $val)
    {
        if (!is_array($val))
            $_POST[$key] = htmlspecialchars($val);
    }
    
    
    if ($_POST["NT_OPTIONS_SAVE"] == "Y")
    {
        \Nextype\Magnet\COptions::setOptions($_POST, SITE_ID);
        \Nextype\Magnet\CSolution::clearTemplateCache(SITE_ID, true);
        LocalRedirect($APPLICATION->GetCurPageParam());
    }
    elseif ($_POST["NT_OPTIONS_RESET"] == "Y")
    {
        \Nextype\Magnet\COptions::resetOptions($_POST, SITE_ID);
        \Nextype\Magnet\CSolution::clearTemplateCache(SITE_ID, true);
        LocalRedirect($APPLICATION->GetCurPageParam());
    }
    
    
}

$arResult['OPTIONS_LIST'] = \Nextype\Magnet\COptions::getOptionsList();
$arResult['CURRENT_OPTIONS'] = \Nextype\Magnet\CSolution::$options;


$this->IncludeComponentTemplate();
