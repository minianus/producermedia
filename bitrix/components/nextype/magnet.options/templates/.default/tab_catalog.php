<?
$arCatalogOptions = Array(
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_FLY_HEADER'),
        'OPTION_NAME' => 'CATALOG_FLY_HEADER'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_SHOW_WISH_LIST'),
        'OPTION_NAME' => 'CATALOG_SHOW_WISH_LIST'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_SHOW_COMPARE_LIST'),
        'OPTION_NAME' => 'CATALOG_SHOW_COMPARE_LIST'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_SHOW_FAST_VIEW'),
        'OPTION_NAME' => 'CATALOG_SHOW_FAST_VIEW'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_BASKET_SHOW_POPUP'),
        'OPTION_NAME' => 'CATALOG_BASKET_SHOW_POPUP'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_CATALOG_SHOW_RATING'),
        'OPTION_NAME' => 'CATALOG_SHOW_RATING'
    ),
    Array(
        'TITLE' => GetMessage('NT_OPTIONS_BUY1CLICK_ENABLED'),
        'OPTION_NAME' => 'BUY1CLICK_ENABLED'
    ),
);

$arSelectFields1 = Array (
    'CATALOG_BASKET_TYPE' => Array (
        "STATIC" => GetMessage('NT_OPTIONS_CATALOG_BASKET_TYPE_STATIC'),
        "FLY" => GetMessage('NT_OPTIONS_CATALOG_BASKET_TYPE_FLY'),
    ),
    
    'CATALOG_VIEW_MODE_LIST' => Array (
        "CARD" => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_LIST_CARD'),
        "LIST" => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_LIST_LIST'),
        "PRICELIST" => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_LIST_PRICELIST'),
    ),
);

$arSelectFields2 = Array (
    'CATALOG_VIEW_MODE_FILTER' => Array (
        'LEFT' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_LEFT'),
            'ICON' => $templateFolder . "/images/icons/filter_left.svg"
        ),
        'TOP' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_TOP'),
            'ICON' => $templateFolder . "/images/icons/filter_top.svg"
        ),
        'RIGHT' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_RIGHT'),
            'ICON' => $templateFolder . "/images/icons/filter_right.svg"
        ),
        
    ),
    
    'CATALOG_TEMPLATE_INNER_SECTIONS' => Array (
        'LIGHT' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_LIGHT'),
            'ICON' => $templateFolder . "/images/icons/catalog_sections_light.svg"
        ),
        'MAIN' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_MAIN'),
            'ICON' => $templateFolder . "/images/icons/catalog_sections_small.svg"
        ),
        'SUBCATEGORIES' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_SUBCATEGORIES'),
            'ICON' => $templateFolder . "/images/icons/catalog_sections_big.svg"
        ),
    ),
    
    
);
 
$arSelectFields3 = Array (

    'CATALOG_LINKED_TEMPLATE' => Array (
        "slider" => GetMessage('NT_OPTIONS_CATALOG_LINKED_TEMPLATE_SLIDER'),
        "main" => GetMessage('NT_OPTIONS_CATALOG_LINKED_TEMPLATE_MAIN'),
    ),
    
    
);
?>

<? foreach ($arSelectFields1 as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select">
        <? foreach($arOptions as $key => $value): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" <?if($key == $arResult['CURRENT_OPTIONS'][$code]):?>class="active"<?endif;?>><?=$value?></a>
        <? endforeach; ?>
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>

<? foreach ($arSelectFields2 as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select visual">
        <? foreach ($arOptions as $key => $arOption): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" class="item<?=($key == $arResult['CURRENT_OPTIONS'][$code]) ? ' active' : ''?>">
            <span><?=$arOption['TITLE']?></span>
            <img src="<?=$arOption['ICON']?>" alt="" />
        </a>
        <? endforeach; ?>
        
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>

<? foreach ($arSelectFields3 as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select">
        <? foreach($arOptions as $key => $value): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" <?if($key == $arResult['CURRENT_OPTIONS'][$code]):?>class="active"<?endif;?>><?=$value?></a>
        <? endforeach; ?>
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>

<? foreach ($arCatalogOptions as $arOption): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=$arOption['TITLE']?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="<?=$arOption['OPTION_NAME']?>" value="<?=$arResult['CURRENT_OPTIONS'][$arOption['OPTION_NAME']]?>" />
    </div>
</div>
<? endforeach; ?>

