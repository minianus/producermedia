<?
$arViewModes = Array (
    'BRANDS_VIEW_MODE' => Array(
        "normal" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_BRANDS_VIEW_MODE_TYPE_1'),
            "ICON" => $templateFolder . "/images/icons/brands_list_default.svg"
        ),
        "alphabet" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_BRANDS_VIEW_MODE_TYPE_2'),
            "ICON" => $templateFolder . "/images/icons/brands_list_alphabet.svg"
        ),
    )
);
    
?>


<? foreach ($arViewModes as $optionCode => $arValues): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_'.$optionCode.'_H_T')?></div>
    <div class="nt-public-options-select visual">
        <? foreach ($arValues as $key => $arField): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" class="item<?=$arResult['CURRENT_OPTIONS'][$optionCode] == $key ? ' active' : ''?>">
            <span><?=$arField['TITLE']?></span>
            <img src="<?=$arField['ICON']?>" alt="" />
        </a>
        <? endforeach; ?>
        <input name="<?=$optionCode?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$optionCode]?>" />
    </div>
</div>
<? endforeach; ?>