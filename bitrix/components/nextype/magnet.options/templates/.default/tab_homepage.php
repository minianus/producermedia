<?
$arHomepageBlocks = Array(
    'homepage/top_banners.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_TOP_BANNERS'),
    'homepage/products.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_PRODUCTS'),
    'homepage/catalog.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_CATALOG'),
    'homepage/promo.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_PROMO'),
    'homepage/products_sections.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_PRODUCTS_SECTIONS'),
    'homepage/actions.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_ACTIONS'),
    'homepage/news_and_about.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_NEWS_AND_ABOUT'),
    'homepage/brands.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_BRANDS'),
    'homepage/instagram.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_INSTAGRAM'),
    'viewed.php' => GetMessage('NT_OPTIONS_HOMEPAGE_BLOCK_VIEWED'),
);

$arSelectFields['HOMEPAGE_SLIDER_VIEW_MODE'] = Array(
    "type1" => Array(
        "TITLE" => GetMessage('NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_1'),
        "ICON" => $templateFolder . "/images/icons/slider_type1.svg"
    ),
    "type2" => Array(
        "TITLE" => GetMessage('NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_2'),
        "ICON" => $templateFolder . "/images/icons/slider_type2.svg"
    ),
    "type3" => Array(
        "TITLE" => GetMessage('NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_3'),
        "ICON" => $templateFolder . "/images/icons/slider_type3.svg"
    ),
);


$arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS'] = unserialize($arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS']);
if (is_array($arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS']))
{
    $arNewHomepageBlocks = Array ();
    foreach ($arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS'] as $key => $val)
    {
        if (isset($arHomepageBlocks[strtolower($key)]))
            $arNewHomepageBlocks[strtolower($key)] = $arHomepageBlocks[strtolower($key)];
        
    }
    
    $arHomepageBlocks = $arNewHomepageBlocks;
}
?>

<div class="nt-public-options-content-h">
    <?=GetMessage('NT_OPTIONS_HOMEPAGE_BLOCKS_H_T')?>
</div>
<? $containerID = md5("homepage_blocks"); ?>
<ul id="<?=$containerID?>">
<? foreach ($arHomepageBlocks as $key => $value): ?>
<li class="nt-public-options-content-f drag">
    <div class="nt-public-options-content-f-label"><?=$value?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="HOMEPAGE_BLOCKS[<?=$key?>][ACTIVE]" value="<?=$arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS'][$key]['ACTIVE']?>" />
    </div>
    <input type="hidden" name="HOMEPAGE_BLOCKS[<?=$key?>][PROP_MOBILE_VISIBLE]" value="<?=$arResult['CURRENT_OPTIONS']['HOMEPAGE_BLOCKS'][$key]['PROP_MOBILE_VISIBLE']?>" />
</li>
<? endforeach; ?>
</ul>

<script>
$( function() {
    $("#<?=$containerID?>").sortable();
} );
</script>


<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_HOMEPAGE_SLIDER_H_T')?></div>
    <div class="nt-public-options-select visual">
        <? foreach ($arSelectFields['HOMEPAGE_SLIDER_VIEW_MODE'] as $key => $arField): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" class="item<?=$arResult['CURRENT_OPTIONS']['HOMEPAGE_SLIDER_VIEW_MODE'] == $key ? ' active' : ''?>">
            <span><?=$arField['TITLE']?></span>
            <img src="<?=$arField['ICON']?>" alt="" />
        </a>
        <? endforeach; ?>
        <input name="HOMEPAGE_SLIDER_VIEW_MODE" type="hidden" value="<?=$arResult['CURRENT_OPTIONS']['HOMEPAGE_SLIDER_VIEW_MODE']?>" />
    </div>
</div>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_HOMEPAGE_SLIDER_AUTOPLAY_H_T')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="HOMEPAGE_SLIDER_AUTOPLAY" value="<?=$arResult['CURRENT_OPTIONS']['HOMEPAGE_SLIDER_AUTOPLAY']?>" />
    </div>
</div>