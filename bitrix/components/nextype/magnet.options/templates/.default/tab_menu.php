<?

    $arSelectFields['SIDEBAR_MENU_TYPE'] = Array (
        "type1" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_SIDEBAR_MENU_TYPE_1'),
            "ICON" => $templateFolder . "/images/icons/menu_default.svg"
        ),
        "type2" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_SIDEBAR_MENU_TYPE_2'),
            "ICON" => $templateFolder . "/images/icons/menu_icons_w_text.svg"
        ),
        "type3" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_SIDEBAR_MENU_TYPE_3'),
            "ICON" => $templateFolder . "/images/icons/menu_only_text.svg"
        ),
        "type4" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_SIDEBAR_MENU_TYPE_4'),
            "ICON" => $templateFolder . "/images/icons/menu_header.svg"
        ),
        "type5" => Array (
            "TITLE" => GetMessage('NT_OPTIONS_SIDEBAR_MENU_TYPE_5'),
            "ICON" => $templateFolder . "/images/icons/menu_small_icons.svg"
        ),
        
    );
?>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_SIDEBAR_MENU_OPEN_ON_HOVER_H_T')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="SIDEBAR_MENU_OPEN_ON_HOVER" value="<?=$arResult['CURRENT_OPTIONS']['SIDEBAR_MENU_OPEN_ON_HOVER']?>" />
    </div>
</div>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_SIDEBAR_MENU_TYPE_H_T')?></div>
    <div class="nt-public-options-select visual">
        <? foreach ($arSelectFields['SIDEBAR_MENU_TYPE'] as $key => $arField): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" class="item<?=$arResult['CURRENT_OPTIONS']['SIDEBAR_MENU_TYPE'] == $key ? ' active' : ''?>">
            <span><?=$arField['TITLE']?></span>
            <img src="<?=$arField['ICON']?>" alt="" />
        </a>
        <? endforeach; ?>
        <input name="SIDEBAR_MENU_TYPE" type="hidden" value="<?=$arResult['CURRENT_OPTIONS']['SIDEBAR_MENU_TYPE']?>" />
    </div>
</div>