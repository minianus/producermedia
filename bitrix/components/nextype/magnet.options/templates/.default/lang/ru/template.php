<?php

$MESS['COLORPICKER_CHOOSE'] = "Выбрать";
$MESS['COLORPICKER_CANCEL'] = "Отмена";

$MESS['NT_OPTIONS_HEADER_TITLE'] = "Настройки сайта";
$MESS['NT_OPTIONS_DEFAULT_OPTIONS'] = "Настройки по умолчанию";
$MESS['NT_OPTIONS_MENU_TITLE_MAIN'] = "Основные";
$MESS['NT_OPTIONS_MENU_TITLE_HEADER'] = "Шапка";
$MESS['NT_OPTIONS_MENU_TITLE_FOOTER'] = "Подвал";
$MESS['NT_OPTIONS_MENU_TITLE_HOMEPAGE'] = "Главная страница";
$MESS['NT_OPTIONS_MENU_TITLE_REGIONS'] = "Регионы";
$MESS['NT_OPTIONS_MENU_TITLE_CATALOG'] = "Каталог товаров";
$MESS['NT_OPTIONS_MENU_TITLE_DOCS'] = "Документация";
$MESS['NT_OPTIONS_MENU_TITLE_MENU'] = "Меню каталога";


$MESS['NT_OPTIONS_MAIN_COLOR_SHEME_H_T'] = "Предзаданные цветовые схемы";
$MESS['NT_OPTIONS_MAIN_CUSTOM_COLOR_SHEME_H_T'] = "Тонкая настройка цвета";
$MESS['NT_OPTIONS_MAIN_SUBDOMAINS_H_T'] = "Мультирегиональность";
$MESS['NT_OPTIONS_MAIN_SUBDOMAINS_ON_SUBDOMAIN_H_T'] = "Мультирегиональсть на поддоменах";
$MESS['NT_OPTIONS_MAIN_FONT_FAMILY_H_T'] = "Шрифт";
$MESS['NT_OPTIONS_MAIN_FONT_SIZE_H_T'] = "Размер шрифта";
$MESS['NT_OPTIONS_MAIN_SIDEBAR_TYPE_H_T'] = "Боковое меню";
$MESS['NT_OPTIONS_MAIN_SITE_WIDTH_H_T'] = "Ширина сайта";
$MESS['NT_OPTIONS_MAIN_BUTTONS_TYPE_ROUNDED'] = "Скругленные";
$MESS['NT_OPTIONS_MAIN_BUTTONS_TYPE_RECTANGULAR'] = "Прямоугольные";
$MESS['NT_OPTIONS_MAIN_BUTTONS_TYPE_H_T'] = "Тип кнопок";
$MESS['NT_OPTIONS_COOKIES_AGREEMENT_ENABLED_H_T'] = "Согласие на обработку Cookie";

$MESS['NT_OPTIONS_MAIN_TITLES_TYPE_H_T'] = "Заголовки";
$MESS['NT_OPTIONS_MAIN_TITLES_TYPE_BOLD'] = "Жирный";
$MESS['NT_OPTIONS_MAIN_TITLES_TYPE_SEMIBOLD'] = "Полужирный";

$MESS['NT_OPTIONS_MAIN_SIDEBAR_MENU_TYPE_H_T'] = "Вариант отображения";
$MESS['NT_OPTIONS_SIDEBAR_MENU_TYPE_1'] = "Только иконка";
$MESS['NT_OPTIONS_SIDEBAR_MENU_TYPE_2'] = "Иконка + заголовок";
$MESS['NT_OPTIONS_SIDEBAR_MENU_TYPE_3'] = "Только заголовок";
$MESS['NT_OPTIONS_SIDEBAR_MENU_TYPE_4'] = "В шапке";
$MESS['NT_OPTIONS_SIDEBAR_MENU_TYPE_5'] = "Маленькие иконки";
$MESS['NT_OPTIONS_SIDEBAR_MENU_OPEN_ON_HOVER_H_T'] = "Показывать при наведении";

$MESS['NT_OPTIONS_HEADER_CALLBACK_H_T'] = "Форма «Заказать звонок»";
$MESS['NT_OPTIONS_HEADER_TYPE_H_T'] = "Вариант шапки";

$MESS['NT_OPTIONS_FOOTER_TYPE_H_T'] = "Вариант подвала";

$MESS['NT_OPTIONS_HOMEPAGE_BLOCKS_H_T'] = "Отображаемые блоки:";
$MESS['NT_OPTIONS_HOMEPAGE_SLIDER_AUTOPLAY_H_T'] = "Автоматическая прокрутка слайдера";

$MESS['NT_OPTIONS_SUBMIT'] = "Применить настройки";

$MESS['NT_OPTIONS_CATALOG_SHOW_WISH_LIST'] = "Избранные товары";
$MESS['NT_OPTIONS_CATALOG_SHOW_COMPARE_LIST'] = "Сравнение товаров";
$MESS['NT_OPTIONS_CATALOG_SHOW_FAST_VIEW'] = "Быстрый просмотр карточки товара";
$MESS['NT_OPTIONS_CATALOG_BASKET_SHOW_POPUP'] = "Всплывающее окно при добавлении в корзину";
$MESS['NT_OPTIONS_CATALOG_SHOW_RATING'] = "Рейтинг товаров";
$MESS['NT_OPTIONS_CATALOG_FLY_HEADER'] = "Плавающая шапка в карточке товара";

$MESS['NT_OPTIONS_CATALOG_BASKET_TYPE_STATIC'] = "Статичная";
$MESS['NT_OPTIONS_CATALOG_BASKET_TYPE_FLY'] = "Летающая";
$MESS['NT_OPTIONS_CATALOG_BASKET_TYPE_STATIC'] = "Статичная";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_LIST_CARD'] = "Плитка";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_LIST_LIST'] = "Список";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_LIST_PRICELIST'] = "Прайслист";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_LEFT'] = "Слева";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_RIGHT'] = "Справа";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_TOP'] = "Сверху";
$MESS['NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_SUBCATEGORIES'] = "Без подразделов";
$MESS['NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_MAIN'] = "С подразделами";
$MESS['NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_LIGHT'] = "Компактный";
$MESS['NT_OPTIONS_CATALOG_LINKED_TEMPLATE_SLIDER'] = "Слайдер";
$MESS['NT_OPTIONS_CATALOG_LINKED_TEMPLATE_MAIN'] = "Плитка";
$MESS['NT_OPTIONS_BUY1CLICK_ENABLED'] = "Купить в 1 клик";

$MESS['NT_OPTIONS_CATALOG_BASKET_TYPE_H_T'] = "Тип корзины";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_LIST_H_T'] = "Отображение списка товаров по умолчанию";
$MESS['NT_OPTIONS_CATALOG_VIEW_MODE_FILTER_H_T'] = "Расположение фильтра";
$MESS['NT_OPTIONS_CATALOG_TEMPLATE_INNER_SECTIONS_H_T'] = "Разделы внутри каталога";
$MESS['NT_OPTIONS_CATALOG_LINKED_TEMPLATE_H_T'] = "Блок \"Связанные товары\"";

$MESS['NT_OPTIONS_HOMEPAGE_SLIDER_H_T'] = "Вариант слайдера";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_TOP_BANNERS'] = "Слайдер + баннеры";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_PRODUCTS'] = "Товары";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_CATALOG'] = "Разделы каталога";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_PROMO'] = "Промо-баннеры";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_NEWS_AND_ABOUT'] = "Новости и О компании";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_BRANDS'] = "Бренды";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_VIEWED'] = "Просмотренные товары";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_INSTAGRAM'] = "Лента Instagram";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_ACTIONS'] = "Акции";
$MESS['NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_1'] = "Слайдер + баннеры";
$MESS['NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_2'] = "Баннеры + слайдер";
$MESS['NT_OPTIONS_HOMEPAGE_SLIDER_VIEW_MODE_TYPE_3'] = "Баннеры + слайдер + баннеры";
$MESS['NT_OPTIONS_HOMEPAGE_BLOCK_PRODUCTS_SECTIONS'] = "Разделы с товарами";

$MESS['NT_OPTIONS_MAIN_LOCATIONS_LIST_TEMPLATE_H_T'] = "Вид списка регионов";
$MESS['NT_OPTIONS_LOCATIONS_LIST_TEMPLATE_SHORT'] = "Сокращенный (единым списком)";
$MESS['NT_OPTIONS_LOCATIONS_LIST_TEMPLATE_REGIONS'] = "С разбивкой на регионы/разделы";
$MESS['NT_OPTIONS_MAIN_LOCATIONS_GEO_ENABLED'] = "Определение города по IP";

$MESS['NT_INTERACTIVE_HELP_TITLE'] = "Интерактивные подсказки";
$MESS['NT_INTERACTIVE_HELP_TEXT'] = "Визуальные подсказки помогут быстро настроить и заполнить сайт";

$MESS['NT_OPTIONS_MENU_TITLE_SECTIONS'] = "Разделы";
$MESS['NT_OPTIONS_BRANDS_VIEW_MODE_H_T'] = "Бренды";
$MESS['NT_OPTIONS_BRANDS_VIEW_MODE_TYPE_1'] = "Крупыне логотипы";
$MESS['NT_OPTIONS_BRANDS_VIEW_MODE_TYPE_2'] = "По алфавиту";