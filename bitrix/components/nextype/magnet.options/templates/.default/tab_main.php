<div class="nt-public-options-content-f column" style="padding-top: 0;">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_COLOR_SHEME_H_T')?></div>
    
    <div class="nt-public-options-colors">
        <? foreach ($arDefaultSchemes as $scheme): ?>
        <a<?=($scheme['color1'] == $arResult['CURRENT_OPTIONS']['COLOR_1']) ? ' class="active"' : '' ?> data-color1="<?=$scheme['color1']?>" href="javascript:void(0);">
            <div style="background-color: <?=$scheme['color1']?>"></div>
        </a>
        <? endforeach; ?>
    </div>
</div>

<div class="nt-public-options-content-f column">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_CUSTOM_COLOR_SHEME_H_T')?></div>
    
    <div class="nt-public-options-colors">
        <a href="javascript:void(0);" class="custom nt-public-options-colorpicker nt-public-options-custom-color1">
            <div style="background-color: <?=$arResult['CURRENT_OPTIONS']['COLOR_1']?>"></div>
            <input type="hidden" value="<?=$arResult['CURRENT_OPTIONS']['COLOR_1']?>" name="COLOR_1" />
        </a>
        
    </div>
</div>


<? $arSelectFields = Array (
    'FONT_FAMILY' => Array (
        "Roboto" => "Roboto",
        "OpenSans" => "Open Sans",
        "Ubuntu" => "Ubuntu",
        "PTSans" => "PT Sans"
    ),
    
    'FONT_SIZE' => Array (
        "14px" => "14px",
        "15px" => "15px",
        "16px" => "16px",
    ),
    
    /*'SITE_WIDTH' => Array (
        "100%" => "100%",
        "1440px" => "1440px",
        "1280px" => "1280px",
    ),*/
    
    
);
?>

<? foreach ($arSelectFields as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select">
        <? foreach($arOptions as $key => $value): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" <?if($key == $arResult['CURRENT_OPTIONS'][$code]):?>class="active"<?endif;?>><?=$value?></a>
        <? endforeach; ?>
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>

<? 
$arVisibleSelect = Array (
    'BUTTONS_TYPE' => Array (
        'ROUNDED' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_MAIN_BUTTONS_TYPE_ROUNDED'),
            'ICON' => $templateFolder . "/images/icons/button_round_v2.png"
        ),
        'RECTANGULAR' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_MAIN_BUTTONS_TYPE_RECTANGULAR'),
            'ICON' => $templateFolder . "/images/icons/button_rectangular_v2.png"
        )
    ),
    
    'TITLES_TYPE' => Array (
        'BOLD' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_MAIN_TITLES_TYPE_BOLD'),
            'ICON' => $templateFolder . "/images/icons/titles_bold_v2.png"
        ),
        'SEMIBOLD' => Array (
            'TITLE' => GetMessage('NT_OPTIONS_MAIN_TITLES_TYPE_SEMIBOLD'),
            'ICON' => $templateFolder . "/images/icons/titles_semibold_v2.png"
        )
    )
);
?>

<? foreach ($arVisibleSelect as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select visual">
        <? foreach ($arOptions as $key => $arOption): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" class="item<?=($key == $arResult['CURRENT_OPTIONS'][$code]) ? ' active' : ''?>">
            <span><?=$arOption['TITLE']?></span>
            <img src="<?=$arOption['ICON']?>" alt="" />
        </a>
        <? endforeach; ?>
        
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_COOKIES_AGREEMENT_ENABLED_H_T')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="COOKIES_AGREEMENT_ENABLED" value="<?=$arResult['CURRENT_OPTIONS']['COOKIES_AGREEMENT_ENABLED']?>" />
    </div>
</div>