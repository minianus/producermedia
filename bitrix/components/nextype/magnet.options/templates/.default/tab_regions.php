<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_SUBDOMAINS_H_T')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="LOCATIONS_ENABLED" value="<?=$arResult['CURRENT_OPTIONS']['LOCATIONS_ENABLED']?>" />
    </div>
</div>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_SUBDOMAINS_ON_SUBDOMAIN_H_T')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="LOCATIONS_ON_SUBDOMAIN" value="<?=$arResult['CURRENT_OPTIONS']['LOCATIONS_ON_SUBDOMAIN']?>" />
    </div>
</div>

<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_LOCATIONS_GEO_ENABLED')?></div>
    <div class="nt-public-options-switcher">
        <input type="hidden" name="LOCATIONS_GEO_ENABLED" value="<?=$arResult['CURRENT_OPTIONS']['LOCATIONS_GEO_ENABLED']?>" />
    </div>
</div>

<? $arSelectFields = Array (
    'LOCATIONS_LIST_TEMPLATE' => Array (
        "SHORT" => GetMessage('NT_OPTIONS_LOCATIONS_LIST_TEMPLATE_SHORT'),
        "REGIONS" => GetMessage('NT_OPTIONS_LOCATIONS_LIST_TEMPLATE_REGIONS'),
        
    )
);
?>
<? foreach ($arSelectFields as $code => $arOptions): ?>
<div class="nt-public-options-content-f">
    <div class="nt-public-options-content-f-label"><?=GetMessage('NT_OPTIONS_MAIN_'.$code.'_H_T')?></div>
    <div class="nt-public-options-select">
        <? foreach($arOptions as $key => $value): ?>
        <a href="javascript:void(0);" data-value="<?=$key?>" <?if($key == $arResult['CURRENT_OPTIONS'][$code]):?>class="active"<?endif;?>><?=$value?></a>
        <? endforeach; ?>
        <input name="<?=$code?>" type="hidden" value="<?=$arResult['CURRENT_OPTIONS'][$code]?>" />
    </div>
</div>
<? endforeach; ?>