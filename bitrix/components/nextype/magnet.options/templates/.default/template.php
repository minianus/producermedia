<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->setFrameMode(false);

$formId = "nt_options_" . randString(5);
CJSCore::Init('jquery');
$APPLICATION->SetAdditionalCSS($templateFolder.'/css/colorpicker.css');
$APPLICATION->AddHeadScript($templateFolder.'/js/colorpicker.js');
$APPLICATION->AddHeadScript($templateFolder.'/js/cookie.js');
$APPLICATION->AddHeadScript($templateFolder.'/js/jquery-ui.min.js');

if ($_SESSION['NT_MAGNET_HELPER'] == "Y")
{
    $APPLICATION->SetAdditionalCSS('//nextype.ru/solutions/helpers/magnet/styles.css?'. time());
    $APPLICATION->AddHeadScript('//nextype.ru/solutions/helpers/magnet/script.js.php');
}

$arTemplateOptions = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/options.json"), true);
if (is_array($arTemplateOptions))
    $arDefaultSchemes = $arTemplateOptions['schemes'];

if (in_array($_REQUEST['nt_magnet_helper'], Array ("Y", "N")))
{
    $_SESSION['NT_MAGNET_HELPER'] = $_REQUEST['nt_magnet_helper'];
    LocalRedirect($APPLICATION->GetCurPageParam('', Array ('nt_magnet_helper')));
}

$styles = '<style>
    .nt-public-options-icon,
    .nt-public-options-mainmenu a.active,
    .nt-public-options-select a.active,
    .nt-public-options-actions button[type=submit], .nt-public-interactive-help-icon, .nt-public-options-select-preview a span {background-color: '.$arResult['CURRENT_OPTIONS']['COLOR_1'].';}
    .nt-public-options-tab::-webkit-scrollbar-thumb { background-color: '.$arResult['CURRENT_OPTIONS']['COLOR_1'].'; }
    .nt-public-options-select a.active, .nt-public-options-select-preview a.active, .nt-public-options-select.visual .item.active {
        border-color: '.$arResult['CURRENT_OPTIONS']['COLOR_1'].';
    }
</style>';
$APPLICATION->AddHeadString($styles);

?>

<div class="nt-public-options-overflow<?=($_COOKIE['ntOptions'] == 'open') ? ' open' : ''?>"></div>
<div class="nt-public-options<?=($_COOKIE['ntOptions'] == 'open') ? ' open' : ''?>" <?=($_COOKIE['ntOptions'] == 'open') ? 'style="right:0"' : ''?>>
    
    <div class="nt-public-options-container">
        <form method="post" id="<?=$formId?>" action="<?=$APPLICATION->GetCurPageParam()?>">
        <div class="nt-public-options-header">
            <div class="nt-public-options-header-t">
                <?=GetMessage('NT_OPTIONS_HEADER_TITLE')?>
            </div>
            <button type="submit" name="NT_OPTIONS_RESET" class="nt-public-options-header-b" value="Y"><?=GetMessage('NT_OPTIONS_DEFAULT_OPTIONS')?></button>
            
        </div>
        <div class="nt-public-options-content-box">
            
            <? $arTabs = Array (
                Array (
                    'ID' => 'nt-public-options-tab-main',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_MAIN'),
                    'INCLUDE_FILE' => 'tab_main.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-menu',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_MENU'),
                    'INCLUDE_FILE' => 'tab_menu.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-header',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_HEADER'),
                    'INCLUDE_FILE' => 'tab_header.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-footer',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_FOOTER'),
                    'INCLUDE_FILE' => 'tab_footer.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-homepage',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_HOMEPAGE'),
                    'INCLUDE_FILE' => 'tab_homepage.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-regions',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_REGIONS'),
                    'INCLUDE_FILE' => 'tab_regions.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-catalog',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_CATALOG'),
                    'INCLUDE_FILE' => 'tab_catalog.php'
                ),
                Array (
                    'ID' => 'nt-public-options-tab-sections',
                    'TITLE' => GetMessage('NT_OPTIONS_MENU_TITLE_SECTIONS'),
                    'INCLUDE_FILE' => 'tab_sections.php'
                )
                
                
            );
            $bSetActive = false;
            foreach ($arTabs as $key => $arTab)
            {
                if (isset($_COOKIE['ntOptionsTab']) && $_COOKIE['ntOptionsTab'] == $arTab['ID'])
                {
                    $arTabs[$key]['ACTIVE'] = "Y";
                    $bSetActive = true;
                    break;
                }
            }
            if (!$bSetActive)
                $arTabs[0]['ACTIVE'] = "Y";
            
            ?>
            
            <div class="nt-public-options-content">
                <? foreach ($arTabs as $arTab): ?>
                <div id="<?=$arTab['ID']?>" class="nt-public-options-tab<?=isset($arTab['ACTIVE']) && $arTab['ACTIVE'] == "Y" ? ' active' : ''?>">
                    <? include(__DIR__ . "/" . $arTab['INCLUDE_FILE']); ?>
                </div>
                <? endforeach; ?>
                
                <div class="nt-public-options-actions">
                    <button type="submit" name="NT_OPTIONS_SAVE" value="Y"><?=GetMessage('NT_OPTIONS_SUBMIT')?></button>
                </div>
                
            </div>
            
            <div class="nt-public-options-mainmenu">
                
                <? foreach ($arTabs as $arTab): ?>
                <a href="javascript:void(0);" <?if(isset($arTab['ACTIVE']) && $arTab['ACTIVE'] == 'Y'):?>class="active"<?endif;?> data-open-tab="<?=$arTab['ID']?>"><?=$arTab['TITLE']?></a>
                <? $bFirst = false; ?>
                <? endforeach; ?>
                
                <a href="https://nextype.ru/docs/course/?COURSE_ID=11&INDEX=Y" target="_blank"><?=GetMessage('NT_OPTIONS_MENU_TITLE_DOCS')?></a>
            </div>
        </div>
            
        </form>
    </div>
    <div class="nt-public-options-icon"></div>
    
</div>

<div class="nt-public-interactive-help"<?=($_COOKIE['ntOptions'] == 'open') ? ' style="display:none"' : ''?>>
    <div class="nt-public-interactive-help-icon"></div>
    <div class="nt-public-interactive-help-container">
		<div class="nt-public-interactive-help-tooltip">
		    <div class="title"><?=GetMessage('NT_INTERACTIVE_HELP_TITLE')?></div>
		    <div class="text"><?=GetMessage('NT_INTERACTIVE_HELP_TEXT')?></div>
		    <div class="nt-public-options-content-f">
		        <div class="nt-public-options-switcher">
		            <input id="nt_magnet_helper_switch" type="hidden" value="<?=$_SESSION['NT_MAGNET_HELPER'] == "Y" ? "Y" : "N"?>" />
		        </div>
		    </div>
		</div>
    </div>
</div>

<script>
(function( $ ) {
    $().ready(function() {
        
        function setColorpicker (el) {
            $(el).spectrum({
                color: $(el).data('color'),
                preferredFormat: "hex",
                showInput: true,
                chooseText: "<?=GetMessage('COLORPICKER_CHOOSE')?>",
                cancelText: "<?=GetMessage('COLORPICKER_CANCEL')?>",
                change: function(color) {
                    $(this).find('div').css('backgroundColor', color.toHexString());
                    $(this).find('input').val(color.toHexString());
                    $(".nt-public-options-colors a").removeClass('active');
                    $(this).addClass('active');
                }
            });
        };
        
        $(".nt-public-options-switcher").each(function () {
            var val = $(this).find('input').val();
            if (val == "Y") $(this).addClass('active'); else $(this).removeClass('active');
        });
        
        $("#nt_magnet_helper_switch").on('change', function () {
            if ($(this).val() == "Y")
                window.location='<?=$APPLICATION->GetCurPageParam('nt_magnet_helper=Y', Array ('nt_magnet_helper'))?>';
            else
                window.location='<?=$APPLICATION->GetCurPageParam('nt_magnet_helper=N', Array ('nt_magnet_helper'))?>';
        });
        
        $("body").on("submit", "#<?=$formId?>", function () {
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                data: $(this).serialize() + '&IS_AJAX=Y',
                success: function (data) {
                    data = $(data).find('body > .wrapper').html();
                    $("body > .wrapper").html(data);
                }
            });
        });
        
        $("body").on('click', '.nt-public-options-switcher', function () {
            var val = $(this).find('input').val();
            if (val == "Y") {
                $(this).removeClass('active');
                $(this).find('input').val('N');
            } else {
                $(this).addClass('active');
                $(this).find('input').val('Y');
            }
            $(this).find('input').trigger('change');
        });
        
        $("body").on('click', '.nt-public-options-mainmenu [data-open-tab]', function () {
            $(".nt-public-options-tab").removeClass('active');
            var tab = $(this).data('open-tab');
            $("#" + tab).addClass('active');
            $(this).parent().find('a').removeClass('active');
            $(this).addClass('active');
            $.cookie('ntOptionsTab', tab, {path: '/'});
        });
        
        $("body").on('click', '.nt-public-options-colors a:not(.custom)', function () {
            $(this).parent().find('a').removeClass('active');
            $(".nt-public-options-colors a.custom").removeClass('active');
            $(this).addClass('active');
            
            var color1 = $(this).data('color1'), self = $(this).parents('form');
            self.find('input[name="COLOR_1"]').val(color1);
            self.find('.nt-public-options-custom-color1 div').css('background-color', color1);
        });
        
        $(".nt-public-options-colorpicker").each (function () {
            setColorpicker(this);
        });
        
        
        $("body").on('click', '.nt-public-options-icon', function (event) {
            event.preventDefault();
            if ($('.nt-public-options').hasClass('open'))
                $('.nt-public-options').trigger('close');
            else
                $('.nt-public-options').trigger('open');
            return false;
        });
        
        $(document).click(function(event) {
            if ($(event.target).closest(".nt-public-options").length) return;
            $(".nt-public-options.open").trigger('close');
            event.stopPropagation();
        });
        
        
        $("body").on('open', '.nt-public-options', function () {
            $(".nt-public-interactive-help").hide();
            $(".nt-public-options-overflow").addClass('open');
            $(this).addClass('open').animate({right:'0px'}, 200);
            $("html, body").stop().animate({scrollTop:180}, 200, 'swing');
            $.cookie('ntOptions', 'open', {path: '/'});
        });

        $("body").on('close', '.nt-public-options', function () {
            $(this).addClass('open').animate({right:'-700px'}, 200, function () {
                $(this).removeClass('open');
                $(".nt-public-interactive-help").show();
                $(".nt-public-options-overflow").removeClass('open');
            });
            $.removeCookie('ntOptions', {path: '/'});
        });
        
        $("body").on('click', '.nt-public-options-select a', function () {
            $(this).parent().find('a').removeClass('active');
            $(this).addClass('active');
            var val = $(this).data('value');
            $(this).parent().find('input[type=hidden]').val(val);
        });
        
        $("body").on('click', '.nt-public-options-select-preview a', function () {
            $(this).parent().find('a').removeClass('active');
            $(this).addClass('active');
            var val = $(this).data('value');
            $(this).parent().find('input[type=hidden]').val(val);
        });
        
        
    });
})(jQuery);
</script>