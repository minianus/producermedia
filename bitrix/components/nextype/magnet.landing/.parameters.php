<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
	Bitrix\Main\Web\Json,
	Bitrix\Iblock,
	Bitrix\Catalog,
	Bitrix\Currency;

$catalogIncluded = Loader::includeModule('catalog');

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
            "ID" => Array (
                "PARENT" => "VISUAL",
		"NAME"		=> GetMessage("LANDING_ID"),
		"TYPE"		=> "STRING",
		"DEFAULT"	=> 5
            )
        )
);
        
if ($catalogIncluded)
{

    $arSort = CIBlockParameters::GetElementSortFields(
	array('SHOWS', 'SORT', 'TIMESTAMP_X', 'NAME', 'ID', 'ACTIVE_FROM', 'ACTIVE_TO'),
	array('KEY_LOWERCASE' => 'Y')
    );

    $arPrice = array();
    if ($catalogIncluded)
    {
            $arOfferSort = array_merge($arSort, CCatalogIBlockParameters::GetCatalogSortFields());
            if (isset($arSort['CATALOG_AVAILABLE']))
                    unset($arSort['CATALOG_AVAILABLE']);
            $arPrice = CCatalogIBlockParameters::getPriceTypesList();
    }
    else
    {
            $arOfferSort = $arSort;
            $arPrice = $arProperty_N;
    }
    
    $arStore = array();
	$storeIterator = CCatalogStore::GetList(
		array(),
		array('ISSUING_CENTER' => 'Y'),
		false,
		false,
		array('ID', 'TITLE')
	);
	while ($store = $storeIterator->GetNext())
		$arStore[$store['ID']] = "[".$store['ID']."] ".$store['TITLE'];
    
    
     
        
    $arComponentParameters["PARAMETERS"]['PRICE_CODE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('PRICE_CODE'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'VALUES' => $arPrice,
    );
    
       
    $arComponentParameters["PARAMETERS"]['HIDE_NOT_AVAILABLE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('HIDE_NOT_AVAILABLE'),
		'TYPE' => 'LIST',
		'DEFAULT' => 'N',
		'VALUES' => array(
			'Y' => GetMessage('HIDE_NOT_AVAILABLE_HIDE'),
			'L' => GetMessage('HIDE_NOT_AVAILABLE_LAST'),
			'N' => GetMessage('HIDE_NOT_AVAILABLE_SHOW')
		),
		'ADDITIONAL_VALUES' => 'N'
	);
    
    $arComponentParameters["PARAMETERS"]['STORES'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('STORES'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => $arStore,
		'ADDITIONAL_VALUES' => 'Y'
	);
    
    $arComponentParameters["PARAMETERS"]["LINK_PRODUCTS_ELEMENT_COUNT"] = array(
		"PARENT" => "VISUAL",
		"NAME"		=> GetMessage("LINK_PRODUCTS_ELEMENT_COUNT"),
		"TYPE"		=> "STRING",
		"DEFAULT"	=> 5
	);
    
    $arComponentParameters["PARAMETERS"]["DISPLAY_LIST_SORT"] = Array(
                'PARENT' => 'VISUAL',
		"NAME" => GetMessage("CPT_LIST_SORT"),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'DEFAULT' => '',
		'VALUES' => Array (
                    'POPULAR' => GetMessage('CPT_LIST_SORT_POPULAR'),
                    'NAME' => GetMessage('CPT_LIST_SORT_NAME'),
                    'PRICE' => GetMessage('CPT_LIST_SORT_PRICE'),
                )
);
    
}