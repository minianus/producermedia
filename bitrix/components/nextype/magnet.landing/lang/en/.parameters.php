<?php
$MESS['LANDING_ID'] = "Landing page ID";
$MESS['PRICE_CODE'] = "Price type";
$MESS['HIDE_NOT_AVAILABLE'] = "Inaccessible goods";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "do not display";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "display at the end";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "display in the general list";
$MESS['STORES'] = "Stores";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Number of items to display";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Block Title \"Related Products\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Related Products";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE'] = "Block Title \"Other Promotions\"";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE_DEFAULT'] = "Other Promotions ";
$MESS['CPT_LIST_SORT'] = "Active Sorts";
$MESS['CPT_LIST_SORT_POPULAR'] = "Popularity";
$MESS['CPT_LIST_SORT_NAME'] = "Name";
$MESS['CPT_LIST_SORT_PRICE'] = "Price";