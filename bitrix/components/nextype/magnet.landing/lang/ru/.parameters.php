<?php
$MESS['LANDING_ID'] = "ИД посадочной";
$MESS['PRICE_CODE'] = "Тип цены";
$MESS['HIDE_NOT_AVAILABLE'] = "Недоступные товары";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "не отображать";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "отображать в конце";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "отображать в общем списке";
$MESS['STORES'] = "Склады";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Количество товаров для отображения";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Заголовок блока \"Связанные товары\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Связанные товары";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE'] = "Заголовок блока \"Другие акции\"";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE_DEFAULT'] = "Другие акции";
$MESS['CPT_LIST_SORT'] = "Активные сортировки";
$MESS['CPT_LIST_SORT_POPULAR'] = "Популярность";
$MESS['CPT_LIST_SORT_NAME'] = "Наименование";
$MESS['CPT_LIST_SORT_PRICE'] = "Цена";