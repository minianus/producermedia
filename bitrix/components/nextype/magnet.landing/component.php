<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

if (!empty($arParams['ID']))
{
    $arItem = \Nextype\Magnet\CCache::CIBlockElement_GetByID(intval($arParams['ID']));
    if (!empty($arItem))
    {
        \Nextype\Magnet\CSolution::getInstance(SITE_ID);
        $isEnabeld = \Nextype\Magnet\CSolution::$options['LOCATIONS_ENABLED'];
        
        $rsProps = \CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array(), Array());
        while ($arProp = $rsProps->fetch())
        {
            
            if (strpos($arProp['CODE'], "BLOCK_") === 0)
            {
                $arBlockCode = explode("_", $arProp['CODE']);
                $blockName = is_string($arBlockCode[1]) && !empty($arBlockCode[1]) ? $arBlockCode[1] : false;
                $blockPropCode = is_string($arBlockCode[3]) && !empty($arBlockCode[3]) ? $arBlockCode[3] : false;
                
                if (!$blockName || !$blockPropCode)
                    continue;
                
                $blockId = is_string($arBlockCode[2]) && !empty($arBlockCode[2]) ? intval($arBlockCode[2]) : 0;
                
                if ($arProp['MULTIPLE'] != 'Y' && $arProp['PROPERTY_TYPE'] == "L")
                    $value = Array (
                        'VALUE' => $arProp['VALUE'],
                        'VALUE_XML_ID' => $arProp['VALUE_XML_ID'],
                    );
                elseif ($arProp['MULTIPLE'] != 'Y' && $arProp['PROPERTY_TYPE'] == "F")
                    $value = !empty($arProp['VALUE']) ? \CFile::GetFileArray($arProp['VALUE']) : '';
                elseif ($arProp['MULTIPLE'] != 'Y' && $arProp['PROPERTY_TYPE'] == "S" && $arProp['USER_TYPE'] == 'HTML')
                    $value = !empty($arProp['VALUE']['TEXT']) ? $arProp['VALUE']['TEXT'] : '';
                elseif ($arProp['MULTIPLE'] != 'Y' && $arProp['USER_TYPE'] == 'MagnetProductConditions')
                {
                    $value = Array (
                        'CONDITIONS' => CheckSerializedData($arProp['VALUE']) ? unserialize($arProp['VALUE']) : false
                    );
                }
                elseif ($arProp['MULTIPLE'] == 'Y')
                {
                    $value = Array ();
                    $rsEnums = \CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort" => "asc"), Array ("ID" => $arProp['ID']));
                    
                    while ($arEnum = $rsEnums->fetch())
                    {
                        $value[] = Array (
                            'VALUE' => $arEnum['VALUE'],
                            'VALUE_XML_ID' => $arEnum['VALUE_XML_ID'],
                        );
                    }
                }
                
                else
                    $value =  $arProp['VALUE'];
                
                if ($blockPropCode == 'SORT')
                    $value = intval($value);
                
                $arResult['BLOCKS'][$blockName][$blockId][$blockPropCode] = $value;
                
            }
            else
            {
                if ($arProp['MULTIPLE'] == 'Y')
                {
                    $rsEnums = \CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort" => "asc"), Array ("ID" => $arProp['ID']));
                    
                    $arProp['VALUE'] = Array ();
                    while ($arEnum = $rsEnums->fetch())
                    {
                        $arProp['VALUE'][] = Array (
                            'VALUE' => $arEnum['VALUE'],
                            'VALUE_XML_ID' => $arEnum['VALUE_XML_ID'],
                        );
                    }
                    
                    $arResult['PROPERTIES'][$arProp['CODE']] = $arProp;
                }
                else
                    $arResult['PROPERTIES'][$arProp['CODE']] = $arProp;
            }

        }
        
        if ($isEnabeld == "Y")
        {
            $arRegion = \Nextype\Magnet\CLocations::getCurrentRegion();
            if (!empty($arRegion['ID']) && is_array($arResult['PROPERTIES']['REGION']['VALUE']))
            {
                $bFindRegion = false;
                foreach ($arResult['PROPERTIES']['REGION']['VALUE'] as $arValue)
                {
                    if ($arValue['VALUE'] == $arRegion['ID'])
                    {
                        $bFindRegion = true;
                        break;
                    }
                }
                
                
            }

            if (!$bFindRegion)
            {
                /*CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
                include($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "404.php");*/
                \Bitrix\Iblock\Component\Tools::process404("", true, true, true, "");
                return;
            }
            
        }
        
        $arResult['SEO_DATA'] = \Nextype\Magnet\CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);

        foreach (Array(
            'title' => 'ELEMENT_META_TITLE',
            'description' => 'ELEMENT_META_DESCRIPTION',
            'keywords' => 'ELEMENT_META_KEYWORDS'
        ) as $key1 => $key2)
        {
            if (!empty($arResult['SEO_DATA'][$key2]))
            {
                $APPLICATION->SetPageProperty($key1, $arResult['SEO_DATA'][$key2]);
            }
        }
    }
    
    $this->IncludeComponentTemplate();
}