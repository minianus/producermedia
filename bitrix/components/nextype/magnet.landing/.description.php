<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("NT_COM_LANDING_NAME"),
	"DESCRIPTION" => GetMessage("NT_COM_LANDING_DESCRIPTION"),
);

?>