<?
if (!function_exists('custom_mail') && COption::GetOptionString("webprostor.smtp", "USE_MODULE") == "Y")
{
	function custom_mail($to, $subject, $message, $additional_headers='', $additional_parameters='')
	{
		if(CModule::IncludeModule("webprostor.smtp"))
		{
			$smtp = new CWebprostorSmtp("pm");
			$result = $smtp->SendMail($to, $subject, $message, $additional_headers, $additional_parameters);

			if($result)
				return true;
			else
				return false;
		}
	}
}



\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(

    'sale',

    'OnSaleComponentOrderJsData',

    'SaleOrderEvents::removeSendRegistrationLinkCheckbox'

);



class SaleOrderEvents

{

    function removeSendRegistrationLinkCheckbox(&$arResult, &$arParams)

    {



        $tmp = $arResult["JS_DATA"]["ORDER_PROP"]["properties"];

        $arResult["JS_DATA"]["ORDER_PROP"]["properties"] = array();

        foreach($tmp as $property)

        {

            if($property["CODE"]!="BONUS")

                $arResult["JS_DATA"]["ORDER_PROP"]["properties"][] = $property;

        }

    }

}
?>