<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

CSolution::getInstance(SITE_ID);
?>
</div>
<? CSolution::ShowPageBlock('aside.right'); ?>
</div>
<? $APPLICATION->ShowViewContent('before_footer_content'); ?>
<? if (CSolution::isCustomPage()): ?>
    </div>
    </main>
<? else: ?>
    <? CSolution::ShowPageBlock('catalog.viewed'); ?>
    </div>
<? endif; ?>

</div>
<? CSolution::ShowBanners('footer_before'); ?>
<footer class="<?= CSolution::$options['FOOTER_TYPE']; ?> bg-footer_white">

    <? CSolution::ShowPageBlock('footer'); ?>

</footer>
<? CSolution::ShowPageBlock('basket.fly'); ?>
<? CSolution::ShowJsScripts('before_close_body'); ?>

<? CSolution::ShowPageBlock('cookies.agreement'); ?>
<? $APPLICATION->IncludeComponent("nextype:magnet.options", ".default", array(), false, array("HIDE_ICONS" => "Y")); ?>

<? CSolution::ShowPageBlock('scrolltop.button'); ?>
<?
$curURI = explode('?', $_SERVER['REQUEST_URI'])[0];
$arLogo = \Nextype\Magnet\CSolution::getSiteLogo();

\Nextype\Magnet\CSolution::setMetaStrings(array(
    "og:type" => "website",
    "og:title" => $APPLICATION->GetTitle(),
    "og:url" => CSolution::getCurrentHost() . $curURI,
    "og:image" => CSolution::getCurrentHost() . $arLogo['SRC'],
    "og:description" => $APPLICATION->GetPageProperty('description')
));
?>
</body>
</html>
