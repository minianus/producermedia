<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (is_array($arResult["ACCOUNT_LIST"]))
{
	?>
	<div class="sale-personal-account-wallet-container">
        <h2 style="font-size: 2em;">Мой баланс</h2>
<!--		<div class="sale-personal-account-wallet-title">-->
<!--			--><?//=Bitrix\Main\Localization\Loc::getMessage('SPA_BILL_AT')?>
<!--			--><?//=$arResult["DATE"];?>
<!--		</div>-->

		<div class="sale-personal-account-wallet-list-container">
			<div class="sale-personal-account-wallet-list">
				<?
					foreach($arResult["ACCOUNT_LIST"] as $accountValue)
					{
						?>
						<div class="sale-personal-account-wallet-list-item" style="border-bottom: unset; margin: 0 0 30px 0;">
							<div class="sale-personal-account-wallet-sum" style="color: #282828;"><?=$accountValue['SUM']?></div>
                            <div class="sale-personal-account-wallet-currency">
<!--								<div class="sale-personal-account-wallet-currency-item">--><?//=$accountValue['CURRENCY']?><!--</div>-->
<!--								<div class="sale-personal-account-wallet-currency-item">--><?//=$accountValue["CURRENCY_FULL_NAME"]?><!--</div>-->
							</div>
						</div>


                        <div style="display:flex; align-items:center;">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/icons/icon-crown.png" width=32px height=29px>
                            <span style="font-size: 2.5em; color: #282828; margin-left: 10px;">0</span>
                        </div>
                        <div style="border-bottom: 1px solid #b3bcc5;">&nbsp;</div>

                        <div class="pe1">
                            <div class="p1e">
                                <div>
                                    <div><span class="e2p">Premium бонусы</span></div>
                                    <div class="pe2">
                                        <span class="p2e">Баллами можно оплатить до 99% стоимости заказа по курсу 1 балл = 1 рубль.</span>
                                    </div>
                                </div>
                            </div>
                        </div>

						<?
					}
				?>
			</div>
		</div>
	</div>
	<?
}