<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$hlModuleInclude = \Bitrix\Main\Loader::includeModule('highloadblock');

$arParams['BRAND_PROP_CODE'] = is_array($arParams['BRAND_PROP_CODE']) ? $arParams['BRAND_PROP_CODE'][0] : $arParams['BRAND_PROP_CODE'];


if (!empty($arResult['PROPERTIES']) && is_array($arResult['PROPERTIES']))
{
    foreach ($arResult['PROPERTIES'] as $key => $arProp)
    {
        if ($arProp['MULTIPLE'] == "Y" && $arProp['VALUE'][0])
        {
            $arProp['VALUE'] = Array();
            $rsMultiProps = \CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), Array("ID" => $arProp['ID']));
            while ($arMultiProp = $rsMultiProps->fetch())
            {
                
                if ($arProp['USER_TYPE'] == 'directory' && !empty($arProp['USER_TYPE_SETTINGS']['TABLE_NAME']))
                {
                    
                
                    $arMultiProp['VALUE_RESULT'] = CCache::GetHLPropertyValue(array_merge($arProp, $arMultiProp));
                    
                    
                    if (isset($arMultiProp['VALUE_RESULT']['UF_NAME']))
                        $arMultiProp['VALUE'] = $arMultiProp['VALUE_RESULT']['UF_NAME'];

                    $arProp['VALUE'][$arMultiProp['PROPERTY_VALUE_ID']] = $arMultiProp['VALUE'];
                    $arProp['VALUE_RESULT'][$arMultiProp['PROPERTY_VALUE_ID']] = $arMultiProp['VALUE_RESULT'];
                    
                    
                }
                elseif ($arProp['PROPERTY_TYPE'] == "E" && !empty($arMultiProp['VALUE']))
                {
                    $arElement = \CIBlockElement::GetByID($arMultiProp['VALUE'])->GetNext();
                    if ($arElement)
                    {
                        $arProp['VALUE'][] = $arElement['NAME'];
                    }
                }
                else
                {
                    $arProp['VALUE'][] = $arMultiProp['VALUE'];
                }
            }
        }
        elseif ($arProp['MULTIPLE'] != "Y")
        {
            if ($arProp['USER_TYPE'] == 'directory' && !empty($arProp['USER_TYPE_SETTINGS']['TABLE_NAME']))
            {
                $arProp['VALUE_RESULT'] = CCache::GetHLPropertyValue($arProp);
                if (isset($arProp['VALUE_RESULT']['UF_NAME']))
                    $arProp['VALUE'] = $arProp['VALUE_RESULT']['UF_NAME'];
            }
            elseif ($arProp['PROPERTY_TYPE'] == "E" && !empty($arProp['VALUE']))
            {
                $arElement = \CIBlockElement::GetByID($arProp['VALUE'])->GetNext();
                if ($arElement)
                {
                    $arProp['VALUE'] = $arElement['NAME'];
                }
            }
        }

        $arResult['PROPERTIES'][$key] = $arProp;
    }

}
                
if ($arParams['BRAND_USE'] === 'Y' && !empty($arParams['BRAND_PROP_CODE']))
{
    if (!empty($arResult['PROPERTIES'][$arParams['BRAND_PROP_CODE']]['~VALUE']))
    {
        if ($arBrand = CIBlockElement::GetByID($arResult['PROPERTIES'][$arParams['BRAND_PROP_CODE']]['~VALUE'])->GetNext())
        {
            if (!empty($arBrand['PREVIEW_PICTURE']))
            {
                $arImage = CFile::ResizeImageGet($arBrand['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                $arBrand['PREVIEW_PICTURE'] = $arImage['src'];
            }
            
            
            $arResult['BRAND'] = $arBrand;
        }
    }
}

if (!empty($arResult['PROPERTIES']['DOCUMENTS']['VALUE']))
{
    foreach ($arResult['PROPERTIES']['DOCUMENTS']['VALUE'] as $arDocId)
    {
        if (!empty($arDocId))
        {
            if ($arFile = CFile::GetFileArray($arDocId))
            {
                $fileinfo = explode(".", $arFile['ORIGINAL_NAME']);
                
                $arFile['EXTENSION'] = trim(strtolower(end($fileinfo)));
                $arFile['NAME'] = str_replace("." . end($fileinfo), "", $arFile['ORIGINAL_NAME']);
                $arFile['PATH'] = CFile::GetPath($arFile['ID']);
                
                $arFile['FILE_SIZE'] = $arFile['FILE_SIZE'] / 1000;
        
                if ($arFile['FILE_SIZE'] < 1024)
                    $arFile['FILE_SIZE'] = round($arFile['FILE_SIZE'], 0) . " " . GetMessage('FILE_SIZE_KB');
                else
                    $arFile['FILE_SIZE'] = round($arFile['FILE_SIZE'] / 1024, 2) . " " . GetMessage('FILE_SIZE_MB');
                
                $arResult['DOCUMENTS'][] = $arFile;
            }
        }
    }
}

$arFilterProperties = Array ();
if ($arParams['USE_FILTER'] == 'Y')
{
    $arParams["FILTER_NAME"] = htmlspecialcharsbx($arParams["FILTER_NAME"]);
    foreach (CIBlockSectionPropertyLink::GetArray($arParams['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID']) as $arProp)
    {
        if ($arProp['ACTIVE'] != "Y" || $arProp['SMART_FILTER'] != "Y")
            continue;

        $arFilterProperties[] = $arProp['PROPERTY_ID'];
    }
}

$arResult['TAB_PROPERTIES'] = Array ();
$arResult['SHORT_PROPERTIES_LIST'] = Array ();
if (!empty($arParams['DISPLAY_PROPERTIES']) && !empty($arResult['PROPERTIES']))
{
    foreach ($arParams['DISPLAY_PROPERTIES'] as $propCode)
    {
        if (in_array($propCode, Array ('vote_count', 'vote_sum', 'rating', 'BLOG_POST_ID', 'BLOG_COMMENTS_CNT', $arParams['BRAND_PROP_CODE'])) ||
            !isset($arResult['PROPERTIES'][$propCode])
        )
            continue;
        
        $arProp = $arResult['PROPERTIES'][$propCode];

        if (!empty($arProp['VALUE']))
        {
            
            if (isset($arProp['VALUE']['TEXT']))
                $arProp['VALUE'] = $arProp['~VALUE']['TEXT'];
            elseif (is_array($arProp['VALUE']) && $arProp['PROPERTY_TYPE'] == 'L')
            {
                $arEnumValues = Array ();
                foreach ($arProp['VALUE'] as $value)
                {
                    if (!empty($value))
                    {
                        $arEnum = CIBlockPropertyEnum::GetByID($value);
                        if ($arEnum)
                        {
                            $arEnumValues[] = $arEnum['VALUE'];
                        }
                    }
                }
                
                $arProp['VALUE'] = implode(", ", $arEnumValues);
            }
            elseif (is_array($arProp['VALUE']) && $arProp['PROPERTY_TYPE'] != 'L')
            {
                $arProp['VALUE'] = implode(", ", $arProp['VALUE']);
            }
            
            if (in_array($arProp['ID'], $arFilterProperties))
            {
                
                if ($arProp['PROPERTY_TYPE'] == 'L' && !empty($arProp['VALUE_ENUM_ID']))
                {
                    $keyCrc = abs(crc32($arProp['VALUE_ENUM_ID']));
                    $arProp["FILTER_URL"] = $arResult['SECTION']['SECTION_PAGE_URL'] . "?" . $arParams["FILTER_NAME"] . "_" . $arProp['ID'] . "_" . $keyCrc . "=Y&set_filter=Y";
                    $arProp["FILTER_INPUT_NAME"] = $arParams["FILTER_NAME"] . "_" . $arProp['ID'] . "_" . $keyCrc;
                    $arResult['PROPERTIES_HAS_FILTER'] = "Y";
                }
                
            }
            
            $arResult['TAB_PROPERTIES'][$arProp['CODE']] = $arProp;
        }
    }
}

if (!empty($arParams['MAIN_BLOCK_PROPERTY_CODE']) && !empty($arResult['PROPERTIES']))
{
    foreach ($arParams['MAIN_BLOCK_PROPERTY_CODE'] as $propCode => $pos)
    {
        $arProp = $arResult['PROPERTIES'][$propCode];
        
        if (!empty($arProp['VALUE']))
        {
            if (isset($arProp['VALUE']['TEXT']))
                $arProp['VALUE'] = $arProp['~VALUE']['TEXT'];
            elseif (is_array($arProp['VALUE']) && $arProp['PROPERTY_TYPE'] == 'L')
            {
                $arEnumValues = Array ();
                foreach ($arProp['VALUE'] as $value)
                {
                    if (!empty($value))
                    {
                        $arEnum = CIBlockPropertyEnum::GetByID($value);
                        if ($arEnum)
                        {
                            $arEnumValues[] = $arEnum['VALUE'];
                        }
                    }
                }
                
                $arProp['VALUE'] = implode(", ", $arEnumValues);
            }
            elseif (is_array($arProp['VALUE']) && $arProp['PROPERTY_TYPE'] != 'L')
            {
                $arProp['VALUE'] = implode(", ", $arProp['VALUE']);
            }
            
            $arResult['SHORT_PROPERTIES_LIST'][$arProp['CODE']] = $arProp;
        }
    }
}

$arResult['VIDEOS'] = Array ();

if (!empty($arResult['PROPERTIES']['VIDEO']['VALUE']))
{
    if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']))
    {
        foreach ($arResult['PROPERTIES']['VIDEO']['VALUE'] as $codeVideo)
        {
            if (!empty($codeVideo))
                $arResult['VIDEOS'][] = htmlspecialchars_decode($codeVideo);
        }
    }
    else
    {
        if (!empty($arResult['PROPERTIES']['VIDEO']['VALUE']))
            $arResult['VIDEOS'][] = htmlspecialchars_decode($arResult['PROPERTIES']['VIDEO']['VALUE']);
    }
}



$bHasPhoto = false;
if (is_array($arResult['MORE_PHOTO']))
{
    foreach ($arResult['MORE_PHOTO'] as $photo)
    {
        if (!empty($photo))
        {
            $bHasPhoto = true;
            break;
        }
    }
}

if (!$bHasPhoto)
{
    $arResult['MORE_PHOTO'] = Array (
        0 => CSolution::getEmptyImage()
    );
    $arResult['EMPTY_PHOTO'] = "Y";
}

if (is_array($arResult['JS_OFFERS']) && !empty($arResult['JS_OFFERS']))
{
    $arSkuIDs = Array ();
    
    foreach ($arResult['JS_OFFERS'] as $key => $arOffer)
    {
        if (empty($arOffer['PREVIEW_PICTURE']) && empty($arOffer['DETAIL_PICTURE']) && (empty($arOffer['SLIDER']) || empty($arOffer['SLIDER'][0])))
        {
            $arResult['JS_OFFERS'][$key]['SLIDER'][0] = $arResult['JS_OFFERS'][$key]['PREVIEW_PICTURE'] = $arResult['JS_OFFERS'][$key]['DETAIL_PICTURE'] = CSolution::getEmptyImage();
        }
        
        if (!empty($arParams['ARTICLE_SKU_PROP']))
        {
            if (!empty($arResult['OFFERS'][$key]['PROPERTIES'][$arParams['ARTICLE_SKU_PROP']]['VALUE']))
            {
                $arResult['JS_OFFERS'][$key]['ARTICLE'] = $arResult['OFFERS'][$key]['PROPERTIES'][$arParams['ARTICLE_SKU_PROP']]['VALUE'];
                $arResult['OFFERS_HAS_ARTICLE'] = true;
            }
            else
                $arResult['JS_OFFERS'][$key]['ARTICLE'] = "";
        }
        
        if ($arParams['SHOW_SKU_NAME'] == "Y")
        {
            $arSkuIDs[] = $arOffer['ID'];
        }
    }
    
    if ($arParams['SHOW_SKU_NAME'] == "Y")
    {
        $rsSkuItems = \CIBlockElement::GetList(Array(), Array ('=ID' => $arSkuIDs), false, false, Array ('ID', 'NAME'));
        while ($arSkuItem = $rsSkuItems->fetch())
        {
            foreach ($arResult['JS_OFFERS'] as $key => $arOffer)
            {
                if ($arOffer['ID'] == $arSkuItem['ID'])
                {
                    $arResult['JS_OFFERS'][$key]['NAME'] = trim($arSkuItem['NAME']);
                }
            }
        }
    }
    
}

if (!empty($arResult['PRODUCT']['TYPE']) && $arResult['PRODUCT']['TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_SET)
{
    $arSets = \CCatalogProductSet::getAllSetsByProduct($arResult['ID'], \CCatalogProductSet::TYPE_SET);
    if (!empty($arSets))
    {
        $arSets = reset($arSets);
        if (!empty($arSets['ITEMS']))
        {
            $arMeasures = Array ();
            $rsMeasures = \CCatalogMeasure::getList();
            while ($arMeasure = $rsMeasures->GetNext())
            {
                $arMeasures[$arMeasure['ID']] = $arMeasure;
            }
            
            $arSetIDs = $arSetItems = Array ();
            foreach ($arSets['ITEMS'] as $arItem)
            {                
                $arSetItems[$arItem['ITEM_ID']] = Array (
                    'SORT' => $arItem['SORT'],
                    'QUANTITY' => $arItem['QUANTITY'],
                    'MEASURE' => isset($arMeasures[$arItem['MEASURE']]) ? $arMeasures[$arItem['MEASURE']]['SYMBOL'] : ""
                );
                
                $arSetIDs[] = $arItem['ITEM_ID'];
            }
            
            $rsSetItems = \CIBlockElement::GetList(Array(), Array ('=ID' => $arSetIDs), false, false);
            while ($arSetItem = $rsSetItems->GetNext())
            {
                $arSetItem = array_merge($arSetItem, $arSetItems[$arSetItem['ID']]);
                if (!empty($arSetItem['PREVIEW_PICTURE']))
                {
                    $arPreview = \CFile::ResizeImageGet($arSetItem['PREVIEW_PICTURE'], Array ('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                    $arSetItem['PREVIEW_PICTURE'] = Array (
                        'SRC' => $arPreview['src'],
                        'TITLE' => $arSetItem['NAME'],
                        'ALT' => $arSetItem['NAME'],
                    );
                }
                else
                {
                    $arSetItem['PREVIEW_PICTURE'] = CSolution::getEmptyImage();
                }
                
                $arSetItem = array_merge(\CCatalogProduct::GetOptimalPrice($arSetItem['ID'], $arSetItem['QUANTITY']), $arSetItem);
                
                $arSetItems[$arSetItem['ID']] = $arSetItem;
            }
            
            if (!empty($arSetItems))
            {
                usort($arSetItems, function($a, $b)
                {
                    if ($a['SORT'] == $b['SORT'])
                        return 0;
                    
                    return ($a['SORT'] < $b['SORT']) ? -1 : 1;
                });
                
                $arResult['SET_ITEMS'] = $arSetItems;
                unset($arSetItems);
            }
        }
    }
   
}

if (!empty($arResult['SECTION']["PATH"]))
{
    if (count($arResult['SECTION']["PATH"]) > 1)
    {
        foreach ($arResult['SECTION']["PATH"] as $arSection)
        {
            $arDataFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arParams['IBLOCK_ID']."_SECTION", $arSection['ID']);
            if (!empty($arDataFields['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
            {
                $arResult['SHOW_ORDER_FORM'] = true;
                break;
            }
        }
    }
    else
    {
        $arSectionsIds = array(
            0 => current($arResult['SECTION']["PATH"])['ID'],
            1 => $arResult['IBLOCK_SECTION_ID']
        );

        foreach ($arSectionsIds as $sId)
        {
            if (empty($sId))
                continue;
            
            $arDataFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arParams['IBLOCK_ID']."_SECTION", $ars);
            if (!empty($arDataFields['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
            {
                $arResult['SHOW_ORDER_FORM'] = true;
                break;
            }
        }
    }
}


if (!$arResult['SHOW_ORDER_FORM'])
{
    $arResult['SHOW_ORDER_FORM'] = ($arResult['PROPERTIES']['MAGNET_SHOW_ORDER_FORM']['VALUE_XML_ID'] == 'Y');
}

