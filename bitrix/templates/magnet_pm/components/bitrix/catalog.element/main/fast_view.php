<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Nextype\Magnet\CSolution;
?>

<div class="product-fast-view" id="<?=$itemIds['ID']?>">
    <?
//    echo "<pre>";
//    print_r($arResult["ITEM_PRICES"]);
//    echo "</pre>";
    ?>
	<a href="javascript:void(0);" onclick="$('.popup').jqmHide(); return false;" class="close-popup-icon pull-right icon-custom"></a>
    <div class="left">
        <div class="product-img" id="<?=$itemIds['BIG_SLIDER_ID']?>">
            <div class="labels <?=strtolower($arParams['LABEL_PROP_POSITION'])?>" id="<?=$itemIds['STICKER_ID']?>">
			
                        <?
                        if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE']))
                        {
                            foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value)
                            {
                                ?>
                                <div class="label <?=strtolower($code)?><?= (!isset($arParams['LABEL_PROP_MOBILE'][$code]) ? ' m-hidden' : '') ?>">
                                    <?= $value ?>
                                </div>
                                <?
                            }
                        }
                        ?>
                        
                        <?
                        if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
                        {
                            if ($haveOffers)
                            {
                                ?>
                                <div class="label" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
                                     style="display: none;">
                                </div>
                                <?
                            }
                            else
                            {
                                if ($price['DISCOUNT'] > 0)
                                {
                                    ?>
                                    <div class="label" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>">
                                        <?= -$price['PERCENT'] ?>%
                                    </div>
                                    <?
                                }
                            }
                        }
                        ?>
		</div>
		<div class="additional-links">
                    <? if ($arParams['DISPLAY_COMPARE']): ?>
                    <a href="javascript:void(0);" id="<?= $itemIds['COMPARE_LINK'] ?>" class="compare icon-custom"></a>
                    <? endif; ?>
                        
                    <? if ($arParams['DISPLAY_WISH_LIST'] === 'Y'): ?>
                    <a href="javascript:void(0);" id="<?=$itemIds['WISH_LIST_ID']?>" class="wishlist icon-custom"></a>
                    <? endif; ?>
			
		</div>

        <script>
            $('.product-item-scu-item-text-block').on('click', function(){
                $(window).resize();
            })
        </script>
		<div class="main-img" data-entity="images-container">
            <?
            if (!empty($actualItem['MORE_PHOTO']))
            {
                foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                {
                    ?>
                    <div <?= ($key == 0 ? 'class="active"' : '') ?> data-entity="image" data-id="<?= $photo['ID'] ?>">
                        <img src="<?= $photo['SRC'] ?>" alt="<?= $alt ?>" <?= ($key == 0 ? ' itemprop="image"' : '') ?>>
                    </div>
                    <?
                }
            }
            ?>
		</div>

        <?if(!$showDiscount){?>
            <div class="add-bonus_fast--view">
                <div class="bonus bonus-color">
                    <span class="bonus-icon"></span>
                    <span><? $DefaultBuyPrice = $arResult["CATALOG_PURCHASING_PRICE"];

                        $CurrentPrice = $arResult["ITEM_PRICES"][0]["PRICE"];

                        $Bonus = $CurrentPrice-$DefaultBuyPrice;
                        echo $Bonus;

                        ?></span>
                </div>
            </div>
        <?}?>
		
                    <?
                    if ($showSliderControls)
                    {
                        if ($haveOffers)
                        {
                            foreach ($arResult['OFFERS'] as $keyOffer => $offer)
                            {
                                if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0)
                                    continue;

                                $strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
                                ?>
                                <div class="product-item-detail-slider-controls-block" id="<?= $itemIds['SLIDER_CONT_OF_ID'] . $offer['ID'] ?>" style="display: <?= $strVisible ?>;">
                                    <div class="thumbnails swiper-container">
                                        <div class="swiper-wrapper">
                                            <?
                                            foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo)
                                            {
                                                ?>
                                                <a href="javascript:void(0)" class="swiper-slide preview<?= ($keyPhoto == 0 ? ' active' : '') ?>" data-entity="slider-control" data-value="<?= $offer['ID'] . '_' . $photo['ID'] ?>">
                                                    <img src="<?= $photo['SRC'] ?>" alt="" title="">
                                                </a>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                        
                                        let tumbnailSwiperSlide = new Swiper('#<?= $itemIds['SLIDER_CONT_OF_ID'] . $offer['ID'] ?> .swiper-container', {
                                            loop:false,
                                            breakpoints: {
                                                0: {slidesPerView: 6},1025: {slidesPerView: 4}
                                            }
                                        });
                                </script>
                                <?
                            }
                        }
                        else
                        {
                            ?>
                            <div class="thumbnails swiper-container" id="<?= $itemIds['SLIDER_CONT_ID'] ?>">
                                <div class="swiper-wrapper">
                                    <?
                                    if (!empty($actualItem['MORE_PHOTO']))
                                    {
                                        foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                                        {
                                            ?>
                                            <a href="javascript:void(0)" class="swiper-slide preview<?= ($key == 0 ? ' active' : '') ?>" data-entity="slider-control" data-value="<?= $photo['ID'] ?>">
                                                <img src="<?= $photo['SRC'] ?>" alt="" title="">
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <script>
                                let tumbnailSwiperSlide = new Swiper('#<?= $itemIds['SLIDER_CONT_ID'] ?>.swiper-container', {
                                    loop:false,
                                    breakpoints: {
                                        0: {slidesPerView: 6},1025: {slidesPerView: 4}
                                    }
                                });
                            </script>
                            <?
                        }
                    }
                    ?>
			
		
    
	</div>
    </div>
    <div class="center">
        <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="product-name" id="fastview-product-title">
            <?=$arResult['NAME']?>
        </a>
        
        <? if ($arResult['SET_ITEMS']): ?>
                <div class="set-items-container">
                    <div class="set-items">
                        <? foreach ($arResult['SET_ITEMS'] as $arSetItem): ?>
                        <div class="item">
                            <a href="<?=$arSetItem['DETAIL_PAGE_URL']?>" class="img">
                                <img src="<?=$arSetItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arSetItem['PREVIEW_PICTURE']['ALT']?>" title="<?=$arSetItem['PREVIEW_PICTURE']['TITLE']?>" />
                            </a>
                            <a href="<?=$arSetItem['DETAIL_PAGE_URL']?>" class="name">
                                <?=$arSetItem['NAME']?>
                            </a>
                            <div class="price">
                                <? if (!empty($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'])): ?>
                                    <? if ($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'] < $arSetItem['RESULT_PRICE']['BASE_PRICE']): ?>
                                    <span><?=CurrencyFormat($arSetItem['RESULT_PRICE']['BASE_PRICE'], $arSetItem['RESULT_PRICE']['CURRENCY'])?></span>
                                    <? endif; ?>
                                    
                                    <?=CurrencyFormat($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'], $arSetItem['RESULT_PRICE']['CURRENCY'])?>
                                <? endif; ?>
                            </div>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
        <? elseif (!empty($arResult['PREVIEW_TEXT'])): ?>
        <div class="preview-text">
            <?=$arResult['PREVIEW_TEXT']?>
        </div>
        <? endif; ?>

								<?
                                    if ($haveOffers && !empty($arResult['OFFERS_PROP']))
                                    {
                                        ?>
			<div class="details">
				<div>
                                    
                                    
                                        <div id="<?= $itemIds['TREE_ID'] ?>">
                                            <?
                                            foreach ($arResult['SKU_PROPS'] as $skuProperty)
                                            {
                                                if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                                    continue;

                                                $propertyId = $skuProperty['ID'];
                                                $skuProps[] = array(
                                                    'ID' => $propertyId,
                                                    'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                                    'VALUES' => $skuProperty['VALUES'],
                                                    'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                                                );
                                                ?>
                                                <div class="product-item-info-container product-item-hidden" data-entity="sku-line-block">
                                                    
                                                    <div class="product-item-scu-container">
                                                        <? if ($arParams['OFFERS_HIDE_TITLE'] != "Y"): ?>
                                                        <div class="product-item-scu-name"><?= htmlspecialcharsEx($skuProperty['NAME']) ?></div>
                                                        <? endif; ?>
                                                        <div class="product-item-scu-block">
                                                            <div class="product-item-scu-list">
                                                                <ul class="product-item-scu-item-list">
                                                                    <?
                                                                    foreach ($skuProperty['VALUES'] as &$value)
                                                                    {
                                                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                        if ($skuProperty['SHOW_MODE'] === 'PICT')
                                                                        {
                                                                            ?>
                                                                            <li class="product-item-scu-item-color-container" title="<?= $value['NAME'] ?>"
                                                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                                data-onevalue="<?= $value['ID'] ?>">
                                                                                <div class="product-item-scu-item-color-block">
                                                                                    <div class="product-item-scu-item-color" title="<?= $value['NAME'] ?>"
                                                                                         <? if (!empty($value['PICT']['SRC'])):?>style="background-image: url('<?= $value['PICT']['SRC'] ?>');"<?endif;?>>
                                                                                        <? if (empty($value['PICT']['SRC'])): ?>
                                                                                        <span class="scu-item-color-title"><?= $value['NAME'] ?></span>
                                                                                        <? endif; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <?
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <li class="product-item-scu-item-text-container" title="<?= $value['NAME'] ?>"
                                                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                                data-onevalue="<?= $value['ID'] ?>">
                                                                                <div class="product-item-scu-item-text-block">
                                                                                    <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                                                                </div>
                                                                            </li>
                                                                            <?
                                                                        }
                                                                    }
                                                                    ?>
                                                                </ul>
                                                                <div style="clear: both;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                        
				</div>
			</div>
                    <?
                                    }
                                    ?>
        
        <? if (!empty($arResult['SHORT_PROPERTIES_LIST'])): ?>
                            <div class="characteristics">
                                <table>
                                    <tbody>
                                        <? foreach ($arResult['SHORT_PROPERTIES_LIST'] as $arProp): ?>
                                        <tr>
                                            <td class="type"><span><?=$arProp['NAME']?></span></td>
                                            <td class="value"><?=$arProp['VALUE']?></td>
                                        </tr>
                                        <? endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <? endif; ?>
    </div>
    <div class="right">
        <? if ($arParams['DISPLAY_RATING'] === "Y"): ?>
                            <?
                            $APPLICATION->IncludeComponent(
                                    'nextype:magnet.iblock.vote', 'view', array(
                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                'ELEMENT_ID' => $arResult['ID'],
                                'ELEMENT_CODE' => '',
                                'MAX_VOTE' => '5',
                                'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                                'SET_STATUS_404' => 'N',
                                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                "DISPLAY_AS_RATING" => 'vote_avg',
                                'CACHE_TIME' => $arParams['CACHE_TIME']
                                    ), $component, array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                        <? endif; ?>
        
        <? if ((!empty($arParams['ARTICLE_PROP']) && !empty($arResult['PROPERTIES'][$arParams['ARTICLE_PROP']]['VALUE'])) || $arResult['OFFERS_HAS_ARTICLE']): ?>
			<div class="code" id="<?=$itemIds['ARTICLE_ID']?>">
                            <?=GetMessage('CT_BCE_CATALOG_ARTICLE', Array (
                                '#VALUE#' => $arResult['PROPERTIES'][$arParams['ARTICLE_PROP']]['VALUE']
                            )); ?>
                        </div>
                        <? endif; ?>
        
        <? CSolution::ShowProductQuantity($arResult, $arParams, $itemIds);?>
                                
        <? if (!empty($arResult['BRAND'])): ?>
			<div class="brand-img">
                            <? if (!empty($arResult['BRAND']['PREVIEW_PICTURE'])): ?>
				<img src="<?=$arResult['BRAND']['PREVIEW_PICTURE']?>" alt="<?=$arResult['BRAND']['NAME']?>">
                            <? endif; ?>
			</div>
                        <? endif; ?>
        
        <div class="price-container">
                            <?
                            if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                            {
                                ?>
                                <div class="old-price" id="<?= $itemIds['OLD_PRICE_ID'] ?>"
                                     style="display: <?= ($showDiscount ? '' : 'none') ?>;">
                                         <?= ($showDiscount ? ($showMeasureRatio == 'Y' ? $price['PRINT_RATIO_BASE_PRICE'] : $price['PRINT_BASE_PRICE']) : '') ?>
                                </div>
                                <?
                            }
                            ?>
                            <div class="price">
                                <span id="<?= $itemIds['PRICE_ID'] ?>">
                                <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                                </span>
                                <span class="measure" id="<?=$itemIds['QUANTITY_MEASURE']?>">
                                    <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                        <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                                    <? else: ?>
                                        <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                                    <? endif; ?>
                                </span>
                        		<?
                        		if ($arParams['USE_PRICE_COUNT'])
                        		{
                        		    $showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
                        		    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
                        		    ?>
                        		    <div class="product-item-detail-info-container" <?= $showRanges ? '' : 'style="display: none;"' ?> data-entity="price-ranges-block">
                        		        
                        		        <dl class="product-item-detail-properties" data-entity="price-ranges-body">
                        		            <?
                        		            if ($showRanges)
                        		            {
                        		                foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
                        		                {
                        		                    if ($range['HASH'] !== 'ZERO-INF')
                        		                    {
                        		                        $itemPrice = false;

                        		                        foreach ($arResult['ITEM_PRICES'] as $itemPrice)
                        		                        {
                        		                            if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                        		                            {
                        		                                break;
                        		                            }
                        		                        }

                        		                        if ($itemPrice)
                        		                        {
                        		                            ?>
                        		                            <div>
                        		                            <dt>
                        		                                <?
                        		                                echo Loc::getMessage(
                        		                                        'CT_BCE_CATALOG_RANGE_FROM', array('#FROM#' => $range['SORT_FROM'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                        		                                ) . ' ';

                        		                                if (is_infinite($range['SORT_TO']))
                        		                                {
                        		                                    echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                        		                                }
                        		                                else
                        		                                {
                        		                                    echo Loc::getMessage(
                        		                                            'CT_BCE_CATALOG_RANGE_TO', array('#TO#' => $range['SORT_TO'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                        		                                    );
                        		                                }
                        		                                ?>
                        		                            </dt>
                        		                            <dd><?= ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) ?></dd>
                        		                            </div>
                        		                            <?
                        		                        }
                        		                    }
                        		                }
                        		            }
                        		            ?>
                        		        </dl>
                        		    </div>
                        		    <?
                        		    unset($showRanges, $useRatio, $itemPrice, $range);
                        		}
                        		?>
                            </div>

                            <?
                            if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                            {
                                ?>
                                <div class="saving" id="<?= $itemIds['DISCOUNT_PRICE_ID'] ?>"
                                     style="display: <?= ($showDiscount ? '' : 'none') ?>;">
                                    <?
                                    if ($showDiscount)
                                    {
                                        echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
                                    }
                                    ?>
                                </div>
                                <?
                            }
                            ?>
				
			</div>  
        
            <div class="product-timer colored animated" id="<?=$itemIds['DISCOUNT_TIMER_ID']?>">
                <div class="name"><?=GetMessage('CT_BCE_CATALOG_TIMER_TITLE')?></div>
                <div class="time">
                    <div class="item"><span data-entity="days">00</span><?=GetMessage('CT_BCE_CATALOG_TIMER_DAYS')?></div>
                    <div class="item"><span data-entity="hours">00</span><?=GetMessage('CT_BCE_CATALOG_TIMER_HOURS')?></div>
                    <div class="item"><span data-entity="minutes">00</span><?=GetMessage('CT_BCE_CATALOG_TIMER_MINUTES')?></div>
                    <div class="item" style="display: none;"><span data-entity="seconds">00</span><?=GetMessage('CT_BCE_CATALOG_TIMER_SECONDS')?></div>
                </div>
            </div>
        
        <div class="order-container<?=(!$haveOffers) ? ' no-offers' : ''?>">
                    
			<div class="buttons">
                            
                            <?
                            if ($arParams['USE_PRODUCT_QUANTITY'])
                            {
                                ?>
                                <div class="counter" <?= (!$actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?> data-entity="quantity-block">
                                    <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" class="minus icon-custom"></a>
                                    <input class="count" id="<?=$itemIds['QUANTITY_ID']?>" type="number" value="<?= $price['MIN_QUANTITY'] ?>">
                                    <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_UP_ID']?>" class="plus icon-custom"></a>
                                </div>
                                <?
                            }
                            ?>
                            
				
				<div class="product-item-button-container" data-entity="main-button-container">
                                    <div id="<?= $itemIds['BASKET_ACTIONS_ID'] ?>"<?=($actualItem['CAN_BUY'] ? '' : ' style="display:none";')?>>
                                        <?
                                        if ($showAddBtn)
                                        {
                                            ?>
                                            <a class="btn detail-buy-button" id="<?= $itemIds['ADD_BASKET_LINK'] ?>" href="javascript:void(0);"><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
                                            <?
                                        }

                                        if ($showBuyBtn)
                                        {
                                            ?>
                                                <a class="btn detail-buy-button" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0);"><?= $arParams['MESS_BTN_BUY'] ?></a>
                                            <?
                                        }
                                        ?>
                                        <? if (CSolution::$options['BUY1CLICK_ENABLED'] == "Y"): ?>        
                                        <a href="javascript:void(0);" id="<?= $itemIds['BUY_1_CLICK'] ?>" buy1click data-id="<?=$actualItem['ID']?>" data-qty="1" class="btn one-click white" <?if(!empty(CSolution::$options['CATALOG_ORDER_MIN_PRICE'])):?>style="display:none;"<?endif?>>
                                            <?=GetMessage('CT_BCE_BUY_1_CLICK')?>
                                        </a>
                                        <? endif; ?>
                                    </div>
                                    <?
                                    
                                    if ($showSubscribe):?>
                                        <div class="product-item-detail-info-container">
                                            <?
                                            $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.product.subscribe', 'main', array(
                                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                                'PRODUCT_ID' => $arResult['ID'],
                                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
                                                'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                                'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                    ), $component, array('HIDE_ICONS' => 'Y')
                                            );
                                            ?>
                                            
                                        </div>
                                    <?endif?>
				</div>
			</div>
		</div>
        
        <?if($showOrderForm):?>
            <a id="<?=$itemIds['ORDER_BTN']?>" style="<?=($actualItem['CAN_BUY']) ? 'display:none;' : ''?>" href="javascript:void(0)" class="btn detail-buy-button make-order"><?=Loc::getMessage('TPL_ORDER_FORM_BTN_TEXT')?></a>
        <?endif?>
    </div>
    </div>
</div>
