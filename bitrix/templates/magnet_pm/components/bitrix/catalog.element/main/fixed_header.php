<div class="fixed-product-header show-mobile" id="<?=$itemIds['SMALL_CARD_PANEL_ID']?>">
    <div class="container">
        <div class="fixed-product-header-container">
            <div class="logo">
                <? $APPLICATION->IncludeFile(SITE_DIR . 'include/logo.php'); ?>
            </div>
            <div class="product-container">
                <div class="img">
                    <? $arEmptyPhoto = \Nextype\Magnet\CSolution::getEmptyImage(); ?>
                    <img src="<?= $arEmptyPhoto['SRC'] ?>" alt="<?=$name?>" title="<?=$name?>" data-entity="panel-picture" />
                </div>
                <div class="info">
                    <div class="name"><?=$name?></div>
                    <? if ($arParams['DISPLAY_RATING'] === "Y"): ?>
                            <?
                            $APPLICATION->IncludeComponent(
                                    'nextype:magnet.iblock.vote', 'view', array(
                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                'ELEMENT_ID' => $arResult['ID'],
                                'ELEMENT_CODE' => '',
                                'MAX_VOTE' => '5',
                                'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                                'SET_STATUS_404' => 'N',
                                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                "DISPLAY_AS_RATING" => 'vote_avg',
                                'CACHE_TIME' => $arParams['CACHE_TIME']
                                    ), $component, array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                        <? endif; ?>
                    
                    <? \Nextype\Magnet\CSolution::ShowProductQuantity($arResult, $arParams, $itemIds, true);?>

                </div>
                <? if (!$haveOffers): ?>
                <div class="price-container">
                    
                    <?
                    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                    {
                        ?>
                        <div class="old-price" style="display: <?=($showDiscount ? '' : 'none')?>;" data-entity="panel-old-price">
                            <?=($showDiscount ? ($showMeasureRatio == 'Y' ? $price['PRINT_RATIO_BASE_PRICE']: $price['PRINT_BASE_PRICE']) : '')?>
                        </div>
                        <?
                    }
                    ?>
                    <div class="price" data-entity="panel-price">
                        <span>
                            <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                        </span>
                        <span class="measure">
                            <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? else: ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? endif; ?>
                        </span>
                    </div>
                    
                </div>
                <div class="btn-container">
                    
                        <?
                        if ($showAddBtn)
                        {
                            ?>
                            <a class="btn" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;" data-entity="panel-add-button" id="<?= $itemIds['ADD_BASKET_LINK'] ?>" href="javascript:void(0);"><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
                            <?
                        }

                        if ($showBuyBtn)
                        {
                            ?>
                            <a class="btn" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;" data-entity="panel-buy-button" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0);"><?= $arParams['MESS_BTN_BUY'] ?></a>
                            <?
                        }
                        
                        if ($showOrderForm && !$actualItem['CAN_BUY'])
                        {
                            ?>
                            <a class="btn" data-entity="panel-order-button" id="" href="javascript:void(0);" onclick="<?=$obName?>.openOrderForm()"><?=GetMessage('TPL_ORDER_FORM_BTN_TEXT')?></a>
                            <?
                        }
                        ?>
                        
                </div>
                <? else: ?>
                <div class="price-container">
                    
                    <?
                    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                    {
                        ?>
                        <div class="old-price" style="display: <?=($showDiscount ? '' : 'none')?>;" data-entity="panel-old-price">
                            <?=($showDiscount ? ($showMeasureRatio == 'Y' ? $price['PRINT_RATIO_BASE_PRICE'] : $price['PRINT_BASE_PRICE']) : '')?>
                        </div>
                        <?
                    }
                    ?>
                    <div class="price">
                        <span data-entity="panel-price">
                            <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                        </span>
                        <span class="measure" data-entity="panel-measure">
                            <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? else: ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? endif; ?>
                        </span>
                    </div>
                    
                </div>
                <div class="btn-container">
                    <a href="javascript:void(0)" data-entity="panel-show-more" class="btn"><?=GetMessage('CT_BCE_CATALOG_SMALLCARD_BTN_MORE')?></a>
                </div>
                <? endif; ?>
                <div class="additional-links">
                    <? if ($arParams['DISPLAY_COMPARE']): ?>
                    <a href="javascript:void(0);" data-entity="panel-compare" class="compare icon-custom"></a>
                    <? endif; ?>
                        
                    <? if ($arParams['DISPLAY_WISH_LIST'] === 'Y'): ?>
                    <a href="javascript:void(0);" data-entity="panel-wishlist" class="wishlist icon-custom"></a>
                    <? endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
