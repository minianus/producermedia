<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Nextype\Magnet\CSolution;

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";
?>
    <div class="product-main-info" itemscope itemtype="http://schema.org/Product" id="<?= $itemIds['ID'] ?>">
        <meta itemprop="name" content="<?= strip_tags($arResult['NAME']) ?>"/>
        <div class="product-main-container">
            <div class="product-img" id="<?= $itemIds['BIG_SLIDER_ID'] ?>" data-helper="product::images">
                <div class="labels <?= strtolower($arParams['LABEL_PROP_POSITION']) ?>"
                     id="<?= $itemIds['STICKER_ID'] ?>">

                    <?
                    if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE'])) {
                        foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value) {
                            ?>
                            <div class="label <?= strtolower($code) ?><?= (!isset($arParams['LABEL_PROP_MOBILE'][$code]) ? ' m-hidden' : '') ?>">
                                <?= $value ?>
                            </div>
                            <?
                        }
                    }
                    ?>

                    <?
                    if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
                        if ($haveOffers) {
                            ?>
                            <div class="label" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
                                 style="display: none;">
                            </div>
                            <?
                        } else {
                            if ($price['DISCOUNT'] > 0) {
                                ?>
                                <div class="label" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>">
                                    <?= -$price['PERCENT'] ?>%
                                </div>
                                <?
                            }
                        }
                    }
                    ?>

                </div>
                <div class="additional-links">
                    <? if ($arParams['DISPLAY_COMPARE']): ?>
                        <a href="javascript:void(0);" id="<?= $itemIds['COMPARE_LINK'] ?>"
                           class="compare icon-custom"></a>
                    <? endif; ?>

                    <? if ($arParams['DISPLAY_WISH_LIST'] === 'Y'): ?>
                        <a href="javascript:void(0);" id="<?= $itemIds['WISH_LIST_ID'] ?>"
                           class="wishlist icon-custom"></a>
                    <? endif; ?>

                </div>
                <div class="main-img" data-entity="images-container">
                    <?
                    if (!empty($actualItem['MORE_PHOTO'])) {
                        foreach ($actualItem['MORE_PHOTO'] as $key => $photo) {
                            ?>
                            <div <?= ($key == 0 ? 'class="active"' : '') ?> data-entity="image"
                                                                            data-id="<?= $photo['ID'] ?>">
                                <img src="<?= $photo['SRC'] ?>" alt="<?= $alt ?>"
                                     title="<?= $title ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
                            </div>
                            <?
                        }
                    }
                    ?>
                </div>

                <? if ($arResult["ITEM_PRICES"]["0"]["DISCOUNT"] == 0 && $arResult["OFFERS"][0]["PRICES"]["BASE"]["DISCOUNT_DIFF"] == 0) { ?>
                    <div class="add-bonus_catalog-elem">
                        <div class="bonus bonus-color">
                            <span class="bonus-icon"></span>
                            <span><?
                                if (isset($arResult["PRICE_MATRIX"]["MATRIX"][1])) {
                                    $ar_res = CCatalogProduct::GetByID($arResult["ORIGINAL_PARAMETERS"]["ELEMENT_ID"]);
                                    foreach ($arResult["PRICE_MATRIX"]["MATRIX"][1] as $key => $arRow) {
                                        if ($key == 0) {
                                            echo $arRow["PRICE"] - $ar_res["PURCHASING_PRICE"];

                                        }
                                    }
                                } else {
                                    echo $arResult["OFFERS"][0]["PRICES"]["BASE"]["VALUE"] - $arResult["OFFERS"][0]["CATALOG_PURCHASING_PRICE"];
                                }
                                ?></span>
                        </div>
                    </div>
                <? } ?>

                <?
                if ($showSliderControls) {
                    if ($haveOffers) {
                    foreach ($arResult['OFFERS'] as $keyOffer => $offer) {
                        if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0)
                            continue;

                        $strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
                        ?>
                        <div class="product-item-detail-slider-controls-block"
                             id="<?= $itemIds['SLIDER_CONT_OF_ID'] . $offer['ID'] ?>"
                             style="display: <?= $strVisible ?>;">
                            <div class="thumbnails">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?
                                        foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo) {
                                            ?>
                                            <a href="javascript:void(0)"
                                               class="swiper-slide preview<?= ($keyPhoto == 0 ? ' active' : '') ?>"
                                               data-entity="slider-control"
                                               data-value="<?= $offer['ID'] . '_' . $photo['ID'] ?>">
                                                <img src="<?= $photo['SRC'] ?>" alt="" title="">
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <script>
                            var tumbnailSwiperSlide = new Swiper('#<?= $itemIds['SLIDER_CONT_OF_ID'] . $offer['ID'] ?> .swiper-container', {
                                loop: false,
                                breakpoints: {
                                    0: {slidesPerView: 2},
                                    340: {slidesPerView: 3},
                                    405: {slidesPerView: 4},
                                    421: {slidesPerView: 3},
                                    480: {slidesPerView: 4},
                                    530: {slidesPerView: 5},
                                    610: {slidesPerView: 6},
                                    710: {slidesPerView: 7},
                                    825: {slidesPerView: 3},
                                    940: {slidesPerView: 4},
                                    1025: {slidesPerView: 3},
                                    1120: {slidesPerView: 4},
                                    1271: {slidesPerView: 3},
                                    1530: {slidesPerView: 4},
                                    1710: {slidesPerView: 5}
                                }
                            });
                        </script>
                    <?
                    }
                    }
                    else
                    {
                    ?>
                        <div class="thumbnails" id="<?= $itemIds['SLIDER_CONT_ID'] ?>">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">


                                    <?
                                    if (!empty($actualItem['MORE_PHOTO'])) {
                                        foreach ($actualItem['MORE_PHOTO'] as $key => $photo) {
                                            ?>
                                            <a href="javascript:void(0)"
                                               class="swiper-slide preview<?= ($key == 0 ? ' active' : '') ?>"
                                               data-entity="slider-control" data-value="<?= $photo['ID'] ?>">
                                                <img src="<?= $photo['SRC'] ?>" alt="" title="">
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <script>
                            let tumbnailSwiperSlide = new Swiper('#<?= $itemIds['SLIDER_CONT_ID'] ?> .swiper-container', {
                                loop: false,
                                breakpoints: {
                                    0: {slidesPerView: 2},
                                    340: {slidesPerView: 3},
                                    405: {slidesPerView: 4},
                                    421: {slidesPerView: 3},
                                    480: {slidesPerView: 4},
                                    530: {slidesPerView: 5},
                                    610: {slidesPerView: 6},
                                    710: {slidesPerView: 7},
                                    825: {slidesPerView: 3},
                                    940: {slidesPerView: 4},
                                    1025: {slidesPerView: 3},
                                    1120: {slidesPerView: 4},
                                    1271: {slidesPerView: 3},
                                    1530: {slidesPerView: 4},
                                    1710: {slidesPerView: 5}
                                }
                            });
                        </script>
                        <?
                    }
                }
                ?>


            </div>
            <div class="info">
                <div class="top">
                    <? if ($arParams['DISPLAY_RATING'] === "Y"): ?>
                        <?
                        $APPLICATION->IncludeComponent(
                            'nextype:magnet.iblock.vote', 'catalog', array(
                            'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                            'ELEMENT_ID' => $arResult['ID'],
                            'ELEMENT_CODE' => '',
                            'MAX_VOTE' => '5',
                            "DISPLAY_AS_RATING" => 'vote_avg',
                            'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                            'SET_STATUS_404' => 'N',
                            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                            'CACHE_TIME' => $arParams['CACHE_TIME']
                        ), $component, array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    <? endif; ?>

                    <? if ((!empty($arParams['ARTICLE_PROP']) && !empty($arResult['PROPERTIES'][$arParams['ARTICLE_PROP']]['VALUE']))
                        || $arResult['OFFERS_HAS_ARTICLE']): ?>
                        <div class="code" id="<?= $itemIds['ARTICLE_ID'] ?>">
                            <?= GetMessage('CT_BCE_CATALOG_ARTICLE', array(
                                '#VALUE#' => $arResult['PROPERTIES'][$arParams['ARTICLE_PROP']]['VALUE']
                            )); ?>
                        </div>
                    <? endif; ?>

                    <? $bCanBuy = CSolution::ShowProductQuantity($arResult, $arParams, $itemIds); ?>

                    <? if (!empty($arResult['BRAND'])): ?>
                        <div class="brand-img" data-helper="product::brand">
                            <? if (!empty($arResult['BRAND']['PREVIEW_PICTURE'])): ?>
                                <img src="<?= $arResult['BRAND']['PREVIEW_PICTURE'] ?>"
                                     alt="<?= $arResult['BRAND']['NAME'] ?>">
                            <? endif; ?>
                        </div>
                    <? endif; ?>
                </div>

                <? if ($arResult['SET_ITEMS']): ?>
                    <div class="set-items-container">
                        <div class="set-items">
                            <div class="title"><?= GetMessage('CT_BCE_SET_TITLE') ?></div>
                            <? foreach ($arResult['SET_ITEMS'] as $arSetItem): ?>
                                <div class="item">
                                    <a href="<?= $arSetItem['DETAIL_PAGE_URL'] ?>" class="img">
                                        <img src="<?= $arSetItem['PREVIEW_PICTURE']['SRC'] ?>"
                                             alt="<?= $arSetItem['PREVIEW_PICTURE']['ALT'] ?>"
                                             title="<?= $arSetItem['PREVIEW_PICTURE']['TITLE'] ?>"/>
                                    </a>
                                    <div class="right-block">
                                        <a href="<?= $arSetItem['DETAIL_PAGE_URL'] ?>" class="name">
                                            <?= $arSetItem['NAME'] ?>
                                        </a>
                                        <div class="price">
                                            <? if (!empty($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'])): ?>
                                                <? if ($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'] < $arSetItem['RESULT_PRICE']['BASE_PRICE']): ?>
                                                    <span><?= CurrencyFormat($arSetItem['RESULT_PRICE']['BASE_PRICE'], $arSetItem['RESULT_PRICE']['CURRENCY']) ?></span>
                                                <? endif; ?>

                                                <?= CurrencyFormat($arSetItem['RESULT_PRICE']['DISCOUNT_PRICE'], $arSetItem['RESULT_PRICE']['CURRENCY']) ?>
                                            <? endif; ?>
                                        </div>
                                        <div class="quantity">
                                            <?= !empty($arSetItem['MEASURE']) ? GetMessage('CT_BCE_SET_ITEM_MEASURE', array("#QUANTITY#" => $arSetItem['QUANTITY'], "#MEASURE#" => $arSetItem['MEASURE'])) : ""; ?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                        <div class="set-description">

                            <? if (!empty($price) || $haveOffers): ?>
                                <div class="price-container" itemprop="offers" itemscope
                                     itemtype="http://schema.org/Offer">
                                    <meta itemprop="price" content="<?= floatval($price['PRICE']) ?>"/>
                                    <meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>"/>
                                    <? if ($bCanBuy): ?>
                                        <link itemprop="availability" href="http://schema.org/InStock">
                                    <? endif; ?>

                                    <?
                                    if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                        ?>
                                        <div class="old-price" id="<?= $itemIds['OLD_PRICE_ID'] ?>"
                                            <?= ($showDiscount ? '' : 'style="display: none"') ?>>
                                            <?= ($showDiscount ? $price['PRINT_BASE_PRICE'] : '') ?>
                                        </div>
                                        <?
                                    }
                                    ?>

                                    <div class="price">
                                <span id="<?= $itemIds['PRICE_ID'] ?>">
                                <?= $price['PRINT_PRICE'] ?>
                                </span>
                                        <?
                                        if ($arParams['USE_PRICE_COUNT']) {
                                            $showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
                                            $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
                                            ?>
                                            <div class="product-item-detail-info-container" <?= $showRanges ? '' : 'style="display: none;"' ?>
                                                 data-entity="price-ranges-block">

                                                <dl class="product-item-detail-properties"
                                                    data-entity="price-ranges-body">
                                                    <?
                                                    if ($showRanges) {
                                                        foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range) {
                                                            if ($range['HASH'] !== 'ZERO-INF') {
                                                                $itemPrice = false;

                                                                foreach ($arResult['ITEM_PRICES'] as $itemPrice) {
                                                                    if ($itemPrice['QUANTITY_HASH'] === $range['HASH']) {
                                                                        break;
                                                                    }
                                                                }

                                                                if ($itemPrice) {
                                                                    ?>
                                                                    <div>
                                                                        <dt>
                                                                            <?
                                                                            echo Loc::getMessage(
                                                                                    'CT_BCE_CATALOG_RANGE_FROM', array('#FROM#' => $range['SORT_FROM'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                                                                                ) . ' ';

                                                                            if (is_infinite($range['SORT_TO'])) {
                                                                                echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                                                                            } else {
                                                                                echo Loc::getMessage(
                                                                                    'CT_BCE_CATALOG_RANGE_TO', array('#TO#' => $range['SORT_TO'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                                                                                );
                                                                            }
                                                                            ?>
                                                                        </dt>
                                                                        <dd><?= ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) ?></dd>
                                                                    </div>
                                                                    <?
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </dl>
                                            </div>
                                            <?
                                            unset($showRanges, $useRatio, $itemPrice, $range);
                                        }
                                        ?>
                                    </div>

                                    <?
                                    if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                        ?>
                                        <div class="saving" id="<?= $itemIds['DISCOUNT_PRICE_ID'] ?>"
                                            <?= ($showDiscount ? '' : 'style="display:none"'); ?>>
                                            <?
                                            if ($showDiscount) {
                                                echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
                                            }
                                            ?>
                                        </div>
                                        <?
                                    }
                                    ?>

                                    <div class="product-timer colored animated"
                                         id="<?= $itemIds['DISCOUNT_TIMER_ID'] ?>">
                                        <div class="name"><?= GetMessage('CT_BCE_CATALOG_TIMER_TITLE') ?></div>
                                        <div class="time">
                                            <div class="item"><span
                                                        data-entity="days">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_DAYS') ?>
                                            </div>
                                            <div class="item"><span
                                                        data-entity="hours">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_HOURS') ?>
                                            </div>
                                            <div class="item"><span
                                                        data-entity="minutes">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_MINUTES') ?>
                                            </div>
                                            <div class="item" style="display: none;"><span
                                                        data-entity="seconds">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_SECONDS') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <? endif; ?>

                            <div class="text" itemprop="description" data-helper="product::top-desc">
                                <?= $arResult['PREVIEW_TEXT'] ?>
                            </div>

                            <? if (!empty($arResult['DETAIL_TEXT'])): ?>
                                <a href="javascript:void(0)"
                                   class="more read-detail-text"><?= GetMessage('CT_BCE_GOTO_DETAIL_TEXT') ?></a>
                            <? endif; ?>

                            <? if (!empty($arResult['SHORT_PROPERTIES_LIST'])): ?>
                                <div class="characteristics" data-helper="product::top-properties">
                                    <table>
                                        <tbody>
                                        <? foreach ($arResult['SHORT_PROPERTIES_LIST'] as $arProp): ?>
                                            <tr>
                                                <td class="type"><span><?= $arProp['NAME'] ?></span></td>
                                                <td class="value"><?= $arProp['VALUE'] ?></td>
                                            </tr>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>

                <? else: ?>
                    <div class="main">
                        <? if (!empty($price) || $haveOffers): ?>
                            <div class="price-container" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <meta itemprop="price" content="<?= floatval($price['PRICE']) ?>"/>
                                <meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>"/>
                                <? if ($bCanBuy): ?>
                                    <link itemprop="availability" href="http://schema.org/InStock">
                                <? endif; ?>

                                <?
                                if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                    ?>
                                    <div class="old-price" id="<?= $itemIds['OLD_PRICE_ID'] ?>"
                                        <?= ($showDiscount ? '' : 'style="display: none"') ?>>
                                        <?= ($showDiscount ? ($showMeasureRatio == 'Y' ? $price['PRINT_RATIO_BASE_PRICE'] : $price['PRINT_BASE_PRICE']) : '') ?>
                                    </div>
                                    <?
                                }
                                ?>
                                <div class="price">
                                <span id="<?= $itemIds['PRICE_ID'] ?>">
                                <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                                </span>
                                    <span class="measure" id="<?= $itemIds['QUANTITY_MEASURE'] ?>">
                                    <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                        <?= !empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : '' ?>
                                    <? else: ?>
                                        <?= !empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : '' ?>
                                    <? endif; ?>
                                </span>
                                    <?
                                    if ($arParams['USE_PRICE_COUNT']) {
                                        $showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
                                        $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
                                        ?>
                                        <div class="product-item-detail-info-container" <?= $showRanges ? '' : 'style="display: none;"' ?>
                                             data-entity="price-ranges-block">

                                            <dl class="product-item-detail-properties" data-entity="price-ranges-body">
                                                <?
                                                if ($showRanges) {
                                                    foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range) {
                                                        if ($range['HASH'] !== 'ZERO-INF') {
                                                            $itemPrice = false;

                                                            foreach ($arResult['ITEM_PRICES'] as $itemPrice) {
                                                                if ($itemPrice['QUANTITY_HASH'] === $range['HASH']) {
                                                                    break;
                                                                }
                                                            }

                                                            if ($itemPrice) {
                                                                ?>
                                                                <div>
                                                                    <dt>
                                                                        <?
                                                                        echo Loc::getMessage(
                                                                                'CT_BCE_CATALOG_RANGE_FROM', array('#FROM#' => $range['SORT_FROM'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                                                                            ) . ' ';

                                                                        if (is_infinite($range['SORT_TO'])) {
                                                                            echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                                                                        } else {
                                                                            echo Loc::getMessage(
                                                                                'CT_BCE_CATALOG_RANGE_TO', array('#TO#' => $range['SORT_TO'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                                                                            );
                                                                        }
                                                                        ?>
                                                                    </dt>
                                                                    <dd><?= ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) ?></dd>
                                                                </div>
                                                                <?
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                            </dl>
                                        </div>
                                        <?
                                        unset($showRanges, $useRatio, $itemPrice, $range);
                                    }
                                    ?>
                                </div>

                                <?
                                if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                    ?>
                                    <div class="saving" id="<?= $itemIds['DISCOUNT_PRICE_ID'] ?>"
                                        <?= ($showDiscount ? '' : 'style="display:none"'); ?>>
                                        <?
                                        if ($showDiscount) {
                                            echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' =>
                                                $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_DISCOUNT'] : $price['PRINT_DISCOUNT']));
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                                ?>

                                <div class="product-timer colored animated" id="<?= $itemIds['DISCOUNT_TIMER_ID'] ?>">
                                    <div class="name"><?= GetMessage('CT_BCE_CATALOG_TIMER_TITLE') ?></div>
                                    <div class="time">
                                        <div class="item"><span
                                                    data-entity="days">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_DAYS') ?>
                                        </div>
                                        <div class="item"><span
                                                    data-entity="hours">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_HOURS') ?>
                                        </div>
                                        <div class="item"><span
                                                    data-entity="minutes">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_MINUTES') ?>
                                        </div>
                                        <div class="item" style="display: none;"><span
                                                    data-entity="seconds">00</span><?= GetMessage('CT_BCE_CATALOG_TIMER_SECONDS') ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <? endif; ?>

                        <div class="desc">
                            <div class="text" itemprop="description" data-helper="product::top-desc">
                                <?= $arResult['PREVIEW_TEXT'] ?>
                            </div>

                            <? if (!empty($arResult['DETAIL_TEXT'])): ?>
                                <a href="javascript:void(0)"
                                   class="more read-detail-text"><?= GetMessage('CT_BCE_GOTO_DETAIL_TEXT') ?></a>
                            <? endif; ?>

                            <? if (!empty($arResult['SHORT_PROPERTIES_LIST'])): ?>
                                <div class="characteristics" data-helper="product::top-properties">
                                    <table>
                                        <tbody>
                                        <? foreach ($arResult['SHORT_PROPERTIES_LIST'] as $arProp): ?>
                                            <tr>
                                                <td class="type"><span><?= $arProp['NAME'] ?></span></td>
                                                <td class="value"><?= $arProp['VALUE'] ?></td>
                                            </tr>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <? endif; ?>


                        </div>
                    </div>
                <? endif; ?>

                <div class="order-container<?= (!$haveOffers) ? ' no-offers' : '' ?>">
                    <?
                    if ($haveOffers && !empty($arResult['OFFERS_PROP'])) {
                        ?>
                        <div class="details">
                            <div>


                                <div id="<?= $itemIds['TREE_ID'] ?>">
                                    <?
                                    foreach ($arResult['SKU_PROPS'] as $skuProperty) {
                                        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                            continue;

                                        $propertyId = $skuProperty['ID'];
                                        $skuProps[] = array(
                                            'ID' => $propertyId,
                                            'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                            'VALUES' => $skuProperty['VALUES'],
                                            'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                                        );
                                        ?>
                                        <div class="product-item-info-container product-item-hidden"
                                             data-entity="sku-line-block">

                                            <div class="product-item-scu-container">
                                                <? if ($arParams['OFFERS_HIDE_TITLE'] != "Y"): ?>
                                                    <div class="product-item-scu-name"><?= htmlspecialcharsEx($skuProperty['NAME']) ?></div>
                                                <? endif; ?>
                                                <div class="product-item-scu-block">
                                                    <div class="product-item-scu-list">
                                                        <ul class="product-item-scu-item-list">
                                                            <?
                                                            foreach ($skuProperty['VALUES'] as &$value) {
                                                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                if ($skuProperty['SHOW_MODE'] === 'PICT') {
                                                                    ?>
                                                                    <li class="product-item-scu-item-color-container"
                                                                        title="<?= $value['NAME'] ?>"
                                                                        data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                        data-onevalue="<?= $value['ID'] ?>">
                                                                        <div class="product-item-scu-item-color-block">
                                                                            <div class="product-item-scu-item-color"
                                                                                 title="<?= $value['NAME'] ?>"
                                                                                 <? if (!empty($value['PICT']['SRC'])): ?>style="background-image: url('<?= $value['PICT']['SRC'] ?>');"<?endif;
                                                                            ?>>
                                                                                <? if (empty($value['PICT']['SRC'])): ?>
                                                                                    <span class="scu-item-color-title"><?= $value['NAME'] ?></span>
                                                                                <? endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <?
                                                                } else {
                                                                    ?>
                                                                    <li class="product-item-scu-item-text-container"
                                                                        title="<?= $value['NAME'] ?>"
                                                                        data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                        data-onevalue="<?= $value['ID'] ?>">
                                                                        <div class="product-item-scu-item-text-block">
                                                                            <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                                                        </div>
                                                                    </li>
                                                                    <?
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                        <?
                    }
                    ?>
                    <div class="buttons <?= ($showOrderForm) ? 'with-make-order' : '' ?>">

                        <?
                        if ($arParams['USE_PRODUCT_QUANTITY']) {
                            ?>
                            <div class="counter" <?= (!$actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>
                                 data-entity="quantity-block">
                                <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_DOWN_ID'] ?>"
                                   class="minus icon-custom"></a>
                                <input class="count" autocomplete="off" maxlength="8"
                                       id="<?= $itemIds['QUANTITY_ID'] ?>" type="tel"
                                       value="<?= $price['MIN_QUANTITY'] ?>">
                                <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_UP_ID'] ?>"
                                   class="plus icon-custom"></a>
                            </div>
                            <script data-skip-moving="true">
                                document.querySelector('#<?=$itemIds['QUANTITY_ID']?>').addEventListener('input', function () {
                                    this.style.width = 0;
                                    this.style.width = this.scrollWidth + 'px';
                                    if (parseInt(this.style.width) < 22)
                                        this.style.width = '22px';

                                });
                            </script>
                            <?
                        }
                        ?>


                        <div class="product-item-button-container" data-entity="main-button-container">
                            <div id="<?= $itemIds['BASKET_ACTIONS_ID'] ?>"<?= ($actualItem['CAN_BUY'] ? '' : ' style="display:none";') ?>>
                                <?
                                if ($showAddBtn) {
                                    ?>
                                    <a class="btn detail-buy-button" id="<?= $itemIds['ADD_BASKET_LINK'] ?>"
                                       href="javascript:void(0);"><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
                                    <?
                                }

                                if ($showBuyBtn) {
                                    ?>
                                    <a class="btn detail-buy-button" id="<?= $itemIds['BUY_LINK'] ?>"
                                       href="javascript:void(0);"><?= $arParams['MESS_BTN_BUY'] ?></a>
                                    <?
                                }
                                ?>
                                <? if (CSolution::$options['BUY1CLICK_ENABLED'] == "Y"): ?>
                                    <a href="javascript:void(0);" id="<?= $itemIds['BUY_1_CLICK'] ?>" buy1click
                                       data-id="<?= $actualItem['ID'] ?>" data-qty="1" class="btn one-click"
                                       <? if (!empty(CSolution::$options['CATALOG_ORDER_MIN_PRICE'])): ?>style="display:none;"<? endif ?>>
                                        <?= GetMessage('CT_BCE_BUY_1_CLICK') ?>
                                    </a>
                                <? endif; ?>
                            </div>
                            <?
                            if ($showSubscribe):?>
                                <div class="product-item-detail-info-container">
                                    <?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:catalog.product.subscribe', 'main', array(
                                        'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                        'PRODUCT_ID' => $arResult['ID'],
                                        'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                        'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
                                        'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                    ), $component, array('HIDE_ICONS' => 'Y')
                                    );
                                    ?>
                                </div>
                            <? endif ?>
                        </div>

                        <? if ($showOrderForm): ?>
                            <div class="make-order-container" <?= ($bCanBuy) ? 'style="display:none;"' : '' ?>>
                                <a href="javascript:void(0)" class="btn detail-buy-button make-order"
                                   id="<?= $itemIds['ORDER_BTN'] ?>">
                                    <?= Loc::getMessage('TPL_ORDER_FORM_BTN_TEXT') ?>
                                </a>
                                <div class="order-text"><?= Loc::getMessage('TPL_ORDER_FORM_ADDITIONAL_TEXT_INFO') ?></div>
                            </div>
                        <? endif ?>

                        <? if (CSolution::$options['CATALOG_DELIVERY_ENABLED'] == "Y"): ?>
                            <div class="delivery-link" data-helper="product::delivery">
                                <div class="text" id="catalog_delivery_text">
                                    <?= GetMessage('CT_BCE_CATALOG_DELIVERY_CALCULATING') ?><span></span>
                                </div>
                                <a style="opacity:0;" href="javascript:void(0)" id="catalog_delivery_btn"
                                   class="link"><?= GetMessage('CT_BCE_CATALOG_DELIVERY_MORE') ?></a>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="bottom">
                    <div class="left">

                        <div class="sale" data-helper="product::sale" id="<?= $itemIds['ACTIONS_ID'] ?>"
                             style="display: none"></div>

                        <div class="gifts-tip" id="product-gifts-tip">
                            <div class="name"><?= GetMessage('CT_BCE_CATALOG_GIFTS_TIPS_TITLE') ?></div>
                            <div class="text"><?= GetMessage('CT_BCE_CATALOG_GIFTS_TIPS_DESCR') ?></div>
                            <a href="javascript:void(0);"
                               class="link"><?= GetMessage('CT_BCE_CATALOG_GIFTS_TIPS_LINK') ?></a>
                        </div>
                    </div>
                    <div class="right">
                        <? $APPLICATION->IncludeFile(SITE_DIR . 'include/share_buttons.php'); ?>
                    </div>
                </div>


            </div>
        </div>
        <?
        if ($haveOffers) {
            if ($arResult['OFFER_GROUP']) {
                foreach ($arResult['OFFER_GROUP_VALUES'] as $offerId) {
                    ?>
                    <span id="<?= $itemIds['OFFER_GROUP'] . $offerId ?>" style="display: none;">
                    <?
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.set.constructor', 'main', array(
                        'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                        'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
                        'ELEMENT_ID' => $offerId,
                        'PRICE_CODE' => $arParams['PRICE_CODE'],
                        'BASKET_URL' => $arParams['BASKET_URL'],
                        'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                        'CACHE_TIME' => $arParams['CACHE_TIME'],
                        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                        'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID']
                    ), $component, array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                                            </span>
                    <?
                }
            }
        } else {
            if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP']) {
                $APPLICATION->IncludeComponent(
                    'bitrix:catalog.set.constructor', 'main', array(
                    'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'ELEMENT_ID' => $arResult['ID'],
                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                    'BASKET_URL' => $arParams['BASKET_URL'],
                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                    'CACHE_TIME' => $arParams['CACHE_TIME'],
                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID']
                ), $component, array('HIDE_ICONS' => 'Y')
                );
            }
        }
        ?>

    </div>


<? $this->SetViewTarget('catalog_element_tabs_content'); ?>
    <!-- Element Tabs Content -->
<? if (!empty($arResult['DETAIL_TEXT']) || !empty($arResult['DOCUMENTS']) || !empty($arResult['VIDEOS'])): ?>
    <div class="tab-content">
        <!-- full_description in buffer -->
        <? if (!empty($arResult['DETAIL_TEXT'])): ?>
            <main class="description" data-helper="product::full-desc">
                <? if ($arParams['DETAIL_SHOW_TABS'] == "Y"): ?>
                    <h3 class="product-subtitle"><?= GetMessage('CT_BCE_CATALOG_ONE_PAGE_DESC') ?></h3>
                <? endif; ?>
                <?= $arResult['DETAIL_TEXT'] ?>
            </main>
        <? endif; ?>

        <? if (!empty($arResult['VIDEOS'])): ?>
            <div class="videos">
                <h3 class="name"><?= GetMessage('CT_BCE_CATALOG_VIDEOS') ?></h3>
                <div class="items" data-helper="product::videos">
                    <? foreach ($arResult['VIDEOS'] as $key => $code): ?>
                        <div class="item lazy-video" data-loader="videoLoader"
                             data-video='<?= str_replace("'", '"', $code) ?>'></div>
                    <? endforeach; ?>
                </div>
            </div>
        <? endif; ?>

        <? if (!empty($arResult['DOCUMENTS'])): ?>
            <div class="documents">
                <h3 class="name"><?= GetMessage('CT_BCE_CATALOG_DOCUMENTS') ?></h3>
                <div class="items" data-helper="product::docs">
                    <? foreach ($arResult['DOCUMENTS'] as $arDocument): ?>
                        <div class="item <?= $arDocument['EXTENSION'] ?>">
                            <a href="<?= $arDocument['PATH'] ?>" target="_blank" class="file-name"
                               rel="nofollow"><?= !empty($arDocument['DESCRIPTION']) ? $arDocument['DESCRIPTION'] : $arDocument['NAME'] ?></a>
                            <div class="size"><?= $arDocument['FILE_SIZE'] ?></div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        <? endif; ?>
        <!-- full_description props in buffer -->
    </div>
<? endif; ?>

<? if (!empty($arResult['TAB_PROPERTIES'])): ?>
    <div class="tab-content">
        <!-- props in buffer -->
        <div class="characteristics" data-helper="product::full-props">
            <? if ($arParams['DETAIL_HIDE_TABS'] == 'Y'): ?>
                <h3 class="product-subtitle"><?= GetMessage('CT_BCE_CATALOG_ONE_PAGE_CHARACTERISTICS') ?></h3>
            <? endif; ?>
            <?
            if (\CModule::IncludeModule('yenisite.infoblockpropsplus')):
                $APPLICATION->IncludeComponent('yenisite:ipep.props_groups', '.default', array('DISPLAY_PROPERTIES' => $arResult['DISPLAY_PROPERTIES'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']));
            else:
                ?>

                <? if ($arResult['PROPERTIES_HAS_FILTER'] == "Y"): ?>
                <form method="get" action="<?= $arResult['SECTION']['SECTION_PAGE_URL'] ?>" target="_blank">
            <? endif; ?>
                <table>
                    <tbody>
                    <? foreach ($arResult['TAB_PROPERTIES'] as $arProp): ?>
                        <tr>
                            <td class="type<?= $arResult['PROPERTIES_HAS_FILTER'] == "Y" && empty($arProp['FILTER_INPUT_NAME']) ? ' no-filter' : '' ?>">
                        <span>
                        <? if (!empty($arProp['FILTER_INPUT_NAME'])): ?>
                            <input type="checkbox" data-role="product-prop-filter"
                                   id="prop_filter_<?= $arProp['FILTER_INPUT_NAME'] ?>" value="Y"
                                   name="<?= $arProp['FILTER_INPUT_NAME'] ?>"/>
                            <label for="prop_filter_<?= $arProp['FILTER_INPUT_NAME'] ?>"><?= $arProp['NAME'] ?></label>
                        <? else: ?>
                            <?= $arProp['NAME'] ?>
                        <? endif; ?>
                        </span>
                            </td>
                            <td class="value">
                                <?= $arProp['VALUE'] ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
                <? if ($arResult['PROPERTIES_HAS_FILTER'] == "Y"): ?>
                <div class="filter-action">
                    <div class="text">
                        <?= GetMessage('CT_BCE_CATALOG_PROPERTIES_SIMILAR_FILTER_TEXT') ?>
                    </div>
                    <div class="buttons">
                        <div class="count"><?= GetMessage('CT_BCE_CATALOG_PROPERTIES_SIMILAR_FILTER_SELECTED') ?><span
                                    data-role="product-prop-counter">0</span></div>
                        <button type="submit" class="btn" name="set_filter"
                                value="Y"><?= GetMessage('CT_BCE_CATALOG_PROPERTIES_SIMILAR_FILTER') ?></button>
                    </div>
                </div>
                </form>
            <? endif; ?>
            <? endif; ?>
        </div>
        <!-- end props in buffer -->
    </div>
<? endif; ?>
    <!-- END Element Tabs Content -->
<? $this->EndViewTarget(); ?>
<? $this->SetViewTarget('catalog_element_tabs_title'); ?>
    <!-- Element Tabs Titles -->
<? if (!empty($arResult['DETAIL_TEXT']) || !empty($arResult['DOCUMENTS']) || !empty($arResult['VIDEOS'])): ?>
    <a href="javascript:void(0)" class="tab" tab-detail-text><?= $arParams['MESS_DESCRIPTION_TAB'] ?></a>
<? endif; ?>

<? if (!empty($arResult['TAB_PROPERTIES'])): ?>
    <a href="javascript:void(0)" class="tab"><?= $arParams['MESS_PROPERTIES_TAB'] ?></a>
<? endif; ?>
    <!-- END Element Tabs Titles -->
<? $this->EndViewTarget(); ?>


<?
if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale')) {
    $this->SetViewTarget('catalog_element_tabs_gifts');

    CBitrixComponent::includeComponentClass('bitrix:sale.products.gift');
    $APPLICATION->IncludeComponent(
        'bitrix:sale.products.gift', 'main', array(
        'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
        'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
        'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
        'PRODUCT_ROW_VARIANTS' => "",
        'PAGE_ELEMENT_COUNT' => 0,
        'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
        'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
        'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
        'PRODUCT_DISPLAY_MODE' => 'Y',
        'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],
        'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
        'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
        'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
        'TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
        'LABEL_PROP_' . $arParams['IBLOCK_ID'] => array(),
        'LABEL_PROP_MOBILE_' . $arParams['IBLOCK_ID'] => array(),
        'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
        'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
        'MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
        'MESS_BTN_ADD_TO_BASKET' => $arParams['~GIFTS_MESS_BTN_BUY'],
        'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
        'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
        'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
        'SHOW_PRODUCTS_' . $arParams['IBLOCK_ID'] => 'Y',
        'PROPERTY_CODE_' . $arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE'],
        'PROPERTY_CODE_MOBILE' . $arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE_MOBILE'],
        'PROPERTY_CODE_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
        'OFFER_TREE_PROPS_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
        'CART_PROPERTIES_' . $arResult['OFFERS_IBLOCK'] => $arParams['OFFERS_CART_PROPERTIES'],
        'ADDITIONAL_PICT_PROP_' . $arParams['IBLOCK_ID'] => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
        'ADDITIONAL_PICT_PROP_' . $arResult['OFFERS_IBLOCK'] => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),
        'HIDE_NOT_AVAILABLE' => 'Y',
        'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'PRICE_CODE' => $arParams['PRICE_CODE'],
        'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
        'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'BASKET_URL' => $arParams['BASKET_URL'],
        'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
        'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
        'PARTIAL_PRODUCT_PROPERTIES' => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
        'USE_PRODUCT_QUANTITY' => 'N',
        'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
        'GIFTS_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
        'POTENTIAL_PRODUCT_TO_BUY' => array(
            'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
            'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
            'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
            'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
            'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,
            'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'] : null,
            'SECTION' => array(
                'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
                'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
                'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
                'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
            ),
        ),
        'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
        'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
        'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
    ), $component, array('HIDE_ICONS' => 'Y')
    );
    $this->EndViewTarget();
}
?>

<? $this->SetViewTarget('catalog_element_brand'); ?>
<? if (!empty($arResult['BRAND'])): ?>
    <div class="brand-info" data-helper="product::brand-info">
        <? if (!empty($arResult['BRAND']['PREVIEW_PICTURE'])): ?>
            <div class="img">
                <img src="<?= $arResult['BRAND']['PREVIEW_PICTURE'] ?>" alt="<?= $arResult['BRAND']['NAME'] ?>">
            </div>
        <? endif; ?>
        <div class="desc">
            <?= $arResult['BRAND']['PREVIEW_TEXT'] ?>
        </div>
        <ul class="links">
            <? if (!empty($arResult['SECTION']['SECTION_PAGE_URL'])): ?>
                <li class="item">
                    <a href="<?= $arResult['SECTION']['SECTION_PAGE_URL'] ?>"><?= GetMessage('CT_BCE_ALL_PRODUCTS_BY_SECTION') ?></a>
                </li>
            <? endif; ?>
            <li class="item">
                <a href="<?= $arResult['BRAND']['DETAIL_PAGE_URL'] ?>"><?= GetMessage('CT_BCE_ALL_PRODUCTS_BY_BRAND', array("#NAME#" => $arResult['BRAND']['NAME'])) ?></a>
            </li>
        </ul>
    </div>
<? endif; ?>
<? $this->EndViewTarget(); ?>