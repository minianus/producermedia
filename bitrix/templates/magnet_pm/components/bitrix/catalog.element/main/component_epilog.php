<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Nextype\Magnet\CSolution;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;


if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;

	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

if (isset($templateData['JS_OBJ']))
{
	?>
	<script>
		BX.ready(BX.defer(function(){
			if (!!window.<?=$templateData['JS_OBJ']?>)
			{
				window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);
			}
		}));
	</script>

	<?
	// check compared state
	if ($arParams['DISPLAY_COMPARE'])
	{
		$compared = false;
		$comparedIds = array();
		$item = $templateData['ITEM'];

		if (!empty($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]))
		{
			if (!empty($item['JS_OFFERS']))
			{
				foreach ($item['JS_OFFERS'] as $key => $offer)
				{
					if (is_array($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']) && array_key_exists($offer['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
					{
						if ($key == $item['OFFERS_SELECTED'])
						{
							$compared = true;
						}

						$comparedIds[] = $offer['ID'];
					}
				}
			}
			elseif (is_array($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']) && array_key_exists($item['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
			{
				$compared = true;
			}
		}

		if ($templateData['JS_OBJ'])
		{
			?>
			<script>
				BX.ready(BX.defer(function(){
					if (!!window.<?=$templateData['JS_OBJ']?>)
					{
						window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');

						<? if (!empty($comparedIds)): ?>
						window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
						<? endif ?>
					}
				}));
			</script>
			<?
		}
	}
        
        // check wishlist state
        if ($arParams['DISPLAY_WISH_LIST'])
        {
            if (\Bitrix\Main\Loader::includeModule('nextype.magnet') )
            {
                $inwishlist = false;
                $wishedIds = array();
                $item = $templateData['ITEM'];
                $arWishList = CSolution::getWishList();

                if (!empty($arWishList))
                { ?> <script> if (typeof wishType === 'undefined') wishType = '';</script> <?
                    if (!empty($item['JS_OFFERS']))
			{
				foreach ($item['JS_OFFERS'] as $key => $offer)
				{
					if (isset($arWishList[$offer['ID']]))
					{
                                            $inwishlist = true;
                                            $wishedIds[] = $offer['ID'];
					}
				}
			}
                        elseif (isset($arWishList[$item['ID']]))
			{
				$inwishlist = true;
			}
                }
                
                if ($inwishlist)
                { ?>
                    <script>
                                BX.ready(BX.defer(function(){
                                    wishType = "<?=(!empty($item['JS_OFFERS'])) ? 'offer' : 'product';?>";
                                        if (!!window.<?=$templateData['JS_OBJ']?> && wishType == 'product')
                                        {
                                                window.<?=$templateData['JS_OBJ']?>.setWishList(true);
                                        }
                                <? if (!empty($wishedIds)): ?>
                                    window.<?=$templateData['JS_OBJ']?>.setWishInfo(<?=CUtil::PhpToJSObject($wishedIds, false, true)?>);
				<? endif ?>
                                }));
                        </script>

                <? }
            }
        }

	// select target offer
	$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$offerNum = false;
	$offerId = (int)$this->request->get('oid');
	$offerCode = $this->request->get('OFFER_CODE');

	if ($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS']))
	{
            $foundKey = array_search($offerId, $templateData['OFFER_IDS']);
            $offerNum = ($foundKey !== false) ? $templateData['OFFER_IDS'][$foundKey] : '';
	}
	elseif (!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES']))
	{
                $foundKey = array_search($offerCode, $templateData['OFFER_CODES']);
                $offerNum = ($foundKey !== false) ? $templateData['OFFER_CODES'][$foundKey] : '';
	}

	if (!empty($offerNum))
	{
		?>
		<script>
			BX.ready(function(){
				if (!!window.<?=$templateData['JS_OBJ']?>)
				{
                                    window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$foundKey?>);
                                        <? if ($arParams['DISPLAY_WISH_LIST'] && in_array($offerNum, $wishedIds)):?>
                                                window.<?=$templateData['JS_OBJ']?>.setWishList(true);
                                        <? endif; ?>
				}
			});
		</script>
		<?
	}
}
?>
                
                <?
                CSolution::getInstance(SITE_ID);
                if (CSolution::$options['CATALOG_ACTION_TIMER_ENABLED'] == "Y")
                {
                    //print_r($arResult);
                    $arDiscounts = CCatalogDiscount::GetDiscount($arResult['ID'], $arResult['IBLOCK_ID'], Array (), $USER->GetUserGroupArray(), "N", SITE_ID);
                    if (!empty($arDiscounts) && is_array($arDiscounts))
                    {
                        $arLastDiscount = false;
                        foreach ($arDiscounts as $arDiscount)
                        {
                            if ($arDiscount['ACTIVE'] == 'Y' && !empty($arDiscount['ACTIVE_TO']))
                            {
                                $arLastDiscount = $arDiscount;
                            }
                            
                            if ($arDiscount['LAST_DISCOUNT'] == 'Y')
                                break;
                        }
                        
                        if ($arLastDiscount)
                        {
                            $arLastDiscount['ACTIVE_TO_UNIXTIME'] = strtotime(ConvertDateTime($arLastDiscount['ACTIVE_TO'], "YYYY-MM-DD HH:MI:SS"));
                        }
                    }
                    if (!empty($arLastDiscount['ACTIVE_TO_UNIXTIME']) && $arLastDiscount['ACTIVE_TO_UNIXTIME'] > time())
                    {
                ?>
                
                <script>
                    
                    BX.ready(function(){
			if (!!window.<?=$templateData['JS_OBJ']?>)
			{
                            window.<?=$templateData['JS_OBJ']?>.setDiscountTimer('<?=date("Y-m-d H:i:s", $arLastDiscount['ACTIVE_TO_UNIXTIME'])?>');
			}
                    });
                
                </script>
                <? }
                }
                
                if ($arParams['ACTIONS_USE'] === 'Y' && !empty($arParams['ACTIONS_IBLOCK_ID']))
                {
                    $arActionsFilter = Array (
                        'IBLOCK_ID' => intval($arParams['ACTIONS_IBLOCK_ID']),
                        'PROPERTY_PRODUCTS' => $templateData['ITEM']['ID'],
                        'ACTIVE' => 'Y',
                        '<PROPERTY_DATE_START' => date("Y-m-d 00:00:00"),
                        '>PROPERTY_DATE_END' => date("Y-m-d 23:59:59"),
                    );
                    
                    $arActions = Array ();
                    $rsActions = CIBlockElement::GetList(Array(), $arActionsFilter, false, Array("nPageSize"=>3), Array ('NAME', 'PREVIEW_TEXT', 'DETAIL_PAGE_URL'));
                    while ($arAction = $rsActions->GetNext())
                    {
                        $arActions[] = $arAction;
                    }
                    
                    if (!empty($arActions))
                    {
                        ?>
                        <script>
                        BX.ready(function(){
                            if (!!window.<?=$templateData['JS_OBJ']?>)
                            {
                                window.<?=$templateData['JS_OBJ']?>.setActions(<?=CUtil::PhpToJSObject($arActions, false, true)?>);
                            }
                        });
                        </script>
                        <?
                    }
                }
                
                ?>

                <script>
                $(function() {
                    $("body").on('click', '.product-main-info .stock', function () {
                        var tabs = $(".product-detail .tabs");
                        if (tabs.length > 0) {
                            var offset = tabs.offset();
                            tabs.find('a[tab-store]').trigger('click');
                            $("html, body").stop().animate({scrollTop:offset.top - 50}, 500, 'swing');
                        }
                        $(".product-detail .tabs a[tab-store]").trigger('click');
                    });

                    $("body").on('click', '#product-gifts-tip .link', function () {
                        var tabs = $("#sale-products-gift-container");
                        if (tabs.length > 0) {
                            var offset = tabs.offset();
                            $("html, body").stop().animate({scrollTop:offset.top - 50}, 500, 'swing');
                        }
                    });

                    $("body").on('click', '.read-detail-text', function () {
                        var tabs = $(".product-detail .tabs");
                        if (tabs.length > 0) {
                            if (!tabs.parents('.product-detail').hasClass('one-page')) {
                                var offset = tabs.offset();
                                tabs.find('a[tab-detail-text]').trigger('click');
                                $("html, body").stop().animate({scrollTop:offset.top - 100}, 500, 'swing');
                            } else {
                                var offset = $(".product-detail .description").offset();
                                if (offset.top > 0)
                                    $("html, body").stop().animate({scrollTop:offset.top - 100}, 500, 'swing');
                            }
                        }
                        $(".product-detail .tabs a[tab-detail-text]").trigger('click');
                    });
                    $("body").on('change', '[data-role="product-prop-filter"]', function () {
                        var count = $(this).parents('table').find('input[data-role="product-prop-filter"]:checked').length;
                        $(this).parents('.characteristics').find('[data-role="product-prop-counter"]').html(count);
                    });
                    $('.lazy-video').lazy({
                        customLoaderName: function(element) {
                            element.load();
                        },
                        videoLoader: function(element) {
                            element.html(element.data("video"));
                        },

                        // loads with a five seconds delay
                        asyncLoader: function(element) {
                            setTimeout(function() {
                                element.load()
                            }, 5000);
                        },

                        // always fail
                        errorLoader: function(element) {
                            element.error();
                        }
                    });
                    
                    <? if (CSolution::$options['CATALOG_FLY_HEADER'] == "Y" && $_REQUEST['is_fast_view'] != "Y"): ?>
                    $("body").on('click', '[data-entity="panel-show-more"]', function (event) {
                        event.preventDefault();
                        var offset = $(".product-main-container .price-container").offset();
                        if (offset.top > 0)
                            $("html, body").stop().animate({scrollTop:offset.top - 100}, 500, 'swing');
                        return false;
                    });
                    
                    window.fixedProductHeader = $('.fixed-product-header');
                    window.offsetProductContainer = $('.product-main-info').offset();
                    if (window.fixedProductHeader.length > 0) {
                        $(window).scroll(function() {
                            if (window.pageYOffset > window.offsetProductContainer.top) {
                                window.fixedProductHeader.addClass('show');
                            } else {
                                window.fixedProductHeader.removeClass('show');
                            }
                        });
                    }
                    <? endif; ?>
                });
                </script>
                
<? if (CSolution::$options['CATALOG_DELIVERY_ENABLED'] == "Y" && $_REQUEST['is_fast_view'] != "Y" && $templateData['JS_OBJ']):
    if (CSolution::$options['CATALOG_DELIVERY_LOCATION_PRIORITY'] == "GEOIP" && CSolution::$options['LOCATIONS_GEO_ENABLED'] == "Y")
    {
        $arUserLocation = CSolution::$userLocationData;
        $cityName = $arUserLocation["CITY"];
        $locationCode = $arUserLocation["LOCATION_CODE"];
    }
    elseif (CSolution::$options['LOCATIONS_ENABLED'] == "Y")
    {
        $arCurrentRegion = \Nextype\Magnet\CLocations::getCurrentRegion();
        $cityName = $arCurrentRegion["NAME"];
        $locationCode = \Nextype\Magnet\CGeo::getLocationCodeByName($cityName);
    }
    ?>
    <script>
        BX.ready(() => {
            let textNode = BX("catalog_delivery_text"),
                btnNode = BX("catalog_delivery_btn");
            if (!(textNode && btnNode))
                return;
            
            if (!!window.<?=$templateData['JS_OBJ']?>)
            {
                window.<?=$templateData['JS_OBJ']?>.calculateDelivery("<?=$templateData['ITEM']['ID']?>", "<?=$locationCode?>", "<?=\Nextype\Magnet\CLocations::getCityGenitive($cityName)?>");
            }
        })
    </script>
<? endif; ?>
