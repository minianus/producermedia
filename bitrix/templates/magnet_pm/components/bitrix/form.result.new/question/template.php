<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CRecaptcha;

$CSolution = CSolution::getInstance(SITE_ID);

$sID = str_replace("NT_MAGNET_", "", $arResult['arForm']['SID']);
$arMessages = Array (
    'FORM_SUCCESS_TITLE' => (!empty(CSolution::$options['FORM_' . $sID . '_SUCCESS_TITLE'])) ? CSolution::$options['FORM_' . $sID . '_SUCCESS_TITLE'] : GetMessage('FORM_SUCCESS_TITLE'),
    'FORM_SUCCESS_TEXT' => (!empty(CSolution::$options['FORM_' . $sID . '_SUCCESS_TEXT'])) ? CSolution::$options['FORM_' . $sID . '_SUCCESS_TEXT'] : GetMessage('FORM_SUCCESS_TEXT'),
);
$strId = "form_" . randString(5);
?>

<div class="feedback" id="<?=$strId?>" data-helper="product::feedback">
        <div class="text">
            <div class="name"><?=$arResult["FORM_TITLE"]?></div>
            <? if (!empty($arResult["arForm"]['DESCRIPTION'])): ?>
            <div class="desc">
                <?=$arResult["arForm"]['DESCRIPTION']?>
            </div>
            <? endif; ?>
        </div>
    <div class="form">
        <?=$arResult['FORM_HEADER']?>
        <?=bitrix_sessid_post();?>
        
        <? if ($arResult["isFormErrors"] == "N" && $arResult["isFormNote"] == "Y"): ?>
                <div class="success-message">
                    <div class="title"><?=$arMessages['FORM_SUCCESS_TITLE']?></div>
                    <div class="text"><?=$arMessages['FORM_SUCCESS_TEXT']?></div>
                </div>
        <? endif; ?>
        
        <? if ($arResult["isFormErrors"] == "Y"): ?>
            <div class="errors-message">
                <?= $arResult["FORM_ERRORS_TEXT"]; ?>
            </div>
        <? endif; ?>
        
        <div class="column big">
            <? foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
                <?
                if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea')
                    echo $CSolution->formField($sid, $arQuestion, $arResult, true);
                ?>
            
            <? endforeach; ?>
        </div>
        <div class="column small">
            <? foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
                <? if ($sid == $arParams['AUTOCOMPLETE_FIELD_NAME'] || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea' || $sid == 'PRODUCT') continue; ?>
                <?=$CSolution->formField($sid, $arQuestion, $arResult, true)?>
            <? endforeach; ?>
        </div>

        <? foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
            <? if ($sid != 'PRODUCT') continue; ?>
            <?= str_replace('value=""', 'value="'.$arParams["PRODUCT_NAME"].'"', $arQuestion['HTML_CODE']); ?>
        <? endforeach; ?>

        <? if ($arResult['arForm']['USE_CAPTCHA'] == "Y"): ?>
            <? $arResult['CAPTCHA_FIELD'] = str_replace('name=', 'required name=', $arResult['CAPTCHA_FIELD']); ?>
            <div class="form-captcha <?=(CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?> <?=($GLOBALS['NT_PRIME_CRECAPTCHA_CHECK_ERROR'] === "Y") ? ' error' : ''?>">
                <div class="field">
                    <div class="control-label">
                        <? if ($CSolution::$options['RECAPTCHA_TYPE'] != 'scoring')
                                echo GetMessage('CAPTCHA_LABEL');
                        ?>
                        <span class="required">*</span></div>
                    <div class="captcha-content">
                    	<div class="image"><?= $arResult['CAPTCHA_IMAGE'] ?></div>
                    	<?= $arResult['CAPTCHA_FIELD'] ?>
                    </div>
                </div>
            </div>
            <?if ($CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                <input type="hidden" name="g-recaptcha-response" />
            <?endif?>
        <? endif; ?>
        
        <?if (CSolution::$options['FORM_POLICY'] == 'Y')
        {
            $APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"PATH" => SITE_DIR . "include/form_policy_checkbox.php",
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => "standard.php",
                                "FORM_ID" => $sID,
                                "CHECKED" => CSolution::$options['FORM_POLICY_CHECKED'],
                                "DETAIL_PAGE_URL" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PROCESSING_POLICY']),
			),
			false
		);
            
        }
        ?>
        
        <div class="buttons">
            <button type='submit' value="submit" name="web_form_submit" class="btn"
                <?if($CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && $CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                    onclick="window.CSolution.onSubmitReCaptcha(<?=(string)$arResult['arForm']['SID']?>)"
                <?endif?>
                    >
                <?=$arResult['arForm']['BUTTON']?>
            </button>
        </div>

    <?=$arResult["FORM_FOOTER"]?>
    </div>
</div>
<script>
    $('#<?=$strId?> [is-phone]').mask(window.arOptions['phone_mask']);
</script>