<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['SEND'] = "Отправить.";
$MESS ['CAPTCHA_LABEL'] = "Введите код с картинки";
$MESS ['FIELD_REQUIRED'] = "Обязательно для заполнения";

$MESS ['FORM_SUCCESS_TITLE'] = "Ваше сообщение принято!";
$MESS ['FORM_SUCCESS_TEXT'] = "Мы свяжемся с Вами в ближайшее рабочее время";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Вы были распознаны как робот. Пожалуйста, обновите страницу.";
?>
