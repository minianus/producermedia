<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution,
    Nextype\Magnet\CRecaptcha;

$CSolution = CSolution::getInstance(SITE_ID);
$sID = str_replace("NT_MAGNET_", "", $arResult['arForm']['SID']);
$arMessages = Array (
    'FORM_SUCCESS_TITLE' => (!empty($CSolution::$options['FORM_' . $sID . '_SUCCESS_TITLE'])) ? $CSolution::$options['FORM_' . $sID . '_SUCCESS_TITLE'] : GetMessage('FORM_SUCCESS_TITLE'),
    'FORM_SUCCESS_TEXT' => (!empty($CSolution::$options['FORM_' . $sID . '_SUCCESS_TEXT'])) ? $CSolution::$options['FORM_' . $sID . '_SUCCESS_TEXT'] : GetMessage('FORM_SUCCESS_TEXT'),
);

?>

<div class="form popup-dialog">
    <div class="popup-content">
                    
            <?=$arResult['FORM_HEADER']?>
            <?=bitrix_sessid_post();?>
            <? if ($arResult["isFormErrors"] == "N" && $arResult["isFormNote"] == "Y"): ?>
                <div class="popup-success">
                	<a href="javascript:void(0);" onclick="hidePopup(this); return false;" class="close-popup-icon pull-right icon-custom"></a>
                    <div class="title"><?=$arMessages['FORM_SUCCESS_TITLE']?></div>
                    <div class="text"><?=$arMessages['FORM_SUCCESS_TEXT']?></div>
                </div>
            
            <? else: ?>
        
            <div class="popup-header">
                <? if ($arResult["isFormTitle"]): ?>
                <div class="popup-title"><?=$arResult["FORM_TITLE"]?></div>
                <? endif; ?>
                <a href="javascript:void(0);" onclick="hidePopup(this); return false;" class="close-popup-icon pull-right icon-custom"></a>
            </div>
            
            <? if (!empty($arResult["arForm"]['DESCRIPTION'])): ?>
            <div class="popup-note">
                <?=$arResult["arForm"]['DESCRIPTION']?>
            </div>
            <? endif; ?>
            
            <div class="popup-body">
                
                <? foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
                <? if ($sid == $arParams['AUTOCOMPLETE_FIELD_NAME']) continue; ?>
                <div class="form-group" <?if (stripos(current($arQuestion['STRUCTURE'])['FIELD_PARAM'], 'hidden') !== false):?>style="display:none;"<?endif?>>
                    <div class="control-label"><?=$arQuestion['CAPTION']?>
                        <?if($arQuestion['REQUIRED'] == "Y"):?><span class="danger-color">*</span><?endif;?>
                    </div>
                    <?$html = $CSolution->formField($sid, $arQuestion, $arResult);
                    if (!empty($arParams['PRODUCT_NAME']) && $sid == 'PRODUCT')
                        $html = preg_replace('/value=\"(.*?)\"/', $arParams['PRODUCT_NAME'], $html);
                    echo $html; ?>
                </div>
                <? endforeach; ?>
                
                <? if ($arResult['arForm']['USE_CAPTCHA'] == "Y"):?>
                <? $arResult['CAPTCHA_FIELD'] = str_replace('name=', 'required name=', $arResult['CAPTCHA_FIELD']); ?>
                <div class="form-captcha <?=(CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?> <?=($GLOBALS['NT_MAGNET_CRECAPTCHA_CHECK_ERROR'] === "Y") ? ' error' : ''?>">
                    <div class="field">
                        <div class="control-label">
                        <? if ($CSolution::$options['RECAPTCHA_TYPE'] != 'scoring')
                                echo GetMessage('CAPTCHA_LABEL') . '<span class="danger-color">*</span>';
                        ?>
                        </div>
                        <div class="image"><?=$arResult['CAPTCHA_IMAGE']?></div>
                        <?=$arResult['CAPTCHA_FIELD']?>
                    </div>
                </div>
                    <?if ($CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                        <input type="hidden" name="g-recaptcha-response" />
                    <?endif?>
                <? endif; ?>
                
                <?if (CSolution::$options['FORM_POLICY'] == 'Y')
                {
                    $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                        "PATH" => SITE_DIR . "include/form_policy_checkbox.php",
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "",
                                        "AREA_FILE_RECURSIVE" => "Y",
                                        "EDIT_TEMPLATE" => "standard.php",
                                        "FORM_ID" => $sID,
                                        "CHECKED" => CSolution::$options['FORM_POLICY_CHECKED'],
                                        "DETAIL_PAGE_URL" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PROCESSING_POLICY']),
                                ),
                                false
                        );

                }
                ?>
            </div>
            <?=$CSolution->formAutocompleteFields($arParams, $arResult) ?>
            <div class="popup-footer">
                <button type='submit' value="submit" name="web_form_submit" class="btn btn-primary pull-left send-cv"
                    <?if($CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && $CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                        onclick="window.CSolution.onSubmitReCaptcha(<?=(string)$arResult['arForm']['SID']?>)"
                    <?endif?>
                        >
                        <?=$arResult['arForm']['BUTTON']?>
                </button>
            </div>
            <?if ($arResult["isFormErrors"] == "Y"):?>
                <div class="popup-errors">
                    <?=$arResult["FORM_ERRORS_TEXT"];?>
                </div>
            <?endif;?>
            <? endif; ?>
            <?=$arResult["FORM_FOOTER"]?>
        
    </div>
</div>
<script>
    function hidePopup(node)
    {
        let container = $(node).parents('.popup');
        if (container.length)
            container.jqmHide();
        else
            $('.popup').jqmHide();
    }
    $('[name="<?=$arResult['arForm']['SID']?>"] [is-phone]').mask(window.arOptions['phone_mask']);
</script>