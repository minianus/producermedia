<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";
$MESS ['SEND'] = "Send";
$MESS ['CAPTCHA_LABEL'] = "Enter the code from the image";
$MESS ['FIELD_REQUIRED'] = "Required";

$MESS ['FORM_SUCCESS_TITLE'] = "Your message has been accepted!";
$MESS ['FORM_SUCCESS_TEXT'] = "We will contact you shortly.";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Captcha check fail. Please, refresh this page.";
?>