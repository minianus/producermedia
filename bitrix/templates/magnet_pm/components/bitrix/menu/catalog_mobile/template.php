<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="scroll">
    <ul class="list">
        <? foreach ($arResult as $key => $arItem): ?>
        <li class="item<?=!empty($arItem['SELECTED']) ? ' active' : '' ?><?=is_array($arItem['SUB']) ? ' has-sub' : '' ?>">
                <a href="<?= $arItem['LINK'] ?>"<?=!empty($arItem['SELECTED']) ? ' class="active"' : '' ?>>
                    <span class="text"><?= $arItem['TEXT'] ?></span>
                </a>
                <? if (is_array($arItem['SUB'])): ?>
                <div class="sub">
                        <div class="title"><?= $arItem['TEXT'] ?></div>
                        <ul class="items">
                        	<li class="item back">
                        		<a href="javascript:void(0);"><?=GetMessage('BACK_LINK');?></a>
                        	</li>
                            <? foreach ($arItem['SUB'] as $key2 => $arItemSub): ?>
                            <li class="item">
                                            <a href="<?=$arItemSub['LINK']?>"<?=!empty($arItemSub['SELECTED']) ? ' class="active"' : '' ?>>
                                                
                                                <span class="text"><?=$arItemSub['TEXT']?></span>
                                            </a>
                                        </li>
                            <? endforeach; ?>
                        </ul>

                      
                    </div>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
</div>