<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? if (!empty($arResult)): ?>
 <aside class="additional-menu">
<ul class="items">
    <? foreach ($arResult as $key => $arItem): ?>
    <li class="item<?=!empty($arItem['SELECTED']) ? ' active' : '' ?>"><a class="link" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
    <? endforeach; ?>
</ul>
 </aside>
<? endif; ?>
