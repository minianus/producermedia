<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="menu-left_personal">

        <div class="menu-left_personal--container">
            <div class="menu-left_personal--content">
                <div class="menu-left_personal--header">
                    <span style="margin-right: 3px;">Станьте миллионером</span>
                    <div class="tooltip">
                        <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M19.387 7.79a1 1 0 011.736-.991A10.456 10.456 0 0122.5 12c0 5.799-4.701 10.5-10.5 10.5S1.5 17.799 1.5 12 6.201 1.5 12 1.5c1.854 0 3.64.482 5.215 1.385a1 1 0 01-.994 1.735A8.456 8.456 0 0012 3.5a8.5 8.5 0 108.5 8.5 8.455 8.455 0 00-1.113-4.21zM12 10a1 1 0 011 1v5a1 1 0 11-2 0v-5a1 1 0 011-1zm1-2a1 1 0 10-2 0 1 1 0 002 0z" fill="#4917fa"></path>
                        </svg>
                        <span class="tooltiptext"><?= GetMessage('TOOLTIP_TEXT') ?></span>
                    </div>
                </div>
                <div class="menu-left_personal--bottom">
                    <a href="/">Подробнее</a>
                </div>
            </div>
        </div>



    <? if (!empty($arResult)): ?>
        <ul class="links">
            <? foreach ($arResult as $key => $arItem): ?>

            <?

            $sClassStyle = " ";
            if (isset($arItem["PARAMS"]["CLASS_STYLE"])) {
                $sClassStyle = $arItem["PARAMS"]["CLASS_STYLE"];
            }

            ?>

                <li<?= !empty($arItem['SELECTED']) ? ' class="active"' : '' ?>>
                    <a href="<?= $arItem['LINK'] ?>" class="<?=$sClassStyle;?>"><?= $arItem['TEXT'] ?></a>
                </li>
            <? endforeach; ?>
            <li class="exit">
                <a href="<?= $APPLICATION->GetCurPageParam('logout=yes&' . bitrix_sessid_get(), array('logout')) ?>" class="icon-custom"><?= GetMessage('EXIT_LINK') ?></a>
            </li>
        </ul>
    <? endif; ?>

</div>
