<script data-skip-moving="true">
var outerWidth = function (el) {
  var width = el.offsetWidth;
  var style = getComputedStyle(el);
  width += parseInt(style.marginLeft) + parseInt(style.marginRight);
  return width;
}
var mobileNavigationClone = false;
var funcResponsiveMobileNavigation = function () {
	if(document.documentElement.clientWidth < 1025) {
		var containerWidth = 0,
		    sumWidth = 0,
		    startRemove = 0,
		    items = [];
		var nItems = '';
		var container = document.getElementById('top-menu-mobile');
                
                if (container == null)
                    return;
    
		if (!mobileNavigationClone && container !== null)
		    mobileNavigationClone = container.cloneNode(true);

		container.classList.remove('calc');
		container.innerHTML = mobileNavigationClone.innerHTML;
		containerWidth = parseInt(container.offsetWidth);


		items = container.querySelectorAll('.list > .item');
		items.forEach(function(item, index) {
			if (containerWidth < sumWidth) {
		        if (!startRemove) {
		            startRemove = index - 1;
					var start = items[startRemove];
		            nItems += '<li class="item">' + start.innerHTML + '</li>';
		            start.parentNode.removeChild(start);
		        }
		        nItems += '<li class="item">' + item.innerHTML + '</li>';
		        item.parentNode.removeChild(item);
			} else {
				sumWidth += parseInt(outerWidth(item));
			}
		});

		if (nItems !== '') {
		    nItems = '<a href="javascript:void(0)">...</a><ul class="sub-menu level2">' + nItems + '</ul>';
		    var li = document.createElement('li');
		    li.classList.add('item','more');
			li.innerHTML = nItems;
		    container.querySelector('.list').appendChild(li);
		}

		container.classList.add('calc');
	}  
};

// funcResponsiveMobileNavigation();

setTimeout(function() {
	checkMobileMenu();
}, 0);

function checkMobileMenu() {
	if ($('header .header-mobile-menus.menu-2-0').length && document.documentElement.clientWidth > 480) {
		funcResponsiveMobileNavigation();
	
	} else if (!($('header .header-mobile-menus.menu-2-0').length)) {
		funcResponsiveMobileNavigation();
	}

}

window.addEventListener('resize', function(){
	// funcResponsiveMobileNavigation();

	checkMobileMenu();
	
}, true);
</script>