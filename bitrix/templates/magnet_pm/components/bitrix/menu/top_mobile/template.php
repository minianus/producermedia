<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="content-mobile-menu" id="top-menu-mobile">
<ul class="list">
    <? foreach ($arResult as $key => $arItem): ?>
    <li class="item<?=!empty($arItem['SELECTED']) ? ' active' : ''?>">
        <a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
        <? if (!empty($arItem['SUB'])): ?>
        <ul class="sub-menu level2">
            <? foreach ($arItem['SUB'] as $arItem2): ?>
            <li class="item">
                <a href="<?=$arItem2['LINK']?>"><?=$arItem2['TEXT']?></a>
                <? if (!empty($arItem2['SUB'])): ?>
                    <ul class="sub-menu level3">
                        <? foreach ($arItem2['SUB'] as $arItem3): ?>
                        <li class="item">
                            <a href="<?=$arItem3['LINK']?>"><?=$arItem3['TEXT']?></a>
                        </li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>
            </li>
            <? endforeach; ?>
        </ul>
        <? endif; ?>
    </li>
    <? endforeach; ?>
</ul>
</div>
