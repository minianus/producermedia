<script>
    var funcResize = function(event) {
        var menuHeight = document.querySelector('.catalog-menu nav');
        if (menuHeight !== null) {
            menuHeight = menuHeight.clientHeight;
            
            if (menuHeight > window.innerHeight) {
                document.querySelector('.catalog-menu nav').style['max-height'] = window.innerHeight + 'px';
            } else {
                document.querySelector('.catalog-menu nav').style['max-height'] = '100%';
            }
        }
    };
    funcResize();
    window.addEventListener("resize", funcResize);
</script>