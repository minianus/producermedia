<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="scroll scrollbar-inner show-on-hover">
    <ul class="list">
        <? foreach ($arResult as $key => $arItem): ?>
        <li class="item<?=!empty($arItem['SELECTED']) ? ' active' : '' ?><?=is_array($arItem['SUB']) ? ' has-sub' : '' ?>">
                <a href="<?= $arItem['LINK'] ?>"<?=!empty($arItem['SELECTED']) ? ' class="active"' : '' ?>>
                    <span class="img">
                        <img src="<?=$arItem['PARAMS']['PICTURE']?>" alt="<?= $arItem['TEXT'] ?>">
                    </span>
                    <span class="text"><?= $arItem['TEXT'] ?></span>
                </a>
                <? if (is_array($arItem['SUB']) && $arItem['MAX_SUB_DEPTH'] <= 1): ?>
                <div class="sub">
                        <div class="title"><?= $arItem['TEXT'] ?></div>
                        <div class="scrollbar-inner">
                        <ul class="items">
                            <? foreach ($arItem['SUB'] as $key2 => $arItemSub): ?>
                            <li class="item">
                                            <a href="<?=$arItemSub['LINK']?>"<?=!empty($arItemSub['SELECTED']) ? ' class="active"' : '' ?>>
                                                <span class="img">
                                                    <img data-lazy="mobile-off" data-src="<?=$arItemSub['PARAMS']['PICTURE']?>" alt="<?=$arItemSub['TEXT']?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg">
                                                </span>
                                                <span class="text"><?=$arItemSub['TEXT']?></span>
                                            </a>
                                        </li>
                            <? endforeach; ?>
                        </ul>
                        </div>
                        <? if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_I'])): ?>
                        <div class="banner">
                            <? if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_L'])): ?>
				<a href="<?=$arItem['PARAMS']['UF_SIDEBAR_BANNER_L']?>">
                            <? endif; ?>
                                    <img data-lazy="mobile-off" data-src="<?=$arItem['PARAMS']['UF_SIDEBAR_BANNER_I']?>" alt="" src="<?= SITE_TEMPLATE_PATH?>/img/lazy-image.svg">
                            <? if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_L'])): ?>
                                </a>
                            <? endif; ?>
                        </div>
                        <? endif; ?>
                    </div>
                <? elseif (is_array($arItem['SUB']) && $arItem['MAX_SUB_DEPTH'] > 1): ?>
                    <div class="sub mega">
                        <div class="title"><?= $arItem['TEXT'] ?></div>
                        <div class="sub-columns scrollbar-inner">
                            <? foreach ($arItem['SUB'] as $key2 => $arItemSub): ?>
                            <div class="column">
                                <div class="column-title">
                                    <? if (!empty($arItemSub['PARAMS']['PICTURE'])): ?>
                                    <div class="img">
                                        <img data-lazy="mobile-off" data-src="<?=$arItemSub['PARAMS']['PICTURE']?>" alt="<?=$arItemSub['TEXT']?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg">
                                    </div>
                                    <? endif; ?>
                                    <a href="<?=$arItemSub['LINK']?>"><?=$arItemSub['TEXT']?></a>
                                </div>
                                
                                    <? if (!empty($arItemSub['SUB'])): ?>
                                    <div class="column-sub-list<?=!empty($arItemSub['PARAMS']['PICTURE']) ? ' has-img' : ''?>">
                                        <? foreach ($arItemSub['SUB'] as $key3 => $arItemSub3): ?>
                                        <a href="<?=$arItemSub3['LINK']?>"><?=$arItemSub3['TEXT']?></a>
                                        <?endforeach; ?>
                                    </div>
                                    <? endif; ?>
                            </div>
                            <? endforeach; ?>
                        </div>
                        <? if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_I'])): ?>
                            <? if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_L'])): ?>
                                <a href="<?=$arItem['PARAMS']['UF_SIDEBAR_BANNER_L']?>" style="background-image: url('<?=$arItem['PARAMS']['UF_SIDEBAR_BANNER_I']?>')" class="banner"></a>
                            <? else: ?>
                                    <div class="banner" style="background-image: url('<?=$arItem['PARAMS']['UF_SIDEBAR_BANNER_I']?>')"></div>
                            <? endif; ?>
                        <? endif; ?>
                    </div>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
</div>