<?php
$CSolution = \Nextype\Magnet\CSolution::getInstance();
$bChildUnset = false;

foreach ($arResult as $key => $arItem)
{
    if ($bChildUnset !== false && $arItem["DEPTH_LEVEL"] > $bChildUnset)
        unset($arResult[$key]);
    
    if (!is_bool($bChildUnset) && $arItem["DEPTH_LEVEL"] <= $bChildUnset)
        $bChildUnset = false;
    
    if ($arItem['PARAMS']['UF_HIDE_MAINMENU'] === '1')
    {
        if ($arItem["IS_PARENT"])
            $bChildUnset = $arItem["DEPTH_LEVEL"];
        unset($arResult[$key]);
    }

}

$arResult = $CSolution->GetMenuMultilevel(array_values($arResult));

if (!empty($arResult))
{
    foreach ($arResult as $key => $arItem)
    {
        $arResult[$key]['MAX_SUB_DEPTH'] = 0;

        if (!empty($arItem['SUB']))
            $arResult[$key]['MAX_SUB_DEPTH'] = $CSolution->GetMaxDepthMenu($arItem['SUB']);
        
    }
}
