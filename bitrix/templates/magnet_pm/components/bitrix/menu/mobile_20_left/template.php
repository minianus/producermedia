<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

if (empty($arResult))
    return;

?>
<? if (!empty($arParams['ROOT_ITEM_TEXT'])): ?>
    <div class="sub">
        <div class="items list">
            <div class="item back">
                <a href="javascript:void(0);"><?= GetMessage('MENU_MOBILE_20_BACK_BUTTON') ?></a>
            </div>
            <div class="item category-name">
                <a href="<?=$arParams['ROOT_ITEM_LINK']?>"><?=$arParams['ROOT_ITEM_TEXT']?></a>
            </div>
            <? foreach ($arResult as $key => $arItem): ?>
            <div class="item<?= !empty($arItem['SUB']) ? ' has-sub' : '' ?><?= !empty($arItem['SELECTED']) ? ' active' : '' ?>">
                <a href="<?= $arItem['LINK'] ?>">                                                
                    <span class="text"><?= $arItem['TEXT'] ?></span>                                         
                </a>
                
                <? if (!empty($arItem['SUB'])): ?>
                    <div class="sub">
                        <div class="items list">
                            <div class="item back">
                                <a href="javascript:void(0);"><?= GetMessage('MENU_MOBILE_20_BACK_BUTTON') ?></a>
                            </div>
                            <div class="item category-name">
                                <a href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                            </div>
                            <? foreach ($arItem['SUB'] as $arItem2): ?>
                                <div class="item">
                                    <a href="<?= $arItem2['LINK'] ?>">                                                
                                        <span class="text"><?= $arItem2['TEXT'] ?></span>                                         
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </div>

                    </div>
                <? endif; ?>
            </div>
            <? endforeach; ?>
        </div>
    </div>
<? else: ?>
    <? foreach ($arResult as $key => $arItem): ?>
        <li class="item<?= !empty($arItem['SUB']) ? ' has-sub' : '' ?><?= !empty($arItem['SELECTED']) ? ' active' : '' ?>">
            <a href="<?= $arItem['LINK'] ?>">
                <span class="text"><?= $arItem['TEXT'] ?></span>
            </a>
            <? if (!empty($arItem['SUB'])): ?>
                <div class="sub">
                    <div class="items">
                        <div class="item back">
                            <a href="javascript:void(0);"><?= GetMessage('MENU_MOBILE_20_BACK_BUTTON') ?></a>
                        </div>
                        <div class="item category-name">
                            <a href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a>
                        </div>
                        <? foreach ($arItem['SUB'] as $arItem2): ?>
                            <div class="item">
                                <a href="<?= $arItem2['LINK'] ?>">                                                
                                    <span class="text"><?= $arItem2['TEXT'] ?></span>                                         
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>

                </div>
            <? endif; ?>
        </li>
    <? endforeach; ?>
<? endif; ?>



<script>
    
    $(document).ready(function () {
        $("body").on('click', ".catalog-mobile-menu .content .list .item.has-sub > a", function (event) {
            event.preventDefault();
            $(".menu-container").addClass('open');
            var sub = $(this).parent().find('> .sub');
            sub.addClass('open');
            $(this).closest('.item').addClass('open');

            // $(this).closest('.header-mobile-menus.menu-2-0').find('.container').css('height', sub.height()).css('overflow', 'hidden');
            
            // $(".header-mobile-menus.menu-2-0 .container").css('height', sub.height()).css('overflow', 'hidden');
            return false;
        });
        
        $("body").on('click', ".catalog-mobile-menu.open nav .sub .items .item.back a", function (event) {
            event.preventDefault();
            var sub = $(this).parent().parent().parent();
            if (sub.hasClass('open')) {
                sub.removeClass('open');
                // $(this).closest('.header-mobile-menus.menu-2-0').find('.container').css('height', sub.closest('.sub.open').height());
                // $(".header-mobile-menus.menu-2-0 .container").css('height', sub.closest('.sub.open').height());

            }

            if ($(this).closest('.item.has-sub').hasClass('open')) {
                $(this).closest('.item.has-sub').removeClass('open');
            }

            if (!$('.header-mobile-menus.menu-2-0 .catalog-mobile-menu .container .content nav .list .item').hasClass('open')) {
                // $(this).closest('.header-mobile-menus.menu-2-0').find('.container').css('height', sub.height()).css('overflow', 'visible');
                // $(".header-mobile-menus.menu-2-0 .container").css('height', sub.height()).css('overflow', 'visible');
            }
            
            return false;
        });
     
    });
    
    

</script>