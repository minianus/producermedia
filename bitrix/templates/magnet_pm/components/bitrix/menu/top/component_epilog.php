<script data-skip-moving="true">
var outerWidth = function (el) {
  var width = el.offsetWidth;
  var style = getComputedStyle(el);
  width += parseInt(style.marginLeft) + parseInt(style.marginRight);
  return width;
}
var topNavigationClone = false;
var funcResponsiveTopNavigation = function () {
	if(document.documentElement.clientWidth > 1024) {
		var containerWidth = 0,
		    sumWidth = 0,
		    startRemove = 0,
		    items = [];
		var nItems = '';
		var container = document.getElementById('top-menu-desktop');
                
                if (container == null)
                    return;

		if (!topNavigationClone)
		    topNavigationClone = container.cloneNode(true);

		container.classList.remove('calc');
		container.innerHTML = topNavigationClone.innerHTML;
		containerWidth = parseInt(container.offsetWidth);


		items = container.querySelectorAll('.list > .item');
		items.forEach(function(item, index) {
			if (containerWidth < sumWidth) {
		        if (!startRemove) {
		            startRemove = index - 1;
		            var start = items[startRemove];
		            nItems += '<li class="item">' + start.innerHTML + '</li>';
		            start.parentNode.removeChild(start);
		        }
		        nItems += '<li class="item">' + item.innerHTML + '</li>';
		        item.parentNode.removeChild(item);
			} else {
				sumWidth += parseInt(outerWidth(item));
			}
		});

		if (nItems !== '') {
		    nItems = '<a href="javascript:void(0)">...</a><ul class="sub-menu level2">' + nItems + '</ul>';
		    var li = document.createElement('li');
		    li.classList.add('item','more');
			li.innerHTML = nItems;
		    container.querySelector('.list').appendChild(li);
		}

		container.classList.add('calc');
	}  
};

$(document).ready(function(){
	funcResponsiveTopNavigation();
})

window.addEventListener('resize', function(){
	funcResponsiveTopNavigation();
}, true);
</script>