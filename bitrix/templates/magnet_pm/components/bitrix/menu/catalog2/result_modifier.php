<?php
$CSolution = \Nextype\Magnet\CSolution::getInstance();
$bChildUnset = false;

foreach ($arResult as $key => $arItem)
{
    if ($bChildUnset !== false && $arItem["DEPTH_LEVEL"] > $bChildUnset)
        unset($arResult[$key]);
    
    if (!is_bool($bChildUnset) && $arItem["DEPTH_LEVEL"] <= $bChildUnset)
        $bChildUnset = false;
    
    if ($arItem['PARAMS']['UF_HIDE_MAINMENU'] === '1')
    {
        if ($arItem["IS_PARENT"])
            $bChildUnset = $arItem["DEPTH_LEVEL"];
        unset($arResult[$key]);
        continue;
    }
    
    if (empty($arItem['PARAMS']['PICTURE']))
    {
        $arResult[$key]['PARAMS']['PICTURE'] = SITE_TEMPLATE_PATH . "/img/no-photo.png";
    }
    else
    {
    
        $arResizeParams = Array (
            'width' => 180,
            'height' => 180
        );
        $arResizeImage = CFile::ResizeImageGet($arItem['PARAMS']['PICTURE'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
        $arResult[$key]['PARAMS']['PICTURE'] = $arResizeImage['src'];
    }
    
    
    if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_I']))
    {
        $arResizeParams = Array (
            'width' => 1000,
            'height' => 1000
        );
        $arResizeImage = CFile::ResizeImageGet($arItem['PARAMS']['UF_SIDEBAR_BANNER_I'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $arResult[$key]['PARAMS']['UF_SIDEBAR_BANNER_I'] = $arResizeImage['src'];
        
    }
    
    if (!empty($arItem['PARAMS']['UF_SIDEBAR_BANNER_L']))
        $arResult[$key]['PARAMS']['UF_SIDEBAR_BANNER_L'] = str_replace ("#SITE_DIR#", SITE_DIR, $arItem['PARAMS']['UF_SIDEBAR_BANNER_L']);

}

$arResult = $CSolution->GetMenuMultilevel(array_values($arResult));

if (!empty($arResult))
{
    foreach ($arResult as $key => $arItem)
    {
        $arResult[$key]['MAX_SUB_DEPTH'] = 0;

        if (!empty($arItem['SUB']))
            $arResult[$key]['MAX_SUB_DEPTH'] = $CSolution->GetMaxDepthMenu($arItem['SUB']);
        
    }
}