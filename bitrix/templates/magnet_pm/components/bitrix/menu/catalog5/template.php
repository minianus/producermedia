<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
<nav class="inline-menu" id="inline-menu">
    <ul class="list">
        <? foreach ($arResult as $key => $arItem): ?>
        <li class="item<?=!empty($arItem['SUB']) ? ' has-sub' : ''?><?=!empty($arItem['SELECTED']) ? ' active' : ''?>"><a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
        <? if (!empty($arItem['SUB'])): ?>
            <ul class="sub">
                <? foreach ($arItem['SUB'] as $arItem2): ?>
                    <li class="item<?=!empty($arItem2['SELECTED']) ? ' active' : ''?>"><a href="<?=$arItem2['LINK']?>"><?=$arItem2['TEXT']?></a></li>
                <?endforeach;?>
            </ul>
        <?endif;?>
        </li>
        <? endforeach; ?>
    </ul>
</nav>
<? endif; ?>