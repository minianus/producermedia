<script data-skip-moving="true">
var outerWidth = function (el) {
  var width = el.offsetWidth;
  var style = getComputedStyle(el);
  width += parseInt(style.marginLeft) + parseInt(style.marginRight);
  return width;
}
var catalogNavigationClone = false;
var funcResponsiveCatalogNavigation = function () {

    var containerWidth = 0,
        sumWidth = 0,
        startRemove = 0,
        items = [];
    var nItems = '';
    var container = document.getElementById('inline-menu');
    

    if (container == null)
        return;
    
    if (!catalogNavigationClone)
        catalogNavigationClone = container.cloneNode(true);

    container.classList.remove('calc');
    container.parentNode.classList.remove('done');
    container.innerHTML = catalogNavigationClone.innerHTML;
    containerWidth = parseInt(container.offsetWidth);


    items = container.querySelectorAll('.list > .item');
    items.forEach(function(item, index) {
		if (containerWidth < sumWidth) {
            if (!startRemove) {
                startRemove = index - 1;
                var start = items[startRemove];
                nItems += '<li class="item">' + start.innerHTML + '</li>';
                start.parentNode.removeChild(start);
            }
            nItems += '<li class="item">' + item.innerHTML + '</li>';
            item.parentNode.removeChild(item);
		} else {
			sumWidth += parseInt(outerWidth(item));
		}
	});

	if (nItems !== '') {
	    nItems = '<a href="javascript:void(0)">...</a><ul class="sub">' + nItems + '</ul>';
	    var li = document.createElement('li');
	    li.classList.add('item','more','has-sub');
		li.innerHTML = nItems;
	    container.querySelector('.list').appendChild(li);
	}

    container.classList.add('calc');
    if (container.parentNode.className != 'container') {
    	container.parentNode.parentNode.classList.add('done');
    } else {
    	container.parentNode.classList.add('done');
    }
};

funcResponsiveCatalogNavigation();

window.addEventListener('resize', function(){
	funcResponsiveCatalogNavigation();
}, true);
</script>