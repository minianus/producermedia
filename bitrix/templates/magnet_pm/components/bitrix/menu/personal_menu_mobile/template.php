<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<style>
    .e0t {
        color: rgba(0, 26, 52, .16);
        flex-shrink: 0;
        right: 0;
        display: flex;
        position: absolute;
        margin-right: 20px;
    }

    .menu-left_personal--mobile {
        width: 100vw;
        margin-left: calc(-50vw + 50%);
        margin-right: calc(-50vw + 50%);
    }
</style>


<style>

    .er {
        background: linear-gradient(0deg,rgba(0,26,52,.16),rgba(0,26,52,.16) 48px,#005bff 0);
        flex-direction: column;
        padding-bottom: 12px;
        padding-top: 44px;
    }

    .er {
        display: flex;
    }

    .ck8 {
        align-items: flex-start;
        box-sizing: border-box;
        cursor: pointer;
        display: flex;
        flex: 0 0 auto;
        padding-left: 16px;
    }

    .ck8.c8k {
        align-items: center;
    }

    .cl {
        align-items: center;
        box-sizing: border-box;
        display: flex;
        justify-content: space-between;
        min-height: 44px;
        padding: 8px 16px 8px 0;
        width: 100%;
    }

    .lc0 {
        display: flex;
        flex-direction: column;
        width: 100%;
    }

    .f-subtitle {
        font-family: GTEestiPro,arial,sans-serif;
        font-size: 16px;
        line-height: 1.25;
    }

    .c1l {
        flex-shrink: 0;
    }

    .q9e svg {
        color: #fff!important;
    }

    .er1 {
        margin-top: 16px;
        overflow-x: scroll;
    }

    .e1r {
        display: inline-flex;
        flex-direction: row;
        padding-left: 16px;
        padding-right: 12px;
        width: -webkit-max-content;
        width: -moz-max-content;
        width: max-content;
    }

    .re1 {
        background-color: #fff;
        border-radius: 6px;
        box-shadow: 0 6px 16px 0 rgb(0 0 0 / 12%);
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        flex-shrink: 0;
        margin-right: 8px;
        max-width: 252px;
        min-height: 84px;
        min-width: 108px;
        padding: 12px;
        position: relative;
    }

    .q6e {
        color: #001a34;
        font-size: 14px;
        line-height: 18px;
        margin: 0 0 auto;
        transition: .2s cubic-bezier(.4,0,.2,1);
        transition-property: color;
    }

    .qe7 {
        align-items: center;
        display: flex;
        white-space: nowrap;
    }

    .qe7 {
        color: #001a34;
        font-size: 16px;
        font-weight: 700;
        line-height: 20px;
        margin: auto 0 0;
    }

    .e9q {
        margin-right: 8px;
    }

</style>




    <div class="menu-left_personal--mobile">

        <div class="er">
            <a href="/personal/private/">
                <div class="q9e ck8 c8k">
                    <div class="cl">
                        <div class="lc0">
                            <div class="f-subtitle" style="color:#fff;">Личные данные</div>
                        </div>
                        <div class="c1l">
                            <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" class="l1c">
                                <path d="M5.293 12.293a1 1 0 101.414 1.414l5-5a1 1 0 000-1.414l-5-5a1 1 0 00-1.414 1.414L9.586 8l-4.293 4.293z"
                                      fill="currentColor"></path>
                            </svg>
                        </div>
                    </div>
                </div>
            </a>
            <div class="er1">
                <div class="e1r">
                    <div>
                        <a href="/">
                            <div class="re1">
                                <p class="q6e">Станьте миллионером!</p>
                                <p class="qe7">
                                    <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg" class="e9q"
                                         style="color:;">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M11.132 2.504a1 1 0 011.736 0l3.35 5.863 5.075-5.074a1 1 0 011.697.845l-1.666 12a1 1 0 01-.99.862H10.5a1 1 0 010-2h8.963l1.136-8.185-3.892 3.892a1 1 0 01-1.575-.21L12 5.015l-3.132 5.48a1 1 0 01-1.575.211L3.4 6.815 4.537 15H6.5a1 1 0 110 2H3.667a1 1 0 01-.99-.862l-1.668-12a1 1 0 011.698-.845l5.074 5.074 3.35-5.863zM4 20a1 1 0 100 2h16a1 1 0 100-2H4z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <span>Подробнее</span></p>
                            </div>
                        </a>
                    </div>
                    <div>
                        <div class="re1"><p class="q6e">Premium бонусы</p>
                            <p class="qe7">
                                <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg" class="e9q"
                                     style="color:;">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M11.132 2.504a1 1 0 011.736 0l3.35 5.863 5.075-5.074a1 1 0 011.697.845l-1.666 12a1 1 0 01-.99.862H10.5a1 1 0 010-2h8.963l1.136-8.185-3.892 3.892a1 1 0 01-1.575-.21L12 5.015l-3.132 5.48a1 1 0 01-1.575.211L3.4 6.815 4.537 15H6.5a1 1 0 110 2H3.667a1 1 0 01-.99-.862l-1.668-12a1 1 0 011.698-.845l5.074 5.074 3.35-5.863zM4 20a1 1 0 100 2h16a1 1 0 100-2H4z"
                                          fill="currentColor"></path>
                                </svg>
                                <span>0 Б</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <? if (!empty($arResult)): ?>
            <ul class="links-mobile">
                <? foreach ($arResult as $key => $arItem): ?>

                    <?

                    $sClassStyle = " ";
                    if (isset($arItem["PARAMS"]["CLASS_STYLE"])) {
                        $sClassStyle = $arItem["PARAMS"]["CLASS_STYLE"];
                    }

                    ?>

                    <a href="<?= $arItem['LINK'] ?>" class="<?=$sClassStyle;?>">
                        <li<?= !empty($arItem['SELECTED']) ? ' class="active"' : '' ?> style="<?=$arItem["PARAMS"]["CAPTION_BLOCK"];?> position: unset; margin-bottom: 0; color: #001a34;">
                            <?= $arItem['TEXT'] ?>
                            <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" class="e0t" style="<?=$arItem["PARAMS"]["SVG_HIDDEN"];?>">
                                <path d="M5.293 12.293a1 1 0 101.414 1.414l5-5a1 1 0 000-1.414l-5-5a1 1 0 00-1.414 1.414L9.586 8l-4.293 4.293z"
                                      fill="currentColor"></path>
                            </svg>
                        </li>
                    </a>

                <? endforeach; ?>

                <li class="exit" style="position: unset; margin-bottom: 0;">
                    <a href="<?= $APPLICATION->GetCurPageParam('logout=yes&' . bitrix_sessid_get(), array('logout')) ?>" class="icon-custom"><?= GetMessage('EXIT_LINK') ?></a>
                </li>
            </ul>
        <? endif; ?>

    </div>
