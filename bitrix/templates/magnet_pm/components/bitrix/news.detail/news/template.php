<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$this->setFrameMode(true);

$arMetaProperties = $arResult['IPROPERTY_VALUES'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['NAME'],
    "og:url" => CSolution::getCurrentHost() . $arResult['DETAIL_PAGE_URL'],
    "og:image" => !empty($arResult['PREVIEW_PICTURE']) ? CSolution::getCurrentHost() . $arResult['PREVIEW_PICTURE']['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : (!empty($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'])
));
?>

<? if ($arResult['PICTURE']): ?>
<div class="detail-img">
    <img src="<?= $arResult['PICTURE']['SRC'] ?>" alt="<?= $arResult['PICTURE']['ALT'] ?>" title="<?= $arResult['PICTURE']['TITLE'] ?>">
</div>
<? endif; ?>

<? if (!empty($arResult['ACTIVE_FROM']) && $arParams['DISPLAY_DATE'] == 'Y'): ?>
    <div class="date"><?= $arResult['DISPLAY_ACTIVE_FROM'] ?></div>
<? endif; ?>

<? if ($arParams['DISPLAY_PREVIEW_TEXT_ON_DETAIL'] == "Y"): ?>
    <div class="desc">
        <?= $arResult['FIELDS']['PREVIEW_TEXT'] ?>
    </div>
    <div class="content content-page">
        <?= $arResult['FIELDS']['DETAIL_TEXT'] ?>
    </div>
<? else: ?>
    <? if (!empty($arResult['FIELDS']['DETAIL_TEXT'])): ?>
        <div class="content"><?= $arResult['FIELDS']['DETAIL_TEXT'] ?></div>
    <? elseif (!empty($arResult['FIELDS']['PREVIEW_TEXT'])): ?>
        <div class="content"><?= $arResult['FIELDS']['PREVIEW_TEXT'] ?></div>
    <? endif; ?>
    
<? endif; ?>
 
        
    <div class="buttons">
            
            <div class="right">
                <? if ($arParams['USE_SHARE'] == 'Y'): ?>
                    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/share_buttons.php'); ?>
                <? endif; ?>
            </div>
        </div>
    
    
    
