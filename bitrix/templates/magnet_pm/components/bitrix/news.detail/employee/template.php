<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$this->setFrameMode(true);

$arMetaProperties = $arResult['IPROPERTY_VALUES'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['NAME'],
    "og:url" => CSolution::getCurrentHost() . $arResult['DETAIL_PAGE_URL'],
    "og:image" => !empty($arResult['PREVIEW_PICTURE']) ? CSolution::getCurrentHost() . $arResult['PREVIEW_PICTURE']['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : (!empty($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'])
));
?>


    <div class="info">
        <? if (!empty($arResult['PICTURE'])): ?>
        <div class="img">
            <img src="<?=$arResult['PICTURE']['SRC']?>" alt="<?=$arResult['PICTURE']['ALT']?>" title="<?=$arResult['PICTURE']['TITLE']?>">
        </div>
        <? endif; ?>
        
        <div class="content">
            <? if (!empty($arResult['FIELDS']['DETAIL_TEXT'])): ?>
            <div class="desc"><?=$arResult['FIELDS']['DETAIL_TEXT']?></div>
            <? elseif (!empty($arResult['FIELDS']['PREVIEW_TEXT'])): ?>
            <div class="desc"><?=$arResult['FIELDS']['PREVIEW_TEXT']?></div>
            <? endif; ?>
            
            <div class="employee-contacts">
                
                <? if (!empty($arResult['DISPLAY_PROPERTIES']['JOB']['VALUE'])): ?>
                <div class="position"><?=GetMessage('JOB_TITLE')?> <?=$arResult['DISPLAY_PROPERTIES']['JOB']['VALUE']?></div>
                <? endif; ?>
                
                <? if (!empty($arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])): ?>
                <div class="email">E-mail: <a href="mailto:<?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?>"><?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?></a></div>
                <? endif; ?>
                
                <? if (!empty($arResult['DISPLAY_PROPERTIES']['PHONE']['VALUE'])): ?>
                <div class="phone"><?=$arResult['DISPLAY_PROPERTIES']['PHONE']['VALUE']?></div>
                <? endif; ?>
                
                <?
                $arSocials = Array();
                foreach (Array('VK', 'FB', 'TWITTER') as $code)
                {
                    if (!empty($arResult['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE']))
                    {
                        $arSocials[strtolower($code)] = $arResult['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE'];
                    }
                }
                ?>
                <? if (!empty($arSocials)): ?>
                    <div class="social">
                        <? foreach ($arSocials as $code => $link): ?>
                            <a href="<?= $link ?>" target="_blank" class="link icon-custom <?= $code ?>"></a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
    
