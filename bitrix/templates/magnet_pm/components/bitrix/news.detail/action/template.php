<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$this->setFrameMode(true);

$arMetaProperties = $arResult['IPROPERTY_VALUES'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['NAME'],
    "og:url" => CSolution::getCurrentHost() . $arResult['DETAIL_PAGE_URL'],
    "og:image" => !empty($arResult['PREVIEW_PICTURE']) ? CSolution::getCurrentHost() . $arResult['PREVIEW_PICTURE']['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : (!empty($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'])
));
?>

    <div class="content">
        <main class="main-content">
            <? if ($arResult['PICTURE']): ?>
            <div class="img">
                <img src="<?=$arResult['PICTURE']['SRC']?>" alt="<?=$arResult['PICTURE']['ALT']?>" title="<?=$arResult['PICTURE']['TITLE']?>">
            </div>
            <? endif; ?>
            
            <div class="text-container">
                <? if ($arParams['SHOW_ACTIVE'] == "Y" && !empty($arResult['DATE_RANGE'])): ?>
                <div class="date"><?=$arResult['IS_DISABLED'] == 'Y' ? GetMessage('DISBLED_ACTION') : $arResult['DATE_RANGE']?></div>
                <? endif; ?>
                
                <? if (!empty($arResult['FIELDS']['DETAIL_TEXT'])): ?>
                <div class="text"><?=$arResult['FIELDS']['DETAIL_TEXT']?></div>
                <? elseif (!empty($arResult['FIELDS']['PREVIEW_TEXT'])): ?>
                <div class="text"><?=$arResult['FIELDS']['PREVIEW_TEXT']?></div>
                <? endif; ?>
            </div>
        </main>
        <div class="buttons">
            <? if (!empty($arResult['LIST_PAGE_URL'])): ?>
            <a href="<?=$arResult['LIST_PAGE_URL']?>" class="btn transparent"><?=GetMessage('BACK_LINK_TITLE')?></a>
            <? endif; ?>
            <div class="right">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/share_buttons.php'); ?>
            </div>
        </div>
    </div>
    
