<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$arResult['SEO_DATA'] = CSolution::getSeoData($arResult['IBLOCK_ID'], $arResult['ID']);

if (!empty($arResult['FIELDS']['DETAIL_PICTURE']['ID']))
{
    $arResized = CFile::ResizeImageGet($arResult['FIELDS']['DETAIL_PICTURE']['ID'], Array ('width' => 600, 'height' => 400), BX_RESIZE_IMAGE_EXACT);
    $arResult['PICTURE'] = Array (
        'SRC' => $arResized['src'],
        'ALT' => $arResult['SEO_DATA']['ELEMENT_DETAIL_PICTURE_FILE_ALT'],
        'TITLE' => $arResult['SEO_DATA']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    );
}
elseif (!empty($arResult['FIELDS']['PREVIEW_PICTURE']['ID']))
{
    $arResized = CFile::ResizeImageGet($arResult['FIELDS']['PREVIEW_PICTURE']['ID'], Array ('width' => 600, 'height' => 400), BX_RESIZE_IMAGE_EXACT);
    $arResult['PICTURE'] = Array (
        'SRC' => $arResized['src'],
        'ALT' => $arResult['SEO_DATA']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'],
        'TITLE' => $arResult['SEO_DATA']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
    );
}

$arParams['RANGE_DATE_FORMAT'] = !empty($arParams['RANGE_DATE_FORMAT']) ? $arParams['RANGE_DATE_FORMAT'] : "d F";

if (!empty($arResult['DISPLAY_PROPERTIES']['DATE_START']) || !empty($arResult['DISPLAY_PROPERTIES']['DATE_END']))
{
    $arDateRange = Array();

    if (!empty($arResult['DISPLAY_PROPERTIES']['DATE_START']['VALUE']))
        $arDateRange['DATE_START'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arResult['DISPLAY_PROPERTIES']['DATE_START']['VALUE']));

    if (!empty($arResult['DISPLAY_PROPERTIES']['DATE_END']['VALUE']))
        $arDateRange['DATE_END'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arResult['DISPLAY_PROPERTIES']['DATE_END']['VALUE']));

    if ($arDateRange['DATE_START'] && $arDateRange['DATE_END'])
        $arResult['DATE_RANGE'] = implode(" - ", $arDateRange);
    elseif ($arDateRange['DATE_START'] && !$arDateRange['DATE_END'])
        $arResult['DATE_RANGE'] = GetMessage("FROM_DATE", Array("#DATE#" => $arDateRange['DATE_START']));
    elseif (!$arDateRange['DATE_START'] && $arDateRange['DATE_END'])
        $arResult['DATE_RANGE'] = GetMessage("TO_DATE", Array("#DATE#" => $arDateRange['DATE_END']));

    if (!empty(MakeTimeStamp($arResult['DISPLAY_PROPERTIES']['DATE_END']['VALUE'])) && MakeTimeStamp($arResult['DISPLAY_PROPERTIES']['DATE_END']['VALUE']) < time())
    {
        $arResult['IS_DISABLED'] = "Y";
    }
}
