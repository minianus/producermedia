<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$this->setFrameMode(true);

$arMetaProperties = $arResult['IPROPERTY_VALUES'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['NAME'],
    "og:url" => CSolution::getCurrentHost() . $arResult['DETAIL_PAGE_URL'],
    "og:image" => !empty($arResult['PREVIEW_PICTURE']) ? CSolution::getCurrentHost() . $arResult['PREVIEW_PICTURE']['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : (!empty($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'])
));
?>

<? if ($arResult['PICTURE']): ?>
<? $this->SetViewTarget('aside_right_top_content'); ?>
    <div class="brand-img mobile-hide">
        <div class="img">
            <img src="<?= $arResult['PICTURE']['SRC'] ?>" alt="<?= $arResult['PICTURE']['ALT'] ?>" title="<?= $arResult['PICTURE']['TITLE'] ?>">
        </div>
    </div>
<? $this->EndViewTarget(); ?>
<? endif; ?>

<? if (!empty($arResult['FIELDS']['DETAIL_TEXT'])): ?>
    <div class="text content-page">
    	<span class="img-mobile"><img src="<?= $arResult['PICTURE']['SRC'] ?>" alt="<?= $arResult['PICTURE']['ALT'] ?>" title="<?= $arResult['PICTURE']['TITLE'] ?>"></span>
    	<?= $arResult['FIELDS']['DETAIL_TEXT'] ?>		
    </div>
<? elseif (!empty($arResult['FIELDS']['PREVIEW_TEXT'])): ?>
    <div class="text content-page">
	    <span class="img-mobile"><img src="<?= $arResult['PICTURE']['SRC'] ?>" alt="<?= $arResult['PICTURE']['ALT'] ?>" title="<?= $arResult['PICTURE']['TITLE'] ?>"></span>
    	<?= $arResult['FIELDS']['PREVIEW_TEXT'] ?>
    </div>
<? endif; ?>

    <div class="buttons">
            <? if (!empty($arResult['LIST_PAGE_URL'])): ?>
            <a href="<?=$arResult['LIST_PAGE_URL']?>" class="btn transparent"><?=GetMessage('BACK_LINK_TITLE')?></a>
            <? endif; ?>
            <div class="right">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/share_buttons.php'); ?>
            </div>
        </div>
    
    
    
