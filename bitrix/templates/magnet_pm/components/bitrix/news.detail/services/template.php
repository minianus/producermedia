<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$this->setFrameMode(true);

$arMetaProperties = $arResult['IPROPERTY_VALUES'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['NAME'],
    "og:url" => CSolution::getCurrentHost() . $arResult['DETAIL_PAGE_URL'],
    "og:image" => !empty($arResult['PREVIEW_PICTURE']) ? CSolution::getCurrentHost() . $arResult['PREVIEW_PICTURE']['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : (!empty($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'])
));
?>

<div class="service-header-columns">
    <? if ($arResult['PICTURE']): ?>
    <div class="detail-img">
        <img src="<?= $arResult['PICTURE']['SRC'] ?>" alt="<?= $arResult['PICTURE']['ALT'] ?>" title="<?= $arResult['PICTURE']['TITLE'] ?>">
    </div>
    <? endif; ?>
    <div class="preview-desc">
        <?
        if ($arParams['DISPLAY_PREVIEW_TEXT_ON_DETAIL'] == "Y" && !empty($arResult['FIELDS']['PREVIEW_TEXT']))
            echo $arResult['FIELDS']['PREVIEW_TEXT'];
        elseif ($arParams['DISPLAY_PREVIEW_TEXT_ON_DETAIL'] == "Y" && !empty($arResult['FIELDS']['DETAIL_TEXT']))
            echo $arResult['FIELDS']['DETAIL_TEXT'];
        elseif ($arParams['DISPLAY_PREVIEW_TEXT_ON_DETAIL'] == "N")
            echo !empty($arResult['FIELDS']['DETAIL_TEXT']) ? $arResult['FIELDS']['DETAIL_TEXT'] : $arResult['FIELDS']['PREVIEW_TEXT'];
        ?>
    </div>

</div>

<? if (!empty($arResult['PROPERTIES']['SHOW_FORM']['VALUE'])): ?>
<div class="service-order-block">
    <div class="info">
        <div class="name"><?=$arParams['BLOCK_FORM_TITLE']?></div>
        <?=$arParams['BLOCK_FORM_TEXT']?>
    </div>
    <a href="javascript:void(0)" service-form class="btn"><?=$arParams['BLOCK_FORM_BUTTON']?></a>
</div>
<? endif; ?>

<? if ($arParams['DISPLAY_PREVIEW_TEXT_ON_DETAIL'] == "Y" && !empty($arResult['FIELDS']['PREVIEW_TEXT']) && !empty($arResult['FIELDS']['DETAIL_TEXT'])): ?>
<div class="detail-desc content-page content">
    <?=$arResult['FIELDS']['DETAIL_TEXT']?>
</div>
<? endif; ?>
 
        
<div class="buttons">  
	<div class="right">
		<? if ($arParams['USE_SHARE'] == 'Y'): ?>
			<? $APPLICATION->IncludeFile(SITE_DIR . 'include/share_buttons.php'); ?>
		<? endif; ?>
	</div>
</div>

    
    
<script>
    $("body").on('click', '[service-form]', function (event) {
        event.preventDefault();	
        $('.popup').jqmHide();
        jqmPopup('service', 'include/ajax/form.php?id=service', true, false, function (form) {
            $(form).find('[data-sid="service"]').val('<?= strip_tags($arResult['NAME'])?>');
        });
        return false;
    });
    </script>