<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$arResult['SEO_DATA'] = CSolution::getSeoData($arResult['IBLOCK_ID'], $arResult['ID']);

if (!empty($arResult['FIELDS']['DETAIL_PICTURE']['ID']))
{
    $arResized = CFile::ResizeImageGet($arResult['FIELDS']['DETAIL_PICTURE']['ID'], Array ('width' => 512, 'height' => 340), BX_RESIZE_IMAGE_EXACT);
    $arResult['PICTURE'] = Array (
        'SRC' => $arResized['src'],
        'ALT' => $arResult['SEO_DATA']['ELEMENT_DETAIL_PICTURE_FILE_ALT'],
        'TITLE' => $arResult['SEO_DATA']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    );
}
elseif (!empty($arResult['FIELDS']['PREVIEW_PICTURE']['ID']))
{
    $arResized = CFile::ResizeImageGet($arResult['FIELDS']['PREVIEW_PICTURE']['ID'], Array ('width' => 512, 'height' => 340), BX_RESIZE_IMAGE_EXACT);
    $arResult['PICTURE'] = Array (
        'SRC' => $arResized['src'],
        'ALT' => $arResult['SEO_DATA']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'],
        'TITLE' => $arResult['SEO_DATA']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
    );
}
