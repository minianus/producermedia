<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Magnet\CSolution;

CSolution::getInstance(SITE_ID);
/**
 * @var array $arParams
 */

//    echo "<pre>";
//    print_r($arResult);
//    echo "</pre>";
?>
<script id="basket-total-template" type="text/html">
	<div class="basket-checkout-container" data-entity="basket-checkout-aligner">
		<?
		if ($arParams['HIDE_COUPON'] !== 'Y')
		{
			?>
			<div class="basket-coupon-section">
				<div class="basket-coupon-block-field">
					<div class="basket-coupon-block-field-description">
						<?=Loc::getMessage('SBB_COUPON_ENTER')?>:
					</div>
					<div class="form">
						<div class="form-group" style="position: relative;">
							<input type="text" class="form-control" id="" placeholder="" data-entity="basket-coupon-input">
							<span class="basket-coupon-block-coupon-btn"></span>
						</div>
					</div>
				</div>
				<div class="basket-coupon-alert-section">
					<div class="basket-coupon-alert-inner">
						{{#COUPON_LIST}}
						<div class="basket-coupon-alert text-{{CLASS}}">
							<span class="basket-coupon-text">
								<strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
								{{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}}
							</span>
							<span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}"></span>
						</div>
						{{/COUPON_LIST}}
					</div>
				</div>
			</div>
			<?
		}
		?>

		<div class="basket-checkout-section">
			<div class="basket-checkout-section-inner">
				<div class="basket-checkout-block basket-checkout-block-total">
					<div class="basket-checkout-block-total-inner">
						<div class="basket-checkout-block-total-title"><?=Loc::getMessage('SBB_TOTAL')?>:</div>
						<div class="basket-checkout-block-total-description">
							{{#WEIGHT_FORMATED}}
								<?=Loc::getMessage('SBB_WEIGHT')?>: {{{WEIGHT_FORMATED}}}
								{{#SHOW_VAT}}<br>{{/SHOW_VAT}}
							{{/WEIGHT_FORMATED}}
							{{#SHOW_VAT}}
								<?=Loc::getMessage('SBB_VAT')?>: {{{VAT_SUM_FORMATED}}}
							{{/SHOW_VAT}}
						</div>
					</div>
				</div>

				<div class="basket-checkout-block basket-checkout-block-total-price">
					<div class="basket-checkout-block-total-price-inner">
						{{#DISCOUNT_PRICE_FORMATED}}
							<div class="basket-coupon-block-total-price-old">
								{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
							</div>
						{{/DISCOUNT_PRICE_FORMATED}}

						<div class="basket-coupon-block-total-price-current" data-entity="basket-total-price">
							{{{PRICE_FORMATED}}}
						</div>

						{{#DISCOUNT_PRICE_FORMATED}}
							<div class="basket-coupon-block-total-price-difference">
								<?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
								<span style="white-space: nowrap;">&nbsp;{{{DISCOUNT_PRICE_FORMATED}}}</span>
							</div>
						{{/DISCOUNT_PRICE_FORMATED}}
					</div>

                    <div style="color: #282828; font-weight: 700; margin-top: 20px;">Получите до {{full_bonus}} бонусов</div>
                    <div class="add-bonus_basket">
                        <span>Пользователи <a href="/bonus/">Premium</a></span>
                        <div class="bonus-basket">
                            <span>{{full_bonus}}</span>
                            <span class="bonus-icon__basket"></span>
                        </div>
                    </div>

				</div>

                            <?
                            $bHideSubmit = (floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']) > 0 && $arResult['TOTAL_RENDER_DATA']['PRICE'] < floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE'])) ? 'Y' : 'N';
                            ?>
                            <? if (floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']) > 0): ?>
				<div class="basket-checkout-minimum" id="basket-checkout-minimum" <?=$arResult['TOTAL_RENDER_DATA']['PRICE'] > floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']) ? 'style="display:none"' : ''?>>
					<div class="icon"></div>
					<div class="name"><?=Loc::getMessage('MINIMUM_NAME', Array ("#VALUE#" => CurrencyFormat(floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']), $arResult['TOTAL_RENDER_DATA']['CURRENCY'])))?></div>
					<div class="desc"><?=Loc::getMessage('MINIMUM_DESC')?></div>
				</div>
                            <? endif; ?>

				<div class="basket-checkout-block basket-checkout-block-btn">
					<button <?=$bHideSubmit == 'Y' ? 'style="display:none"' : '' ?> class="btn btn-lg btn-default basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
						data-entity="basket-checkout-button">
						<?=Loc::getMessage('SBB_ORDER')?>
					</button>
                                    
                                        <? if (CSolution::$options['BUY1CLICK_ENABLED'] == "Y"): ?>
                                        <a href="javascript:void(0)" class="btn buy-one-click transparent" <?=$bHideSubmit == 'Y' ? 'style="display:none"' : '' ?> {{#DISABLE_CHECKOUT}}style="display:none"{{/DISABLE_CHECKOUT}} buy1click><?=Loc::getMessage('SBB_BUY_ONE_CLICK_BTN')?></a>
                                        <? endif; ?>
				</div>
			</div>
		</div>


	</div>
</script>