<?php

if (!empty($arResult['ITEM']) && empty($arResult['ITEM']['PREVIEW_PICTURE']))
    $arResult['ITEM']['PREVIEW_PICTURE'] = \Nextype\Magnet\CSolution::getEmptyImage();

if (!empty($arResult['ITEM']['JS_OFFERS']))
{
    $arOffersIDs = array();
    foreach ($arResult['ITEM']['JS_OFFERS'] as $key => $arOffer)
    {
        $arOffersIDs[] = $arOffer['ID'];
    }
    
    $obOffers = CIBlockElement::GetList(Array(), Array("=ID" => $arOffersIDs), false, Array(), Array('DETAIL_PAGE_URL'));
    $key = 0;
    while ($arOfferDetail = $obOffers->GetNext())
    {
        if (!empty($arOfferDetail['DETAIL_PAGE_URL']))
        {
            $arResult['ITEM']['JS_OFFERS'][$key]['DETAIL_PAGE_URL'] = $arOfferDetail['DETAIL_PAGE_URL'];
        }
        $key++;
    }
}

$arResult['ITEM']['SHOW_ORDER_FORM'] = $bShowOrderForm = false;

if (!is_bool($arParams['SHOW_ORDER_FORM']) && !empty($arResult['ITEM']['IBLOCK_SECTION_ID']))
{
    $bShowOrderForm = false;
    $sFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arResult['ITEM']['IBLOCK_ID']."_SECTION", $arResult['ITEM']['IBLOCK_SECTION_ID']);
    if (!empty($sFields['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
        $bShowOrderForm = true;
    
    if (!$bShowOrderForm)
    {
        $arParents = \Nextype\Magnet\CCache::CIBlockSection_GetList(
                array('LEFT_MARGIN' => 'ASC'),
                array('IBLOCK_ID' => $arResult['ITEM']['IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ID' => $arResult['ITEM']['IBLOCK_SECTION_ID']),
                false,
                array('ID', 'IBLOCK_SECTION_ID')
        );
        foreach ($arParents as $arParent)
        {
            if ($arParent['IBLOCK_SECTION_ID'] == $arResult['ITEM']['IBLOCK_SECTION_ID'])
                continue;
            $arDataFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arResult['ITEM']['IBLOCK_ID']."_SECTION", $arParent['IBLOCK_SECTION_ID']);
            if (!empty($arDataFields['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
            {
                $bShowOrderForm = true;
                break;
            }
        }
    }

    $arResult['ITEM']['SHOW_ORDER_FORM'] = $bShowOrderForm;
}
else
{
    $bShowOrderForm = !!$arParams['SHOW_ORDER_FORM'];
}

if (!$bShowOrderForm)
{
    $bShowOrderForm = $arResult['ITEM']['PROPERTIES']['MAGNET_SHOW_ORDER_FORM']['VALUE_XML_ID'] == 'Y';
}
$arResult['ITEM']['SHOW_ORDER_FORM'] = $bShowOrderForm;

