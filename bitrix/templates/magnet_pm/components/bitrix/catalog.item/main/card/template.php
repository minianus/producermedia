<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

CSolution::getInstance(SITE_ID);

//echo "<pre>";
//print_r();
//echo "</pre>";
//$ar_res = CCatalogProduct::GetByID("232");
//
//echo "<pre>";
//print_r($ar_res);
//echo "</pre>";
?>
<? //echo "<pre>";
//print_r($arResult["ITEM"]["OFFERS"][0]["PRICES"]["BASE"]["DISCOUNT_DIFF"]);
//echo "</pre>";?>
<div class="product-card<?= $haveOffers ? ' has-offers' : '' ?>">
    <div class="product-card-wrap">
        <div class="static">
            <? if (!empty($item['LABEL_ARRAY_VALUE']) || ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y' && $price['PERCENT'] > 0)): ?>
                <div class="labels <?= $labelPositionClass ?>" id="<?= $itemIds['STICKER_ID'] ?>">
                    <? foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value): ?>
                        <div class="label <?= strtolower($code) ?><?= (!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' hide-mobile' : '') ?>"><?= $value ?></div>
                    <? endforeach; ?>
                    <? if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y'): ?>
                        <div class="label hit" id="<?= $itemIds['DSC_PERC'] ?>"
                            <?= ($price['PERCENT'] > 0 ? '' : 'style="display: none;"') ?>>
                            <?= -$price['PERCENT'] ?>%
                        </div>
                    <? endif; ?>
                </div>
            <? endif; ?>
            <div class="additional-links">
                <!-- compare -->
                <? if ($arParams['DISPLAY_COMPARE'] == 'Y' && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')): ?>
                    <a href="javascript:void(0);" title="<?= GetMessage('CT_BCE_COMPARE_TOOLTIP') ?>"
                       id="<?= $itemIds['COMPARE_LINK'] ?>" class="compare icon-custom"></a>
                <? endif; ?>
                <!-- end compare -->
                <!-- wish list -->
                <? if ($arParams['DISPLAY_WISH_LIST'] === 'Y'): ?>
                    <a href="javascript:void(0);" title="<?= GetMessage('CT_BCE_WISHLIST_TOOLTIP') ?>"
                       id="<?= $itemIds['WISH_LIST_ID'] ?>" class="wishlist icon-custom"></a>
                <? endif; ?>
                <!-- end wish list -->
            </div>

            <? if (CSolution::$options['CATALOG_SHOW_FAST_VIEW'] != "Y"): ?>
                <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="img">
                    <img <? if ($arParams['IMAGES_LAZY_LOAD'] == 'Y'): ?>data-lazy="default"
                         data-src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
                         src="<?= SITE_TEMPLATE_PATH ?>/img/no-photo.svg"
                         <? else: ?>src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"<? endif; ?> alt="<?= $productTitle ?>"
                         title="<?= $productTitle ?>" id="<?= $itemIds['PICT'] ?>">
                </a>
            <? else: ?>
                <div class="img fast-view">
                    <a href="javascript:void(0)" data-url="<?= $item['DETAIL_PAGE_URL'] ?>" data-role="product-fastview" class="link-fast-view"><?= GetMessage('CT_BCI_TPL_MESS_FAST_VIEW_BUTTON') ?></a>
                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="img-product-link">
                        <img <? if ($arParams['IMAGES_LAZY_LOAD'] == 'Y'): ?>data-lazy="default"
                             data-src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
                             src="<?= SITE_TEMPLATE_PATH ?>/img/no-photo.svg"
                             <? else: ?>src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"<? endif; ?>
                             alt="<?= $productTitle ?>" title="<?= $productTitle ?>" id="<?= $itemIds['PICT'] ?>">
                    </a>
                </div>
            <? endif; ?>

            <?
            if (isset($arResult["ITEM"]["PRICES"]["BASE"]["DISCOUNT_DIFF"]) && $arResult["ITEM"]["PRICES"]["BASE"]["DISCOUNT_DIFF"]==0){?>
            <div class="add-bonus_main--card">
                <div class="bonus bonus-color">
                    <span class="bonus-icon"></span>
                    <span><?=$arResult["ITEM"]["PRICES"]["BASE"]["VALUE_VAT"]-$arResult["ITEM"]["CATALOG_PURCHASING_PRICE"];?></span>
                </div>
            </div>
            <?}elseif(isset($arResult["ITEM"]["OFFERS"][0]["CATALOG_PURCHASING_PRICE"]) && $arResult["ITEM"]["OFFERS"][0]["PRICES"]["BASE"]["DISCOUNT_DIFF"]==0){//условие для бонусов товаров с множественными торговыми предложениями?>
            <div class="add-bonus_main--card">
                <div class="bonus bonus-color">
                    <span class="bonus-icon"></span>
                    <span><?=$arResult["ITEM"]["OFFERS"][0]["PRICES"]["BASE"]["VALUE"]-$arResult["ITEM"]["OFFERS"][0]["CATALOG_PURCHASING_PRICE"];?></span>
                </div>
            </div>
            <?}elseif(isset($arResult["ITEM"]["PRICE_MATRIX"]["MATRIX"][1]) && $arResult["ITEM"]["ITEM_PRICES"][0]["DISCOUNT"]==0){//условие для бонусов товаров с различными оптовыми ценами?>
            <div class="add-bonus_main--card">
                <div class="bonus bonus-color">
                    <span class="bonus-icon"></span>
                    <span><?
                        foreach ($arResult["ITEM"]["PRICE_MATRIX"]["MATRIX"][1] as $key=>$arRow){
                            if ($key==0){//бонусы будем рассчитывать исходя из цены за розничную покупку
                                echo $arRow["PRICE"]-$arResult["ITEM"]["CATALOG_PURCHASING_PRICE"];
                            }
                        }

                        ?></span>
                </div>
            </div>
            <?}?>


            <? if ($arParams['DISPLAY_RATING'] === "Y"): ?>

                <? $APPLICATION->IncludeComponent(
                    "nextype:magnet.iblock.vote",
                    "catalog",
                    array(
                        "IBLOCK_ID" => $item["IBLOCK_ID"],
                        "ELEMENT_ID" => $item['ID'],
                        "MAX_VOTE" => 5,
                        "VOTE_NAMES" => $arParams["VOTE_NAMES"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "DISPLAY_AS_RATING" => 'vote_avg'
                    ),
                    $component
                ); ?>
            <? endif; ?>


            <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="name"><?= $productTitle ?></a>
            <div class="price-container">
                <? if ($arParams['SHOW_OLD_PRICE'] === 'Y'): ?>
                    <div class="old-price"
                         id="<?= $itemIds['PRICE_OLD'] ?>" <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
                        <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_BASE_PRICE'] : $price['PRINT_BASE_PRICE']; ?>
                    </div>
                <? endif; ?>


                <div class="price">
                    <?
                    if (!empty($price)) {
                        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                            echo Loc::getMessage(
                                'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE', array(
                                    '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                    '#VALUE#' => ($measureRatio > 1) ? " " . $measureRatio : "",
                                    '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                )
                            );
                        } else {
                            ?>
                            <span id="<?= $itemIds['PRICE'] ?>">
                                <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                            </span>
                            <span class="measure" id="<?= $itemIds['QUANTITY_MEASURE'] ?>">
                                <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                    <?= !empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : '' ?>
                                <? else: ?>
                                    <?= !empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : '' ?>
                                <? endif; ?>
                            </span>
                            <?
                        }
                    }

                    ?>
                </div>
                <? if ($arParams['SHOW_OLD_PRICE'] === 'Y'): ?>
                    <div class="saving"
                         id="<?= $itemIds['PRICE_ECONOMY_ID'] ?>" <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
                        <?= GetMessage('CT_BCI_TPL_MESS_PRICE_ECONOMY') ?><?= CurrencyFormat($showMeasureRatio == 'Y' ? $price['RATIO_DISCOUNT'] : $price['DISCOUNT'], $price['CURRENCY']); ?>
                    </div>
                <? endif; ?>
            </div>

            <? $actualItem['CAN_BUY'] = CSolution::ShowProductQuantity($item, $arParams, $itemIds); ?>

        </div>
    </div>
    <div class="hover-content">


        <!-- sku -->
        <?
        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])) {
            ?>
            <div id="<?= $itemIds['PROP_DIV'] ?>">
                <?
                foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                    $propertyId = $skuProperty['ID'];
                    $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                    if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                        continue;
                    ?>
                    <div class="product-item-info-container product-item-hidden" data-entity="sku-block">
                        <div class="product-item-scu-container" data-entity="sku-line-block">
                            <? if ($arParams['OFFERS_HIDE_TITLE'] != "Y"): ?>
                                <div class="product-item-scu-name"><?= $skuProperty['NAME'] ?></div>
                            <? endif; ?>
                            <div class="product-item-scu-block">
                                <div class="product-item-scu-list">
                                    <ul class="product-item-scu-item-list">
                                        <?
                                        foreach ($skuProperty['VALUES'] as $value) {
                                            if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                                continue;

                                            $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                            if ($skuProperty['SHOW_MODE'] === 'PICT') {
                                                ?>
                                                <li class="product-item-scu-item-color-container"
                                                    title="<?= $value['NAME'] ?>"
                                                    data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                    data-onevalue="<?= $value['ID'] ?>">
                                                    <div class="product-item-scu-item-color-block">
                                                        <div class="product-item-scu-item-color"
                                                             title="<?= $value['NAME'] ?>"
                                                             <? if (!empty($value['PICT']['SRC'])): ?>style="background-image: url('<?= $value['PICT']['SRC'] ?>');"<?endif;
                                                        ?>>
                                                            <? if (empty($value['PICT']['SRC'])): ?>
                                                                <span class="scu-item-color-title"><?= $value['NAME'] ?></span>
                                                            <? endif; ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?
                                            } else {
                                                ?>
                                                <li class="product-item-scu-item-text-container"
                                                    title="<?= $value['NAME'] ?>"
                                                    data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                    data-onevalue="<?= $value['ID'] ?>">
                                                    <div class="product-item-scu-item-text-block">
                                                        <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                                    </div>
                                                </li>
                                                <?
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
            <?
            foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                    continue;

                $skuProps[] = array(
                    'ID' => $skuProperty['ID'],
                    'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                    'VALUES' => $skuProperty['VALUES'],
                    'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                );
            }

            unset($skuProperty, $value);

            if ($item['OFFERS_PROPS_DISPLAY']) {
                foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer) {
                    $strProps = '';

                    if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                        foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty) {
                            $strProps .= '<dt>' . $displayProperty['NAME'] . '</dt><dd>'
                                . (is_array($displayProperty['VALUE'])
                                    ? implode(' / ', $displayProperty['VALUE'])
                                    : $displayProperty['VALUE'])
                                . '</dd>';
                        }
                    }

                    $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                }
                unset($jsOffer, $strProps);
            }
        } ?>
        <!-- end sku -->

        <!-- product select qty -->
        <? if (!$haveOffers): ?>
            <? if (!empty($price) && $actualItem['CAN_BUY'] && $arParams['USE_PRODUCT_QUANTITY']): ?>
                <div class="counter" data-entity="quantity-block">
                    <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_DOWN'] ?>" class="minus icon-custom"></a>
                    <input class="count" id="<?= $itemIds['QUANTITY'] ?>" type="number"
                           name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="<?= $measureRatio ?>">
                    <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_UP'] ?>" class="plus icon-custom"></a>
                </div>
                <!--<span style="display:none;" id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
            <span style="display:none;" id="<?= $itemIds['PRICE_TOTAL'] ?>"></span>-->
            <? endif; ?>
        <? elseif (!empty($price) && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y'): ?>
            <? if ($arParams['USE_PRODUCT_QUANTITY']): ?>
                <div class="counter" data-entity="quantity-block">
                    <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_DOWN'] ?>" class="minus icon-custom"></a>
                    <input class="count" id="<?= $itemIds['QUANTITY'] ?>" type="number"
                           name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="<?= $measureRatio ?>">
                    <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_UP'] ?>" class="plus icon-custom"></a>
                </div>
                <!--<span style="display:none;" id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
            <span style="display:none;" id="<?= $itemIds['PRICE_TOTAL'] ?>"></span>-->
            <? endif; ?>
        <? endif; ?>
        <!-- end product select qty -->

        <!-- buttons -->
        <? if (!$haveOffers): ?>
            <? if (!empty($price) && $actualItem['CAN_BUY']): ?>
                <div class="product-item-button-container" id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                    <a id="<?= $itemIds['BUY_LINK'] ?>" rel="nofollow" href="javascript:void(0)" class="btn order-btn">
                        <span><?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?></span>
                    </a>
                </div>
            <? else: ?>
                <div class="product-item-button-container <?= (($showOrderForm || $showSubscribe) && !$actualItem['CAN_BUY']) ? 'under-order' : '' ?>">
                    <? if ($showSubscribe) {
                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.product.subscribe', 'main', array(
                            'PRODUCT_ID' => $actualItem['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-default ' . $buttonSizeClass,
                            'DEFAULT_DISPLAY' => true,
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ), $component, array('HIDE_ICONS' => 'Y')
                        );
                    } ?>

                    <? if ($showOrderForm): ?>
                        <span class="btn btn-default btn-sm bx-catalog-subscribe-button"
                              id="<?= $itemIds['ORDER_BTN_ID'] ?>">
                                <span><?= Loc::getMessage('TPL_ORDER_BTN_TEXT') ?></span>
                            </span>
                    <? endif; ?>
                </div>
            <? endif; ?>
        <? else: ?>
            <?
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                ?>
                <div class="product-item-button-container <?= (($showOrderForm || $showSubscribe) && !$actualItem['CAN_BUY']) ? 'under-order' : '' ?>">

                    <?
                    if ($showSubscribe) {
                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.product.subscribe', 'main', array(
                            'PRODUCT_ID' => $item['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-default ' . $buttonSizeClass,
                            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ), $component, array('HIDE_ICONS' => 'Y')
                        );
                    }
                    ?>

                    <? if ($showOrderForm):?>
                        <span class="btn btn-default btn-sm bx-catalog-subscribe-button"
                              id="<?= $itemIds['ORDER_BTN_ID'] ?>">
                            <span><?= Loc::getMessage('TPL_ORDER_BTN_TEXT') ?></span>
                        </span>
                    <?endif ?>

                    <div id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
                        <a class="btn order-btn" id="<?= $itemIds['BUY_LINK'] ?>"
                           href="javascript:void(0)" rel="nofollow"><span>
        <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?></span>
                        </a>
                    </div>
                </div>
                <?
            }
            ?>
        <? endif; ?>

        <div class="product-item-button-container" <?= ($haveOffers || (!$haveOffers && $actualItem['CAN_BUY'])) ? ' style="display:none"' : '' ?>
             id="<?= $itemIds['DETAIL_LINK'] ?>">
            <a class="btn detail" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                <span><?= $arParams['MESS_BTN_DETAIL'] ?></span>
            </a>
        </div>

        <? if ($arParams['PRODUCT_DISPLAY_MODE'] == 'N' && $haveOffers): ?>
            <div class="product-item-button-container">
                <a class="btn detail" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                    <span><?= $arParams['MESS_BTN_DETAIL'] ?></span>
                </a>
            </div>
        <? endif; ?>
        <!--        <div class="product-item-button-container sku-only">-->
        <!--            <a class="btn detail" href="--><? //= $item['DETAIL_PAGE_URL'] ?><!--"><span>-->
        <? //= $arParams['MESS_BTN_DETAIL'] ?><!--</span></a>-->
        <!--        </div>-->
        <!-- end buttons -->


    </div>

</div>



