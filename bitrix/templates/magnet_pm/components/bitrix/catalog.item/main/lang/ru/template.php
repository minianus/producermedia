<?
$MESS["CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE"] = "от #PRICE#/#UNIT#.";
$MESS["CT_BCI_TPL_MESS_QTY_BEFORE_TEXT"] = "Наличие: ";
$MESS["CT_BCI_TPL_MESS_QTY_NOT_AVALIABLE"] = "Нет в наличии";
$MESS["CT_BCI_TPL_MESS_PRICE_ECONOMY"] = "Экономия: ";
$MESS["CT_BCI_TPL_MESS_FAST_VIEW_BUTTON"] = "Быстрый просмотр";
$MESS["CT_BCE_CATALOG_ARTICLE"] = "Код товара: #VALUE#";
$MESS['CT_BCE_COMPARE_LINK'] = "Сравнить";
$MESS['CT_BCE_WISHLIST_LINK'] = "В избранное";
$MESS['CT_BCE_COMPARE_TOOLTIP'] = "Добавить с список сравнения";
$MESS['CT_BCE_WISHLIST_TOOLTIP'] = "Добавить в избранные товары";
$MESS['TPL_ORDER_BTN_TEXT'] = "Заказать товар";
