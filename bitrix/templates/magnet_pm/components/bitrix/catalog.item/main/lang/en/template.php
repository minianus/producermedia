<?
$MESS["CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE"] = "to #PRICE#/#UNIT#.";
$MESS["CT_BCI_TPL_MESS_QTY_BEFORE_TEXT"] = "Availability: ";
$MESS["CT_BCI_TPL_MESS_QTY_NOT_AVALIABLE"] = "Not available";
$MESS["CT_BCI_TPL_MESS_PRICE_ECONOMY"] = "Economy: ";
$MESS["CT_BCI_TPL_MESS_FAST_VIEW_BUTTON"] = "Quick view";
$MESS["CT_BCE_CATALOG_ARTICLE"] = "Product code: #VALUE#";
$MESS['CT_BCE_COMPARE_LINK'] = "Compare";
$MESS['CT_BCE_WISHLIST_LINK'] = "To favorites";
$MESS['CT_BCE_COMPARE_TOOLTIP'] = "Add to comparison list";
$MESS['CT_BCE_WISHLIST_TOOLTIP'] = "Add to Favorite Products";
$MESS['TPL_ORDER_BTN_TEXT'] = "To order";
