<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use Nextype\Magnet\CSolution;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
CSolution::getInstance(SITE_ID);

if (isset($arResult['ITEM']))
{
    
	$item = $arResult['ITEM'];
	$areaId = $arResult['AREA_ID'];
        
        $productTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $item['NAME'];

	$imgTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $item['NAME'];
        
        $haveOffers = !empty($item['OFFERS']);
	if ($haveOffers)
	{
		$actualItem = isset($item['OFFERS'][$item['OFFERS_SELECTED']])
			? $item['OFFERS'][$item['OFFERS_SELECTED']]
			: reset($item['OFFERS']);
	}
	else
	{
		$actualItem = $item;
	}
        
        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
	{
		$price = $item['ITEM_START_PRICE'];
		$minOffer = $item['OFFERS'][$item['ITEM_START_PRICE_SELECTED']];
		$measureRatio = $minOffer['ITEM_MEASURE_RATIOS'][$minOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
		$morePhoto = $item['MORE_PHOTO'];
	}
	else
	{
		$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
		$measureRatio = $price['MIN_QUANTITY'];
		$morePhoto = $actualItem['MORE_PHOTO'];
	}

    $showMeasureRatio = CSolution::$options['CATALOG_SHOW_MEASURE_RATIO'];
?>
<a href="<?=$item['DETAIL_PAGE_URL']?>" class="item">
    <span class="img">
        <img <? if ($arParams['IMAGES_LAZY_LOAD'] == 'Y'): ?>data-lazy="default" data-src="<?=$item['PREVIEW_PICTURE']['SRC']?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg"<?else:?>src="<?=$item['PREVIEW_PICTURE']['SRC']?>"<?endif;?> alt="<?=$productTitle?>" title="<?=$productTitle?>" <?= !empty($itemIds['PICT'])? 'id="'.$itemIds['PICT'].'"' : '' ?>>
    </span>
    <span class="text">
        <span class="product-name"><?=$productTitle?></span>
        <span class="price">
            <?
                if (!empty($price))
                {
                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
                    {
                        echo GetMessage(
                                'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE', array(
                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                            '#VALUE#' => ($measureRatio > 1) ? " " . $measureRatio : "",
                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                )
                        );
                    }
                    else
                    {
                    ?>
                        <span>
                            <?= $showMeasureRatio == 'Y' ? $price['PRINT_RATIO_PRICE'] : $price['PRINT_PRICE'] ?>
                        </span>
                        <span class="measure">
                            <? if ($showMeasureRatio == 'Y' && (int)$price['MIN_QUANTITY'] != 1): ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['RATIO_PRICE']) ? ' / ' . $price['MIN_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? else: ?>
                                <?=!empty($actualItem['ITEM_MEASURE']['TITLE']) && !empty($price['PRICE']) ? ' / ' . $actualItem['ITEM_MEASURE']['TITLE'] . '.' : ''?>
                            <? endif; ?>
                        </span>
                    <?
                    }
                }
                ?>
        </span>
    </span>
</a>
<? } ?>