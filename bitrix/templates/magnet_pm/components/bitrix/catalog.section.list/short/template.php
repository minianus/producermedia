<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strRand = randString(5) . "_catalog_section_list";
?>

<? if (!empty($arResult['SECTIONS'])): ?>
<div class="catalog" id="<?=$strRand?>" data-helper="catalog::categories">
    <div class="short-categories">
        <? foreach ($arResult['SECTIONS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
    	<a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="short-category" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
    		<div class="img">
                    <? if (!empty($arItem['PICTURE']['SRC'])): ?>
                    <img src="<?= $arItem['PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>" />
                    <? endif; ?>
                </div>
    		<div class="text">
                    <div class="name"><?= $arItem['NAME'] ?></div>
                    <? if (!empty($arItem['DESCRIPTION']) && $arParams['HIDE_DESCRIPTION'] !== "Y"): ?>
                    <div class="desc"><?= $arItem['DESCRIPTION'] ?></div>
                    <? endif; ?>
    		</div>
    	</a>
        <? endforeach; ?>
    	
    </div>
</div>
<script>
    window.CSolution.setProductsStaticHeight($('#<?=$strRand?> .short-category'));
	$(document).ready(function(){
		$('.short-category').addClass('loaded');
	});
</script>
<? endif; ?>