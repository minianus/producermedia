<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strRand = randString(5) . "_catalog_section_list";
?>

<? if (!empty($arResult['SECTIONS'])): ?>
<div class="subcategories" id="<?=$strRand?>" data-helper="catalog::subcategories">
    <? foreach ($arResult['SECTIONS'] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                ?>
    <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="item" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
        <? if (!empty($arItem['PICTURE']['SRC'])): ?>
        <span class="img"><img src="<?= $arItem['PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>"></span>
        <? endif; ?>
        <span class="name"><?= $arItem['NAME'] ?></span>
    </a>
    <? endforeach; ?>
</div>
<? endif; ?>