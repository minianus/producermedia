<?php

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die('Not install module nextype.magnet');

$viewSectionDepth = intval($arResult['SECTION']['DEPTH_LEVEL']) > 0 ? intval($arResult['SECTION']['DEPTH_LEVEL']) + 1 : 1;
$arNewSections = Array ();

$arResizeParams = Array(
    'width' => 400,
    'height' => 400
);

foreach ($arResult['SECTIONS'] as $key => $arItem)
{

    if ($viewSectionDepth == $arItem['DEPTH_LEVEL'])
    {
        if (!empty($arItem['PICTURE']['ID']))
        {
            $arResizeImage = CFile::ResizeImageGet($arItem['PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL, true);

            $arItem['PICTURE'] = Array(
                'SRC' => $arResizeImage['src'],
            );
        }
        
        $arNewSections[] = $arItem;
    }
}

$arResult['SECTIONS'] = $arNewSections;
unset($arNewSections);