<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strRand = randString(5) . "_catalog_section_list";
?>

<? if (!empty($arResult['SECTIONS'])): ?>
<div class="catalog" id="<?=$strRand?>" data-helper="catalog::categories">
    <div class="light-categories more-list">
        <div class="items-wrap">
            <div class="items">
            <? foreach ($arResult['SECTIONS'] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                ?>
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
                    <span><?= $arItem['NAME'] ?></span><? if ($arParams["COUNT_ELEMENTS"]): ?><i><?= $arItem['ELEMENT_CNT'] ?></i><? endif; ?>
                </a>
            <? endforeach; ?>
            </div>
        </div>
        <a href="javascript:void(0)" class="show-more-cats icon-custom"></a>
    </div>
</div>
<? endif; ?>

<? if (!empty($arResult['SECTIONS'])): ?>
<div class="catalog" id="<?=$strRand?>" data-helper="catalog::categories">
    <div class="light-categories">
        
            <div class="items swiper-container">
                <div class="swiper-wrapper">
                    <? foreach ($arResult['SECTIONS'] as $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                        ?>
                        <div class="item-wrap swiper-slide">
                        <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
                            <span><?= $arItem['NAME'] ?></span><? if ($arParams["COUNT_ELEMENTS"]): ?><i><?= $arItem['ELEMENT_CNT'] ?></i><? endif; ?>
                        </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        
    </div>
</div>
<? endif; ?>

<script>      

    $("body").on('click', '.show-more-cats', function () {
        $(".light-categories.more-list").toggleClass('open');
    });

    window.CSolution.setLightCategoriesHeight($('.light-categories.more-list .items'));

    var lightCategoriesSlider = new Swiper('.light-categories .swiper-container', {
        slidesPerView: 'auto',
        speed: 400,
        freeMode: true,
    });
</script>