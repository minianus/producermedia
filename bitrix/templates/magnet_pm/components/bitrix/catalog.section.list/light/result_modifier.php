<?php

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die('Not install module nextype.magnet');

$viewSectionDepth = intval($arResult['SECTION']['DEPTH_LEVEL']) > 0 ? intval($arResult['SECTION']['DEPTH_LEVEL']) + 1 : 1;

$arNewSections = Array ();
foreach ($arResult['SECTIONS'] as $key => $arItem)
{    
    if ($viewSectionDepth == $arItem['DEPTH_LEVEL'])
    {
        $arNewSections[] = $arItem;
    }
}

$arResult['SECTIONS'] = $arNewSections;
unset($arNewSections);