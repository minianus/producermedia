<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strRand = randString(5) . "_catalog_section_list";
?>

<? if (!empty($arResult['SECTIONS'])): ?>
<div class="catalog" id="<?=$strRand?>" data-helper="catalog::categories">
    <div class="categories">
        <? foreach ($arResult['SECTIONS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
        <div class="category-wrap" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="category<?=empty($arItem['DESCRIPTION']) ? ' no-description' : ''?>">
                <div class="static">
                    <? if (!empty($arItem['PICTURE']['SRC'])): ?>
                    <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="img">
                        <img src="<?= $arItem['PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>">
                    </a>
                    <? endif; ?>
                    <div class="text">
                        <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="name"><?= $arItem['NAME'] ?></a>
                        <? if (is_array($arItem['SUB']) && !empty($arItem['SUB'])): ?>
                        <div class="sub">
                            <? foreach ($arItem['SUB'] as $arSubItem): ?>
                            <a href="<?= $arSubItem['SECTION_PAGE_URL'] ?>" class="item"><?=$arSubItem['NAME']?></a>
                            <? endforeach; ?>
                        </div>
                        <? endif; ?>
                    </div>
                    <? if (!empty($arItem['DESCRIPTION']) && $arParams['HIDE_DESCRIPTION'] !== "Y"): ?>
                    <div class="desc"><?= $arItem['DESCRIPTION'] ?></div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    </div>
</div>
<script>
    // window.CSolution.setProductsStaticHeight($('#<?=$strRand?> .category-wrap'));
</script>
<? endif; ?>