<?php

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die('Not install module nextype.mmarket');

foreach ($arResult['SECTIONS'] as $key => $arItem)
{
    if ($arItem['UF_HIDE_HOMEPAGE'] === '1')
    {
        unset($arResult['SECTIONS'][$key]);
        continue;
    }
    
    if (!empty($arItem['PICTURE']['ID']))
        {
            $arResizeParams = Array (
                'width' => 240,
                'height' => 240
            );
  
            $arResizeImage = CFile::ResizeImageGet($arItem['PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL, true);
            
            $arResult['SECTIONS'][$key]['PICTURE'] = Array (
                'SRC' => $arResizeImage['src'],
            );
        }
        
}


$obj = \Nextype\Magnet\CSolution::getInstance();
$arResult['SECTIONS'] = $obj->GetSectionsMultilevel(array_values($arResult['SECTIONS']));

if (!empty($arParams['BLOCK_MORE_LINK']))
    $arParams['BLOCK_MORE_LINK'] = str_replace ("#SITE_DIR#", SITE_DIR, $arParams['BLOCK_MORE_LINK']);
