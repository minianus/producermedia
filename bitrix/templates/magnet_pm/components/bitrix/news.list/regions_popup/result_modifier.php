<?php
if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

$CSolution = \Nextype\Magnet\CSolution::getInstance(SITE_ID);

if (count($arResult['ITEMS']) > 0)
{
    $arItems2Sections = Array ();
    $arNavChains = Array ();
   
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if (!empty($arItem['IBLOCK_SECTION_ID']))
        {
            $arItems2Sections[$arItem['IBLOCK_SECTION_ID']][] = $arItem;
            if (empty($arNavChains[$arItem['IBLOCK_SECTION_ID']]))
            {
                $bufStr = Array ();
                $rsNavChain = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID']);
                while ($arNavChain = $rsNavChain->fetch())
                    $bufStr[] = $arNavChain['NAME'];
                
                $arNavChains[$arItem['IBLOCK_SECTION_ID']] = implode(" / ", $bufStr);
            }
        }
        
        
        if ($arItem['PROPERTIES']['IS_FAVORITE']['VALUE_XML_ID'] == "Y")
        {
            $arResult['FAVORITE_ITEMS'][] = $arItem;
        }
        
        $arResult['AUTOCOMPLETE'][] = Array (
            "value" => $arItem['ID'],
            "label" => empty($arNavChains[$arItem['IBLOCK_SECTION_ID']]) ? $arItem['NAME'] : $arItem['NAME'] . " (" . $arNavChains[$arItem['IBLOCK_SECTION_ID']] . ")",
        );
    }
    
    $arSectionsByDepth = Array ();
    $maxDepth = 0;
    $rsSections = CIBlockSection::GetList(Array("left_margin"=>"asc", "SORT" => "ASC"), Array ('IBLOCK_ID' => $arParams['IBLOCK_ID']));
    while ($arSection = $rsSections->GetNext())
    {
        if ($arSection['DEPTH_LEVEL'] > $maxDepth)
            $maxDepth = $arSection['DEPTH_LEVEL'];
        
        $arSection['ITEMS'] = !empty($arItems2Sections[$arSection['ID']]) ? $arItems2Sections[$arSection['ID']] : Array ();
        $arSectionsByDepth[$arSection['DEPTH_LEVEL']][] = $arSection;
    }
    
    
    $arResult['JS_DATA'] = Array ();
    foreach ($arSectionsByDepth as $depth => $arSections)
    {

        $bFirst = true;
        foreach ($arSections as $arSection)
        {
            $bActiveSection = false;
            if (empty($_REQUEST['section'][$depth]) && $bFirst)
                $bActiveSection = true;
            elseif (!empty($_REQUEST['section'][$depth]) && $_REQUEST['section'][$depth] == $arSection['ID'])
                $bActiveSection = true;
            
            $parentId = !$arSection['IBLOCK_SECTION_ID'] ? 0 : $arSection['IBLOCK_SECTION_ID'];
            $arItems = Array ();
            if (!empty($arSection['ITEMS']))
            {
                foreach ($arSection['ITEMS'] as $arItem)
                {
                    $arResult['JS_DATA'][$depth + 1][$arItem['IBLOCK_SECTION_ID']][] = Array (
                        'id' => $arItem['ID'],
                        'is_location' => true,
                        'name' => $arItem['NAME'],
                        'subdomain' => $arItem['PROPERTIES']['SUBDOMAIN']['VALUE']
                    );
                }
            }

            $arResult['JS_DATA'][$depth][$parentId][] = Array (
                'id' => $arSection['ID'],
                'is_active' => $bActiveSection ? true : false,
                'name' => $arSection['NAME'],
                'subdomain' => $arItem['PROPERTIES']['SUBDOMAIN']['VALUE']
            );
            
            $bFirst = false;
        }
    }
    
    if (!empty($arResult['JS_DATA']))
    {
        ksort($arResult['JS_DATA']);
    }
    

}