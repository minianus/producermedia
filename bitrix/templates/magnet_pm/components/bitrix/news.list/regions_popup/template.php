<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CLocations;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strID = randString(5) . "_locations";
?>

<div class="popup-dialog">
    <div class="popup-content">
        <div class="popup-header">            
            <div class="popup-title"><?= GetMessage('HEADER_TITLE') ?></div>            
            <a href="javascript:void(0);" onclick="$('.popup').jqmHide();" class="close-popup-icon icon-custom"></a>
        </div>
        <div class="popup-body">
            <div class="search">
                <input type="text" id="<?=$strID?>_autocomplete" placeholder="<?= GetMessage('AUTOCOMPLETE_PLACEHOLDER') ?>" />
            </div>
            <? if (!empty($arResult['FAVORITE_ITEMS'])): ?>
            <div class="favorite">
                <span><?= GetMessage('FAVORITE_TITLE') ?></span>
                <? foreach ($arResult['FAVORITE_ITEMS'] as $arItem): ?>
                <a href="javascript:void(0);" onclick="setcity('<?=$arItem['ID']?>', '<?=CLocations::getSubdomainLink($arItem['PROPERTIES']['SUBDOMAIN']['VALUE'])?>');"><?=$arItem['NAME']?></a>
                <? endforeach; ?>
            </div>
            <? endif; ?>
            <form class="items" id="<?=$strID?>_items">
                <? if ($_REQUEST['is_ajax_request'] == "Y") $APPLICATION->RestartBuffer(); ?>
                <? $current_parent = 0; ?>
                <? foreach ($arResult['JS_DATA'] as $depth => $arSections): ?>
                <div class="section scrollbar-inner" data-section-depth="<?=$depth?>">
                    <? $current_section = ""; ?>
                    <?$arSections = $arSections[$current_parent];?>
                    <?
                    if (is_array($arSections) && !empty($arSections)):
                        foreach ($arSections as $arItem): ?>
                    
                    <? if ($arItem['is_active']) {$current_parent = $arItem['id']; $current_section = $arItem['id'];} ?>
                    <div class="link"><a href="javascript:void(0);" class="item<?=$arItem['is_active'] ? ' active' : ''?>" <?if($arItem['is_location']):?>data-is-location<?else: ?>data-is-section<?endif;?> data-depth="<?=$depth?>" data-domain="<?=CLocations::getSubdomainLink($arItem['subdomain'])?>" data-id="<?=$arItem['id']?>"><?=$arItem['name']?></a></div>
                    <? endforeach; ?>
                    <input type="hidden" name="section[<?=$depth?>]" value="<?=$current_section?>" />
                    
                    <? endif; ?>
                </div>
                <? endforeach; ?>
                    <script>$(".scrollbar-inner").scrollbar();</script>
                <? if ($_REQUEST['is_ajax_request'] == "Y") exit(); ?>
            </form>
        </div>
    </div>
</div>

<script>
function setcity(cityid, url) {
    if (window.arOptions['regions_on_subdomains'] == 'Y')
        window.location = url + window.location.pathname;
    else
        window.location = BX.message('SITE_DIR') + 'include/components/set_region.php?id='+cityid+'&r=' + window.location.pathname; $('.popup').jqmHide();
};
$(document).ready(function(){
    $('.scrollbar-inner').scrollbar();
});
$("body").on('click', "#<?=$strID?>_items .section .item[data-is-section]", function () {
    var id = $(this).data('id');
    if (id !== undefined) {
        $(this).parents('.section').find('input[name*="section"]').val(id);
        $.ajax({
            url: '<?=$APPLICATION->GetCurPageParam('is_ajax_request=Y', array ('section'))?>',
            type: 'post',
            data: $("#<?=$strID?>_items").serialize(),
            success: function (data) {
                $("#<?=$strID?>_items").html(data);
            }
        });
    }
});

$("body").on('click', "#<?=$strID?>_items .section .item[data-is-location]", function () {
    var id = $(this).data('id'), subdomain = $(this).data('domain');
    if (id !== undefined && subdomain !== undefined) {
        setcity(id, subdomain);
    }
});

$("#<?=$strID?>_autocomplete").autocomplete({
    source: <?=CUtil::PhpToJSObject($arResult['AUTOCOMPLETE'])?>,
    select: function( event, ui ) {
        if (ui.item.value !== undefined) {
            var url = BX.message('SITE_DIR') + 'include/components/set_region.php?id=' + ui.item.value + '&r=' + window.location.pathname;
            window.location = url;
            $('.popup').jqmHide();
        }
        
        return false;
    }
});
</script> 