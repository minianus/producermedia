<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;
?>

<? if (!empty($arResult['ITEMS'])): ?>
    <div class="items">
        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?
            if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 300, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                ?>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" title="<?=$arItem['NAME']?>">
                    <span class="img">
                        <img src="<?= $preview['src'] ?>" alt="<?= $arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] ?>" title="<?= $arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] ?>">
                    </span>
                </a>
            <? endif; ?>

        <? endforeach; ?>
    </div>

<? endif; ?>



