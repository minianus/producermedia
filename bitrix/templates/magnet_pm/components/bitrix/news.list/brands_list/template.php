<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;

if (empty($arParams['VIEW_MODE']) || $arParams['VIEW_MODE'] == "normal" || !file_exists(__DIR__ . "/" . $arParams['VIEW_MODE'] . ".php"))
{
    include(__DIR__ . "/normal.php");
}
else
{
    include(__DIR__ . "/" . $arParams['VIEW_MODE'] . ".php");
}

$this->SetViewTarget('news_pagination');
if($arParams["DISPLAY_BOTTOM_PAGER"])
{
    echo $arResult["NAV_STRING"];
}
$this->EndViewTarget();
