<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arParams['VIEW_MODE'] == 'alphabet')
{
    $arResult['ITEMS_ALPHABET'] = Array (
        'ENG_ALPHABET' => Array (),
        'OTHER_ALPHABET' => Array (),
        'NUMBER' => Array ()
    );
    foreach (str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ") as $char)
        $arResult['ITEMS_ALPHABET']['ENG_ALPHABET'][$char] = Array ();
    
    
    if (!empty($arResult['ITEMS']))
    {
        foreach ($arResult['ITEMS'] as $arItem)
        {
            if (is_array($arItem['PROPERTIES']['TAGS']['VALUE']))
            {
                foreach ($arItem['PROPERTIES']['TAGS']['VALUE_ENUM_ID'] as $key => $propId)
                {
                    $arItem['TAGS_IDS'][] = ":" . $propId . ":";
                    $arResult['ITEMS_TAGS'][$propId] = $arItem['PROPERTIES']['TAGS']['VALUE'][$key];
                }
            }
            elseif (!is_array($arItem['PROPERTIES']['TAGS']['VALUE']) && !empty($arItem['PROPERTIES']['TAGS']['VALUE']))
            {
                $arItem['TAGS_IDS'][] = $arItem['PROPERTIES']['TAGS']['VALUE_ENUM_ID'];
                $arResult['ITEMS_TAGS'][$arItem['PROPERTIES']['TAGS']['VALUE_ENUM_ID']] = $arItem['PROPERTIES']['TAGS']['VALUE'];
            }
                
            $fChar = strtoupper(substr($arItem['NAME'], 0, 1));
            if (isset($arResult['ITEMS_ALPHABET']['ENG_ALPHABET'][$fChar]))
            {
                $arResult['ITEMS_ALPHABET']['ENG_ALPHABET'][$fChar][] = $arItem;
            }
            elseif (!isset($arResult['ITEMS_ALPHABET']['ENG_ALPHABET'][$fChar]) && is_numeric($fChar))
            {
                $arResult['ITEMS_ALPHABET']['NUMBER'][$fChar][] = $arItem;
            }
            else
            {
                $arResult['ITEMS_ALPHABET']['OTHER_ALPHABET'][$fChar][] = $arItem;
            }
            
        }
        
        // clear empty cats
        foreach ($arResult['ITEMS_ALPHABET'] as $type => $arChars)
        {
            $bHasItems = false;
            foreach ($arChars as $char => $arItems)
            {
                if (!empty($arItems))
                {
                    $bHasItems = true;
                }
                else
                {
                    unset($arResult['ITEMS_ALPHABET'][$type][$char]);
                }
                
            }
            
            if (!$bHasItems)
                unset($arResult['ITEMS_ALPHABET'][$type]);
        }
    }
    
    
}