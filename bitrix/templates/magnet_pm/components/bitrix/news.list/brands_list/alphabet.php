<div class="brands-list">
	<div class="search-panel">
		<a href="javascript:void(0)" class="mobile-link"><?=GetMessage('FILTER_TITLE')?></a>
                
                <? foreach ($arResult['ITEMS_ALPHABET'] as $type => $arChars):
                    $firstChar = key($arChars);
                    end($arChars);
                    $endChar = key($arChars);
                ?>
		<div class="line">
			<div class="label">
                            <?=$firstChar != $endChar ? $firstChar . "-" . $endChar : $firstChar?>
                        </div>
			<div class="links">
                            <? foreach ($arChars as $char => $arItems): ?>                            
				<a href="javascript:void(0)" data-char="<?=strtolower($type)?>_<?= strtolower($char)?>" class="link"><?=$char?></a>
                            <? endforeach; ?>
			</div>
		</div>
		<? endforeach; ?>
		
                <? if (!empty($arResult['ITEMS_TAGS'])): ?>
		<div class="line tags">
			<div class="label"></div>
			<div class="links">
				<a href="javascript:void(0)" data-tags-id="" class="link"><?=GetMessage('FILTER_ALL_TITLE')?></a>
                                <? foreach ($arResult['ITEMS_TAGS'] as $propId => $propValue): ?>
				<a href="javascript:void(0)" data-tags-id=":<?=$propId?>:" class="link"><?=$propValue?></a>
				<? endforeach; ?>
			</div>
			<select class="mobile-select" data-mobile-tags-selector="true">
				<option value=""><?=GetMessage('FILTER_ALL_TITLE')?></option>
                                <? foreach ($arResult['ITEMS_TAGS'] as $propId => $propValue): ?>
				<option value="<?=$propId?>"><?=$propValue?></option>
				<? endforeach; ?>
			</select>
		</div>
                <? endif; ?>
	</div>
	<main class="list">
                <? foreach ($arResult['ITEMS_ALPHABET'] as $type => $arChars):
                    foreach ($arChars as $char => $arItems):
                ?>
                <div class="row" data-char-id="<?=strtolower($type)?>_<?= strtolower($char)?>">
                    <div class="label"><?=$char?></div>
                    <div class="table">
                        <? foreach ($arItems as $arItem):
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
           
                        ?>
                        <div class="item" data-tags-ids="<?=is_array($arItem['TAGS_IDS']) ? implode(";",$arItem['TAGS_IDS']) : ''?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="link"><?=$arItem['NAME']?></a>
                            <?
                            if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                                $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                                ?>
                                <span title="<?=$arItem['NAME']?>" style="background-image: url(<?= $preview['src'] ?>);"></span>
                            <? endif; ?>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <?
                    endforeach;
                endforeach; ?>

	</main>
</div>