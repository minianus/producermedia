<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if ($arParams['VIEW_MODE'] == 'alphabet'): ?>
<script>
$(function() {
    $("body").on('click', 'a[data-char]', function () {
        var char = $(this).data('char');
        if (char !== undefined) {
            var destination = $("[data-char-id='"+char+"']");
            
            if (destination.length > 0) {
                var top = parseInt(destination.offset().top) - 50;
                $('body, html').animate({ scrollTop: top + 'px' }, 'slow');
            }
        }
    });
    
    $("body").on('click', 'a[data-tags-id]', function () {
        var tagId = $(this).data('tags-id');
        if (tagId === undefined || tagId == "") {
            $("[data-tags-ids]").show().parents('.row').show();
        } else {
            $("[data-tags-ids]").hide().parents('.row').hide();
            $("[data-tags-ids*='"+tagId+"']").show().parents('.row').show();;
        }
    });
    
    $("body").on('change', 'select[data-mobile-tags-selector]', function () {
        var tagId = $(this).val();
        if (tagId === undefined || tagId == "") {
            $("[data-tags-ids]").show().parents('.row').show();
        } else {
            $("[data-tags-ids]").hide().parents('.row').hide();
            $("[data-tags-ids*='"+tagId+"']").show().parents('.row').show();;
        }
    });
    
    
});
</script>
<? endif; ?>
