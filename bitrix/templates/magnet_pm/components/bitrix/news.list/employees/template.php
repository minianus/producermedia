<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<? if (!empty($arResult['ITEMS']) || !empty($arResult['ITEMS_EMPTY_SECTION'])): ?>
	<main class="employees-list" data-helper="employees::list">
            <? foreach ($arResult['ITEMS'] as $arSection): ?>
            <?
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
		<div class="department" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<div class="title"><?=$arSection['NAME']?></div>
                        <? if (!empty($arSection['DESCRIPTION'])): ?>
                        <div class="description">
                            <?=$arSection['DESCRIPTION']?>
                        </div>
                        <? endif; ?>
			
			<div class="items">
                            <? foreach ($arSection['ITEMS'] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
				<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                        <? if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                                            $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                                            $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array ('width' => 250, 'height' => 250), BX_RESIZE_IMAGE_EXACT);
                                        ?>
                                        
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="img">
                                            <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>">
                                        </a>
                                        <? endif; ?>
                                        <? if (!empty($arItem['FIELDS']['NAME'])):?>
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name"><?=$arItem['FIELDS']['NAME']?></a>
                                        <? endif; ?>
                                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['JOB']['VALUE'])):?>
					<div class="position"><?=$arItem['DISPLAY_PROPERTIES']['JOB']['VALUE']?></div>
                                        <? endif; ?>
                                        
                                        <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])):?>
					<div class="desc"><?=$arItem['FIELDS']['PREVIEW_TEXT']?></div>
                                        <? endif; ?>
                                        
                                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])):?>
					<div class="email"><a href="mailto:<?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?>"><?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?></a></div>
                                        <? endif; ?>
                                        
                                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])):?>
					<div class="phone"><?=$arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE']?></div>
                                        <? endif; ?>
                                        
                                        <?
                                        $arSocials = Array ();
                                        foreach (Array ('VK', 'FB', 'TWITTER') as $code)
                                        {
                                            if (!empty($arItem['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE']))
                                            {
                                                $arSocials[strtolower($code)] = $arItem['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE'];
                                            }
                                        }
                                        ?>
                                        <? if (!empty($arSocials)): ?>
					<div class="social">
                                            <? foreach ($arSocials as $code => $link): ?>
						<a href="<?=$link?>" target="_blank" class="link icon-custom <?=$code?>"></a>
                                            <? endforeach; ?>
					</div>
                                        <? endif; ?>
				</div>
                            <? endforeach; ?>
			</div>
		</div>
            <? endforeach; ?>

            <? if (!empty($arResult['ITEMS_EMPTY_SECTION'])): ?>
            <div class="department">
                <div class="items">
                <? foreach ($arResult['ITEMS_EMPTY_SECTION'] as $arItem): ?>
                    <div class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                    <?
                                    if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                                        $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                                        $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 250, 'height' => 250), BX_RESIZE_IMAGE_EXACT);
                                        ?>

                                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="img">
                                            <img src="<?= $preview['src'] ?>" alt="<?= $arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] ?>" alt="<?= $arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] ?>">
                                        </a>
                                    <? endif; ?>
                                    <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="name"><?= $arItem['FIELDS']['NAME'] ?></a>
                                    <? endif; ?>
                                    <? if (!empty($arItem['DISPLAY_PROPERTIES']['JOB']['VALUE'])): ?>
                                        <div class="position"><?= $arItem['DISPLAY_PROPERTIES']['JOB']['VALUE'] ?></div>
                                    <? endif; ?>

                                    <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])): ?>
                                        <div class="desc"><?= $arItem['FIELDS']['PREVIEW_TEXT'] ?></div>
                                    <? endif; ?>

                                    <? if (!empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])): ?>
                                        <div class="email"><a href="mailto:<?= $arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'] ?>"><?= $arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'] ?></a></div>
                                    <? endif; ?>

                                    <? if (!empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])): ?>
                                        <div class="phone"><?= $arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'] ?></div>
                                    <? endif; ?>

                                    <?
                                    $arSocials = Array();
                                    foreach (Array('VK', 'FB', 'TWITTER') as $code)
                                    {
                                        if (!empty($arItem['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE']))
                                        {
                                            $arSocials[strtolower($code)] = $arItem['DISPLAY_PROPERTIES'][$code . '_LINK']['VALUE'];
                                        }
                                    }
                                    ?>
                                        <? if (!empty($arSocials)): ?>
                                        <div class="social">
                                            <? foreach ($arSocials as $code => $link): ?>
                                                <a href="<?= $link ?>" target="_blank" class="link icon-custom <?= $code ?>"></a>
                                        <? endforeach; ?>
                                        </div>
                                <? endif; ?>
                                </div>
                <? endforeach; ?>
                </div>
            </div>
            <? endif; ?>
	</main>
<? endif; ?>