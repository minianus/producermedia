<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $key => $arItem)
{
    if (empty($arItem['PREVIEW_PICTURE']['ID']))
    {
        unset($arResult['ITEMS'][$key]);
        continue;
    }
    
    $arResizeParams = Array (
        'width' => 500,
        'height' => 500
    );
    $arResizeImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL, true);
    
    $arResult['ITEMS'][$key]['PROPERTIES']['LINK']['VALUE'] = str_replace("#SITE_DIR#", SITE_DIR, $arItem['PROPERTIES']['LINK']['VALUE']);
    $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $arResizeImage['src'];
}