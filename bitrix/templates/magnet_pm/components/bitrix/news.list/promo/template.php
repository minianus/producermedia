<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if (!empty($arResult['ITEMS'])): ?>
    <div class="promo" data-helper="homepage::promo">
        <div class="items swiper-container">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>" class="item swiper-slide"
                       id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <span class="background" data-lazy="default"
                              data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"></span>
                        <span class="gradient"></span>
                        <? if (!empty($arItem['PROPERTIES']['SUBTITLE']['VALUE'])): ?>
                            <span class="label"><?= $arItem['PROPERTIES']['SUBTITLE']['VALUE'] ?></span>
                        <? endif; ?>
                        <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                            <span class="text"><?= $arItem['PREVIEW_TEXT'] ?></span>
                        <? endif; ?>
                    </a>
                <? endforeach; ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>


    <script>


        var promoSlider = new Swiper('.promo .swiper-container', {
            slidesPerView: 1,
            // init: false,
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                320: {
                    sliderPerView:1,
                    spaceBetween: 12,
                },
                375: {
                    sliderPerView:1,
                    spaceBetween: 24,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 24,
                },
                1271: {
                    slidesPerView: 3,
                    spaceBetween: 24,
                },


            }
        });

        // function sld() {
        //     console.log("ghgh");
        //     if ($(window).width() < 1271) {
        //         $('.promo .items').addClass('swiper-container');
        //         // promoSlider.init();
        //     } else {
        //       $('.promo .items').removeClass('swiper-container');
        //         // promoSlider.destroy();
        //     }

        //     console.log($(window).width() < 1271);
        // }


        // sld();
        // $(window).resize(sld);


    </script>

<? endif; ?>
