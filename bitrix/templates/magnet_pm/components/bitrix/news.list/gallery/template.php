<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;

$strRand = randString(5) . "_gallery";
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>

<? if (!empty($arResult['ITEMS']) || !empty($arResult['ITEMS_EMPTY_SECTION'])): ?>
    <div class="gallery" id="<?=$strRand?>" data-helper="gallery::list">
        <main class="content">
            <? foreach ($arResult['ITEMS'] as $arSection): ?>
            <?
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
            <div class="gallery-category" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                <div id="<?=strtolower($arSection['CODE'])?>">
                <div class="title"><?=$arSection['NAME']?></div>
                <? if (!empty($arSection['DESCRIPTION'])): ?>
                <div class="desc">
                    <?=$arSection['DESCRIPTION']?>
                </div>
                <? endif; ?>
                <div class="items">
                    <? foreach ($arSection['ITEMS'] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                    <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <? if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                            $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                            $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array ('width' => 200, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                        ?>
                        <a href="<?=$arItem['FIELDS']['PREVIEW_PICTURE']['SRC']?>" data-lightbox="category-<?=$arSection['ID']?>" data-title="<?= strip_tags($arItem['FIELDS']['NAME'])?>" class="img">
                            <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>">
                        </a>
                        <? endif; ?>
                        <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                        <div class="name"><?=$arItem['FIELDS']['NAME']?></div>
                        <? endif; ?>
                        <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])): ?>
                        <div class="description"><?=$arItem['FIELDS']['PREVIEW_TEXT']?></div>
                        <? endif; ?>
                        </div>
                    <? endforeach; ?>
                </div>
                </div>
            </div>
                        
            <? endforeach; ?>
            
            <? if (!empty($arResult['ITEMS_EMPTY_SECTION'])): ?>
            <div class="gallery-category">
                <div class="items">
                <? foreach ($arResult['ITEMS_EMPTY_SECTION'] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <? if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                                $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                                $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array ('width' => 200, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                            ?>
                            <a href="<?=$arItem['FIELDS']['PREVIEW_PICTURE']['SRC']?>" data-lightbox="category-<?=$arSection['ID']?>" data-title="<?= strip_tags($arItem['FIELDS']['NAME'])?>" class="img">
                                <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>">
                            </a>
                        <? endif; ?>
                        <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                            <div class="name"><?=$arItem['FIELDS']['NAME']?></div>
                        <? endif; ?>
                        <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])): ?>
                            <div class="description"><?=$arItem['FIELDS']['PREVIEW_TEXT']?></div>
                        <? endif; ?>
                    </div>
                <? endforeach; ?>
                </div>
            </div>
            <? endif; ?>
        </main>
    </div>
<? endif; ?>
