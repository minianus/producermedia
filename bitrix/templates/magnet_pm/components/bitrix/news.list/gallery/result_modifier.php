<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

CSolution::getInstance(SITE_ID);
$arBufResult = $arNewResult = Array ();
if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if (!empty($arItem['IBLOCK_SECTION_ID']))
            $arBufResult[$arItem['IBLOCK_SECTION_ID']][] = $arItem;
        else
            $arResult['ITEMS_EMPTY_SECTION'][] = $arItem;
    }
    
    
    $arSections = CCache::CIBlockSection_GetList(Array ($arParams['SORT_BY1'] => $arParams['SORT_ORDER1']), Array ("IBLOCK_ID" => $arParams['IBLOCK_ID'], "GLOBAL_ACTIVE" => "Y"));
    
    foreach ($arSections as $arSection)
    {
        if (!empty($arBufResult[$arSection['ID']]))
        {
            $arButtons = CIBlock::GetPanelButtons(
                $arSection["IBLOCK_ID"],
                0,
                $arSection["ID"],
                false
            );
            
            $arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
            $arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];
        
            $arNewResult[] = array_merge($arSection, Array ('ITEMS' => $arBufResult[$arSection['ID']]));
        }
    }
    
    $arResult['ITEMS'] = $arNewResult;
}
