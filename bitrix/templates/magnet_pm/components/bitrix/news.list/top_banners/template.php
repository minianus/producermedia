<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<? if (!empty($arResult['ITEMS_COUNT'])): ?>
<? if ($arParams['VIEW_MODE'] == 'type3'):
    include(__DIR__ . "/template_center.php");
else: ?>
<div data-helper="homepage::top_banners" class="banners<?=$arParams['VIEW_MODE'] == 'type2' ? ' static-left' : ''?><?=(!empty($arResult['ITEMS']['SMALL'])) ? ' has-static' : ''?>">
    <? if (!empty($arResult['ITEMS']['BIG'])): ?>
    <div class="carousel">
    	<div class="owl-carousel swiper-container" id="top_banner_carousel" data-helper="homepage::top_banners::carousel"> 
            <div class="swiper-wrapper">
            
                <? foreach ($arResult['ITEMS']['BIG'] as $arItem): ?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                <?if($arItem['IS_LINK_BANNER'] == 'Y'):?>
                    <a href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE']?>"
                <? else: ?>
                    <div
                <? endif; ?>
                        class="item swiper-slide <?=implode(" ", $arItem['CLASSES'])?>" data-background="<?=$arItem['BACKGROUND_IMAGE']?>" data-mobile-background="<?=$arItem['MOBILE_BACKGROUND_IMAGE']?>" data-video="<?=$arItem['VIDEO_BACKGROUND']?>" style="<?=implode(" ", $arItem['STYLES'])?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <div class="text">
                                    <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                                    <div class="name"><?=$arItem['PREVIEW_TEXT']?></div>
                                    <? endif; ?>
                                    <? if (!empty($arItem['BUTTON']) && $arItem['IS_LINK_BANNER'] !== 'Y'): ?>
                                    <a href="<?=$arItem['BUTTON']['LINK']?>" class="button<?=$arItem['BUTTON']['COLOR'] == 'BASE' ? ' base' : '' ?>"><?=$arItem['BUTTON']['TEXT']?></a>
                                    <? endif; ?>
                                </div>
                <?if($arItem['IS_LINK_BANNER'] == 'Y'):?>
                    </a>
                <? else: ?>
                            </div>
                <? endif; ?>
                <? endforeach; ?>
            </div>
            <div class="swiper-button-prev icon-custom"></div>
            <div class="swiper-button-next icon-custom"></div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="preloader">
			<svg width="200px" height="200px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-dual-ring" style="background: none;"><circle cx="50" cy="50" fill="none" stroke-linecap="round" r="40" stroke-width="4" stroke="#333" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(161.792 50 50)"></circle></svg>
        </div>
    </div>
    <? endif; ?>
    <? if (!empty($arResult['ITEMS']['SMALL'])): ?>
        <div class="static" data-helper="homepage::top_banners::static" >
            <? $counter = 0;
               foreach ($arResult['ITEMS']['SMALL'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            	<? if ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] = 'LARGE'): ?>
            		<? $counter += 200;?>
            	<? elseif ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] = 'MEDIUM'): ?>
            		<? $counter += 100;?>
            	<? elseif ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] = 'SMALL'): ?>
            		<? $counter += 50;?>
            	<? endif; ?>
            	<? if ($counter > 200):?> 
            		<? //break; ?>
            	<? endif; ?> 
                <a href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE']?>" data-background="<?=$arItem['BACKGROUND_IMAGE']?>" data-mobile-background="<?=$arItem['MOBILE_BACKGROUND_IMAGE']?>" class="item <?=implode(" ", $arItem['CLASSES'])?>" style="<?=implode(" ", $arItem['STYLES'])?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="text">
                        <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                            <div class="name"><?=$arItem['PREVIEW_TEXT']?></div>
                            <? endif; ?>
                            
                    </div>
                </a>
            <? endforeach; ?>
            
           
            </div>
    <? endif; ?>
</div>
<? endif; ?>
<? endif; ?>
