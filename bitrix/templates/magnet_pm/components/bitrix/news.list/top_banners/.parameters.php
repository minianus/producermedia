<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arTemplateParameters["VIEW_MODE"] = Array(
    "PARENT" => "VISUAL",
    "NAME" => GetMessage("T_VIEW_MODE"),
    "TYPE" => "LIST",
    "DEFAULT" => "type1",
    "VALUES" => Array(
        "type1" => GetMessage('T_VIEW_MODE_TYPE1'),
        "type2" => GetMessage('T_VIEW_MODE_TYPE2'),
        "type3" => GetMessage('T_VIEW_MODE_TYPE3'),
    )
);
