<? $sCount = 0; ?>
<div data-helper="homepage::top_banners" class="banners<?=(!empty($arResult['ITEMS']['SMALL'])) ? ' has-static' : ''?> center">
	<? if (count($arResult['ITEMS']['SMALL']) > 0): ?>
	<div class="static" data-helper="homepage::top_banners::static">
	    <? for ($sCount; $sCount < 2; $sCount++): 
                if (!$arItem = $arResult['ITEMS']['SMALL'][$sCount])
                    break;
	    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	    ?>
	        <a href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE']?>" data-background="<?=$arItem['BACKGROUND_IMAGE']?>" data-mobile-background="<?=$arItem['MOBILE_BACKGROUND_IMAGE']?>" class="item small <?=implode(" ", $arItem['CLASSES'])?>" style="<?=implode(" ", $arItem['STYLES'])?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	            <div class="text">
	                <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
	                    <div class="name"><?=$arItem['PREVIEW_TEXT']?></div>
	                <? endif; ?>
	            </div>
	        </a>
	    <? endfor; ?>
	</div>
	 <? endif; ?>

    <? if (!empty($arResult['ITEMS']['BIG'])): ?>
    <div class="carousel">
    	<div class="owl-carousel" id="top_banner_carousel" data-helper="homepage::top_banners::carousel">
        <div class="swiper-wrapper">

            <? foreach ($arResult['ITEMS']['BIG'] as $arItem): ?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
            <?if($arItem['IS_LINK_BANNER'] == 'Y'):?>
                <a href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE']?>"
            <? else: ?>
                <div
            <? endif; ?>
                    class="item swiper-slide <?=implode(" ", $arItem['CLASSES'])?>" style="<?=implode(" ", $arItem['STYLES'])?>" data-background="<?=$arItem['BACKGROUND_IMAGE']?>" data-mobile-background="<?=$arItem['MOBILE_BACKGROUND_IMAGE']?>" data-video="<?=$arItem['VIDEO_BACKGROUND']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <div class="text">
                                <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                                <div class="name"><?=$arItem['PREVIEW_TEXT']?></div>
                                <? endif; ?>
                                <? if (!empty($arItem['BUTTON']) && $arItem['IS_LINK_BANNER'] !== 'Y'): ?>
                                <a href="<?=$arItem['BUTTON']['LINK']?>" class="button<?=$arItem['BUTTON']['COLOR'] == 'BASE' ? ' base' : '' ?>"><?=$arItem['BUTTON']['TEXT']?></a>
                                <? endif; ?>
                            </div>
            <?if($arItem['IS_LINK_BANNER'] == 'Y'):?>
                </a>
            <? else: ?>
                        </div>
            <? endif; ?>
            <? endforeach; ?>
        </div>
        </div>
        <div class="preloader">
			<svg width="200px" height="200px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-dual-ring" style="background: none;"><circle cx="50" cy="50" fill="none" stroke-linecap="round" r="40" stroke-width="4" stroke="#333" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(161.792 50 50)"></circle></svg>
        </div>
    </div>
    <? endif; ?>

	<? if (count($arResult['ITEMS']['SMALL']) > 2): ?>
    <div class="static" data-helper="homepage::top_banners::static">
        <? for ($sCount; $sCount < 4; $sCount++): 
                if (!$arItem = $arResult['ITEMS']['SMALL'][$sCount])
                    break;
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
            <a href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE']?>" data-background="<?=$arItem['BACKGROUND_IMAGE']?>" data-mobile-background="<?=$arItem['MOBILE_BACKGROUND_IMAGE']?>" class="item small <?=implode(" ", $arItem['CLASSES'])?>" style="<?=implode(" ", $arItem['STYLES'])?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="text">
                    <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                        <div class="name"><?=$arItem['PREVIEW_TEXT']?></div>
                    <? endif; ?>
                </div>
            </a>
        <? endfor; ?>
    </div>
	<? endif; ?>
        
</div>