<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arNewItems = Array (
    'BIG' => Array (),
    'SMALL' => Array ()
);
$arResult['ITEMS_COUNT'] = 0;
foreach ($arResult['ITEMS'] as $arItem)
{
    $arResult['ITEMS_COUNT'] += 1;
    $typeID = $arItem['PROPERTIES']['TYPE']['VALUE_XML_ID'];
    if (isset($arNewItems[$typeID]))
    {
        $arStyles = Array ();
        if (!empty($arItem['PROPERTIES']['BACKGROUND_COLOR']['VALUE']))
            $arStyles[] = 'background-color:' . $arItem['PROPERTIES']['BACKGROUND_COLOR']['VALUE'] . ';';
        
        if (!empty($arItem['PROPERTIES']['MOBILE_BACKGROUND_IMAGE']['VALUE']))
            $arItem['MOBILE_BACKGROUND_IMAGE'] = CFile::GetPath($arItem['PROPERTIES']['MOBILE_BACKGROUND_IMAGE']['VALUE']);
        
        if (!empty($arItem['PROPERTIES']['BACKGROUND_IMAGE']['VALUE']))
            $arItem['BACKGROUND_IMAGE'] = CFile::GetPath($arItem['PROPERTIES']['BACKGROUND_IMAGE']['VALUE']);
        
        if (!empty($arItem['PROPERTIES']['VIDEO_BACKGROUND']['VALUE']))
            $arItem['VIDEO_BACKGROUND'] = CFile::GetPath($arItem['PROPERTIES']['VIDEO_BACKGROUND']['VALUE']);
        
        $arItem['STYLES'] = $arStyles;
        $arItem['PROPERTIES']['BUTTON_LINK']['VALUE'] = str_replace("#SITE_DIR#", SITE_DIR, $arItem['PROPERTIES']['BUTTON_LINK']['VALUE']);
        
        if (!empty($arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']))
        {
            $arItem['BUTTON'] = Array (
                'TEXT' => $arItem['PROPERTIES']['BUTTON_TEXT']['VALUE'],
                'LINK' => $arItem['PROPERTIES']['BUTTON_LINK']['VALUE'],
                'COLOR' => $arItem['PROPERTIES']['BUTTON_COLOR']['VALUE_XML_ID']
            );
        }
        
        $arItem['CLASSES'] = Array ();

        if ($arItem['PROPERTIES']['TEXT_POSITION_HORIZONT']['VALUE_XML_ID'] == 'CENTER')
            $arItem['CLASSES'][] = 'center';
        elseif ($arItem['PROPERTIES']['TEXT_POSITION_HORIZONT']['VALUE_XML_ID'] == 'RIGHT')
            $arItem['CLASSES'][] = 'right';
        
        if ($arItem['PROPERTIES']['TEXT_POSITION_VERTICAL']['VALUE_XML_ID'] == 'TOP')
            $arItem['CLASSES'][] = 'top';
        elseif ($arItem['PROPERTIES']['TEXT_POSITION_VERTICAL']['VALUE_XML_ID'] == 'BOTTOM')
            $arItem['CLASSES'][] = 'bottom';
        
        if ($arParams['VIEW_MODE'] != 'type3')
        {
            if ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] == 'LARGE')
            {
                $arItem['CLASSES'][] = 'large';
                $arItem['WEIGHT'] = 4;
            }
            elseif ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] == 'MEDIUM')
            {
                $arItem['CLASSES'][] = 'medium';
                $arItem['WEIGHT'] = 2;
            }
            elseif ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'] == 'SMALL' || empty($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID']))
            {
                $arItem['CLASSES'][] = 'small';
                $arItem['WEIGHT'] = 1;
            }
            
        }
        
        if ($arItem['PROPERTIES']['TEXT_COLOR']['VALUE_XML_ID'] == 'BLACK')
            $arItem['CLASSES'][] = 'black';
        
        if ($arItem['PROPERTIES']['HIDE_TEXT_ON_MOBILE']['VALUE_XML_ID'] == "Y")
            $arItem['CLASSES'][] = 'hide-mobile-text';
        
        if ($arItem['PROPERTIES']['LINK_BANNER']['VALUE_XML_ID'] === 'Y')
            $arItem['IS_LINK_BANNER'] = 'Y';
        
        $arNewItems[$typeID][] = $arItem;
    }
    
}

if (!empty($arNewItems['SMALL']) && $arParams['VIEW_MODE'] != 'type3')
{

    if (count($arNewItems['SMALL']) > 0)
    {
        $arMap = Array (0 => 2, 1 => 2);
        $tmpArNewItems = Array ();
        $bFullFill = false;
        $key = 0;
        
        for ($row = 0; $row < count($arMap); $row++)
        {
            if ($bFullFill)
                break;
            
            for ($key; $key < count($arNewItems['SMALL']); $key++)
            {
                if ($arNewItems['SMALL'][$key]['WEIGHT'] >= 4)
                {
                    if ($row == 0 && $key == 0)
                    {
                        $tmpArNewItems[] = $arNewItems['SMALL'][$key];
                        $bFullFill = true;
                        break;
                    }
                }
                else
                {
                    if ($arMap[$row] < $arNewItems['SMALL'][$key]['WEIGHT'])
                        break;
                    
                    $arMap[$row] -= $arNewItems['SMALL'][$key]['WEIGHT'];
                    $tmpArNewItems[] = $arNewItems['SMALL'][$key];
                }
            }
        }
        
       
    }
    
    $arNewItems['SMALL'] = $tmpArNewItems;
    unset($tmpArNewItems);
}


$arResult['ITEMS'] = $arNewItems;


