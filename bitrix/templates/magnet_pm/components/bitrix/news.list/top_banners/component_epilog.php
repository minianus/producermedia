<?php
use \Nextype\Magnet\CSolution;
CSolution::getInstance(SITE_ID);
?>

<script>
$(document).ready(function () {
    
    var mobileDetect = new MobileDetect(window.navigator.userAgent);
    var setSlideBackground = function (item) {
        if (!!item && item.data('init') === undefined) {
            if (!item.hasClass('black'))
                $("#top_banner_carousel").removeClass('dark');
            else
                $("#top_banner_carousel").addClass('dark');

            if ((!!mobileDetect.mobile() || document.documentElement.clientWidth <= 480) && item.data('mobile-background') !== undefined) {
                item.css({backgroundImage: 'url(' + item.data('mobile-background') + ')'});
            } else if (item.data('video') !== undefined && item.data('video') != "") {
                item.prepend($('<video loop="loop" autoplay="autoplay" muted="muted" class="video-bg" src="'+item.data('video')+'"></video>'));
            } else if (item.data('background') !== undefined) {
                item.css({backgroundImage: 'url(' + item.data('background') + ')'});
            }
            
            item.attr('data-init', '');
        }
    };
    
    $(".banners.has-static .static .item").each(function () {
        setSlideBackground($(this));
    }); 

    var productsSwiper = new Swiper('#top_banner_carousel', {
        slidesPerView: 1,
        <?if(CSolution::$options['HOMEPAGE_SLIDER_AUTOPLAY'] == 'Y'):?> 
            autoplay: {
                delay: <?= (!empty(CSolution::$options['HOMEPAGE_SLIDER_AUTOPLAY_TIMEOUT'])) ? intval(CSolution::$options['HOMEPAGE_SLIDER_AUTOPLAY_TIMEOUT']) : '5000' ?>
            },
        <?else: ?>
            autoplay: false,
        <?endif?>
        navigation: {
            nextEl: '#top_banner_carousel .swiper-button-next',
            prevEl: '#top_banner_carousel .swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
        },
        on: {
            init: function (){
                setSlideBackground.call(event, $('#top_banner_carousel .swiper-slide-active'))
            },
            slideChange: function () {
                setSlideBackground.call(event, $('#top_banner_carousel .swiper-slide-next'))
            }
        }
    });
});
</script>
