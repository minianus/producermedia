<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;
?>

<? if (!empty($arResult['ITEMS'])): ?>

<div class="items">

    <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
    
    <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <?
                if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID']) && $arParams['DISPLAY_PICTURE'] == 'Y'):
                    $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                    $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 600, 'height' => 400), BX_RESIZE_IMAGE_EXACT);
                ?>
                    <? if (empty($arItem['DETAIL_TEXT']) && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == "Y"): ?>
                    <div class="img">
                    <? else: ?>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="img">
                    <? endif; ?>
                        <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>" />
                    <? if (empty($arItem['DETAIL_TEXT']) && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == "Y"): ?>
                    </div>
                    <? else: ?>
                    </a>
                    <? endif; ?>
                <? endif; ?>
        
                <div class="content">
                    <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                    <div class="name">
                        <? if ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] != "Y" || (empty($arItem['DETAIL_TEXT']) && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == "Y")): ?>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <? endif; ?>
                            <?=$arItem['FIELDS']['NAME']?>
                        <? if ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] != "Y" || (empty($arItem['DETAIL_TEXT']) && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == "Y")): ?>
                        </a>
                        <? endif; ?>
                    </div>
                    <? endif; ?>
                    <? if (!empty($arItem['ACTIVE_FROM']) && $arParams['DISPLAY_DATE'] == 'Y'): ?>
                    <div class="date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                    <? endif; ?>
                    
                    <? if (!empty($arItem['PREVIEW_TEXT']) && $arParams['DISPLAY_PREVIEW_TEXT'] == "Y"): ?>
                    <div class="desc"><?=$arItem['PREVIEW_TEXT']?></div>
                    <? endif; ?>
                    
                    <? if ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] != "Y" || (empty($arItem['DETAIL_TEXT']) && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == "Y")): ?>
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn white"><?=GetMessage('DETAIL_LINK')?></a>
                    <? endif; ?>
                </div>
            </div>

    <? endforeach; ?>
</div>

<? endif; ?>

<? $this->SetViewTarget('news_pagination'); ?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<? $this->EndViewTarget(); ?>

