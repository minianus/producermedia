<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;
?>

<? if (!empty($arResult['ITEMS'])): ?>
    <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
    <div class="offer-wrap<?=$key==0 ? ' big' : ''?>">
        
    <?if (!empty($arItem['DETAIL_PAGE_URL'])):?>
        <a class="link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
    <?endif?>
        <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">                    
                <div class="static">
                    <?
                    if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                        $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                        $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 600, 'height' => 400), BX_RESIZE_IMAGE_EXACT);
                    ?>
                        <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>" />
                    <? endif; ?>

                    <? if ($arParams['SHOW_ACTIVE'] == "Y" && !empty($arItem['DATE_RANGE'])): ?>
                    <div class="date<?=$arItem['IS_DISABLED'] == 'Y' ? ' disable' : ''?>">
                            <?=$arItem['IS_DISABLED'] == 'Y' ? GetMessage('DISBLED_ACTION') : $arItem['DATE_RANGE']?>
                    </div>
                    <? endif; ?>

                    <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                    <span class="name"><?=$arItem['FIELDS']['NAME']?></span>
                    <? endif; ?>

                    <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])): ?>
                    <div class="desc"><?=$arItem['FIELDS']['PREVIEW_TEXT']?></div>
                    <? endif; ?>
                </div>
        </div>
            
    <?if (!empty($arItem['DETAIL_PAGE_URL'])):?>
        </a>
    <?endif?>
        
    </div>
    <? endforeach; ?>


<? endif; ?>

<? $this->SetViewTarget('news_pagination'); ?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<? $this->EndViewTarget(); ?>
<script>
    BX.ready(function(){
        window.CSolution.setProductsHeight($('.offer-wrap .item'));
        $(window).resize(function(){
                window.CSolution.setProductsHeight($('.offer-wrap .item'));
        })
    })
</script>