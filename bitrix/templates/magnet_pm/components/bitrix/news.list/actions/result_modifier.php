<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$arParams['RANGE_DATE_FORMAT'] = !empty($arParams['RANGE_DATE_FORMAT']) ? $arParams['RANGE_DATE_FORMAT'] : "d F";

if ($arParams['SHOW_ELEMENTS_WO_REGION_LINK'] == 'Y' && \Nextype\Magnet\CSolution::$options['LOCATIONS_ENABLED'] == 'Y')
{
    $arElementFilter = array(
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'ACTIVE' => ($arParams['CHECK_DATES'] === true || $arParams['CHECK_DATES'] == 'Y') ? 'Y' : 'N',
        'PROPERTY_REGION' => false
    );
    $arSort = array(
        $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
        $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"],
    );

    $arElements = \Nextype\Magnet\CCache::CIBlockElement_GetList($arSort, $arElementFilter, true);
    if (!empty($arElements))
    {
        foreach ($arElements as $arElement)
        {
            $arElement['FIELDS'] = array(
                'PREVIEW_PICTURE' => array('ID' => $arElement['PREVIEW_PICTURE']),
                'NAME' => $arElement['NAME'],
                'PREVIEW_TEXT' => $arElement['PREVIEW_TEXT'],
            );
            $arElement['DISPLAY_PROPERTIES'] = array(
                'DATE_START' => array('VALUE' => $arElement['PROPERTIES']['DATE_START']['VALUE']),
                'DATE_END' => array('VALUE' => $arElement['PROPERTIES']['DATE_END']['VALUE']),
            );
            $arResult['ITEMS'][(int)$arElement['ID']] = $arElement;
        }
        
        uasort($arResult['ITEMS'], 
                function($a, $b)
                {
                    $result = 0;
                    foreach ($arSort as $by => $order)
                    {
                        if ($a[$by] == $b[$by])
                            continue;
                        $result = ($a[$by] < $b[$by]) ? -1 : 1;
                        if ($order == 'desc')
                            $result = -$result;
                    }
                    return $result;
                }
        ); 
    }
}

if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_START']) || !empty($arItem['DISPLAY_PROPERTIES']['DATE_END']))
        {
            $arDateRange = Array ();
            
            if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_START']['VALUE']))
                $arDateRange['DATE_START'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_START']['VALUE']));
            
            if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']))
                $arDateRange['DATE_END'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']));
            
            if ($arDateRange['DATE_START'] && $arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = implode(" - ", $arDateRange);
            elseif ($arDateRange['DATE_START'] && !$arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = GetMessage("FROM_DATE", Array ("#DATE#" => $arDateRange['DATE_START']));
            elseif (!$arDateRange['DATE_START'] && $arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = GetMessage("TO_DATE", Array ("#DATE#" => $arDateRange['DATE_END']));

            if (!empty(MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE'])) && MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']) < time())
            {
                $arResult['ITEMS'][$key]['IS_DISABLED'] = "Y";
            }
        }
    }
}