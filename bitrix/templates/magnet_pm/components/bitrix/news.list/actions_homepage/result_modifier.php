<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$arParams['RANGE_DATE_FORMAT'] = !empty($arParams['RANGE_DATE_FORMAT']) ? $arParams['RANGE_DATE_FORMAT'] : "d F";

if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_START']) || !empty($arItem['DISPLAY_PROPERTIES']['DATE_END']))
        {
            $arDateRange = Array ();
            
            if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_START']['VALUE']))
                $arDateRange['DATE_START'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_START']['VALUE']));
            
            if (!empty($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']))
                $arDateRange['DATE_END'] = FormatDate($arParams['RANGE_DATE_FORMAT'], MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']));
            
            if ($arDateRange['DATE_START'] && $arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = implode(" - ", $arDateRange);
            elseif ($arDateRange['DATE_START'] && !$arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = GetMessage("FROM_DATE", Array ("#DATE#" => $arDateRange['DATE_START']));
            elseif (!$arDateRange['DATE_START'] && $arDateRange['DATE_END'])
                $arResult['ITEMS'][$key]['DATE_RANGE'] = GetMessage("TO_DATE", Array ("#DATE#" => $arDateRange['DATE_END']));

            if (!empty(MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE'])) && MakeTimeStamp($arItem['DISPLAY_PROPERTIES']['DATE_END']['VALUE']) < time())
            {
                $arResult['ITEMS'][$key]['IS_DISABLED'] = "Y";
            }
        }
    }
}