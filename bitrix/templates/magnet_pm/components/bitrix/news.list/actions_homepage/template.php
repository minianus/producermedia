<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$strRandCarousel = randString(5) . "_carousel";

use Nextype\Magnet\CSolution;
?>

<? if (!empty($arResult['ITEMS'])): ?>
<div class="actions section" data-helper="homepage::actions">
	<div class="offers swiper-container" id="<?=$strRandCarousel?>">
		<div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
		
		
		
		<div class="offer-wrap swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="item">
				<div class="static">
                                    <? if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                                        $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                                        $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 400, 'height' => 250), BX_RESIZE_IMAGE_EXACT);
                                    ?>
                                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="img">
                                        <img data-lazy="default" data-src="<?=$preview['src']?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg" alt="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" title="<?=$arSeoData['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']?>" />
                                    </a>
                                    <? endif; ?>
                                    
                                    <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name"><?=$arItem['FIELDS']['NAME']?></a>
                                    <? endif; ?>
                                    
                                    <? if ($arParams['SHOW_ACTIVE'] == "Y" && !empty($arItem['DATE_RANGE'])): ?>
                                    <div class="date<?=$arItem['IS_DISABLED'] == 'Y' ? ' disable' : ''?>">
                                            <?=$arItem['IS_DISABLED'] == 'Y' ? GetMessage('DISBLED_ACTION') : $arItem['DATE_RANGE']?>
                                    </div>
                                    <? endif; ?>
                                    <? if (!empty($arItem['FIELDS']['PREVIEW_TEXT'])): ?>
                                        <div class="desc"><?=$arItem['FIELDS']['PREVIEW_TEXT']?></div>
                                    <? endif; ?>
                                    
				</div>
				<div class="hover"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn"><?=GetMessage('DETAIL_LINK')?></a></div>
			</div>
		</div>
            <? endforeach; ?>
		</div>	
	</div>
	<script>
		var actionsSwiper = new Swiper('.actions .swiper-container', {
			
			spaceBetween: 24,
			breakpoints: {
				0:{
					slidesPerView:1,
				},
				461:{
					slidesPerView:2,
				},
				570:{
					slidesPerView:2,
				},
				768:{
					slidesPerView: 3,
				},
				1090:{
					slidesPerView: 4,
				},
				1475:{
					slidesPerView: 5,
				},
				1742:{
					slidesPerView: 6,
				},
				2140:{
					slidesPerView: 7,
				}
			},
		});
		// $('.offer-wrap.swiper-slide .static').css('height', $('.offer-wrap.swiper-slide .item').height());
		window.CSolution.setProductsHeight($('.offer-wrap.swiper-slide .item'));
		$(window).resize(function(){
			window.CSolution.setProductsHeight($('.offer-wrap.swiper-slide .item'));
		})
	</script>
</div>
<? endif; ?>