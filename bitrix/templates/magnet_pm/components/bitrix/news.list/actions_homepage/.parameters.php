<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    
        "SHOW_ACTIVE" => Array(
                "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_SHOW_ACTIVE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	
        "RANGE_DATE_FORMAT" => Array(
                "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_RANGE_DATE_FORMAT"),
		"TYPE" => "STRING",
		"DEFAULT" => "d F",
	),      
);
  