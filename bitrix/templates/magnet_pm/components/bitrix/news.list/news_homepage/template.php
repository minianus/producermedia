<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<? if (!empty($arResult['ITEMS'])): ?>
<section class="news" data-helper="homepage::news">
	<h2 class="title"><?=$arParams['BLOCK_TITLE']?></h2>
        <? if (!empty($arParams['BLOCK_MORE_TITLE'])): ?>
            <a href="<?= $arParams['BLOCK_MORE_LINK'] ?>" class="all"><?= $arParams['BLOCK_MORE_TITLE'] ?></a>
        <? endif; ?>
	
	<div class="previews<?=(count($arResult['ITEMS']) <= 2) ? ' line' : ''?>">
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
		<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="img"><img data-lazy="default" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>"></a>
			<div class="text">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name"><?=$arItem['NAME']?></a>
                            <? if (!empty($arItem['DISPLAY_ACTIVE_FROM'])): ?>
                            <div class="date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                            <? endif; ?>
                        </div>
		</div>
		<? endforeach; ?>
	</div>
</section>


<? endif; ?>
