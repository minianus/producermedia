<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $key => $arItem)
{
    if (empty($arItem['PREVIEW_PICTURE']['ID']))
    {
        $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = SITE_TEMPLATE_PATH . "/img/no-photo.png";
    }
    else
    {
    
        $arResizeParams = Array (
            'width' => 344,
            'height' => 258
        );
        $arResizeImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_EXACT, true);

        $arResult['ITEMS'][$key]['PROPERTIES']['LINK']['VALUE'] = str_replace("#SITE_DIR#", SITE_DIR, $arItem['PROPERTIES']['LINK']['VALUE']);
        $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $arResizeImage['src'];
    }
}

if (!empty($arParams['BLOCK_MORE_LINK']))
    $arParams['BLOCK_MORE_LINK'] = str_replace ("#SITE_DIR#", SITE_DIR, $arParams['BLOCK_MORE_LINK']);