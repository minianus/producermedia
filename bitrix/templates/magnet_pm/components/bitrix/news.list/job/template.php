<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<? if (!empty($arResult['ITEMS']) || !empty($arResult['ITEMS_EMPTY_SECTION'])): ?>
    <div class="jobs">
	<main class="jobs-list" data-helper="jobs::list">
            <? foreach ($arResult['ITEMS'] as $arSection): ?>
            <?
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
		<div class="department" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<div class="title"><?=$arSection['NAME']?></div>
                        <? if (!empty($arSection['DESCRIPTION'])): ?>
                        <div class="desc">
                            <?=$arSection['DESCRIPTION']?>
                        </div>
                        <? endif; ?>
			<div class="items">
                            <? foreach ($arSection['ITEMS'] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
				<div class="item">
					<a href="javascript:void(0)" class="name"><?=$arItem['NAME']?></a>
					<div class="description">
                                            <?= $arItem['PREVIEW_TEXT'] ?>
					</div>
				</div>
                            <? endforeach; ?>
			</div>
		</div>
            <? endforeach; ?>
		
	</main>
</div>
<? endif; ?>