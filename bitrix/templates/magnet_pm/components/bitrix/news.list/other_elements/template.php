<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Nextype\Magnet\CSolution;
?>

<? if (!empty($arResult['ITEMS'])): ?>

<div class="other-elements">
            <div class="name"><?=$arParams['HEADER_TITLE']?></div>
            <div class="items">
                <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <?
                    if (!empty($arItem['FIELDS']['PREVIEW_PICTURE']['ID'])):
                        $arSeoData = CSolution::getSeoData($arItem['IBLOCK_ID'], $arItem['ID']);
                        $preview = CFile::ResizeImageGet($arItem['FIELDS']['PREVIEW_PICTURE']['ID'], Array('width' => 240, 'height' => 160), BX_RESIZE_IMAGE_EXACT);
                    ?>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="img">
                        <img src="<?=$preview['src']?>" alt="<?=$arSeoData['ALT']?>" title="<?=$arSeoData['TITLE']?>">
                    </a>
                    <? endif; ?>
                    
                    <div class="text">
                        <? if (!empty($arItem['FIELDS']['NAME'])): ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="title"><?=$arItem['FIELDS']['NAME']?></a>
                        <? endif; ?>
                        
                        <? if ($arParams['SHOW_ACTIVE'] == "Y" && !empty($arItem['DATE_RANGE'])): ?>
                        <div class="date<?=$arItem['IS_DISABLED'] == 'Y' ? ' disable' : ''?>">
                                <?=$arItem['IS_DISABLED'] == 'Y' ? GetMessage('DISBLED_ACTION') : $arItem['DATE_RANGE']?>
                        </div>
                        <? endif; ?>
                        
                    </div>
                </div>
                <? endforeach; ?>
                
            </div>
        </div>
<? endif; ?>
