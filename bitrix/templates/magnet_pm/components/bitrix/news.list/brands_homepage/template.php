<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$strRandCarousel = randString(5) . "_carousel";
?>

<? if (!empty($arResult['ITEMS'])): ?>
<div class="brands main-nav" data-helper="homepage::brands">
	<div class="items swiper-container" id="<?=$strRandCarousel?>">
		<div class="swiper-wrapper">

		
				<? foreach ($arResult['ITEMS'] as $arItem): ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
			<div class="item swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<? if (!empty($arItem['DETAIL_PAGE_URL'])): ?>
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<? endif; ?>
						<img data-lazy="default" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" src="<?= SITE_TEMPLATE_PATH?>/img/lazy-image.png" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>" />
						<? if (!empty($arItem['DETAIL_PAGE_URL'])): ?>
						</a>
						<? endif; ?>
					</div>
			<? endforeach; ?>     
		</div>
		<div class="swiper-button-prev icon-custom"></div>
		<div class="swiper-button-next icon-custom"></div>
	</div>
        <script>
			var brandsSwiper = new Swiper('.brands .swiper-container', {
				
				spaceBetween: 15,
				loop: <?=count($arResult['ITEMS']) > 8 ? 'true' : 'false'?>,
				simulateTouch: <?=count($arResult['ITEMS']) > 8 ? 'true' : 'false'?>,
				breakpoints: {
					0:{
						slidesPerView:1,
						spaceBetween:0
					},
					420:{
						slidesPerView:2
					},
					570:{
						slidesPerView:3
					},
					760:{
						slidesPerView: 4
					},
					1090:{
						slidesPerView: 5
					},
					1230:{
						slidesPerView: 6
					},
					1450:{
						slidesPerView:7
					},
					1600:{
						slidesPerView: 8
					}
				},
				on: {
					slideChange: function (event) {
						var lazyImg = $('.brands .swiper-container').find('[data-lazy="default"]');
						if (lazyImg.length > 0) {
							lazyImg.each(function () {
								$(this).attr('src', $(this).data('src')).removeAttr('data-src data-lazy');
							});
						}
					},
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		
		
	</script>
</div>


<? endif; ?>
