<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as $key => $arItem)
{
    if (empty($arItem['PREVIEW_PICTURE']['ID']))
    {
        unset($arResult['ITEMS'][$key]);
        continue;
    }
    
    $arResizeParams = Array (
        'width' => 500,
        'height' => 500
    );
    $arResizeImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
    
    if ($arItem['PROPERTIES']['HOMEPAGE_ACTION']['VALUE_XML_ID'] == "OTHER")
        $arResult['ITEMS'][$key]['DETAIL_PAGE_URL'] = str_replace("#SITE_DIR#", SITE_DIR, $arItem['PROPERTIES']['HOMEPAGE_LINK']['VALUE']);
    elseif ($arItem['PROPERTIES']['HOMEPAGE_ACTION']['VALUE_XML_ID'] == "NOTHING")
        $arResult['ITEMS'][$key]['DETAIL_PAGE_URL'] = false;
    
    $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $arResizeImage['src'];
}