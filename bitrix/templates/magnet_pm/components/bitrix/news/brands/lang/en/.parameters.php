<?php
$MESS['PRICE_CODE'] = "Price type";
$MESS['HIDE_NOT_AVAILABLE'] = "Unavailable products";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "don't show";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "Show at the end";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "show in the general list";
$MESS['STORES'] = "Storage";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Number of related items to display";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Block title \"Related Products\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Related Products";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "View of the list of products";
$MESS['T_SHOW_LINK_PRODUCTS'] = "Show related products";