<?php
$MESS['PRICE_CODE'] = "Тип цены";
$MESS['HIDE_NOT_AVAILABLE'] = "Недоступные товары";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "не отображать";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "отображать в конце";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "отображать в общем списке";
$MESS['STORES'] = "Склады";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Количество связанных товаров для отображения";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Заголовок блока \"Связанные товары\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Связанные товары";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "Вид отображения списка товаров";
$MESS['T_SHOW_LINK_PRODUCTS'] = "Показывать связанные товары";