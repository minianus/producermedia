<?php

$MESS['T_SHOW_FILTER_BY_YEAR'] = "Filtering by year";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Show item date";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Show an image for the announcement";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Show ad text";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Show sharing buttons in social networks";
$MESS['T_SHOW_LINK_PRODUCTS'] = "Show related products";
$MESS['T_LINK_PRODUCTS_TITLE'] = "Block title \"Related products\"";
$MESS['T_LINK_PRODUCTS_TITLE_DEFAULT'] = "Products from this news";
$MESS["T_IBLOCK_DISPLAY_PREVIEW_TEXT_ON_DETAIL"] = "Show ad text on the detail page";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "View block \"Related products\"";

$MESS['PRICE_CODE'] = "Price Type";
$MESS['HIDE_NOT_AVAILABLE'] = "Unavailable products";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "don't show";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "Show at the end";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "show in the general list";
$MESS['STORES'] = "Storage";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Number of related items to show";