<?php

$MESS['T_SHOW_FILTER_BY_YEAR'] = "Фильтрация по годам";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Выводить дату элемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Выводить изображение для анонса";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Выводить текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Отображать кнопки шаринга в социальных сетях";
$MESS['T_SHOW_LINK_PRODUCTS'] = "Показывать связанные товары";
$MESS['T_LINK_PRODUCTS_TITLE'] = "Заголовок блока \"Связанные товары\"";
$MESS['T_LINK_PRODUCTS_TITLE_DEFAULT'] = "Товары из этой новости";
$MESS["T_IBLOCK_DISPLAY_PREVIEW_TEXT_ON_DETAIL"] = "Выводить текст анонса на детальной странице";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "Вид блока \"Связанные товары\"";

$MESS['PRICE_CODE'] = "Тип цены";
$MESS['HIDE_NOT_AVAILABLE'] = "Недоступные товары";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "не отображать";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "отображать в конце";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "отображать в общем списке";
$MESS['STORES'] = "Склады";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Количество связанных товаров для отображения";