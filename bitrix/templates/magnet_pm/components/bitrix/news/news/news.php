<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
use Nextype\Magnet\CCache;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arParams["FILTER_NAME"] = !empty($arParams["FILTER_NAME"]) ? $arParams["FILTER_NAME"] : "arrNewsFilter";
CLocations::setFilter($arParams);

if ($arParams['SHOW_FILTER_BY_YEAR'] == "Y")
{
    $arFilterValues = Array ();
    $arItems = CCache::CIBlockElement_GetList(Array ('ID' => 'DESC'), Array ('ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['IBLOCK_ID']));
    foreach ($arItems as $arItem)
    {
        $year = !empty($arItem['ACTIVE_FROM']) ? date("Y", MakeTimeStamp($arItem['ACTIVE_FROM'])) : date("Y", MakeTimeStamp($arItem['DATE_CREATE']));
        $arFilterValues[$year] = $year;
    }
    
    if (!empty($arFilterValues))
    {
        $arFilterValues['all'] = GetMessage('ALL_LINK');
    }
    
    $curFilterValue = 'all';
    if (!empty($_REQUEST['year']) && isset($arFilterValues[$_REQUEST['year']]))
    {
        $curFilterValue = $_REQUEST['year'];
        if (intval($curFilterValue) > 0)
        {
            $GLOBALS[$arParams["FILTER_NAME"]]['>DATE_ACTIVE_FROM'] = ConvertTimeStamp(strtotime(intval($curFilterValue) . "-01-01 00:00:00"), "FULL");
            $GLOBALS[$arParams["FILTER_NAME"]]['<=DATE_ACTIVE_FROM'] = ConvertTimeStamp(strtotime(intval($curFilterValue) . "-12-31 23:59:59"), "FULL");
        }
    }
    
}
?>

<div class="news">
    <div class="news-previews" data-helper="news::list">
        <? if ($arParams['SHOW_FILTER_BY_YEAR'] == "Y" && !empty($arFilterValues)): ?>
        <div class="tabs">
                <? foreach ($arFilterValues as $key => $value): ?>
			<a href="<?=$APPLICATION->GetCurPageParam('year=' . $key, Array ('year'))?>" class="tab<?=$curFilterValue==$key ? ' active' : ''?>"><?=$value?></a>
                <? endforeach; ?>
	</div>
        <? endif; ?>
        
    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"news",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
	),
	$component
);?>

    </div>
</div>

<? $APPLICATION->ShowViewContent('news_pagination'); ?>