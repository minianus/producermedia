<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Loader,
	Bitrix\Main\Web\Json,
	Bitrix\Iblock,
	Bitrix\Catalog,
	Bitrix\Currency;

$catalogIncluded = Loader::includeModule('catalog');

$arTemplateParameters = array(
	"SHOW_FILTER_BY_YEAR" => Array(
                "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_SHOW_FILTER_BY_YEAR"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
    
        "DISPLAY_DATE" => Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
    	),
        "DISPLAY_PREVIEW_TEXT_ON_DETAIL" => Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_IBLOCK_DISPLAY_PREVIEW_TEXT_ON_DETAIL"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
    	),
    
        
	"USE_SHARE" => Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_USE_SHARE"),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"VALUE" => "Y",
		"DEFAULT" =>"N",
		"REFRESH"=> "Y",
	),
);

if ($catalogIncluded)
{
    $arSort = CIBlockParameters::GetElementSortFields(
	array('SHOWS', 'SORT', 'TIMESTAMP_X', 'NAME', 'ID', 'ACTIVE_FROM', 'ACTIVE_TO'),
	array('KEY_LOWERCASE' => 'Y')
    );

    $arPrice = array();
    if ($catalogIncluded)
    {
            $arOfferSort = array_merge($arSort, CCatalogIBlockParameters::GetCatalogSortFields());
            if (isset($arSort['CATALOG_AVAILABLE']))
                    unset($arSort['CATALOG_AVAILABLE']);
            $arPrice = CCatalogIBlockParameters::getPriceTypesList();
    }
    else
    {
            $arOfferSort = $arSort;
            $arPrice = $arProperty_N;
    }
    
    $arStore = array();
	$storeIterator = CCatalogStore::GetList(
		array(),
		array('ISSUING_CENTER' => 'Y'),
		false,
		false,
		array('ID', 'TITLE')
	);
	while ($store = $storeIterator->GetNext())
		$arStore[$store['ID']] = "[".$store['ID']."] ".$store['TITLE'];
    
        
        $arTemplateParameters["SHOW_LINK_PRODUCTS"] = Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_SHOW_LINK_PRODUCTS"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	);
        
        $arTemplateParameters["LINK_PRODUCTS_TITLE"] = Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_LINK_PRODUCTS_TITLE"),
		"TYPE" => "STRING",
		"DEFAULT" => GetMessage('T_LINK_PRODUCTS_TITLE_DEFAULT'),
	);
        
        $arTemplateParameters["LINK_PRODUCTS_TEMPLATE"] = Array(
            "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_LINK_PRODUCTS_TEMPLATE"),
		"TYPE" => "LIST",
		"DEFAULT" => "slider",
                "VALUES" => Array (
                    "main" => "main",
                    "slider" => "slider"
                )
	);
    
       
        
    $arTemplateParameters['PRICE_CODE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('PRICE_CODE'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'VALUES' => $arPrice,
    );
    
       
    $arTemplateParameters['HIDE_NOT_AVAILABLE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('HIDE_NOT_AVAILABLE'),
		'TYPE' => 'LIST',
		'DEFAULT' => 'N',
		'VALUES' => array(
			'Y' => GetMessage('HIDE_NOT_AVAILABLE_HIDE'),
			'L' => GetMessage('HIDE_NOT_AVAILABLE_LAST'),
			'N' => GetMessage('HIDE_NOT_AVAILABLE_SHOW')
		),
		'ADDITIONAL_VALUES' => 'N'
	);
    
    $arTemplateParameters['STORES'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('STORES'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => $arStore,
		'ADDITIONAL_VALUES' => 'Y'
	);
    
    $arTemplateParameters["LINK_PRODUCTS_ELEMENT_COUNT"] = array(
		"PARENT" => "VISUAL",
		"NAME"		=> GetMessage("LINK_PRODUCTS_ELEMENT_COUNT"),
		"TYPE"		=> "STRING",
		"DEFAULT"	=> 5
	);
    
    
    
}

