<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DETAIL_PAGE" => Array(
		"NAME" => GetMessage("T_DISPLAY_DETAIL_PAGE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	
);
