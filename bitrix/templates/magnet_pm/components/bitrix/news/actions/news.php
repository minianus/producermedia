<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arParams["FILTER_NAME"] = !empty($arParams["FILTER_NAME"]) ? $arParams["FILTER_NAME"] : "arrActionsFilter";
CLocations::setFilter($arParams);

if ($arParams['SHOW_FILTER_BY_ACTIVE'] == "Y" && $arParams['SHOW_ACTIVE'] == "Y")
{
    $bSetActive = false;
    $arFilters = Array (
        'current' => Array (
            'TITLE' => GetMessage('FILTER_CURRENT'),
        ),
        'archive' => Array (
            'TITLE' => GetMessage('FILTER_ARCHIVE'),
        ),
        'all' => Array (
            'TITLE' => GetMessage('FILTER_ALL'),
        )
    );
    
    if (isset($_REQUEST['filter']) && isset($arFilters[$_REQUEST['filter']]))
    {
        $currentDate = date("Y-m-d H:i:s");
        switch ($_REQUEST['filter'])
        {
            case 'current':
                $arFilters['current']['ACTIVE'] = true;
                $GLOBALS[$arParams["FILTER_NAME"]]['<=PROPERTY_DATE_START'] = $currentDate;
                $GLOBALS[$arParams["FILTER_NAME"]]['>=PROPERTY_DATE_END'] = $currentDate;
                $bSetActive = true;
                break;
            
            case 'archive':
                $arFilters['archive']['ACTIVE'] = true;
                $GLOBALS[$arParams["FILTER_NAME"]]['<PROPERTY_DATE_END'] = $currentDate;
                $bSetActive = true;
                break;
        }
    }
    
    if (!$bSetActive)
        $arFilters['all']['ACTIVE'] = true;
    

}
?>
<div class="actions">
    <? if (!empty($arFilters)): ?>
    <div class="tabs">
        <? foreach ($arFilters as $key => $arFilter): ?>
        <a href="<?=$APPLICATION->GetCurPageParam('filter=' . $key, Array ('filter'))?>" class="tab<?=$arFilter['ACTIVE'] ? ' active' : ''?>"><?=$arFilter['TITLE']?></a>
        <? endforeach; ?>
    </div>
    <? endif; ?>
    <main class="offers" data-helper="actions::list">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "actions",
            Array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                    "SORT_BY1" => $arParams["SORT_BY1"],
                    "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                    "SORT_BY2" => $arParams["SORT_BY2"],
                    "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                    "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                    "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                    "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "MESSAGE_404" => $arParams["MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                    "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                    "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                    "SHOW_ACTIVE" => $arParams['SHOW_ACTIVE'],
                    "SHOW_FILTER_BY_ACTIVE" => $arParams['SHOW_FILTER_BY_ACTIVE'],
                    "TEMPLATE_PRODUCTS_LIST" => $arParams['TEMPLATE_PRODUCTS_LIST'],
                    "RANGE_DATE_FORMAT" => $arParams['RANGE_DATE_FORMAT'],
                    "SHOW_ELEMENTS_WO_REGION_LINK" => $arParams['SHOW_ELEMENTS_WO_REGION_LINK']
            ),
            $component
    );?>
    </main>
    <? $APPLICATION->ShowViewContent('news_pagination'); ?>
</div>
