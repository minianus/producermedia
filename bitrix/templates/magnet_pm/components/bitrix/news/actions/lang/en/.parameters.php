<?php

$MESS['T_SHOW_ACTIVE'] = "Display activity of special offer";
$MESS['T_SHOW_FILTER_BY_ACTIVE'] = "Show activity filter";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "View of the list of products";
$MESS['T_RANGE_DATE_FORMAT'] = "The format displaying the date of activity of special offer";


$MESS['PRICE_CODE'] = "Price type";
$MESS['HIDE_NOT_AVAILABLE'] = "Unavailable products";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "don't show";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "show at the end";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "show in the general list";
$MESS['STORES'] = "Storage";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Number of related items to display";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Block title \"Related Products\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Related Products";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE'] = "Block title \"Other special offers\"";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE_DEFAULT'] = "Other special offers";