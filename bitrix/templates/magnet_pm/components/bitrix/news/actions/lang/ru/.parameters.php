<?php

$MESS['T_SHOW_ACTIVE'] = "Отображать активность акции";
$MESS['T_SHOW_ELEMENTS_WO_REGION_LINK'] = "Выводить акции, для которых не установлена привязка к региону";
$MESS['T_SHOW_FILTER_BY_ACTIVE'] = "Показывать фильтр по активности";
$MESS['T_LINK_PRODUCTS_TEMPLATE'] = "Вид отображения списка товаров";
$MESS['T_RANGE_DATE_FORMAT'] = "Формат вывода даты активности акции";


$MESS['PRICE_CODE'] = "Тип цены";
$MESS['HIDE_NOT_AVAILABLE'] = "Недоступные товары";
$MESS['HIDE_NOT_AVAILABLE_HIDE'] = "не отображать";
$MESS['HIDE_NOT_AVAILABLE_LAST'] = "отображать в конце";
$MESS['HIDE_NOT_AVAILABLE_SHOW'] = "отображать в общем списке";
$MESS['STORES'] = "Склады";
$MESS['LINK_PRODUCTS_ELEMENT_COUNT'] = "Количество связанных товаров для отображения";
$MESS['LINK_PRODUCTS_HEADER_TITLE'] = "Заголовок блока \"Связанные товары\"";
$MESS['LINK_PRODUCTS_HEADER_TITLE_DEFAULT'] = "Связанные товары";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE'] = "Заголовок блока \"Другие акции\"";
$MESS['T_OTHER_ELEMENTS_HEADER_TITLE_DEFAULT'] = "Другие акции";