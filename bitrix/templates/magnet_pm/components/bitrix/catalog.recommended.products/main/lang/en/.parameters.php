<?
$MESS["CATALOG_RECOMMENDED_PRODUCTS_LINE_ELEMENT_COUNT"] = "The number of elements displayed in one line";
$MESS["LINE_ELEMENT_COUNT_TIP"] = "The number of elements displayed in the line can be from 1 to 5";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_BLUE"] = "blue (default theme)";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_GREEN"] = "green";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_RED"] = "red";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_WOOD"] = "wood";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_YELLOW"] = "yellow";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_BLACK"] = "dark";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_TEMPLATE_THEME"] = "Color theme";
$MESS["CATALOG_RECOMMENDED_PRODUCTS_TPL_THEME_SITE"] = "Take the theme from the site settings (for solutions bitrix.eshop)";
$MESS["BX_CRP_SORT_ORDER_ASC"] = "ascending";
$MESS["BX_CRP_SORT_ORDER_DESC"] = "descending";
$MESS["BX_CRP_IBLOCK_ELEMENT_SORT_FIELD"] = "By which field we sort the elements";
$MESS["BX_CRP_IBLOCK_ELEMENT_SORT_ORDER"] = "Item sorting order";
$MESS["BX_CRP_IBLOCK_ELEMENT_SORT_FIELD2"] = "Secondary sorting field";
$MESS["BX_CRP_IBLOCK_ELEMENT_SORT_ORDER2"] = "Second sort order";
$MESS["BX_CRP_TPL_PRODUCT_DISPLAY_MODE"] = "Display scheme";
$MESS["BX_CRP_TPL_DML_SIMPLE"] = "simple mode";
$MESS["BX_CRP_TPL_DML_EXT"] = "extended";
?>