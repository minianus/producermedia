<?
$MESS["SEARCH_GO"] = "Search";
$MESS["CT_BSP_ADDITIONAL_PARAMS"] = "Advanced search options";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "In request \"#query#\" restored keyboard layout.";
$MESS["SEARCH_INPUT_PLACEHOLDER"] = "Enter search query";
$MESS['EMPTY_RESULT'] = "Sorry, but nothing found.";
?>