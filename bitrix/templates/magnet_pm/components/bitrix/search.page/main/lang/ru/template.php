<?
$MESS["SEARCH_GO"] = "Поиск";
$MESS["CT_BSP_ADDITIONAL_PARAMS"] = "Дополнительные параметры поиска";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "В запросе \"#query#\" восстановлена раскладка клавиатуры.";
$MESS["SEARCH_INPUT_PLACEHOLDER"] = "Поиск товара";
$MESS['EMPTY_RESULT'] = "Сожалеем, но ничего не найдено.";
?>