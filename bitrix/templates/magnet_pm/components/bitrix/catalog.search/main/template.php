<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;
use Nextype\Magnet\CLocations;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CSolution::getInstance(SITE_ID);

$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
}
else
{
	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
}

$arBasePriceType = Array ();
if (\Bitrix\Main\Loader::includeModule("sale") && \Bitrix\Main\Loader::includeModule("catalog") && in_array("PRICE", $arParams['DISPLAY_LIST_SORT']))
{
    $arBasePriceType = CCatalogGroup::GetList(Array(), Array("BASE" => "Y"))->fetch();
}

$arSortValues = Array (
    'POPULAR' => Array (
        'FIELD' => 'shows',
        'LABEL' => GetMessage('CATALOG_SORT_POPULAR')
    ),
    'NAME' => Array (
        'FIELD' => 'name',
        'LABEL' => GetMessage('CATALOG_SORT_NAME')
    ),
    'PRICE' => Array (
        'FIELD' => 'catalog_PRICE_' . $arBasePriceType['ID'],
        'LABEL' => GetMessage('CATALOG_SORT_PRICE')
    )
);
$arCurrentSorts = Array ();
if (is_array($arParams['DISPLAY_LIST_SORT']))
{
    
    foreach ($arParams['DISPLAY_LIST_SORT'] as $key)
    {
        if (isset($arSortValues[$key]))
        {
            $arSort = array_merge($arSortValues[$key], Array (
                'SORT_ORDER' => 'ASC',
                'INVERT_SORT_ORDER' => 'DESC'
            ));
            
            
            if (!empty($_REQUEST['sort_by']) && $_REQUEST['sort_by'] == strtolower($key))
            {
                $arSort['ACTIVE'] = true;
                if ($_REQUEST['sort_order'] == "desc")
                {
                    $arSort['SORT_ORDER'] = 'DESC';
                    $arSort['INVERT_SORT_ORDER'] = 'ASC';
                }
                
                $arParams["ELEMENT_SORT_FIELD"] = $arSort['FIELD'];
                $arParams["ELEMENT_SORT_ORDER"] = $arSort['SORT_ORDER'];
            }
            
            $arCurrentSorts[$key] = $arSort;
        }
    }
}

$arListDisplayTypes = Array (
    'CARD' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_CARD'),
        'CLASS' => 'block',
    ),
    'LIST' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_LIST'),
        'CLASS' => 'list',
    ),
    'PRICELIST' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_PRICELIST'),
        'CLASS' => 'table',
    ),
);

$currentListDisplayType = ($APPLICATION->get_cookie('LIST_VIEW_TYPE') != "" && isset($arListDisplayTypes[$APPLICATION->get_cookie('LIST_VIEW_TYPE')])) ? $APPLICATION->get_cookie('LIST_VIEW_TYPE') : CSolution::$options['CATALOG_VIEW_MODE_LIST'];
if (!empty($_REQUEST['view_mode']) && $arListDisplayTypes[strtoupper($_REQUEST['view_mode'])])
{
    $currentListDisplayType = strtoupper($_REQUEST['view_mode']);
    $APPLICATION->set_cookie('LIST_VIEW_TYPE', strtoupper($_REQUEST['view_mode']));
}

foreach ($arListDisplayTypes as $key => $arType)
{
    
    if ($key == $currentListDisplayType)
    {
        $arType['ACTIVE'] = true;
    }
    
    $arListDisplayTypes[$key] = $arType;
}

$arParams['FILTER_NAME'] = "searchFilter";

?>
<main class="catalog">
    <?
    $arFilterItemsIDS = Array ();
    $arFilterSectionsIDS = Array ();
    
    $arElements = $APPLICATION->IncludeComponent(
                "bitrix:search.page",
                "main",
                Array(
                        "RESTART" => $arParams["RESTART"],
                        "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
                        "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
                        "CHECK_DATES" => $arParams["CHECK_DATES"],
                        "arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
                        "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
                        "USE_TITLE_RANK" => "N",
                        "DEFAULT_SORT" => "rank",
                        "FILTER_NAME" => "",
                        "SHOW_WHERE" => "N",
                        "arrWHERE" => array(),
                        "SHOW_WHEN" => "N",
                        "PAGE_RESULT_COUNT" => $arParams["PAGE_RESULT_COUNT"],
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "N",
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
        );
    
        $arIblockSku = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"]);
        if (!empty($arIblockSku['IBLOCK_ID']) && !empty($GLOBALS['SEARCH_QUERY']))
        {
            $arSkuSearchableProps = Array ();
            $rsSkuSearchableProps = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID" => $arIblockSku['IBLOCK_ID'], 'SEARCHABLE' => 'Y'));
            while ($arSkuSearchableProp = $rsSkuSearchableProps->fetch())
            {
                $arSkuSearchableProps[] = $arSkuSearchableProp;
            }
            
            if (!empty($arSkuSearchableProps))
            {
                $arSkuSearchableFilter = Array (
                    "ACTIVE" => "Y",
                    'IBLOCK_ID' => $arIblockSku['IBLOCK_ID'],
                    0 => Array (
                        "LOGIC" => "OR",
                    )
                );
                
                foreach ($arSkuSearchableProps as $arProp)
                {
                    array_push($arSkuSearchableFilter[0], Array (
                        "PROPERTY_" . $arProp['CODE'] => $GLOBALS['SEARCH_QUERY'],
                    ));
                    
                    array_push($arSkuSearchableFilter[0], Array (
                        "PROPERTY_" . $arProp['CODE'] => $GLOBALS['SEARCH_QUERY'] . "%",
                    ));
                    
                    array_push($arSkuSearchableFilter[0], Array (
                        "PROPERTY_" . $arProp['CODE'] => "%" . $GLOBALS['SEARCH_QUERY'],
                    ));
                }
                
                $arSkuSearchableElements = CCache::CIBlockElement_GetList(Array (), $arSkuSearchableFilter);
                
                if (!empty($arSkuSearchableElements))
                {
                    foreach ($arSkuSearchableElements as $arItem)
                    {
                        $arCmlLink = CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array(), Array("CODE" => "CML2_LINK"))->fetch();
                        if (!empty($arCmlLink['VALUE']) && !in_array($arCmlLink['VALUE'], $arElements))
                        {
                            $arElements[] = $arCmlLink['VALUE'];
                        }
                        
                    }
                }
            }
            
            
        }

        if (!empty($arElements) && is_array($arElements))
        {
            
            $arSearchElements = CCache::CIBlockElement_GetList(Array (), Array ("=ID" => $arElements));
            foreach ($arSearchElements as $arItem)
            {
                $arFilterItemsIDS[] = $arItem['ID'];
                if (!in_array($arItem['IBLOCK_SECTION_ID'], $arFilterSectionsIDS))
                    $arFilterSectionsIDS[] = $arItem['IBLOCK_SECTION_ID'];
            }
            
        }
    ?>
    <? if (!empty($arElements)): ?>
    <div class="sort">
    	<? if($isFilter): ?>
    	<a href="javascript:void(0);" class="filter-btn <?=(strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER']) != 'top' ? 'desktop-hide' : '')?> <?=(isset($arParams['FILTER_HIDE_ON_MOBILE']) && $arParams['FILTER_HIDE_ON_MOBILE'] === 'Y' ? 'mobile-hidden' : '')?>"><?=GetMessage('FILTER_BUTTON_NAME')?></a>
    	<? endif; ?>
        <div class="sort-filter">
            <? if (!empty($arCurrentSorts)): ?>
                <? foreach ($arCurrentSorts as $key => $arSort): ?>
                <a href="<?=$APPLICATION->GetCurPageParam('sort_by='. strtolower($key).'&sort_order=' . strtolower($arSort['INVERT_SORT_ORDER']), Array ('sort_by', 'sort_order'))?>" rel="nofollow" class="item<?=$arSort['ACTIVE']?' active':''?><?=$arSort['SORT_ORDER'] == 'ASC' ? ' asc' :' desc'?>"><?=$arSort['LABEL']?></a>
                <? endforeach; ?>
            <? endif; ?>
            
        </div>
        <div class="display">
            <? foreach ($arListDisplayTypes as $key => $arType): ?>
            <a href="<?=$APPLICATION->GetCurPageParam('view_mode='. strtolower($key), Array ('view_mode'))?>" title="<?=$arType['LABEL']?>" rel="nofollow" class="item icon-custom <?=$arType['CLASS']?><?=$arType['ACTIVE'] ? ' active' : ''?>"></a>
            <? endforeach; ?>
        </div>
    </div>
    <div class="products-container<?=$isFilter ? ' has-filter' : ''?> <?= strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER'])?>">
        <? if ($isFilter): ?>
        
        <div class="filter <?=(isset($arParams['FILTER_HIDE_ON_MOBILE']) && $arParams['FILTER_HIDE_ON_MOBILE'] === 'Y' ? ' mobile-hidden' : '')?>">
                <?
                $APPLICATION->IncludeComponent(
                        "nextype:magnet.smart.filter", "main", array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ITEM_IDS" => $arFilterItemsIDS,
                    "SECTION_IDS" => $arFilterSectionsIDS,
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "PRICE_CODE" => $arParams["~PRICE_CODE"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "SAVE_IN_SESSION" => "N",
                    "FILTER_PRICE_CODE" => $arParams["~FILTER_PRICE_CODE"],
                    "FILTER_VIEW_MODE" => CSolution::$options['CATALOG_VIEW_MODE_FILTER'],
                    "XML_EXPORT" => "N",
                    "SECTION_TITLE" => "NAME",
                    "SECTION_DESCRIPTION" => "DESCRIPTION",
                    'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                    "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    "SEF_MODE" => $arParams["SEF_MODE"],
                    "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                    "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                        ), $component, array('HIDE_ICONS' => 'Y')
                );
                ?>
        </div>
    <? endif; ?>
        <div class="products">
            
        <?
        
        if (!empty($arElements) && is_array($arElements))
        {
            
            if (!$isFilter || empty($GLOBALS[$arParams['FILTER_NAME']]))
                $GLOBALS[$arParams['FILTER_NAME']] = array(
                    "=ID" => $arElements,
                );

            CLocations::setFilter($arParams);
                        
            $template = isset($arParams['TEMPLATE_LIST']) && $arParams['TEMPLATE_LIST'] == 'CUSTOM' && !empty($arParams['TEMPLATE_LIST_CUSTOM']) ? $arParams['TEMPLATE_LIST_CUSTOM'] : 'main';
            $templateItem = isset($arParams['TEMPLATE_ITEM']) && $arParams['TEMPLATE_ITEM'] == 'CUSTOM' && !empty($arParams['TEMPLATE_ITEM_CUSTOM']) ? $arParams['TEMPLATE_ITEM_CUSTOM'] : 'main';
            $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            $template,
            array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                    "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                    "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                    "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                    "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                    "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                    "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
                    "PROPERTY_CODE_MOBILE" => $arParams["PROPERTY_CODE_MOBILE"],
                    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                    "OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
                    "OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
                    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                    "OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
                    "SECTION_URL" => $arParams["SECTION_URL"],
                    "DETAIL_URL" => $arParams["DETAIL_URL"],
                    "BASKET_URL" => $arParams["BASKET_URL"],
                    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                    "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                    "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                    "PRICE_CODE" => $arParams["~PRICE_CODE"],
                    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                    "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                    "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                    "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                    "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                    "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                    "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                    "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                    'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                    "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                    "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],
                    "FILTER_NAME" => $arParams['FILTER_NAME'],
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SECTION_USER_FIELDS" => array(),
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "META_KEYWORDS" => "",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",

                    'LABEL_PROP' => $arParams['LABEL_PROP'],
                    'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                    'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                    'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
                    'PRODUCT_ROW_VARIANTS' => $arParams['PRODUCT_ROW_VARIANTS'],
                    'ENLARGE_PRODUCT' => $arParams['ENLARGE_PRODUCT'],
                    'ENLARGE_PROP' => $arParams['ENLARGE_PROP'],
                    'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
                    'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
                    'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],

                    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
                    'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
                    'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
                    'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
                    'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
                    'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
                    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                    'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
                    'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE'],
                    'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],

                    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
                    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
                    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],

                    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                    'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
                    'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
                    'COMPARE_PATH' => $arParams['COMPARE_PATH'],
                    'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                    'USE_COMPARE_LIST' => $arParams['USE_COMPARE_LIST'],
                    'DISPLAY_WISH_LIST' => CSolution::$options['CATALOG_SHOW_WISH_LIST'],
                    'DISPLAY_RATING' => CSolution::$options['CATALOG_SHOW_RATING'],
                    "DISPLAY_COMPARE" => CSolution::$options['CATALOG_SHOW_COMPARE_LIST'],
                    "VIEW_MODE" => $currentListDisplayType,
                    'ARTICLE_PROP' => $arParams['ARTICLE_PROP'],
                    'ITEM_TEMPLATE' => $templateItem
            ),
            $arResult["THEME_COMPONENT"],
            array('HIDE_ICONS' => 'Y')
    );
        }
        elseif (is_array($arElements))
        {
                echo GetMessage("CT_BCSE_NOT_FOUND");
        }
        ?>

                    </div>
    </div>
<? endif; ?>
</main>