<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$frame = $this->createFrame()->begin();
$strRandCarousel = 'a' . randString(5) . "_carousel";

if (!empty($arResult['ITEMS']) && count($arResult['ITEMS']) >= 4):?>
<section class="previously" data-helper="catalog::viewed">
	<h2 class="title"><?=GetMessage('CVP_TPL_BLOCK_TITLE')?></h2>
        <div class="items swiper-container" id="<?=$strRandCarousel?>">
			<div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
				<div class="item swiper-slide">
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="img">
								<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>">
							</a>
				<div class="content">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name"><?=$arItem['NAME']?></a>
									<? if (!empty($arItem['MIN_PRICE'])): ?>
					<div class="price">
										<?=(!empty($arItem['OFFERS'])) ? GetMessage('CVP_TPL_PRICE_FROM', Array ("#PRICE#" => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'])) : $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?>
									</div>
									<? endif; ?>
				</div>
			</div>
				<? endforeach; ?>
			</div>
			<div class="swiper-button-prev icon-custom"></div>
    		<div class="swiper-button-next icon-custom"></div>
			<div class="swiper-pagination"></div>
        </div>
</section>
        <script>
		let previouslySwiper = new Swiper('#<?=$strRandCarousel?>', {
			spaceBetween: 24,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				type: 'bullets',
			},
			breakpoints: {
				0:{
					slidesPerView:1,
					spaceBetween: 12,
					nav: false,
					dots: true,
					loop: <?=count($arResult['ITEMS']) > 1 ? 'true' : 'false'?>,
					on: {
						init: function (event) {
            		        var slideHeight = $('#<?=$strRandCarousel?>').find('.swiper-slide.swiper-slide-active').height();
            		        $('#<?=$strRandCarousel?>').height(slideHeight);
			    		},
			    		slideChange: function (event) {
			    		        var currentSlide = $('#<?=$strRandCarousel?>').find('.swiper-slide')[event.item.index],
			    		            slideHeight = $(currentSlide).height();
								$('#<?=$strRandCarousel?>').height(slideHeight);
							}
						}
				},
				768:{
					slidesPerView:2,
					nav: true,
					dots: false,
					loop: <?=count($arResult['ITEMS']) > 2 ? 'true' : 'false'?>
				},
				1150:{
					slidesPerView: 3,
					nav: true,
					dots: false,
					loop: <?=count($arResult['ITEMS']) > 3 ? 'true' : 'false'?>
				},
				1550:{
					slidesPerView:4,
					nav: true,
					dots: false,
					loop: <?=count($arResult['ITEMS']) > 4 ? 'true' : 'false'?>
				}
			}
		})
		// $('#<?=$strRandCarousel?>').owlCarousel({
		// 	margin: 24,
		// 	navText: ["<div class='prev icon-custom'></div>", "<div class='next icon-custom'></div>"],
		// 	responsive:{
		// 		0:{
		// 			items:1,
		// 			margin: 12,
		// 			nav: false,
		// 			dots: true,
		// 			loop: <?=count($arResult['ITEMS']) > 1 ? 'true' : 'false'?>,
        //     		onInitialized: function (event) {
        //     		        var slideHeight = $(event.target).find('.owl-item.active').height();
        //     		        $(event.target).height(slideHeight);
        //     		},
        //     		onTranslate: function (event) {
        //     		        var currentSlide = $(event.target).find('.owl-item')[event.item.index],
        //     		            slideHeight = $(currentSlide).height();
        //     		        $(event.target).height(slideHeight);
        //     		}
		// 		},
		// 		768:{
		// 			items:2,
		// 			nav: true,
		// 			dots: false,
		// 			loop: <?=count($arResult['ITEMS']) > 2 ? 'true' : 'false'?>
		// 		},
		// 		1150:{
		// 			items: 3,
		// 			nav: true,
		// 			dots: false,
		// 			loop: <?=count($arResult['ITEMS']) > 3 ? 'true' : 'false'?>
		// 		},
		// 		1550:{
		// 			items:4,
		// 			nav: true,
		// 			dots: false,
		// 			loop: <?=count($arResult['ITEMS']) > 4 ? 'true' : 'false'?>
		// 		}
		// 	}
		// });
	</script>
<? endif; ?>
<?$frame->beginStub();?>
<?$frame->end();?>