<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="availability" id="catalog_store_amount_div" data-helper="product::availability">
    <?if(!empty($arResult["STORES"])):?>
	<? if ($arParams['DETAIL_HIDE_TABS'] == 'Y'):?>
		<h3 class="product-subtitle"><?=GetMessage('CT_BCE_CATALOG_ONE_PAGE_AVAILABILITY')?></h3>
	<? endif; ?>
        <?if ($arParams['SHOW_DESCRIPTION'] && $arParams['REGION_NAME'] && $arParams["SHOW_GENERAL_STORE_INFORMATION"] != "Y"):?>
                <div class="info-text">
                    <?=GetMessage('DETAIL_REGION_TEXT', array('#CITY#' => $arParams['REGION_NAME']))?>
                </div>
        <?endif?>
    <table>
        <tbody id="c_store_amount">
            <?foreach($arResult["STORES"] as $pid => $arProperty):
                if ($arParams["SHOW_GENERAL_STORE_INFORMATION"] == "Y"):
                    $isOut = false;
                    $amountText = $arProperty["AMOUNT"];
                    if ($arParams["USE_MIN_AMOUNT"] == "Y")
                    {
                        $isOut = ($arProperty["AMOUNT"] == GetMessage("ABSENT"));
                    }
                    else
                    {
                        $isOut = ((float)$arProperty["AMOUNT"] <= 0.00);
                    }
                    
                    if ($arParams["SHOW_MAX_QUANTITY"] === "M" && $arParams["USE_MIN_AMOUNT"] != "Y")
                    {
                        $amountText = ((float)$arProperty["AMOUNT"] >= (float)$arParams["RELATIVE_QUANTITY_FACTOR"]) ? $arParams["MESS_RELATIVE_QUANTITY_MANY"] : $arParams["MESS_RELATIVE_QUANTITY_FEW"];
                    }
                    ?>
                    <tr>
                        <td class="address">
                            <?=($arParams["SHOW_GENERAL_STORE_INFORMATION_FROM_REGION"] != "Y") ? GetMessage("TPL_GENERAL_STORE_INFO_TITLE") : GetMessage("TPL_GENERAL_STORE_INFO_TITLE_REGION")?>
                        </td>
                        <td class="value icon-custom<?=$isOut ? ' out' : ''?>"><span class="balance" id="<?=$arResult['JS']['ID']?>_<?=$arProperty['ID']?>"><?=$amountText?></span></td>
                    </tr>
                <?else:?>
                    <tr<? echo ($arParams['SHOW_EMPTY_STORE'] == 'N' && isset($arProperty['REAL_AMOUNT']) && $arProperty['REAL_AMOUNT'] <= 0 ? ' style="display:none;"' : ''); ?>>
                        <td class="address">
                            <? if (isset($arProperty["TITLE"])): ?>
                                <?if (!empty($arParams["STORE_PATH"])):?>
                                    <a href="<?= $arProperty["URL"] ?>"> <?= $arProperty["TITLE"] ?></a><br />
                                <?else:?>
                                    <?= $arProperty["TITLE"] ?><br/>
                                <?endif?>
                            <? endif; ?>
                        </td>
                        <td class="value icon-custom<?=$arProperty['REAL_AMOUNT'] <= 0 ? ' out' : ''?>"><?=GetMessage('S_AMOUNT')?> <span class="balance" id="<?=$arResult['JS']['ID']?>_<?=$arProperty['ID']?>"><?=$arProperty["AMOUNT"]?></span></td>
                    </tr>
                <?endif?>
            <? endforeach; ?>
        </tbody>
    </table>
    <?if (isset($arResult["IS_SKU"]) && $arResult["IS_SKU"] == 1):?>
	<script type="text/javascript">
		var obStoreAmount = new JCCatalogStoreSKU(<? echo CUtil::PhpToJSObject($arResult['JS'], false, true, true); ?>);
	</script>
    <? endif;?>
    <? else: ?>
        <?=GetMessage('EMPTY_STORE_DATA')?>
    <? endif; ?>
</div>

