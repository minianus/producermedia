<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["PRICES"] = $arConvertParams = array();
$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

if (is_array($arParams["PRICE_CODE"]))
    $arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices(0, $arParams["PRICE_CODE"]);
    
if (!\Bitrix\Main\Loader::includeModule('currency'))
{
    $arParams['CONVERT_CURRENCY'] = 'N';
    $arParams['CURRENCY_ID'] = '';
}
else
{
    if ($arParams['CONVERT_CURRENCY'] === 'Y')
    {
        $arCurrencyInfo = CCurrency::GetByID($arParams['CURRENCY_ID']);
    }
    else
    {
        $baseCurrency = CCurrency::GetBaseCurrency();
        $arCurrencyInfo = CCurrency::GetByID($baseCurrency);
    }

    if (!(is_array($arCurrencyInfo) && !empty($arCurrencyInfo)))
    {
        $arParams['CONVERT_CURRENCY'] = 'N';
        $arParams['CURRENCY_ID'] = '';
    }
    else
    {
        $arParams['CURRENCY_ID'] = $arCurrencyInfo['CURRENCY'];
        $arConvertParams['CURRENCY_ID'] = $arCurrencyInfo['CURRENCY'];
    }
}

if(!empty($arResult["CATEGORIES"]) && \Bitrix\Main\Loader::includeModule('iblock'))
{
    $arItemIds = array();
    foreach($arResult["CATEGORIES"] as $arCategory)
    {
        foreach($arCategory["ITEMS"] as $arItem)
        {
            $arItemIds[] = $arItem['ITEM_ID'];
        }
    }

    $arSelect = array(
        "ID",
        "IBLOCK_ID",
        "PREVIEW_PICTURE",
        "DETAIL_PAGE_URL",
        "NAME"
    );
    $arFilter = array(
        "IBLOCK_LID" => SITE_ID,
        "IBLOCK_ACTIVE" => "Y",
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "Y",
        "MIN_PERMISSION" => "R",
    );

    foreach($arResult["PRICES"] as $arPrice)
    {
        $arSelect[] = $arPrice["SELECT"];
        $arFilter["CATALOG_SHOP_QUANTITY_" . $arPrice["ID"]] = 1;
    }

    $arFilter["=ID"] = $arItemIds;
    $arResult["ITEMS"] = array();
    $obElements = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    while($arElement = $obElements->GetNext())
    {
        $arElement['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arElement['PREVIEW_PICTURE'], Array ('width' => 60, 'height' => 60), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
        $arElement["PRICES"] = CIBlockPriceTools::GetItemPrices($arElement["IBLOCK_ID"], $arResult["PRICES"], $arElement, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
        $maxPrice = 0;
        foreach ($arElement["PRICES"] as $key => $arPrice)
        {
            if ($arPrice['CAN_BUY'] !== 'Y' || $arPrice['CAN_ACCESS'] !== 'Y')
            {
                unset($arElement["PRICES"][$key]);
                continue;
            }
            if ($arPrice['MIN_PRICE'] === 'Y')
            {
                $arElement["MIN_PRICE"] = array(
                    "VALUE_NOVAT" => $arPrice["VALUE_NOVAT"],
                    "VALUE_VAT" => $arPrice["VALUE_VAT"],
                    "DISCOUNT_VALUE_VAT" => $arPrice["DISCOUNT_VALUE_VAT"],
                    "DISCOUNT_VALUE_NOVAT" => $arPrice["DISCOUNT_VALUE_NOVAT"],
                    "PRINT_VALUE_NOVAT" => $arPrice["PRINT_VALUE_NOVAT"],
                    "PRINT_VALUE_VAT" => $arPrice["PRINT_VALUE_VAT"],
                    "PRINT_DISCOUNT_VALUE_VAT" => $arPrice["PRINT_DISCOUNT_VALUE_VAT"],
                    "PRINT_DISCOUNT_VALUE_NOVAT" => $arPrice["PRINT_DISCOUNT_VALUE_NOVAT"],
                    "CURRENCY" => $arPrice['CURRENCY']
                );
            }
            elseif ($arPrice['MIN_PRICE'] !== 'Y' && $arPrice['DISCOUNT_VALUE_VAT'] > $maxPrice)
            {
                $maxPrice = $arPrice['DISCOUNT_VALUE_VAT'];
                $arElement["MAX_PRICE"] = array(
                    "VALUE_NOVAT" => $arPrice["VALUE_NOVAT"],
                    "VALUE_VAT" => $arPrice["VALUE_VAT"],
                    "DISCOUNT_VALUE_VAT" => $arPrice["DISCOUNT_VALUE_VAT"],
                    "DISCOUNT_VALUE_NOVAT" => $arPrice["DISCOUNT_VALUE_NOVAT"],
                    "PRINT_VALUE_NOVAT" => $arPrice["PRINT_VALUE_NOVAT"],
                    "PRINT_VALUE_VAT" => $arPrice["PRINT_VALUE_VAT"],
                    "PRINT_DISCOUNT_VALUE_VAT" => $arPrice["PRINT_DISCOUNT_VALUE_VAT"],
                    "PRINT_DISCOUNT_VALUE_NOVAT" => $arPrice["PRINT_DISCOUNT_VALUE_NOVAT"],
                    "CURRENCY" => $arPrice['CURRENCY']
                );
            }
        }

        if (isset($arElement["MAX_PRICE"]) && isset($arElement["MIN_PRICE"]))
        {
            if ($arElement["MAX_PRICE"]['CURRENCY'] !== $arElement["MIN_PRICE"]['CURRENCY'])
                unset($arElement["MAX_PRICE"]);
            elseif ($arElement["MAX_PRICE"]['DISCOUNT_VALUE_VAT'] === $arElement["MIN_PRICE"]['DISCOUNT_VALUE_VAT']
                    || $arElement["MAX_PRICE"]['DISCOUNT_VALUE_VAT'] < $arElement["MIN_PRICE"]['DISCOUNT_VALUE_VAT'])
                unset($arElement["MAX_PRICE"]);
        }

        $arResult["ITEMS"][$arElement["ID"]] = $arElement;
    }
}