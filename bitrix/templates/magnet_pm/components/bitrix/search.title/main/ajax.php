<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ITEMS'])): ?>
	<div class="content">
    	<? foreach ($arResult['ITEMS'] as $arItem): ?>
    	<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item">
    	    <span class="image">
    	        <img src="<?=$arItem['PREVIEW_PICTURE']['src']?>" />
    	    </span>
    	    <span class="info">
    	        <span class="name"><?=$arItem['NAME']?></span>
    	        <? if (!empty($arItem['PRICES'])): ?>
                <? if (isset($arItem['MAX_PRICE'])): ?>
                <div class="old-price">
                    <?=($arParams["PRICE_VAT_INCLUDE"]) ? $arItem['MAX_PRICE']['PRINT_DISCOUNT_VALUE_VAT'] : $arItem['MAX_PRICE']['PRINT_DISCOUNT_VALUE_NOVAT'];?>
                </div>
                <? endif; ?>
    	        <span class="price">
                    <?=($arParams["PRICE_VAT_INCLUDE"]) ? $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE_VAT'] : $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE_NOVAT'];?>
                </span>
    	        <? endif; ?>
    	    </span>
    	</a>
    	<? endforeach; ?>
    </div>
<? endif; ?>