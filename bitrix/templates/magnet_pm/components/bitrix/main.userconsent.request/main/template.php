<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */

$config = \Bitrix\Main\Web\Json::encode($arResult['CONFIG']);
$strId = "agreement_checkbox_" . randString(5);
?>


<div class="policy" data-bx-user-consent="<?=htmlspecialcharsbx($config)?>">
    <input id="<?=$strId?>" type="checkbox" value="Y" <?=($arParams['IS_CHECKED'] ? 'checked' : '')?> name="<?=htmlspecialcharsbx($arParams['INPUT_NAME'])?>">
    <label for="<?=$strId?>"><?=GetMessage('MAIN_USER_CONSENT_SHORT_TEXT')?></label>
</div>
