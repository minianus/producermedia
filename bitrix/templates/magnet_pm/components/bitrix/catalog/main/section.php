<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;
use Nextype\Magnet\CLocations;

$this->setFrameMode(true);

CSolution::getInstance(SITE_ID);
CLocations::setFilter($arParams);


$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$arSection = CCache::CIBlockSection_GetByID($arResult['VARIABLES']['SECTION_ID']);
$bShowOrderForm = false;
if ($arSection)
{
    $arSection['USER_FIELDS'] = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arParams['IBLOCK_ID']."_SECTION", $arSection['ID']);
    if (!empty($arSection['USER_FIELDS']['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
        $bShowOrderForm = true;
    
    //check parents values
    if (!$bShowOrderForm)
    {
        $arParents = CCache::CIBlockSection_GetList(
                array('LEFT_MARGIN' => 'ASC'),
                array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ID' => $arSection['ID']),
                false,
                array('ID', 'IBLOCK_SECTION_ID')
        );
        foreach ($arParents as $arParent)
        {
            if ($arParent['IBLOCK_SECTION_ID'] == $arSection['ID'])
                continue;
            $arDataFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_".$arParams['IBLOCK_ID']."_SECTION", $arParent['IBLOCK_SECTION_ID']);
            if (!empty($arDataFields['UF_MAGNET_SHOW_ORDER_FORM']['VALUE']))
            {
                $bShowOrderForm = true;
                break;
            }
        }
    }
}

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter)
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
		$arCurSection = array();
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] === 'Y')
{
	$basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
}
else
{
	$basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}

$arBasePriceType = Array ();
if (\Bitrix\Main\Loader::includeModule("sale") && \Bitrix\Main\Loader::includeModule("catalog") && in_array("PRICE", $arParams['DISPLAY_LIST_SORT']))
{
    $arBasePriceType = CCatalogGroup::GetList(Array(), Array("NAME" => $arParams['FILTER_PRICE_CODE']))->fetch();
}

$arSortValues = Array (
    'POPULAR' => Array (
        'FIELD' => 'shows',
        'LABEL' => GetMessage('CATALOG_SORT_POPULAR')
    ),
    'NAME' => Array (
        'FIELD' => 'name',
        'LABEL' => GetMessage('CATALOG_SORT_NAME')
    ),
    'PRICE' => Array (
        'FIELD' => 'catalog_PRICE_' . $arBasePriceType['ID'],
        'LABEL' => GetMessage('CATALOG_SORT_PRICE')
    )
);
$arCurrentSorts = Array ();
if (is_array($arParams['DISPLAY_LIST_SORT']))
{
    
    foreach ($arParams['DISPLAY_LIST_SORT'] as $key)
    {
        if (isset($arSortValues[$key]))
        {
            $arSort = array_merge($arSortValues[$key], Array (
                'SORT_ORDER' => 'ASC',
                'INVERT_SORT_ORDER' => 'DESC'
            ));
            
            
            if (!empty($_REQUEST['sort_by']) && $_REQUEST['sort_by'] == strtolower($key))
            {
                $arSort['ACTIVE'] = true;
                if ($_REQUEST['sort_order'] == "desc")
                {
                    $arSort['SORT_ORDER'] = 'DESC';
                    $arSort['INVERT_SORT_ORDER'] = 'ASC';
                }
                
                $arParams["ELEMENT_SORT_FIELD"] = $arSort['FIELD'];
                $arParams["ELEMENT_SORT_ORDER"] = $arSort['SORT_ORDER'];
            }
            
            $arCurrentSorts[$key] = $arSort;
        }
    }
}

$arListDisplayTypes = Array (
    'CARD' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_CARD'),
        'CLASS' => 'block',
    ),
    'LIST' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_LIST'),
        'CLASS' => 'list',
    ),
    'PRICELIST' => Array (
        'LABEL' => GetMessage('CATALOG_DISPLAY_LIST_TYPE_PRICELIST'),
        'CLASS' => 'table',
    ),
);

$currentListDisplayType = ($APPLICATION->get_cookie('LIST_VIEW_TYPE') != "" && isset($arListDisplayTypes[$APPLICATION->get_cookie('LIST_VIEW_TYPE')])) ? $APPLICATION->get_cookie('LIST_VIEW_TYPE') : CSolution::$options['CATALOG_VIEW_MODE_LIST'];
if (!empty($_REQUEST['view_mode']) && $arListDisplayTypes[strtoupper($_REQUEST['view_mode'])])
{
    $currentListDisplayType = strtoupper($_REQUEST['view_mode']);
    $APPLICATION->set_cookie('LIST_VIEW_TYPE', strtoupper($_REQUEST['view_mode']));
}

foreach ($arListDisplayTypes as $key => $arType)
{
    
    if ($key == $currentListDisplayType)
    {
        $arType['ACTIVE'] = true;
    }
    
    $arListDisplayTypes[$key] = $arType;
}
?>

<main class="catalog">
    
    <?
    $tplSectionList = "subcategories";
    if (!empty(CSolution::$options['CATALOG_TEMPLATE_INNER_SECTIONS']))
        $tplSectionList = strtolower (CSolution::$options['CATALOG_TEMPLATE_INNER_SECTIONS']);
    
    $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list", $tplSectionList, array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
        "HIDE_DESCRIPTION" => "Y",
        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
            ), $component, array("HIDE_ICONS" => "Y")
    );
    ?>
    
    <div class="sort">
    	<? if($isFilter): ?>
        <a href="javascript:void(0);" class="filter-btn <?=(strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER']) != 'top' ? 'desktop-hide' : '')?> <?=(isset($arParams['FILTER_HIDE_ON_MOBILE']) && $arParams['FILTER_HIDE_ON_MOBILE'] === 'Y' ? 'mobile-hidden' : '')?>"><?=GetMessage('FILTER_BUTTON_NAME')?>
            
        </a>
    	<? endif; ?>
        <div class="sort-filter" data-helper="catalog::sort">
            <? if (!empty($arCurrentSorts)): ?>
                <? foreach ($arCurrentSorts as $key => $arSort): ?>
                <a href="<?=$APPLICATION->GetCurPageParam('sort_by='. strtolower($key).'&sort_order=' . strtolower($arSort['INVERT_SORT_ORDER']), Array ('sort_by', 'sort_order'))?>" rel="nofollow" class="item<?=$arSort['ACTIVE']?' active':''?><?=$arSort['SORT_ORDER'] == 'ASC' ? ' asc' :' desc'?>"><?=$arSort['LABEL']?></a>
                <? endforeach; ?>
            <? endif; ?>
            
        </div>
        <div class="display">
            <? foreach ($arListDisplayTypes as $key => $arType): ?>
            <a href="<?=$APPLICATION->GetCurPageParam('view_mode='. strtolower($key), Array ('view_mode'))?>" title="<?=$arType['LABEL']?>" rel="nofollow" class="item icon-custom <?=$arType['CLASS']?><?=$arType['ACTIVE'] ? ' active' : ''?>"></a>
            <? endforeach; ?>
        </div>
    </div>
    
    <div class="products-container<?=$isFilter ? ' has-filter' : ''?> <?= strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER'])?>">
    <? if ($isFilter): ?>
        <div class="filter <?=(isset($arParams['FILTER_HIDE_ON_MOBILE']) && $arParams['FILTER_HIDE_ON_MOBILE'] === 'Y' ? ' mobile-hidden' : '')?>" data-helper="catalog::filter">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter", 'main', array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "SECTION_ID" => $arCurSection['ID'],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "PRICE_CODE" => $arParams["~PRICE_CODE"],
                    "FILTER_PRICE_CODE" => $arParams["~FILTER_PRICE_CODE"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "SAVE_IN_SESSION" => "N",
                    "FILTER_VIEW_MODE" => CSolution::$options['CATALOG_VIEW_MODE_FILTER'],
                    "XML_EXPORT" => "N",
                    "SECTION_TITLE" => "NAME",
                    "SECTION_DESCRIPTION" => "DESCRIPTION",
                    'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                    "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    "SEF_MODE" => $arParams["SEF_MODE"],
                    "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                    "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                        ), $component, array('HIDE_ICONS' => 'Y')
                );
                ?>
        </div>
    <? endif; ?>
        <div class="products" data-helper="catalog::products-list">
            <?
            $template = isset($arParams['TEMPLATE_LIST']) && $arParams['TEMPLATE_LIST'] == 'CUSTOM' && !empty($arParams['TEMPLATE_LIST_CUSTOM']) ? $arParams['TEMPLATE_LIST_CUSTOM'] : 'main';
            $templateItem = isset($arParams['TEMPLATE_ITEM']) && $arParams['TEMPLATE_ITEM'] == 'CUSTOM' && !empty($arParams['TEMPLATE_ITEM_CUSTOM']) ? $arParams['TEMPLATE_ITEM_CUSTOM'] : 'main';

            $intSectionID = $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", $template, array(
                "VIEW_MODE" => $currentListDisplayType,
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "LIST_PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "LIST_PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                'DISPLAY_WISH_LIST' => CSolution::$options['CATALOG_SHOW_WISH_LIST'],
                'DISPLAY_RATING' => CSolution::$options['CATALOG_SHOW_RATING'],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "MESSAGE_404" => $arParams["~MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "DISPLAY_COMPARE" => CSolution::$options['CATALOG_SHOW_COMPARE_LIST'],
                "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                "PRICE_CODE" => $arParams["~PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],
                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                'LABEL_PROP' => $arParams['LABEL_PROP'],
                'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',
                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
                'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                'BRAND_PROPERTY' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
                'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                "ADD_SECTIONS_CHAIN" => "N",
                'ADD_TO_BASKET_ACTION' => $basketAction,
                'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                'USE_COMPARE_LIST' => 'Y',
                'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                'ARTICLE_PROP' => $arParams['ARTICLE_PROP'],
                'IMAGES_LAZY_LOAD' => CSolution::$options['IMAGES_LAZY_LOAD'],
                'OFFERS_HIDE_TITLE' => $arParams['OFFERS_HIDE_TITLE'],
                'SHOW_ORDER_FORM' => $bShowOrderForm,
                'ITEM_TEMPLATE' => $templateItem
                    ), $component
            );
            
            if (CModule::IncludeModule("sotbit.seometa"))
            {
                $APPLICATION->IncludeComponent(
                    "sotbit:seo.meta",
                    "main",
                    Array(
                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "SECTION_ID" => $arCurSection['ID'],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                    )
                );
            }
            
            ?>
        </div>
            
    </div>
    
    <? if ($arParams['AJAX_MODE'] == "Y"): ?>
    <script>
    $('[data-lazy="default"]').lazy({});
    window.CSolution.setProductsHeight($('.products-list.card .product-wrap'));
    </script>
    <? endif; ?>
    
    <? if (!empty($arSection['USER_FIELDS']['UF_SEO_BOTTOM']['VALUE']) || !empty($GLOBALS['sotbitSeoMetaBottomDesc'])): ?>
    <div class="category-desc" data-helper="catalog::desc">
        <?=$arSection['USER_FIELDS']['UF_SEO_BOTTOM']['VALUE']?>
        <?=$GLOBALS['sotbitSeoMetaBottomDesc']?>
    </div>
    <? endif; ?>
    

</main>