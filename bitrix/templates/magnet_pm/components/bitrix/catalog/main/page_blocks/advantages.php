<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

if ($arElement)
{
    $arAdvantages = Array();
    $arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arParams['IBLOCK_ID'] . "_SECTION", $arElement['IBLOCK_SECTION_ID']);
    if (!empty($arUserFields['UF_ADVANTAGES']['VALUE']))
    {
        foreach ($arUserFields['UF_ADVANTAGES']['VALUE'] as $hlId)
        {
            $hlElement = CCache::HLBlock_GetById('MagnetAdvantages', $hlId);
            if (!empty($hlElement))
            {
                $arAdvantages[] = Array(
                    'NAME' => $hlElement['UF_NAME'],
                    'ICON' => CFile::GetPath($hlElement['UF_ICON']),
                );
            }
        }

    }
}
?>
<? if (!empty($arAdvantages)): ?>
    <div class="advantages" data-helper="product::advantages">
        <? foreach ($arAdvantages as $arItem): ?>
        <div class="item">
            <span class="icon" style="background-image: url('<?=$arItem['ICON']?>');"></span>
            <span><?=$arItem['NAME']?></span>
        </div>
        <? endforeach; ?>
    </div>
<? endif; ?>


