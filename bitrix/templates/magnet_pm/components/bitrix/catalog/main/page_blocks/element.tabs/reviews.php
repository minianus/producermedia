<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;
?>
<div class="reviews-container" data-helper="product::reviews">
    <?
    
    if (CSolution::$options['CATALOG_ENABLED_EXTENDED_REVIEWS'] == "Y")
    {
        if ($arParams['DETAIL_HIDE_TABS'] == 'Y')
        {
            ?><h3 class="product-subtitle"><?=GetMessage('CT_BCE_CATALOG_ONE_PAGE_REVIEWS')?><?=$arElement['PROPERTIES']['vote_count']['VALUE'] ? ' (' . $arElement['PROPERTIES']['vote_count']['VALUE'] . ')' : ''?></h3><?
        }
        
        if (!empty($arParams['REVIEWS_CODE']))
        {
            $APPLICATION->IncludeComponent(
                    'nextype:magnet.reviews', 'main', Array(
                'CODE' => $arParams['REVIEWS_CODE'],
                'ELEMENT_ID' => $GLOBALS['CATALOG_CURRENT_ELEMENT_ID'],
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    ), $component
            );
        }
        else
        {
            ShowError(GetMessage('EMPTY_REVIEWS_CODE_PARAM'));
        }
    }
    else
    {
        if ($arParams['DETAIL_HIDE_TABS'] == 'Y')
        {
            ?><h3 class="product-subtitle"><?=GetMessage('CT_BCE_CATALOG_ONE_PAGE_REVIEWS')?></h3><?
        }
        
        Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("reviews");
        $APPLICATION->IncludeComponent(
            "bitrix:forum.topic.reviews", "main", Array(
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
            "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
            "FORUM_ID" => $arParams["FORUM_ID"],
            "ELEMENT_ID" => $GLOBALS['CATALOG_CURRENT_ELEMENT_ID'],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
            "SHOW_RATING" => "N",
            "SHOW_MINIMIZED" => "Y",
            "SECTION_REVIEW" => "Y",
            "POST_FIRST_MESSAGE" => "Y",
            "MINIMIZED_MINIMIZE_TEXT" => GetMessage("HIDE_FORM"),
            "MINIMIZED_EXPAND_TEXT" => GetMessage("ADD_REVIEW"),
            "SHOW_AVATAR" => "N",
            "SHOW_LINK_TO_FORUM" => "N",
            "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                ), false
        );
        Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("reviews", "");
    }
    ?>
    
    
</div>