<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$strTabsId = "tabs_" . randString(6);
$useLocations = CSolution::getInstance(SITE_ID)::$options['LOCATIONS_ENABLED'] == 'Y';
$showStoreDescription = false;
if ($useLocations)
{
    $regionStores = \Nextype\Magnet\Clocations::getCurrentRegion()['PROPERTIES']['STORE']['VALUE'];
    if (!empty($regionStores) && !in_array('COMPONENT', $regionStores))
    {
        $showStoreDescription = true;
        $arParams['STORES'] = ($arParams["SHOW_GENERAL_STORE_INFORMATION"] == "Y" && $arParams["SHOW_GENERAL_STORE_INFORMATION_FROM_REGION"] == "Y") ? $regionStores : $arParams["STORES"];
    }
}
?>

<div class="tabs-container" id="<?=$strTabsId?>">
    <div class="tabs">
        <? $APPLICATION->ShowViewContent('catalog_element_tabs_title'); ?>
        <? if ($elementId > 0 && $arParams['USE_STORE'] == 'Y' && Bitrix\Main\ModuleManager::isModuleInstalled('catalog')): ?>
            <a href="javascript:void(0)" class="tab" tab-store><?= $arMessages['STORES_TAB'] ?></a>
        <? endif; ?>
        <? if ($arParams['DETAIL_USE_REVIEWS'] == "Y"): ?>
            <a href="javascript:void(0)" class="tab"><?= $arMessages['REVIEWS_TAB'] ?><?=CSolution::$options['CATALOG_ENABLED_EXTENDED_REVIEWS'] == "Y" && $arElement['PROPERTIES']['vote_count']['VALUE'] ? ' (' . $arElement['PROPERTIES']['vote_count']['VALUE'] . ')' : ''?></a>
        <? endif; ?>
        <? if ($arParams['DETAIL_SHOW_DELIVERY_TAB'] == "Y"): ?>
            <a href="javascript:void(0)" class="tab"><?= $arMessages['DELIVERY_TAB'] ?></a>
        <? endif; ?>
    </div>
    <div class="tabs-content">
        <? $APPLICATION->ShowViewContent('catalog_element_tabs_content'); ?>

        <? if ($elementId > 0 && $arParams['USE_STORE'] == 'Y' && Bitrix\Main\ModuleManager::isModuleInstalled('catalog')): ?>
            <div class="tab-content">
                <?
                $storeCmpParams = array(
                    'ELEMENT_ID' => $elementId,
                    'STORE_PATH' => $arParams['STORE_PATH'],
                    'CACHE_TYPE' => 'A',
                    'CACHE_TIME' => '36000',
                    'MAIN_TITLE' => $arParams['MAIN_TITLE'],
                    'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
                    'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
                    'STORES' => $arParams['STORES'],
                    'SHOW_EMPTY_STORE' => $arParams['SHOW_EMPTY_STORE'],
                    'SHOW_GENERAL_STORE_INFORMATION' => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
                    "SHOW_GENERAL_STORE_INFORMATION_FROM_REGION" => $arParams["SHOW_GENERAL_STORE_INFORMATION_FROM_REGION"],
                    'USER_FIELDS' => $arParams['USER_FIELDS'],
                    'DETAIL_HIDE_TABS' => $arParams['DETAIL_HIDE_TABS'],
                    'FIELDS' => $arParams['FIELDS']
                );
                if ($useLocations)
                {
                    $storeCmpParams = array_merge($storeCmpParams, array(
                        'SHOW_DESCRIPTION' => $showStoreDescription,
                        'REGION_NAME' => \Nextype\Magnet\Clocations::getCurrentRegion()['NAME']
                    ));
                }
                ?>

                <?
                $APPLICATION->IncludeComponent(
                        'bitrix:catalog.store.amount', 'main', $storeCmpParams, $component, array('HIDE_ICONS' => 'Y')
                );
                ?>


            </div>
        <? endif; ?>

        <? if ($arParams['DETAIL_USE_REVIEWS'] == "Y"): ?>
            <div class="tab-content">
                <? @include(__DIR__ . "/element.tabs/reviews.php"); ?>
            </div>
        <? endif; ?>

        <? if ($arParams['DETAIL_SHOW_DELIVERY_TAB'] == "Y"): ?>
            <div class="tab-content">
                <? @include(__DIR__ . "/element.tabs/delivery.php"); ?>
            </div>
        <? endif; ?>

    </div>
</div>
<script>
    var elementTabs = document.getElementById('<?=$strTabsId?>');
    elementTabs.querySelector(".tabs .tab:first-child").classList.add('active');
    elementTabs.querySelector(".tabs-content .tab-content:first-child").classList.add('active');

</script>