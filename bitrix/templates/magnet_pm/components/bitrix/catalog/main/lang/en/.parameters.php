<?
$MESS["CP_BC_TPL_DML_SIMPLE"] = "Simple mode";
$MESS["CP_BC_TPL_DML_EXT"] = "Extended";
$MESS["CPT_BC_TPL_THEME_SITE"] = "Take the theme from the site settings (for solutions bitrix.eshop)";
$MESS["CPT_BC_TPL_THEME_BLUE"] = "Blue (default theme)";
$MESS["CPT_BC_TPL_THEME_GREEN"] = "Green";
$MESS["CPT_BC_TPL_THEME_RED"] = "Red";
$MESS["CPT_BC_TPL_THEME_WOOD"] = "Wood";
$MESS["CPT_BC_TPL_THEME_YELLOW"] = "Yellow";
$MESS["CP_BC_TPL_THEME_BLACK"] = "Dark";
$MESS["CP_BC_TPL_DVDAR_RATING"] = "Rating";
$MESS["CP_BC_TPL_DVDAR_AVERAGE"] = "average value";
$MESS["CPT_BC_TPL_VIEW_MODE_BANNER"] = "banner";
$MESS["CPT_BC_TPL_VIEW_MODE_SLIDER"] = "slider";
$MESS["CPT_BC_TPL_VIEW_MODE_SECTION"] = "list";
$MESS["CPT_BC_SECTIONS_VIEW_MODE_LIST"] = "Multi-level list";
$MESS["CPT_BC_SECTIONS_VIEW_MODE_LINE"] = "List";
$MESS["CPT_BC_SECTIONS_VIEW_MODE_TEXT"] = "Text";
$MESS["CPT_BC_SECTIONS_VIEW_MODE_TILE"] = "Block";
$MESS["CP_BC_TPL_TEMPLATE_THEME"] = "Color theme";
$MESS["CP_BC_TPL_SEARCH_PAGE_RESULT_COUNT"] = "Number of results per page";
$MESS["CP_BC_TPL_SEARCH_RESTART"] = "Search without morphology (in the absence of a search result)";
$MESS["CP_BC_TPL_SEARCH_NO_WORD_LOGIC"] = "Hide word processing as logical operators";
$MESS["CP_BC_TPL_SEARCH_USE_LANGUAGE_GUESS"] = "Enable auto-detect keyboard layout";
$MESS["CP_BC_TPL_SEARCH_CHECK_DATES"] = "Search only in the date of active documents";
$MESS["CP_BC_TPL_PRODUCT_BLOCKS_ORDER"] = "The order of show of blocks of goods";
$MESS["LIST_PRODUCT_BLOCKS_ORDER_TIP"] = "For some blocks, the order may not be respected due to the peculiarities of the pattern. Also, a product block may not be displayed, since its functionality is disabled or unavailable.";
$MESS["TOP_PRODUCT_BLOCKS_ORDER_TIP"] = "For some blocks, the order may not be respected due to the peculiarities of the pattern. Also, a product block may not be displayed, since its functionality is disabled or unavailable.";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_PRICE"] = "Price";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_QUANTITY_LIMIT"] = "Residue";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_QUANTITY"] = "Amount";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_BUTTONS"] = "Actions";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_PROPS"] = "Property";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_PRICE_RANGES"] = "Price ranges";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_SKU"] = "Sales Proposition";
$MESS["CP_BC_TPL_PRODUCT_BLOCK_COMPARE"] = "Comparison";
$MESS["CP_BC_TPL_PRODUCT_ROW_VARIANTS"] = "Product show option";
$MESS["CP_BC_TPL_SETTINGS_VARIANT"] = "Option";
$MESS["CP_BC_TPL_SETTINGS_DELETE"] = "Delete";
$MESS["CP_BC_TPL_SETTINGS_QUANTITY"] = "Number of products on the page";
$MESS["CP_BC_TPL_SETTINGS_QUANTITY_BIG_DATA"] = "Number of BigData products per page";
$MESS["CP_BC_TPL_ENLARGE_PRODUCT"] = "Highlight items in the list";
$MESS["CP_BC_TPL_ENLARGE_PROP"] = "Select by selected property";
$MESS["CP_BC_TPL_ENLARGE_PRODUCT_STRICT"] = "by selected template (strictly)";
$MESS["CP_BC_TPL_ENLARGE_PRODUCT_PROP"] = "by property, customized by template (possible to change the sorting of elements)";
$MESS["CP_BC_TPL_SHOW_SLIDER"] = "Show slider for products";
$MESS["CP_BC_TPL_SLIDER_INTERVAL"] = "Slide Interval, ms";
$MESS["CP_BC_TPL_SLIDER_PROGRESS"] = "Show progress bar";
$MESS["CPT_BC_SECTIONS_VIEW_MODE"] = "View of the list of subsections";
$MESS["CPT_BC_FILTER_VIEW_MODE"] = "View of smart filter display";
$MESS["CPT_BC_TPL_TOP_VIEW_MODE"] = "Showing top elements";
$MESS["CPT_BC_FILTER_VIEW_MODE_VERTICAL"] = "Vertical";
$MESS["CPT_BC_FILTER_VIEW_MODE_HORIZONTAL"] = "Horizontal";
$MESS["CPT_BC_FILTER_HIDE_ON_MOBILE"] = "Hide smart filter on mobile devices";
$MESS["CPT_BC_INSTANT_RELOAD"] = "Instant filtering with AJAX enabled";
$MESS["CPT_BC_SECTIONS_SHOW_PARENT_NAME"] = "Show section name";
$MESS["CPT_BC_SECTIONS_HIDE_SECTION_NAME"] = "Don't show the names of subsections";
$MESS["CP_BC_TPL_PROP_EMPTY"] = "not chosen";
$MESS["CP_BC_TPL_PROPERTY_CODE_MOBILE"] = "Product properties displayed on mobile devices";
$MESS["CP_BC_TPL_ADD_PICT_PROP"] = "Additional picture of the main product";
$MESS["CP_BC_TPL_LABEL_PROP"] = "Product Tag Property";
$MESS["CP_BC_TPL_LABEL_PROP_MOBILE"] = "Properties of product tags displayed on mobile devices";
$MESS["CP_BC_TPL_LABEL_PROP_POSITION"] = "Product Tag Location";
$MESS["CP_BC_TPL_PRODUCT_DISPLAY_MODE"] = "Show scheme";
$MESS["CP_BC_TPL_OFFER_ADD_PICT_PROP"] = "Additional pictures of the proposal";
$MESS["CP_BC_TPL_OFFER_TREE_PROPS"] = "Properties for the selection of proposals";
$MESS["CP_BC_TPL_DETAIL_DISPLAY_NAME"] = "Show item name";
$MESS["CP_BC_TPL_DETAIL_IMAGE_RESOLUTION"] = "Aspect Ratio";
$MESS["CP_BC_TPL_DETAIL_IMAGE_RESOLUTION_16_BY_9"] = "16:9";
$MESS["CP_BC_TPL_DETAIL_IMAGE_RESOLUTION_1_BY_1"] = "1:1";
$MESS["CP_BC_TPL_PRODUCT_INFO_BLOCK_ORDER"] = "The order of showing blocks of information about the product";
$MESS["CP_BC_TPL_PRODUCT_PAY_BLOCK_ORDER"] = "The order of show of blocks of purchase of goods";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_RATING"] = "Rating";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_PRICE"] = "Price";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_SKU"] = "Trade offer";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_QUANTITY"] = "Amount";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_BUTTONS"] = "Actions";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_COMPARE"] = "Comparison";
$MESS["CP_BC_TPL_DETAIL_PRODUCT_BLOCK_QUANTITY_LIMIT"] = "Remainder";
$MESS["CP_BC_TPL_DETAIL_SHOW_SLIDER"] = "Show slider for products";
$MESS["CP_BC_TPL_DETAIL_SLIDER_INTERVAL"] = "Slide Interval, ms";
$MESS["CP_BC_TPL_DETAIL_SLIDER_PROGRESS"] = "Show progress bar";
$MESS["CP_BC_TPL_DETAIL_DETAIL_PICTURE_MODE"] = "Detailed picture display mode";
$MESS["CP_BC_TPL_DETAIL_ADD_DETAIL_TO_SLIDER"] = "Add detailed image to slider";
$MESS["CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE"] = "Show description for the announcement on the detail page";
$MESS["CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_HIDE"] = "don't show";
$MESS["CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_SHOW"] = "always show";
$MESS["CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_EMPTY_DETAIL"] = "show if detailed description is empty";
$MESS["CP_BC_TPL_DETAIL_SHOW_POPULAR"] = "Show block \"Popular in section\"";
$MESS["CP_BC_TPL_DETAIL_SHOW_VIEWED"] = "Show block \"Viewed\"";
$MESS["CP_BC_TPL_MESS_PRICE_RANGES_TITLE"] = "Block name with extended prices";
$MESS["CP_BC_TPL_MESS_PRICE_RANGES_TITLE_DEFAULT"] = "Prices";
$MESS["CP_BC_TPL_MESS_DESCRIPTION_TAB"] = "Tab Text \"Description\"";
$MESS["CP_BC_TPL_MESS_DESCRIPTION_TAB_DEFAULT"] = "Description";
$MESS["CP_BC_TPL_MESS_PROPERTIES_TAB"] = "Tab Text \"Specifications\"";
$MESS["CP_BC_TPL_MESS_PROPERTIES_TAB_DEFAULT"] = "Specifications";
$MESS["CP_BC_TPL_MESS_REVIEWS_TAB"] = "Tab Text \"Reviews\"";
$MESS["CP_BC_TPL_MESS_REVIEWS_TAB_DEFAULT"] = "Reviews";
$MESS['CP_BC_TPL_MESS_STORES_TAB'] = "Tab Text \"Availability\"";
$MESS['CP_BC_TPL_MESS_STORES_TAB_DEFAULT'] = "Availability";
$MESS['CP_BC_TPL_MESS_DELIVERY_TAB'] = "Tab Teaxt \"Delivery\"";
$MESS['CP_BC_TPL_MESS_DELIVERY_TAB_DEFAULT'] = "Delivery";
$MESS["CP_BC_TPL_PRODUCT_SUBSCRIPTION"] = "Allow alerts for missing items";
$MESS["CP_BC_TPL_SHOW_DISCOUNT_PERCENT"] = "Show discount percentage";
$MESS["CP_BC_TPL_DISCOUNT_PERCENT_POSITION"] = "Discount percentage location";
$MESS["CP_BC_TPL_SHOW_OLD_PRICE"] = "Show old price";
$MESS["CP_BC_TPL_SHOW_MAX_QUANTITY"] = "Show the rest of the goods";
$MESS["CP_BC_TPL_SHOW_MAX_QUANTITY_Y"] = "with display of real balance";
$MESS["CP_BC_TPL_SHOW_MAX_QUANTITY_M"] = "with the substitution of the remainder of the text";
$MESS["CP_BC_TPL_SHOW_MAX_QUANTITY_N"] = "don't show";
$MESS["CP_BC_TPL_MESS_SHOW_MAX_QUANTITY"] = "Text for the remainder";
$MESS["CP_BC_TPL_MESS_SHOW_MAX_QUANTITY_DEFAULT"] = "Availability";
$MESS["CP_BC_TPL_RELATIVE_QUANTITY_FACTOR"] = "Value from which the substitution occurs";
$MESS["RELATIVE_QUANTITY_FACTOR_TIP"] = "The comparison occurs with the value calculated by the formula \"Available quantity\"/\"Unit ratio\"";
$MESS["CP_BC_TPL_MESS_RELATIVE_QUANTITY_MANY"] = "Text for value greater";
$MESS["CP_BC_TPL_MESS_RELATIVE_QUANTITY_MANY_DEFAULT"] = "Lot";
$MESS["CP_BC_TPL_MESS_RELATIVE_QUANTITY_FEW"] = "Text for the value less";
$MESS["CP_BC_TPL_MESS_RELATIVE_QUANTITY_FEW_DEFAULT"] = "Few";
$MESS["CP_BC_TPL_USE_COMMON_SETTINGS_BASKET_POPUP"] = "Same settings for showing add to cart or purchase on all pages";
$MESS["CP_BC_TPL_COMMON_ADD_TO_BASKET_ACTION"] = "Show button to add to cart or purchase";
$MESS["CP_BC_TPL_COMMON_SHOW_CLOSE_POPUP"] = "Show a button to continue shopping in pop-ups";
$MESS["CP_BC_TPL_TOP_ADD_TO_BASKET_ACTION"] = "Show add to cart or purchase button on product page";
$MESS["CP_BC_TPL_SECTION_ADD_TO_BASKET_ACTION"] = "Show add to cart or purchase button on product listing page";
$MESS["CP_BC_TPL_DETAIL_ADD_TO_BASKET_ACTION"] = "Show buttons for add to cart and purchase on the product detail page";
$MESS["CP_BC_TPL_DETAIL_ADD_TO_BASKET_ACTION_PRIMARY"] = "Highlight the add to cart and purchase buttons on the product detail page";
$MESS["CP_BC_TPL_MESS_BTN_LAZY_LOAD"] = "Button text \"Show more\"";
$MESS["CP_BC_TPL_MESS_BTN_LAZY_LOAD_DEFAULT"] = "Show more";
$MESS["CP_BC_TPL_LAZY_LOAD"] = "Show lazy download button";
$MESS["CP_BC_TPL_LOAD_ON_SCROLL"] = "Load goods while scrolling to the end";
$MESS["CP_BC_TPL_MESS_BTN_BUY"] = "Button text \"Buy\"";
$MESS["CP_BC_TPL_MESS_BTN_ADD_TO_BASKET"] = "Button text \"Add to Cart\"";
$MESS["CP_BC_TPL_MESS_BTN_COMPARE"] = "Button text \"Comparison\"";
$MESS["CP_BC_TPL_MESS_NOT_AVAILABLE"] = "Notification message";
$MESS["CP_BC_TPL_MESS_BTN_DETAIL"] = "Button text \"Read more\"";
$MESS["CP_BC_TPL_MESS_BTN_SUBSCRIBE"] = "Button text \"Notify about admission\"";
$MESS["CP_BC_TPL_MESS_BTN_SUBSCRIBE_DEFAULT"] = "Subscribe";
$MESS["CP_BC_TPL_MAIN_BLOCK_PROPERTY_CODE"] = "Properties show in the box to the right of the image";
$MESS["CP_BC_TPL_MAIN_BLOCK_OFFERS_PROPERTY_CODE"] = "Properties of the proposals show in the block to the right of the picture";
$MESS["CP_BC_TPL_DETAIL_USE_VOTE_RATING"] = "Include product rating";
$MESS["CP_BC_TPL_DETAIL_VOTE_DISPLAY_AS_RATING"] = "Show as rating";
//$MESS["CP_BC_TPL_DETAIL_USE_COMMENTS"] = "Include product reviews";
$MESS["CP_BC_TPL_DETAIL_USE_REVIEWS"] = "Include product reviews";
$MESS["CP_BC_TPL_DETAIL_BLOG_USE"] = "Use comments";
$MESS["CP_BC_DETAIL_TPL_BLOG_URL"] = "Blog name in Latin letters";
$MESS["CP_BC_TPL_DETAIL_BLOG_EMAIL_NOTIFY"] = "Email Notification";
$MESS["CP_BC_TPL_DETAIL_VK_USE"] = "Use Vkontakte";
$MESS["CP_BC_TPL_DETAIL_VK_API_ID"] = "Vkontakte application identifier (API_ID)";
$MESS["CP_BC_TPL_DETAIL_FB_APP_ID"] = "Application id (APP_ID)";
$MESS["CP_BC_TPL_DETAIL_FB_USE"] = "Use Facebook";
$MESS["CP_BC_TPL_FB_APP_ID"] = "Application id (APP_ID)";
$MESS["CP_BC_TPL_MESS_BTN_BUY_DEFAULT"] = "Buy";
$MESS["CP_BC_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "To cart";
$MESS["CP_BC_TPL_MESS_BTN_COMPARE_DEFAULT"] = "Comparison";
$MESS["CP_BC_TPL_MESS_BTN_DETAIL_DEFAULT"] = "Read more";
$MESS["CP_BC_TPL_MESS_NOT_AVAILABLE_DEFAULT"] = "Not available";
$MESS["ADD_TO_BASKET_ACTION_BUY"] = "purchases";
$MESS["ADD_TO_BASKET_ACTION_ADD"] = "add to cart";
$MESS["CPT_BC_TPL_TOP_ROTATE_TIMER"] = "Show time of one slide of top elements, sec (0 - turn off automatic slide change)";
$MESS["CP_BC_TPL_DETAIL_BRAND_USE"] = "Use component \"Brands\"";
$MESS["CP_BC_TPL_DETAIL_PROP_CODE"] = "The property of binding the brand to the product";
$MESS["CP_BC_TPL_USE_SALE_BESTSELLERS"] = "Show the list of top sellers";
$MESS["DETAIL_DETAIL_PICTURE_MODE_IMG"] = "normal";
$MESS["DETAIL_DETAIL_PICTURE_MODE_POPUP"] = "pop-up";
$MESS["DETAIL_DETAIL_PICTURE_MODE_MAGNIFIER"] = "magnifier";
$MESS["DETAIL_DETAIL_PICTURE_MODE_GALLERY"] = "gallery";
$MESS["CPT_BC_TPL_COMPARE_POSITION_FIXED"] = "Show comparison list over page";
$MESS["CPT_BC_TPL_COMPARE_POSITION"] = "Position on the page";
$MESS["CPT_BC_TPL_PARAM_COMPARE_POSITION_TOP_LEFT"] = "top left";
$MESS["CPT_BC_TPL_PARAM_COMPARE_POSITION_TOP_RIGHT"] = "top right";
$MESS["CPT_BC_TPL_PARAM_COMPARE_POSITION_BOTTOM_LEFT"] = "down left";
$MESS["CPT_BC_TPL_PARAM_COMPARE_POSITION_BOTTOM_RIGHT"] = "down right";
$MESS["CP_BC_TPL_DETAIL_SHOW_BASIS_PRICE"] = "Show on the detailed page the price per unit";
$MESS["CP_BC_TPL_USE_BIG_DATA"] = "Show personal recommendations";
$MESS["CP_BC_TPL_BIG_DATA_RCM_TYPE"] = "Recommendation type";
$MESS["CP_BC_TPL_RCM_BESTSELLERS"] = "Top Selling";
$MESS["CP_BC_TPL_RCM_PERSONAL"] = "Personal recommendations";
$MESS["CP_BC_TPL_RCM_SOLD_WITH"] = "Sold with this product";
$MESS["CP_BC_TPL_RCM_VIEWED_WITH"] = "Watched with this item";
$MESS["CP_BC_TPL_RCM_SIMILAR"] = "Similar products";
$MESS["CP_BC_TPL_RCM_SIMILAR_ANY"] = "Selling / Viewing / Similar products";
$MESS["CP_BC_TPL_RCM_PERSONAL_WBEST"] = "Top Selling / Personal";
$MESS["CP_BC_TPL_RCM_RAND"] = "Any recommendation";
$MESS["TEMPLATE_THEME_TIP"] = "Color theme to show. The default is the blue theme.";
$MESS["TOP_VIEW_MODE_TIP"] = "The setting determines what the top elements on the page will look like";
$MESS["LABEL_PROP_TIP"] = "Product Tag Property";
$MESS["ADD_PICT_PROP_TIP"] = "Property of additional product images";
$MESS["PRODUCT_DISPLAY_MODE_TIP"] = "Product display scheme (with or without sku, etc.)";
$MESS["OFFER_ADD_PICT_PROP_TIP"] = "Property of additional pictures of sentences (if there is)";
$MESS["OFFER_TREE_PROPS_TIP"] = "Properties whose values will be used to group sales offers";
$MESS["DETAIL_ADD_DETAIL_TO_SLIDER"] = "A detailed image is displayed along with additional images in the product slider";
$MESS["DETAIL_DISPLAY_PREVIEW_TEXT_MODE_TIP"] = "This setting allows you to specify whether and when to display the description of the announcement on the detail page of the item";
$MESS["PRODUCT_SUBSCRIPTION_TIP"] = "Allow notifying the customer that the product of interest is available for purchase";
$MESS["SHOW_DISCOUNT_PERCENT_TIP"] = "Shows the discount percentage if there is a discount";
$MESS["SHOW_OLD_PRICE_TIP"] = "Show the old price if there is a discount";
$MESS["DETAIL_SHOW_MAX_QUANTITY_TIP"] = "Show total quantity if quantity accounting is enabled for the item";
$MESS["MESS_BTN_BUY_TIP"] = "What text to show on the button";
$MESS["MESS_BTN_ADD_TO_BASKET_TIP"] = "What text to show on the button";
$MESS["MESS_BTN_COMPARE"] = "What text to show on the button";
$MESS["MESS_BTN_DETAIL_TIP"] = "What text to show on the button";
$MESS["MESS_NOT_AVAILABLE_TIP"] = "Message about the absence of goods and the inability to buy it";
$MESS["DETAIL_USE_VOTE_RATING_TIP"] = "Enable product rating";
$MESS["DETAIL_USE_COMMENTS_TIP"] = "Enable product reviews";
$MESS["SECTIONS_VIEW_MODE_TIP"] = "Determines how the list of subsections on the page will be displayed. Attention! For all options for displaying the list of subsections (except for the \"Multi-level list \"), you should set the depth of sections equal to 1";
$MESS["SECTIONS_SHOW_PARENT_NAME_TIP"] = "Determines whether to display the name of the current section (except the head)";
$MESS["SECTIONS_HIDE_SECTION_NAME_TIP"] = "For a list of subsections \"Block\" when this option is selected, only pictures of sections will be shown";
$MESS["TOP_ROTATE_TIMER_TIP"] = "Show time for one slide of top elements. Used for automatic slide scrolling.";
$MESS["USE_COMMON_SETTINGS_BASKET_POPUP_TIP"] = "If this option is selected, the display of add to cart or purchase buttons on all pages will be set by a single list of settings. If removed - you can set individual behavior for the list of products, a detailed page and top'a goods";
$MESS["COMMON_SHOW_CLOSE_POPUP_TIP"] = "If the option is checked, a button will appear in the pop-up windows \"Continue shopping\"";
$MESS["USE_BIG_DATA_TIP"] = "Show personalized recommendations on catalog pages";
$MESS["CPT_SIDEBAR_PATH"] = "Path to the included area to display information in the right block";
$MESS["CPT_SIDEBAR_SECTION_SHOW"] = "Show the right block in the product list";
$MESS["CPT_SIDEBAR_DETAIL_SHOW"] = "Show the right block on the detail page";
$MESS["CP_BC_TPL_USE_ENHANCED_ECOMMERCE"] = "Enable data to be sent to e-commerce";
$MESS["USE_ENHANCED_ECOMMERCE_TIP"] = "Requires additional configuration in Google Analytics Enhanced
Ecommerce and / or Yandex. Metrics";
$MESS["CP_BC_TPL_DATA_LAYER_NAME"] = "Data container name";
$MESS["CP_BC_TPL_BRAND_PROPERTY"] = "Brand property";
$MESS["CP_BC_TPL_USE_RATIO_IN_RANGES"] = "Consider pricing factors for price ranges";
$MESS ['CATALOG_ITEM_DISPLAY_WISH_LIST'] = "Add to favorites";
$MESS['CPT_LIST_SORT'] = "Active sorting";
$MESS['CPT_LIST_SORT_POPULAR'] = "Popularity";
$MESS['CPT_LIST_SORT_NAME'] = "Name";
$MESS['CPT_LIST_SORT_PRICE'] = "Price";
$MESS['CPT_ARTICLE_PROP'] = "Property containing product code";
$MESS['CP_BC_TPL_DETAIL_ACTIONS_USE'] = "Display block with stock for product";
$MESS['CP_BC_TPL_DETAIL_ACTIONS_IBLOCK_ID'] = "Iblock with shares";
$MESS['CP_BC_TPL_DETAIL_DISPLAY_PROPERTIES'] = "Properties to display in the block Characteristics";
$MESS['CP_BC_TPL_DETAIL_SHOW_QUESTION_FORM'] = "Display Form \"Ask a Question\"";
$MESS['CP_BC_TPL_DETAIL_LINK_PRODUCTS_USE'] = "Display block \"Related Products\"";
$MESS['CP_BC_TPL_DETAIL_LINK_PRODUCTS_PROP'] = "Property to link goods";
$MESS['CP_BC_TPL_DETAIL_LINK_PRODUCTS_TITLE'] = "Block title \"Related Products\"";
$MESS['CP_BC_TPL_DETAIL_LINK_PRODUCTS_TITLE_DEFAULT'] = "You may need";
$MESS['CP_BC_TPL_DETAIL_SHOW_DELIVERY_TAB'] = "Display tab \"Delivery\"";
$MESS['CPT_ELEMENT_TABS_TEMPLATE'] = "Option show tabs in the item card";
$MESS['CP_BC_TPL_DETAIL_POPULAR_TITLE'] = "Block title \"Popular in section\"";
$MESS['CP_BC_TPL_DETAIL_POPULAR_TITLE_DEFAULT'] = "You may also like";
$MESS['CP_BC_TPL_DETAIL_HIDE_TABS'] = "Tab output in a single list";
$MESS['CP_BC_TPL_OFFERS_HIDE_TITLE'] = "Hide the name of the properties of trade offers";
$MESS['CP_BC_TPL_COMPARE_SHOW_BASKET_BTN'] = "Show basket button";
$MESS['CP_BC_TPL_COMPARE_SHOW_DELAY_BTN'] = "Show delay button";
$MESS['TEMPLATE_ITEM'] = "Product card template";
$MESS['TEMPLATE_ITEM_CUSTOM'] = "Custom template name (catalog.item)";
$MESS['TEMPLATE_ITEM_DEFAULT'] = "Default (main)";
$MESS["CMP_SHOW_GENERAL_STORE_INFORMATION_FROM_REGION"] = "Use region stores";
