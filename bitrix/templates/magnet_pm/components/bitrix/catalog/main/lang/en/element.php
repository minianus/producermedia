<?
$MESS['PRICE_RANGES_TITLE'] = "Prices";
$MESS['DESCRIPTION_TAB'] = "Description";
$MESS['PROPERTIES_TAB'] = "Specifications";
$MESS['REVIEWS_TAB'] = "Reviews";
$MESS['STORES_TAB'] = "Availability";

$MESS["CATALOG_PERSONAL_RECOM"] = "Personal recommendations";
$MESS["CATALOG_POPULAR_IN_SECTION"] = "You may also like";
$MESS["CATALOG_RECOMMENDED_BY_LINK"] = "Recommend";
$MESS["CATALOG_VIEWED"] = "Viewed";
$MESS['CATALOG_QUESTION_FORM_TITLE'] = "Ask a Question";
$MESS['CATALOG_QUESTION_FORM_DESCRIPTION'] = "Ask any question you are interested in the product and our qualified specialists will help you.";
$MESS['DELIVERY_TAB_TEXT'] = "
<p>We try to be as loyal as possible in the choice of methods of delivery of goods from our store so that our customers use quality at the lowest price.</p>
<h4>City courier</h4>
<p>By courier (Mon-Fri). The courier will deliver the goods on the day of order from 6 pm to 9 pm (provided that the order is completed before 6 pm), or in the evening of the next day (if ordered after 6 pm).</p>
<h4>Pickup</h4>
<p>You can pick up the goods ordered on the site any day and time convenient for you. Opening hours and contact phone number of the store can be found here. The booked item will be waiting for you within two working days.</p>
";