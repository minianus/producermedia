<?php

$MESS["CATALOG_SORT_POPULAR"] = "По популярности";
$MESS["CATALOG_SORT_NAME"] = "По алфавиту";
$MESS["CATALOG_SORT_PRICE"] = "По цене";
$MESS['CATALOG_DISPLAY_LIST_TYPE_CARD'] = "Отображать плиткой";
$MESS['CATALOG_DISPLAY_LIST_TYPE_LIST'] = "Отображать списком";
$MESS['CATALOG_DISPLAY_LIST_TYPE_PRICELIST'] = "Отображать прайс-листом";
$MESS['FILTER_BUTTON_NAME'] = "Фильтр";
$MESS['SEARCH_PAGE_TITLE'] = "Поиск в каталоге";