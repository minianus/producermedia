<?
$MESS ['TSB1_YOUR_CART'] = "Your cart";
$MESS ['TSB1_CART'] = "Cart";
$MESS ['TSB1_TOTAL_PRICE'] = "Amount";
$MESS ['TSB1_PERSONAL'] = "Personal section";
$MESS ['TSB1_LOGIN'] = "Log in";
$MESS ['TSB1_LOGOUT'] = "Log out";
$MESS ['TSB1_REGISTER'] = "Sign up";

$MESS ['TSB1_READY'] = "Ready to buy goods";
$MESS ['TSB1_DELAY'] = "Wishlist";
$MESS ['TSB1_NOTAVAIL'] = "Unavailable goods";
$MESS ['TSB1_SUBSCRIBE'] = "Signed goods";

$MESS ['TSB1_SUM'] = "Amount";
$MESS ['TSB1_DELETE'] = "Delete";

$MESS ['TSB1_2ORDER'] = "Checkout";
$MESS ['TSB1_2BASKET'] = "Go to cart";

$MESS ['TSB1_EXPAND'] = "Show";
$MESS ['TSB1_COLLAPSE'] = "Hide";
$MESS ['TSB1_CLEAR_BASKET'] = "Clear cart";

$MESS ['TSB1_COL_NAME'] = "Name";
$MESS ['TSB1_COL_PRICE'] = "Price";
$MESS ['TSB1_COL_QTY'] = "Amount";
$MESS ['TSB1_COL_SUM'] = "Sum";
$MESS ['TSB1_TOTALPRICE'] = "Total: <span>#VALUE#</span>";
$MESS ['TSB1_EMPTY_BAKET_TITLE'] = "Your cart is empty";
$MESS ['TSB1_EMPTY_BAKET_MESSAGE'] = "Select the product of interest in the catalog and add it to the «Cart».";
$MESS ['TSB1_EMPTY_BAKET_2CATALOG'] = "Go to catalog";