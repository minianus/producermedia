<?
$MESS ['TSB1_YOUR_CART'] = "Ваша корзина";
$MESS ['TSB1_CART'] = "Корзина";
$MESS ['TSB1_TOTAL_PRICE'] = "на сумму";
$MESS ['TSB1_PERSONAL'] = "Персональный раздел";
$MESS ['TSB1_LOGIN'] = "Войти";
$MESS ['TSB1_LOGOUT'] = "Выйти";
$MESS ['TSB1_REGISTER'] = "Регистрация";

$MESS ['TSB1_READY'] = "Готовые к покупке товары";
$MESS ['TSB1_DELAY'] = "Избранное";
$MESS ['TSB1_NOTAVAIL'] = "Недоступные товары";
$MESS ['TSB1_SUBSCRIBE'] = "Подписанные товары";

$MESS ['TSB1_SUM'] = "на сумму";
$MESS ['TSB1_DELETE'] = "Удалить";

$MESS ['TSB1_2ORDER'] = "Оформить заказ";
$MESS ['TSB1_2BASKET'] = "Перейти в корзину";

$MESS ['TSB1_EXPAND'] = "Раскрыть";
$MESS ['TSB1_COLLAPSE'] = "Скрыть";
$MESS ['TSB1_CLEAR_BASKET'] = "Очистить корзину";

$MESS ['TSB1_COL_NAME'] = "Наименование";
$MESS ['TSB1_COL_PRICE'] = "Цена";
$MESS ['TSB1_COL_QTY'] = "Количество";
$MESS ['TSB1_COL_SUM'] = "Сумма";
$MESS ['TSB1_TOTALPRICE'] = "Итого: <span>#VALUE#</span>";
$MESS ['TSB1_EMPTY_BAKET_TITLE'] = "Ваша корзина пуста";
$MESS ['TSB1_EMPTY_BAKET_MESSAGE'] = "Выберите в каталоге интересующий товар и добавьте его «В корзину».";
$MESS ['TSB1_EMPTY_BAKET_2CATALOG'] = "Перейти в каталог";