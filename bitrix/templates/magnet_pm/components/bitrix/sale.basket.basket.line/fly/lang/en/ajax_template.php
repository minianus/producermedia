<?

$MESS ['TSB1_PERSONAL'] = "Personal section";

$MESS ['TSB1_EXPAND'] = "Show";
$MESS ['TSB1_COLLAPSE'] = "Hide";

$MESS ['TSB1_CART'] = "Cart";
$MESS ['TSB1_TOTAL_PRICE'] = "Amount";

$MESS ['TSB1_YOUR_CART'] = "Your cart";

$MESS ['TSB1_READY'] = "Ready to buy goods";
$MESS ['TSB1_DELAY'] = "Deferred goods";
$MESS ['TSB1_NOTAVAIL'] = "Unavailable products";
$MESS ['TSB1_SUBSCRIBE'] = "Signed goods";

$MESS ['TSB1_SUM'] = "Amount";
$MESS ['TSB1_DELETE'] = "Delete";

$MESS ['TSB1_2ORDER'] = "Checkout";
$MESS['MINIMUM_PRICE_DESC'] = "Minimum order price #VALUE#";