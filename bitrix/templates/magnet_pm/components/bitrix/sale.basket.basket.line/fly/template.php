<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
$cartStyle = 'bx-basket';
$cartId = "bx_basket".$this->randString();
$arParams['cartId'] = $cartId;

?>
<div id="fly-basket-container-overlay"></div>
<div id="<?=$cartId?>" class="fly-basket-container">
    <?
	/** @var \Bitrix\Main\Page\FrameBuffered $frame */
	$frame = $this->createFrame($cartId, false)->begin();
		require(realpath(dirname(__FILE__)).'/ajax_template.php');
	$frame->beginStub();
		$arResult['COMPOSITE_STUB'] = 'Y';
		require(realpath(dirname(__FILE__)).'/top_template.php');
		unset($arResult['COMPOSITE_STUB']);
	$frame->end();
?></div>
<script type="text/javascript">
    if (typeof(window.CSolution.initBasket) == 'function') {
        window.CSolution.initBasket({
            siteId: '<?=SITE_ID?>',
            cartId: '<?=$cartId?>',
            ajaxPath: '<?=$componentPath?>/ajax.php',
            templateName: '<?=$templateName?>',
            arParams: <?=CUtil::PhpToJSObject ($arParams)?>
        });
        
        $("body").on('click', '.fly-basket-container .basket-link', function (event) {
            if (document.body.clientWidth > 870) {
                event.preventDefault();
                $('.fly-basket-container, #fly-basket-container-overlay').toggleClass('open');
                return false;
            }
        });
        
        $(document).click(function(event) {
            if ($(event.target).closest(".fly-basket-container").length) return;
            $(".fly-basket-container, #fly-basket-container-overlay").removeClass("open");
            event.stopPropagation();
        });
        
        $("body").on('click', '.fly-basket-container .tabs .tab', function () {
            var parent = $(this).parents('.fly-basket-container');
            parent.find('.tabs .tab').removeClass('active');
            $(this).addClass('active');
            parent.find('.tabs-content .tab-content').removeClass('active');
            parent.find('.tabs-content .tab-content:eq('+$(this).index()+')').addClass('active');
        });
        
        $("body").on('click', '.fly-basket-container .counter a', function () {
            // minus and plus events
            var container = $(this).parent(), step = parseFloat(container.data('step')), max = container.data('max'), curValue = parseFloat(container.find('input').val()), newValue = curValue;
            var action = $(this).hasClass('minus') ? 'minus' : 'plus';
            var id = $(this).parents('tr').data('id');
            
            if (id !== undefined) {
                if (action == 'minus' && (curValue - step) >= step)
                        newValue-=step;

                if (max !== undefined) {
                    max = parseFloat(max);

                    if (action == 'plus' && (curValue + step) <= max)
                        newValue += step;
                } else {
                    if (action == 'plus')
                        newValue += step;
                }

                if (curValue != newValue) {
                    container.find('input').val(newValue);

                    $.ajax({
                        url: '<?=$templateFolder?>/ajax_actions.php',
                        type: 'post',
                        data: {
                            siteId: '<?=SITE_ID?>',
                            sessid: BX.bitrix_sessid(),
                            action: 'update',
                            id: id,
                            qty: newValue
                        },
                        success: function () {
                            BX.onCustomEvent('OnBasketChange');
                        }
                    });
                }
            }
            
            
        });
        
        $("body").on('click', '.fly-basket-container .delete a', function () {
            // delete product
            var id = $(this).parents('tr').data('id');
            if (id !== undefined) {
                $.ajax({
                        url: '<?=$templateFolder?>/ajax_actions.php',
                        type: 'post',
                        data: {
                            siteId: '<?=SITE_ID?>',
                            sessid: BX.bitrix_sessid(),
                            action: 'delete',
                            id: id
                        },
                        success: function () {
                            BX.onCustomEvent('OnBasketChange');
                        }
                    });
            }
        });
        
        $("body").on('click', '.fly-basket-container .clear-basket', function () {
            // clear all basket
            $.ajax({
                        url: '<?= $templateFolder ?>/ajax_actions.php',
                        type: 'post',
                        data: {
                            siteId: '<?= SITE_ID ?>',
                            sessid: BX.bitrix_sessid(),
                            action: 'clear'
                        },
                        success: function () {
                            BX.onCustomEvent('OnBasketChange');
                        }
                    });
        });
    }
</script>