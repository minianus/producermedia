<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Nextype\Magnet\CSolution;

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die('Module nextype.magnet not installed');

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

CSolution::getInstance(SITE_ID);
?>

<div class="basket-list">
    <? if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY']))): ?>
    <div class="basket-list-content tabs-container">
        <div class="basket-list-header">
            <div class="title h2"><?=GetMessage('TSB1_CART')?></div>
            <div class="tabs">
                <? $bFirst = true; ?>
                <?foreach ($arResult["CATEGORIES"] as $category => $items):
                    if (empty($items))
                        continue;
		?>
                <a href="javascript:void(0);" class="tab<?=$bFirst ? ' active' : ''?>"><?=GetMessage("TSB1_".$category)?> <span>(<?=count($items)?>)</span></a>
                <? $bFirst = false; ?>
                <? endforeach; ?>
            </div>
            <a href="javascript:void(0)" class="clear-basket"><?=GetMessage('TSB1_CLEAR_BASKET')?></a>
        </div>
        
        <div class="tabs-content">
        <? $bFirst = true; ?>
            
        <?foreach ($arResult["CATEGORIES"] as $category => $items):
                    if (empty($items))
                        continue;
		?>
            <div class="tab-content<?=$bFirst ? ' active' : ''?><?=count($items) > 2 ? ' scrollbar-inner' : ''?>">
                <table>
                    <thead>
                        <tr>
                            
                            <th colspan="2"><?=GetMessage('TSB1_COL_NAME')?></th>
                            <th><?=GetMessage('TSB1_COL_PRICE')?></th>
                            <th><?=GetMessage('TSB1_COL_QTY')?></th>
                            <th><?=GetMessage('TSB1_COL_SUM')?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach ($items as $v):?>
                        <tr data-id="<?=$v['ID']?>">
                            <td class="picture">
                                <? if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]): ?>
                                    <? if ($v["DETAIL_PAGE_URL"]): ?>
                                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>"></a>
                                    <? else: ?>
                                        <img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>" />
                                    <? endif ?>
                                <? endif ?>
                            </td>
                            <td class="name">
                                <?if ($v["DETAIL_PAGE_URL"]):?>
                                    <a href="<?=$v["DETAIL_PAGE_URL"]?>"><?=$v["NAME"]?></a>
				<?else:?>
                                    <?=$v["NAME"]?>
				<?endif?>
                            </td>
                            <td class="price">
                                <?= $v["PRICE_FMT"] ?>
                                <? if ($v["FULL_PRICE"] != $v["PRICE_FMT"]): ?>
                                    <div class="old"><?= $v["FULL_PRICE"] ?></div>
                                <? endif ?>
                            </td>
                            <td class="qty">
                                <div class="counter" data-step="<?=$v['MEASURE_RATIO']?>" <?if(isset($v['PRODUCT_QUANTITY'])):?>data-max="<?=$v['PRODUCT_QUANTITY']?>"<?endif;?>>
                                    <a href="javascript:void(0)" class="minus icon-custom"></a>
                                    <input type="text" class="count" value="<?=$v["QUANTITY"]?>" />
                                    <a href="javascript:void(0)" class="plus icon-custom"></a>
                                </div>
                            </td>
                            <td class="sum">
                                <?=$v["SUM"]?>
                            </td>
                            <td class="delete">
                                <a href="javascript:void(0)"></a>
                            </td>
                        </tr>
                        <? endforeach; ?>
                    </tbody>
                </table>
            </div>
                <? $bFirst = false; ?>
        <? endforeach; ?>
        </div>
    </div>
    <div class="basket-list-footer">
        <div class="left">
            <div class="total-price">
                <?=GetMessage('TSB1_TOTALPRICE', Array ("#VALUE#" => $arResult['TOTAL_PRICE']))?>
            </div>
        </div>
        <div class="right">
            
            <? $cleanTotalPrice = (float)preg_replace("/[^.,0-9]/", '', $arResult['TOTAL_PRICE']); ?>
            <? if (floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']) > 0 && $cleanTotalPrice < floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE'])): ?>
		<div class="basket-checkout-minimum">
                    <div class="icon"></div>
                    <div class="desc"><?=GetMessage('MINIMUM_PRICE_DESC', Array ("#VALUE#" => CurrencyFormat(floatval(CSolution::$options['CATALOG_ORDER_MIN_PRICE']), $arResult['BASE_CURRENCY'])))?></div>
                    
		</div>
            <? else: ?>                
            <a href="<?=$arParams['PATH_TO_ORDER']?>" class="btn transparent"><?=GetMessage('TSB1_2ORDER')?></a>
            <a href="<?=$arParams['PATH_TO_BASKET']?>" class="btn"><?=GetMessage('TSB1_2BASKET')?></a>
            <? endif; ?>
        </div>
    </div>
    <? else: ?>
    <div class="basket-list-content">
        <div class="basket-list-header">
            <div class="title h2"><?=GetMessage('TSB1_CART')?></div>
        </div>
        <div class="basket-list-empty">
            <div class="content">
                <div class="title h4"><?=GetMessage('TSB1_EMPTY_BAKET_TITLE')?></div>
                <div class="message"><?=GetMessage('TSB1_EMPTY_BAKET_MESSAGE')?></div>
                <a href="<?=$arParams['PATH_TO_CATALOG']?>" class="btn"><?=GetMessage('TSB1_EMPTY_BAKET_2CATALOG')?></a>
            </div>
        </div>
    </div>
    <? endif; ?>
</div>

