<?php

if (!empty($arResult['CATEGORIES']))
{
    foreach ($arResult['CATEGORIES'] as $category => $arItems)
    {
        foreach ($arItems as $key => $arItem)
        {
            $arRatio = CCatalogMeasureRatio::getList(array(), array("PRODUCT_ID" => $arItem['PRODUCT_ID']), false, false, array())->fetch();
            $arProduct = CCatalogProduct::GetByID($arItem['PRODUCT_ID']);
            $arResult['CATEGORIES'][$category][$key]['MEASURE_RATIO'] = (!empty($arRatio['RATIO'])) ? $arRatio['RATIO'] : 1;
            if ($arProduct)
                $arResult['CATEGORIES'][$category][$key]['PRODUCT_QUANTITY'] = $arProduct['QUANTITY'];
        }
    }
    
}

$arResult['BASE_CURRENCY'] = "RUB";
$rsCurrency = CCurrency::GetList(($by="name"), ($order="asc"), LANGUAGE_ID);
while ($arCurrency = $rsCurrency->fetch())
{
    if ($arCurrency['BASE'] == "Y")
    {
        $arResult['BASE_CURRENCY'] = $arCurrency['CURRENCY'];
        break;
    }
}