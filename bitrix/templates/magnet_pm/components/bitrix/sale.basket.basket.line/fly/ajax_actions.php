<?php

define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$siteId = !empty($_POST['siteId']) ? $_POST['siteId'] : SITE_ID;
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['action']) && check_bitrix_sessid() && !empty($siteId))
{
    if (\Bitrix\Main\Loader::includeModule('catalog') && \Bitrix\Main\Loader::includeModule('sale'))
    {
        $siteID = 
        $fUserID = CSaleBasket::GetBasketUserID();
        $fUserFilter = array("=FUSER_ID" => $fUserID, "=LID" => $siteId, "ORDER_ID" => null);

        switch ($_POST['action'])
        {
            case 'update':
                $numProducts = CSaleBasket::GetList(
			array(),
			$fUserFilter + array("ID" => intval($_POST['id'])),
			array()
		);

		if ($numProducts > 0)
                {
                    CSaleBasket::Update(intval($_POST['id']), Array ('QUANTITY' => floatval($_POST['qty'])));
                }

                break;
                
            case 'delete':
                $numProducts = CSaleBasket::GetList(
			array(),
			$fUserFilter + array("ID" => intval($_POST['id'])),
			array()
		);

		if ($numProducts > 0)
                {
                    CSaleBasket::Delete(intval($_POST['id']));
                }
                break;
                
            case 'clear':
                $rsBasket = CSaleBasket::GetList(
			array(),
			$fUserFilter,
			false
		);
                while ($arBasketItem = $rsBasket->Fetch())
                {
                    CSaleBasket::Delete($arBasketItem['ID']);
                }
                break;
        }
    }
}
