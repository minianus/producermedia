<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
$cartStyle = 'bx-basket';
$cartId = "bx_basket".$this->randString();
$arParams['cartId'] = $cartId;

?>
<div id="<?=$cartId?>" class="basket-container-items">
    <?
	/** @var \Bitrix\Main\Page\FrameBuffered $frame */
	$frame = $this->createFrame($cartId, false)->begin();
		require(realpath(dirname(__FILE__)).'/ajax_template.php');
	$frame->beginStub();
		$arResult['COMPOSITE_STUB'] = 'Y';
		require(realpath(dirname(__FILE__)).'/top_template.php');
		unset($arResult['COMPOSITE_STUB']);
	$frame->end();
?></div>
<script type="text/javascript">
    if (typeof(window.CSolution.initBasket) == 'function') {
        window.CSolution.initBasket({
            siteId: '<?=SITE_ID?>',
            cartId: '<?=$cartId?>',
            ajaxPath: '<?=$componentPath?>/ajax.php',
            templateName: '<?=$templateName?>',
            arParams: <?=CUtil::PhpToJSObject ($arParams)?>
        });
    }
</script>