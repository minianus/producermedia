<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>

<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="basket-link icon-custom">
    <span class="items-counter"><?=$arResult['NUM_PRODUCTS']?></span>
</a>
<div class="basket-text">
    <div class="name"><?= GetMessage('TSB1_CART') ?></div>
    <div class="sum"><?= $arResult['TOTAL_PRICE'] ?></div>
</div>
<script>
    localStorage.setItem('numberOfTovar', <?=$arResult['NUM_PRODUCTS']?>);
</script>
