<?
$MESS ['AUTH_CHANGE_PASSWORD'] = "Enter the new password in the fields below. After successful password change, go through authorization.";
$MESS ['AUTH_LOGIN'] = "Login:";
$MESS ['AUTH_CHECKWORD'] = "Check string:";
$MESS ['AUTH_NEW_PASSWORD'] = "New password:";
$MESS ['AUTH_NEW_PASSWORD_CONFIRM'] = "Password confirmation:";
$MESS ['AUTH_CHANGE'] = "Change Password";
$MESS ['AUTH_REQ'] = "Required fields";
$MESS ['AUTH_AUTH'] = "Login";
$MESS ['AUTH_NEW_PASSWORD_REQ'] = "New password:";
$MESS["AUTH_SECURE_NOTE"]="Before sending the form, the password will be encrypted in the browser. This will avoid passing the password in the clear.";
$MESS["AUTH_NONSECURE_NOTE"]="Password will be sent in clear text. Enable JavaScript in your browser to encrypt the password before sending.";
$MESS["system_auth_captcha"] = "Enter the characters from the image:";
$MESS["change_pass_code"] = "Confirmation code";
$MESS["change_pass_phone_number"] = "Phone";
?>