<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($_REQUEST["USER_LOGIN"]))
    $arResult["PHONE_REGISTRATION"] = false;

if ($arResult["PHONE_REGISTRATION"]) {
    CJSCore::Init('phone_auth');
}

if (isset($APPLICATION->arAuthResult))
    $arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;
?>

<div class="auth">
    <div class="reg">
        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR'): ?>
            <div class="message-errors"><?= $arResult['ERROR_MESSAGE']['MESSAGE'] ?></div>
        <? endif; ?>

        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'OK'): ?>
            <div class="message-success"><?= $arResult['ERROR_MESSAGE']['MESSAGE'] ?></div>
            <div class="buttons">
                <a href="<?= $arResult["AUTH_AUTH_URL"] ?>" class="btn"><?= GetMessage("AUTH_AUTH") ?></a>
            </div>
        <? endif; ?>
        <? if ($arResult["SHOW_FORM"]): ?>
            <div class="text"><?= GetMessage("AUTH_CHANGE_PASSWORD") ?></div>
            <div class="form">
                <form method="post" action="<?= $arResult["AUTH_FORM"] ?>" name="bform">
                    <? if (strlen($arResult["BACKURL"]) > 0): ?>
                        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                    <? endif ?>
                    <input type="hidden" name="AUTH_FORM" value="Y">
                    <input type="hidden" name="TYPE" value="CHANGE_PWD">

                    <? if ($arResult["PHONE_REGISTRATION"]): ?>
                        <div class="input-container">
                            <div class="label"><?= GetMessage("change_pass_phone_number") ?>:</div>
                            <input type="text" value="<?= htmlspecialcharsbx($arResult["USER_PHONE_NUMBER"]) ?>"
                                   disabled="disabled"/>
                            <input type="hidden" name="USER_PHONE_NUMBER"
                                   value="<?= htmlspecialcharsbx($arResult["USER_PHONE_NUMBER"]) ?>"/>
                        </div>
                        <div class="input-container">
                            <div class="label"><?= GetMessage("change_pass_code") ?>:</div>
                            <input type="text" name="USER_CHECKWORD" maxlength="255"
                                   value="<?= $arResult["USER_CHECKWORD"] ?>" autocomplete="off"/>
                        </div>
                    <? else: ?>
                        <input type="hidden" name="USER_LOGIN" value="<?= $arResult["LAST_LOGIN"] ?>"/>
                        <input type="hidden" name="USER_CHECKWORD" value="<?= $arResult["USER_CHECKWORD"] ?>"/>
                    <? endif; ?>
                    <div class="input-container">
                        <div class="label"><?= GetMessage("AUTH_NEW_PASSWORD_REQ") ?></div>
                        <input type="password" name="USER_PASSWORD" maxlength="50"
                               value="<?= $arResult["USER_PASSWORD"] ?>" autocomplete="off"/>
                    </div>

                    <div class="input-container">
                        <div class="label"><?= GetMessage("AUTH_NEW_PASSWORD_CONFIRM") ?></div>
                        <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50"
                               value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off"/>
                    </div>

                    <? if ($arResult["USE_CAPTCHA"]): ?>
                        <div class="input-container">
                            <div class="label"><?= GetMessage("AUTH_EMAIL") ?>:</div>
                            <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"
                                 width="180" height="40" alt="CAPTCHA"/>
                            <br/>
                            <input type="text" name="captcha_word" maxlength="50" value=""/>
                        </div>
                    <? endif ?>
                    <? if ($arResult["PHONE_REGISTRATION"]): ?>
                        <div class="input-container">
                            <div id="bx_chpass_resend"></div>
                        </div>
                        <div class="message-errors" id="bx_chpass_error" style="display:none"></div>
                    <?endif; ?>
                    <div class="buttons">
                        <button type="submit" name="change_pwd" value="Y"
                                class="btn submit"><?= GetMessage("AUTH_CHANGE") ?></button>
                        <a href="<?= $arResult["AUTH_AUTH_URL"] ?>" class="auth-link"><?= GetMessage("AUTH_AUTH") ?></a>
                    </div>
                </form>
                <? if ($arResult["PHONE_REGISTRATION"]): ?>

                    <script type="text/javascript">
                        new BX.PhoneAuth({
                            containerId: 'bx_chpass_resend',
                            errorContainerId: 'bx_chpass_error',
                            interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                            data:
                                <?=CUtil::PhpToJSObject([
                                    'signedData' => $arResult["SIGNED_DATA"]
                                ])?>,
                            onError:
                                function (response) {
                                    var errorNode = BX('bx_chpass_error');
                                    errorNode.innerHTML = '';
                                    for (var i = 0; i < response.errors.length; i++) {
                                        errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br />';
                                    }
                                    errorNode.style.display = '';
                                }
                        });
                    </script>

                <? endif ?>
            </div>
        <? endif; ?>
    </div>
</div>