<?
$MESS["CVP_TPL_ELEMENT_DELETE_CONFIRM"] = "All information associated with this entry will be deleted. Continue?";
$MESS["CVP_TPL_MESS_BTN_BUY"] = "Buy";
$MESS["CVP_TPL_MESS_BTN_BUY_GIFT"] = "Select";
$MESS["CVP_TPL_MESS_BTN_ADD_TO_BASKET"] = "To cart";
$MESS["CVP_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "Not available";
$MESS["CVP_TPL_MESS_BTN_DETAIL"] = "Read more";
$MESS["CVP_TPL_MESS_BTN_SUBSCRIBE"] = "Subscribe";
$MESS["CVP_CATALOG_SET_BUTTON_BUY"] = "Go to cart";
$MESS["CVP_ADD_TO_BASKET_OK"] = "Item added to cart";
$MESS["CVP_TPL_MESS_PRICE_SIMPLE_MODE"] = "from #PRICE# to #MEASURE#";
$MESS["CVP_TPL_MESS_MEASURE_SIMPLE_MODE"] = "#VALUE# #UNIT#";
$MESS["CVP_TPL_MESS_BTN_COMPARE"] = "Compare";
$MESS["CVP_ADD_TO_BASKET_OK"] = "Item added to cart";
$MESS["CVP_CATALOG_TITLE_ERROR"] = "Error";
$MESS["CVP_CATALOG_TITLE_BASKET_PROPS"] = "Product properties added to cart";
$MESS["CVP_CATALOG_BASKET_UNKNOWN_ERROR"] = "Unknown error adding product to cart";
$MESS["CVP_CATALOG_BTN_MESSAGE_CLOSE"] = "Close";
$MESS["CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Go to cart";
$MESS["CVP_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "Select";
$MESS["CVP_MSG_YOU_HAVE_NOT_YET"] = "You have not viewed any products yet.";
$MESS["SGB_TPL_BLOCK_TITLE_DEFAULT"] = "Select one of the gifts";
$MESS["SGB_TPL_TEXT_LABEL_GIFT_DEFAULT"] = "Gift";
?>