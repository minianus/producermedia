<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$frame = $this->createFrame()->begin();
$strCarouselId = "carousel_" . randString(5);

if (!empty($arResult['ITEMS']))
{

	$arSkuTemplate = array();
	if(is_array($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as $iblockId => $skuProps)
		{
			$arSkuTemplate[$iblockId] = array();
			foreach ($skuProps as &$arProp)
			{
				ob_start();
				if ('TEXT' == $arProp['SHOW_MODE'])
				{
				?>
				<div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
					<span class="product-item-scu-name"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>

					<div class="bx_size_scroller_container">
						<div class="product-item-scu-list">
							<ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list"><?
								foreach ($arProp['VALUES'] as $arOneValue)
								{
									?>
								<li
									data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
									data-onevalue="<? echo $arOneValue['ID']; ?>"
									><div class="product-item-scu-item-text"><? echo htmlspecialcharsex($arOneValue['NAME']); ?></div>
									</li><?
								}
								?></ul>
						</div>
						<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
						<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
					</div>
					</div><?
				}
				elseif ('PICT' == $arProp['SHOW_MODE'])
				{
					
					?>
				<div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
					<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>

					<div class="bx_scu_scroller_container">
						<div class="bx_scu">
							<ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;"><?
								foreach ($arProp['VALUES'] as $arOneValue)
								{
									?>
								<li
									data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID'] ?>"
									data-onevalue="<? echo $arOneValue['ID']; ?>"
									style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"
									><i title="<? echo htmlspecialcharsbx($arOneValue['NAME']); ?>"></i>
							<span class="cnt"><span class="cnt_item"
													style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');"
													title="<? echo htmlspecialcharsbx($arOneValue['NAME']); ?>"
									></span></span></li><?
								}
								?></ul>
						</div>
						<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
						<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
					</div>
					</div><?
				}
				$arSkuTemplate[$iblockId][$arProp['CODE']] = ob_get_contents();
				ob_end_clean();
				unset($arProp);
			}
		}
	}

	?>
	<script type="text/javascript">
		BX.message({
			CVP_MESS_BTN_BUY: '<? echo ('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CVP_TPL_MESS_BTN_BUY_GIFT')); ?>',
			CVP_MESS_BTN_ADD_TO_BASKET: '<? echo ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CVP_TPL_MESS_BTN_ADD_TO_BASKET')); ?>',

			CVP_MESS_BTN_DETAIL: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',

			CVP_MESS_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',
			CVP_BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			CVP_BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			CVP_ADD_TO_BASKET_OK: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			CVP_TITLE_ERROR: '<? echo GetMessageJS('CVP_CATALOG_TITLE_ERROR') ?>',
			CVP_TITLE_BASKET_PROPS: '<? echo GetMessageJS('CVP_CATALOG_TITLE_BASKET_PROPS') ?>',
			CVP_TITLE_SUCCESSFUL: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			CVP_BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CVP_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			CVP_BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			CVP_BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_CLOSE') ?>'
		});
	</script>
	<section class="gift-container" id="<?=$strCarouselId?>">
            <? if(empty($arParams['HIDE_BLOCK_TITLE']) || $arParams['HIDE_BLOCK_TITLE'] !== 'Y'):?>
            <div class="title"><?=($arParams['BLOCK_TITLE']? htmlspecialcharsbx($arParams['BLOCK_TITLE']) : GetMessage('SGB_TPL_BLOCK_TITLE_DEFAULT')) ?></div>
            <? endif; ?>
            
            <div class="products-list swiper-container">
                <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $key => $arItem) :
		$strMainID = $this->GetEditAreaId($arItem['ID'] . $key);

		$arItemIDs = array(
			'ID' => $strMainID,
			'PICT' => $strMainID . '_pict',
			'SECOND_PICT' => $strMainID . '_secondpict',
			'MAIN_PROPS' => $strMainID . '_main_props',

			'QUANTITY' => $strMainID . '_quantity',
			'QUANTITY_DOWN' => $strMainID . '_quant_down',
			'QUANTITY_UP' => $strMainID . '_quant_up',
			'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
			'BUY_LINK' => $strMainID . '_buy_link',
			'SUBSCRIBE_LINK' => $strMainID . '_subscribe',

			'PRICE' => $strMainID . '_price',
			'DSC_PERC' => $strMainID . '_dsc_perc',
			'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

			'PROP_DIV' => $strMainID . '_sku_tree',
			'PROP' => $strMainID . '_prop_',
			'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
			'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
		);

		$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$strTitle = (
		isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);
		$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";

		?>
                <div class="product-wrap slider-element swiper-slide">
                    <div class="product-card" id="<? echo $strMainID; ?>">
				<div class="static">
                                        <div class="labels product-item-label-small">
                                            <div class="label hit"><?=GetMessage('SGB_TPL_TEXT_LABEL_GIFT_DEFAULT')?></div>
                                            <div id="<? echo $arItemIDs['DSC_PERC']; ?>" class="label discount" style="display:<? echo(0 < $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;">
                                                    -<? echo $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%
                                            </div>
                                        </div>
					<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="img" <? if ($arParams['SHOW_IMAGE'] != "Y"):?>style="display:none"<?endif;?>>
                                            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" id="<?=$arItemIDs['PICT']; ?>" alt="<? echo $strTitle; ?>" title="<? echo $strTitle; ?>">

					</a>
                                    
                                        <? if ($arParams['DISPLAY_RATING'] === "Y"): ?>
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:iblock.vote",
                                            "catalog",
                                            Array(
                                               "IBLOCK_ID" => $item["IBLOCK_ID"],
                                               "ELEMENT_ID" => $item['ID'],
                                               "MAX_VOTE" => 5,
                                               "VOTE_NAMES" => $arParams["VOTE_NAMES"],
                                               "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                               "CACHE_TIME" => $arParams["CACHE_TIME"],
                                            ),
                                            $component
                                         );?>
                                        <? endif; ?>
					
					<a href="<?=$arItem['DETAIL_PAGE_URL']; ?>" class="name"><?=$arItem['NAME'];?></a>
					<div class="price-container">
                                                <? if ($arParams['SHOW_OLD_PRICE'] === 'Y'): ?>
                                                <div class="old-price" id="<?= $arItemIDs['PRICE_OLD'] ?>" <?=($arItem['MIN_PRICE']['RATIO_PRICE'] >= $arItem['MIN_PRICE']['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?>>
                                                    <?=$arItem['MIN_PRICE']['PRINT_RATIO_BASE_PRICE']; ?>
                                                </div>
                                                <? endif; ?>
                                                <div class="price crossed" id="<?= $arItemIDs['PRICE'] ?>">
						<?
                                                if (!empty($arItem['MIN_PRICE']))
                                                {
                                                        if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
                                                        {
                                                                echo GetMessage(
                                                                        'CVP_TPL_MESS_PRICE_SIMPLE_MODE',
                                                                        array(
                                                                                '#PRICE#' => $arItem['MIN_PRICE']['PRINT_VALUE'],
                                                                                '#MEASURE#' => GetMessage(
                                                                                        'CVP_TPL_MESS_MEASURE_SIMPLE_MODE',
                                                                                        array(
                                                                                                '#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
                                                                                                '#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
                                                                                        )
                                                                                )
                                                                        )
                                                                );
                                                        }
                                                        else
                                                        {
                                                                echo $arItem['MIN_PRICE']['PRINT_VALUE'];
                                                        }
                                                        
                                                }
                                                ?>
                                                </div>
					</div>
				</div>
                                        
                                        <div class="hover-content">
                                            <? if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])): ?>
                                                <? if ($arItem['CAN_BUY'] && 'Y' == $arParams['USE_PRODUCT_QUANTITY']): ?>
                                                <div class="counter" data-entity="quantity-block">
                                                    <a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_DOWN']?>" class="minus icon-custom"></a>
                                                    <input class="count" id="<?=$arItemIDs['QUANTITY']?>" type="number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$arItem['CATALOG_MEASURE_RATIO']?>">
                                                    <a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_UP']?>" class="plus icon-custom"></a>
                                                </div>
                                                <? endif; ?>
                                                
                                                <? if ($arItem['CAN_BUY']): ?>
                                                <div class="product-item-button-container" id="<?=$itemIds['BASKET_ACTIONS']?>">
                                                    <a id="<? echo $arItemIDs['BUY_LINK']; ?>" rel="nofollow" href="javascript:void(0)" class="btn">
                                                        <?=('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CVP_TPL_MESS_BTN_BUY_GIFT'))?>
                                                    </a>
                                                </div>
                                                <? endif; ?>
                                            
                                                <? if (!$arItem['CAN_BUY']): ?>
                                                <div class="product-item-button-container">
                                                    <a class="btn" href="<?=$arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow">
                                                        <?=('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?>
                                                    </a>
                                                </div>
                                                <? endif; ?>
                                                
                                                <?
                                                $arJSParams = array(
                                                        'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                                                        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                                                        'SHOW_ADD_BASKET_BTN' => false,
                                                        'SHOW_BUY_BTN' => true,
                                                        'SHOW_ABSENT' => true,
                                                        'PRODUCT' => array(
                                                                'ID' => $arItem['ID'],
                                                                'NAME' => $arItem['~NAME'],
                                                                'PICT' => $arItem['PREVIEW_PICTURE'],
                                                                'CAN_BUY' => $arItem["CAN_BUY"],
                                                                'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
                                                                'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
                                                                'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
                                                                'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
                                                                'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
                                                                'ADD_URL' => $arItem['~ADD_URL'],
                                                                'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL']
                                                        ),
                                                        'BASKET' => array(
                                                                'ADD_PROPS' => "N",
                                                                'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                                                                'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                                                                'EMPTY_PROPS' => true
                                                        ),
                                                        'VISUAL' => array(
                                                                'ID' => $arItemIDs['ID'],
                                                                'PICT_ID' => $arItemIDs['PICT'],
                                                                'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                                                                'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                                                                'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                                                                'PRICE_ID' => $arItemIDs['PRICE'],
                                                                'BUY_ID' => $arItemIDs['BUY_LINK'],
                                                                'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV']
                                                        ),
                                                        'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
                                                );
                                                ?>
                                                <script type="text/javascript">
                                                        var <? echo $strObName; ?> = new JCSaleGiftBasket(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
                                                </script>
                                            
                                            <? else: ?>
                                                <!-- have offers -->
                                                <? if (!empty($arItem['OFFERS']) && isset($arSkuTemplate[$arItem['IBLOCK_ID']]))
                                                {
                                                $arSkuProps = array();
                                                ?>
                                                <div class="product-item-info-container">
                                                        <div class="bx_catalog_item_scu" id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
                                                                foreach ($arSkuTemplate[$arItem['IBLOCK_ID']] as $code => $strTemplate)
                                                                {
                                                                        if (!isset($arItem['OFFERS_PROP'][$code]))
                                                                                continue;
                                                                        echo '<div>', str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $strTemplate), '</div>';
                                                                }

                                                                if (isset($arResult['SKU_PROPS'][$arItem['IBLOCK_ID']]))
                                                                {
                                                                        foreach ($arResult['SKU_PROPS'][$arItem['IBLOCK_ID']] as $arOneProp)
                                                                        {
                                                                                if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']]))
                                                                                        continue;
                                                                                $arSkuProps[] = array(
                                                                                        'ID' => $arOneProp['ID'],
                                                                                        'SHOW_MODE' => $arOneProp['SHOW_MODE'],
                                                                                        'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
                                                                                );
                                                                        }
                                                                }
                                                                foreach ($arItem['JS_OFFERS'] as &$arOneJs)
                                                                {
                                                                        if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'])
                                                                                $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-' . $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] . '%';
                                                                }

                                                                ?>
                                                        </div>
                                                </div>
                                                                <?
                                                                if ($arItem['OFFERS_PROPS_DISPLAY'])
                                                                {
                                                                        foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer)
                                                                        {
                                                                                $strProps = '';
                                                                                if (!empty($arJSOffer['DISPLAY_PROPERTIES']))
                                                                                {
                                                                                        foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp)
                                                                                        {
                                                                                                $strProps .= '<br>' . $arOneProp['NAME'] . ' <strong>' . (
                                                                                                        is_array($arOneProp['VALUE'])
                                                                                                                ? implode(' / ', $arOneProp['VALUE'])
                                                                                                                : $arOneProp['VALUE']
                                                                                                        ) . '</strong>';
                                                                                        }
                                                                                }
                                                                                $arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                                                                        }
                                                                }
                                                ?>
                                                <? if ('Y' == $arParams['USE_PRODUCT_QUANTITY']): ?>
                                                <div class="counter" data-entity="quantity-block">
                                                    <a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_DOWN']?>" class="minus icon-custom"></a>
                                                    <input class="count" id="<?=$arItemIDs['QUANTITY']?>" type="number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$arItem['CATALOG_MEASURE_RATIO']?>">
                                                    <a href="javascript:void(0)" id="<?=$arItemIDs['QUANTITY_UP']?>" class="plus icon-custom"></a>
                                                </div>
                                                <? endif; ?>
                                                
                                                
                                                <div class="product-item-button-container" id="<?=$itemIds['BASKET_ACTIONS']?>">
                                                    <a id="<? echo $arItemIDs['BUY_LINK']; ?>" rel="nofollow" href="javascript:void(0)" class="btn">
                                                        <?=('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CVP_TPL_MESS_BTN_BUY_GIFT'))?>
                                                    </a>
                                                </div>

                                                <?
                                                $arJSParams = array(
                                                        'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                                                        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                                                        'SHOW_ADD_BASKET_BTN' => false,
                                                        'SHOW_BUY_BTN' => true,
                                                        'SHOW_ABSENT' => true,
                                                        'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
                                                        'SECOND_PICT' => false,
                                                        'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                                                        'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
                                                        'DEFAULT_PICTURE' => array(
                                                                'PICTURE' => $arItem['PRODUCT_PREVIEW'],
                                                                'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
                                                        ),
                                                        'VISUAL' => array(
                                                                'ID' => $arItemIDs['ID'],
                                                                'PICT_ID' => $arItemIDs['PICT'],
                                                                'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
                                                                'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                                                                'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                                                                'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                                                                'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
                                                                'PRICE_ID' => $arItemIDs['PRICE'],
                                                                'TREE_ID' => $arItemIDs['PROP_DIV'],
                                                                'TREE_ITEM_ID' => $arItemIDs['PROP'],
                                                                'BUY_ID' => $arItemIDs['BUY_LINK'],
                                                                'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                                                                'DSC_PERC' => $arItemIDs['DSC_PERC'],
                                                                'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
                                                                'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                                                        ),
                                                        'BASKET' => array(
                                                                'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                                                                'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE']
                                                        ),
                                                        'PRODUCT' => array(
                                                                'ID' => $arItem['ID'],
                                                                'NAME' => $arItem['~NAME']
                                                        ),
                                                        'OFFERS' => $arItem['JS_OFFERS'],
                                                        'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
                                                        'TREE_PROPS' => $arSkuProps,
                                                        'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
                                                );
                                                ?>
                                                        <script type="text/javascript">
                                                                var <? echo $strObName; ?> = new JCSaleGiftBasket(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
                                                        </script>
                                                <?
                                                }?>
                                            <? endif; ?>
                                            

                                        </div>    
                        </div>
                    </div>
                <? endforeach; ?>
                </div>
                <div class="swiper-button-prev prev icon-custom"></div>
                <div class="swiper-button-next next icon-custom"></div>
            </div>
        
        </section>
        
        <script>
        $(document).ready(function() {
                $('#<?=$strCarouselId?> .owl-carousel .slider-element').css('max-width', 'none');
                var productsGiftsSwiper = new Swiper('#<?=$strCarouselId?> .swiper-container', {
                        spaceBetween: 24,
                        breakpoints: {
                                0:{
                                slidesPerView:2,
                                spaceBetween: 16,
                                },
                                490:{
                                slidesPerView:2,
                                },
                                780:{
                                slidesPerView:3,
                                },
                                1170:{
                                slidesPerView:4,
                                },
                                1405:{
                                slidesPerView:5,
                                },
                                1610:{
                                slidesPerView:6,
                                } 
                        },
                        simulateTouch: false,
                        navigation: {
                                nextEl: '.swiper-button-next next',
                                prevEl: '.swiper-button-prev prev',
                        }
                })
                window.CSolution.setProductsHeight($('#<?=$strCarouselId?> .owl-carousel .slider-element'));
        });
            $('#<?=$strCarouselId?> .products-list').css('height', $('#<?=$strCarouselId?> .product-wrap').height());
    </script>
<?
}
?>
<?$frame->beginStub();?>
<?$frame->end();?>
