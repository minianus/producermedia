<?php

$arResult["GALLERY"] = array();

if (!empty($arResult["IMAGE_ID"]))
{
    $arResult["GALLERY"][] = \CFile::getPath($arResult["IMAGE_ID"]);
}

if (!empty($arParams["MORE_PHOTO"]))
{
    $arStoreFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CAT_STORE", $arResult["ID"]);
    if (!empty($arField = $arStoreFields[$arParams["MORE_PHOTO"]]))
    {
        $fType = ($arField["USER_TYPE"]["USER_TYPE_ID"] == "string") ? "string" : "file";
        if (is_array($arField["VALUE"]))
        {
            foreach ($arField["VALUE"] as $val)
            {
                $r = ($fType == "file") ? \CFile::getPath($val) : $val;
                $arResult["GALLERY"][] = $r;
            }
        }
        else
        {
            $r = ($fType == "file") ? \CFile::getPath($arField["VALUE"]) : $arField["VALUE"];
            $arResult["GALLERY"][] = $r;
        }
    }
}

if ($arResult["ID"] && \Bitrix\Main\Loader::includeModule("catalog"))
{
    $arStore = \Bitrix\Catalog\StoreTable::getList(array(
        "limit" => 1,
        "filter"=> array("ID" => 3),
        "select" => array("EMAIL")
    ))->fetch();
    $arResult["EMAIL"] = $arStore["EMAIL"];
}

