<?php

$mapComponent = ($arResult["MAP"] === 0) ? "bitrix:map.yandex.view" : "bitrix:map.google.view";
$gpsN = substr($arResult["GPS_N"],0,15);
$gpsS = substr($arResult["GPS_S"],0,15);
$gpsText = $arResult["ADDRESS"];
$gpsTextLen = strlen($arResult["ADDRESS"]);

$APPLICATION->IncludeComponent($mapComponent, "", array(
        "INIT_MAP_TYPE" => "MAP",
        "MAP_DATA" => serialize(array("yandex_lat"=>$gpsN,"yandex_lon"=>$gpsS,"yandex_scale"=>11,"PLACEMARKS" => array( 0=>array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arResult["ADDRESS"])))),
        "MAP_WIDTH" => "720",
        "MAP_HEIGHT" => "500",
        "CONTROLS" => array(
            0 => "ZOOM",
        ),
        "OPTIONS" => array(
            0 => "ENABLE_SCROLL_ZOOM",
            1 => "ENABLE_DBLCLICK_ZOOM",
            2 => "ENABLE_DRAGGING",
        ),
        "MAP_ID" => "store_map"
    ),
    false
);