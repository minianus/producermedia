<?
use \Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$templateData = array(
    "TITLE" => $arResult["TITLE"]
);
?>

<main class="contacts detail">
    <div class="contact-container" itemscope itemtype = "http://schema.org/Product">
        <div class="item info">
            <div class="title">
                <?if($arResult["ADDRESS"]):?>
                    <span itemprop="description" class="main-adress"><?=$arResult["ADDRESS"]?></span>
                <?endif?>
                <a href="javascript:void(0);" class="question-popup-btn btn"><?=Loc::getMessage("TPL_QUESTION_TEXT")?></a>
            </div>
            <div class="main-info">
                <?if($arResult["PHONE"] != ''):?>
                    <div class="item" itemprop="description">
                        <span class="name"><?=Loc::getMessage("TPL_PHONE_TEXT")?></span>
                        <span class="content"><?=$arResult["PHONE"]?></span>
                    </div>
                <?endif?>
                
                <?if($arResult["EMAIL"]):?>
                    <div class="item" itemprop="description">
                        <span class="name"><?=Loc::getMessage("TPL_EMAIL_TEXT")?></span>
                        <span class="content"><?=$arResult["EMAIL"]?></span>
                    </div>
                <?endif?>
                
                <?if ($arResult["SCHEDULE"] != ''):?>
                    <div class="item" itemprop="description">
                        <span class="name"><?=Loc::getMessage("TPL_SCHEDULE_TEXT")?></span>
                        <span class="content"><?=$arResult["SCHEDULE"]?></span>
                    </div>
                <?endif?>
                <?/* TODO
                <div class="item">
                    <span class="name"><?=Loc::getMessage("TPL_PAYMENTS_TEXT")?></span>
                    <span class="content"><?=Loc::getMessage("TPL_PAYMENTS_NAL_TEXT")?></span>
                    <span class="content"><?=Loc::getMessage("TPL_PAYMENTS_BEZNAL_TEXT")?></span>
                </div>
                 * 
                 */?>
            </div>
            
            <?if($arResult["DESCRIPTION"]):?>
            <p class="text-block" itemprop="description">
                <?=$arResult["DESCRIPTION"];?>
            </p>
            <?endif?>
            
            <?if ($arResult["GPS_N"] && $arResult["GPS_S"]):?>
            <div class="map" id="store_map">
                <? include("map.php"); ?>
            </div>
            <?endif?>
        </div>
        
        <?if ($arResult["GALLERY"]):?>
            <div class="item photos">
                <div class="items swiper-container" id="contact_photo_swiper">
                    <div class="swiper-wrapper"> 
                        <?foreach ($arResult["GALLERY"] as $src):?>
                            <div class="item swiper-slide">
                                <image src="<?=$src?>" />
                            </div>
                        <?endforeach?>
                    </div>
                    <?if (count($arResult["GALLERY"]) > 1):?>
                        <div class="swiper-button-prev icon-custom"></div>
                        <div class="swiper-button-next icon-custom"></div>
                        <div class="swiper-pagination"></div>
                    <?endif?>
                </div>
            </div>
        <?endif?>
        
    </div>
    
    <?if($arResult["LIST_URL"] && false):?>
        <a href="<?=$arResult["LIST_URL"]?>" class="btn"><?=GetMessage("BACK_STORE_LIST")?></a>
    <?endif?>
    
</main>

<script>
$(document).ready(function () {
       var contactSwiper = new Swiper ('.photos .swiper-container', {
            spaceBetween: 0,
            slidesPerView:1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                },
        });	
});
</script>
