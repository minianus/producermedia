<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($templateData["TITLE"])
{
    //cuz set title by component is not variative
    $GLOBALS["APPLICATION"]->SetTitle($templateData["TITLE"]);
}