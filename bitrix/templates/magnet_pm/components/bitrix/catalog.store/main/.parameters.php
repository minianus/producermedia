<?php

$arUfProps = array();
$arUfProps[0] = GetMessage('CMP_NON_SELECTED');

$arFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CAT_STORE");
foreach ($arFields as $code => $arField)
{
    if (!in_array($arField["USER_TYPE_ID"], array("file", "string")))
        continue;
    
    $name = ($arField["USER_TYPE"]["DESCRIPTION"]) ?: $code;
    $arUfProps[$code] = "[" . $code . "] " . $name;
}

$arTemplateParameters["MORE_PHOTO"] = array(
    "PARENT" => "ADDITIONAL_SETTINGS",
    "NAME" => GetMessage('CMP_MORE_PHOTOS_PROP'),
    "TYPE" => "LIST",
    "VALUES" => $arUfProps,
    "MULTIPLE" => "N",
    "DEFAULT" => "",
);
