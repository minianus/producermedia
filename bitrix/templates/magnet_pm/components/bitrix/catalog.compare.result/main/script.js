BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function()
{
	var CompareClass = function(wrapObjId, reloadUrl)
	{
		this.wrapObjId = wrapObjId;
		this.reloadUrl = reloadUrl;
		BX.addCustomEvent(window, 'onCatalogDeleteCompare', BX.proxy(this.reload, this));
	};

	CompareClass.prototype.MakeAjaxAction = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(this.reloadResult, this)
		);
	};

	CompareClass.prototype.reload = function()
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			this.reloadUrl,
			{
				compare_result_reload: 'Y'
			},
			BX.proxy(this.reloadResult, this)
		);
	};

	CompareClass.prototype.reloadResult = function(result)
	{
		BX.closeWait();
		BX(this.wrapObjId).innerHTML = result;
	};

	CompareClass.prototype.delete = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(this.deleteResult, this)
		);
	};

	CompareClass.prototype.deleteResult = function(result)
	{
		BX.closeWait();
		BX.onCustomEvent('OnCompareChange');
		BX(this.wrapObjId).innerHTML = result;
	};
        
	CompareClass.prototype.addToBasket = function(target)
	{
            if (!target || !target.dataset)
                return;
            BX.addClass(target, 'loaded');
            
            let ajaxUrl = BX.message('SITE_DIR') + 'include/ajax/add_to_basket.php';
            BX.ajax({
                    method: 'POST',
                    dataType: 'json',
                    data: {id: target.dataset.id},
                    url: ajaxUrl,
                    onsuccess: (r) => {
                        if (r && r.success)
                        {
                            BX.onCustomEvent('OnBasketChange');
                            BX.onCustomEvent('OnBasketSuccessAdd');
                            
                            BX.removeClass(target, 'loaded');
                            BX.addClass(target, 'in-basket');
                            setTimeout(() => {
                                BX.removeClass(target, 'in-basket');
                            }, 1500);
                        }
                    }
            });
            
	};
        
	CompareClass.prototype.addToFavorite = function(target)
	{
            if (!target || !target.dataset)
                return;
            
            let url = BX.message('SITE_DIR') + 'include/ajax/to_wish_list.php', wishlistLink;
        
            BX.toggleClass(target, 'active');
            
            wishlistLink = url + '?id=' + target.dataset.id.toString() + '&clear_cache=y';
            BX.ajax({
                method: 'POST',
                dataType: 'json',
                url: wishlistLink,
                onsuccess: function () {
                    BX.onCustomEvent('OnBasketChange');
                    if (window['bFlyBasketShowed'] === undefined || !window['bFlyBasketShowed']) {
                        if ($(".fly-basket-container").length > 0) {
                            $(".fly-basket-container").addClass('open');
                            window['bFlyBasketShowed'] = true;
                        }
                    }
                }
            });
            
	};

	return CompareClass;
})();