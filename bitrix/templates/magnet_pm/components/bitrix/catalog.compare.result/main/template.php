<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$isAjax = (
		(isset($_POST['ajax_action']) && $_POST['ajax_action'] == 'Y')
		|| (isset($_POST['compare_result_reload']) && $_POST['compare_result_reload'] == 'Y')
	);
}
//$ar_res = CCatalogProduct::GetByID(285);
//echo "<pre>";
//print_r($ar_res);
//echo "</pre>";
//$ar_res1 = CPrice::GetBasePrice(285);
//echo "<pre>";
//print_r($ar_res1);
//echo "</pre>";
?>


<div class="compare-page" id="bx_catalog_compare_block">
<? if ($isAjax) $APPLICATION->RestartBuffer(); ?>
    
    <div class="top">
        <div class="tabs-container">
            <a href="<?=$arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=N'; ?>" rel="nofollow" class="tab<?=(!$arResult["DIFFERENT"] ? ' active' : ''); ?>"><?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?></a>
            <? if (is_array($arResult["ITEMS"]) && count($arResult["ITEMS"]) > 1): ?>
            <a href="<?=$arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=Y'; ?>" rel="nofollow" class="tab<?=($arResult["DIFFERENT"] ? ' active' : ''); ?>"><?=GetMessage("CATALOG_ONLY_DIFFERENT")?></a>
            <? endif; ?>
        </div>


        <div class="checkbox-container">
            <label data-role="compare" class="param_label " for="compare" >
                <span class="input_checkbox">
                    <input type="checkbox" <?=($arResult["DIFFERENT"] ? ' checked' : ''); ?> value="Y" name="compare" id="compare"
                        onclick="">
                    <span class="param_text" title="������ �������">������ �������</span>
                </span>
            </label>
        </div>

        <script>
            $('.compare-page .input_checkbox input').on('click', function(){  
                <? if ($arResult["DIFFERENT"]): ?>  
                    location.href='<?=$arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=N'; ?>'
                <? else: ?>
                    location.href='<?=$arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=Y'; ?>'
                <? endif; ?>
            })
        </script>

        <a href="<?=$APPLICATION->GetCurPageParam('clear_compare=y', Array ('clear_compare'))?>" class="btn transparent"><?=GetMessage('CATALOG_CLEAR_COMPARE')?> <span><?=GetMessage('CATALOG_CLEAR_COMPARE_DESKTOP')?></span></a>
    </div>
    
    <div class="content">
        <? if (!empty($arResult["ALL_FIELDS"]) || !empty($arResult["ALL_PROPERTIES"]) || !empty($arResult["ALL_OFFER_FIELDS"]) || !empty($arResult["ALL_OFFER_PROPERTIES"])): ?>
        <table class="compare-table">
            <tbody>
                
                <? if (isset($arResult['TABLE']["PREVIEW_PICTURE"]) || isset($arResult['TABLE']["DETAIL_PICTURE"])): ?>
                    <?
                    $codeRow = isset($arResult['TABLE']["DETAIL_PICTURE"]) ? "DETAIL_PICTURE" : "PREVIEW_PICTURE";
                    $arRow = $arResult['TABLE'][$codeRow];
                    ?>
                    <tr class="image">
                        <td class="empty">&nbsp;</td>
                        <? foreach ($arRow['ITEMS'] as $key =>$arItemProp): ?>
                        <td>
                            <a href="<?=$arResult['ITEMS'][$key]["DETAIL_PAGE_URL"]?>" class="link">
                                <img src="<?=$arItemProp["SRC"]?>" alt="<?=$arItemProp["ALT"]?>" title="<?=$arItemProp["TITLE"]?>" />
                            </a>
                            <a href="javascript:void(0);" onclick="CatalogCompareObj.delete('<?=CUtil::JSEscape($arResult['ITEMS'][$key]['~DELETE_URL'])?>');" class="close icon-custom"></a>
                        </td>
                        <? endforeach; ?>
                    </tr>
                <? endif; ?>

                <? foreach ($arResult['TABLE'] as $codeRow => $arRow):
                    if ($codeRow == "PREVIEW_PICTURE" || $codeRow == "DETAIL_PICTURE") continue;
                ?>
                    <tr class="<?=strtolower($codeRow)?>">
                        
                            <? if ($codeRow == "NAME"): ?>
                                <td class="empty">&nbsp;</td>
                                <? foreach ($arRow['ITEMS'] as $key =>$arItemProp): ?>
                                <td>
                                    <a href="<?=$arResult['ITEMS'][$key]["DETAIL_PAGE_URL"]?>"><?=$arItemProp?></a>
                                </td>
                                <? endforeach; ?>
                            <? else: ?>
                                <? if ($codeRow == "PRICE"): ?>
                                <td class="empty">&nbsp;</td>
                                <? else: ?>
                                <td><?=$arRow['TITLE']?></td>
                                <? endif; ?>

                            <Style>
                                .rovniy {
                                    display: flex;
                                }
                            </Style>

                                <? foreach ($arRow['ITEMS'] as $key =>$arItemProp): ?>
                                <td <?= empty($arItemProp) ? 'class="clear"' : '';?>>
                                    <span class="row-title"><?=$arRow['TITLE']?></span>
                                    <span><?=$arItemProp?></span>
                                    <?if($codeRow=="PRICE"){?>
                                        <div class="add-bonus_compare">
                                            <div class="bonus bonus-color">
                                                <span class="bonus-icon"></span>
                                                <span><?
                                                    $ar_res = CCatalogProduct::GetByID($key);//������� ���������� ���� ��� ������� �������
                                                    $ar_res1 = CPrice::GetBasePrice($key);
                                                    echo $ar_res1["PRICE"]-$ar_res["PURCHASING_PRICE"];
                                                    ?></span>
                                            </div>
                                        </div>
                                    <?}?>
                                </td>
                                <? endforeach; ?>
                            <? endif; ?>

                    </tr>
                    <?if ($codeRow == 'PRICE' && ($arParams['COMPARE_SHOW_BASKET_BTN'] == 'Y' || $arParams['COMPARE_SHOW_DELAY_BTN'] == 'Y')):?>
                        <tr class="image">
                            <td class="empty">&nbsp;</td>
                            <? foreach ($arRow['ITEMS'] as $pId => $price): ?>
                            <td>
                                <?if ($arParams['COMPARE_SHOW_BASKET_BTN'] == 'Y'):?>
                                <a onclick="CatalogCompareObj.addToBasket(this)" data-id="<?=$pId?>" class="btn detail-buy-button" href='javascript:void(0)'>
                                    <?=GetMessage('TPL_ADD_TO_BASKET_BTN_TEXT')?>
                                </a>
                                <?endif?>
                                
                                <?if ($arParams['COMPARE_SHOW_DELAY_BTN'] == 'Y'):?>
                                <a href="javascript:void(0);" title="<?=GetMessage('TPL_ADD_TO_DELAY_BTN_TEXT')?>" data-id="<?=$pId?>" class="wishlist <?=(in_array($pId, $arResult['DELAYED_ITEMS'])) ? 'active' : ''?> icon-custom"
                                       onclick="CatalogCompareObj.addToFavorite(this)">
                                    </a>
                                <?endif?>
                            </td>
                            <? endforeach; ?>
                        </tr>
                   <?endif?>
                        
                <? endforeach; ?>
            </tbody>
        </table>
        <? endif; ?>
    </div>
    
<? if ($isAjax) die(); ?>
</div>

<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block", '<?=CUtil::JSEscape($arResult['~COMPARE_URL_TEMPLATE']); ?>');
</script>
