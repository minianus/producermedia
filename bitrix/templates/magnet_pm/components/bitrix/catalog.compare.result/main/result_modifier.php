<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global array $arParams */
use Bitrix\Main\Type\Collection;

$arResult['ALL_FIELDS'] = array();
$existShow = !empty($arResult['SHOW_FIELDS']);
$existDelete = !empty($arResult['DELETED_FIELDS']);
if ($existShow || $existDelete)
{
	if ($existShow)
	{
		foreach ($arResult['SHOW_FIELDS'] as $propCode)
		{
			$arResult['SHOW_FIELDS'][$propCode] = array(
				'CODE' => $propCode,
				'IS_DELETED' => 'N',
				'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_FIELD_TEMPLATE']),
				'SORT' => $arResult['FIELDS_SORT'][$propCode]
			);
		}
		unset($propCode);
		$arResult['ALL_FIELDS'] = $arResult['SHOW_FIELDS'];
	}
	if ($existDelete)
	{
		foreach ($arResult['DELETED_FIELDS'] as $propCode)
		{
			$arResult['ALL_FIELDS'][$propCode] = array(
				'CODE' => $propCode,
				'IS_DELETED' => 'Y',
				'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_FIELD_TEMPLATE']),
				'SORT' => $arResult['FIELDS_SORT'][$propCode]
			);
		}
		unset($propCode, $arResult['DELETED_FIELDS']);
	}
	Collection::sortByColumn($arResult['ALL_FIELDS'], array('SORT' => SORT_ASC));
}

$arResult['ALL_PROPERTIES'] = array();
$existShow = !empty($arResult['SHOW_PROPERTIES']);
$existDelete = !empty($arResult['DELETED_PROPERTIES']);
if ($existShow || $existDelete)
{
	if ($existShow)
	{
		foreach ($arResult['SHOW_PROPERTIES'] as $propCode => $arProp)
		{
			$arResult['SHOW_PROPERTIES'][$propCode]['IS_DELETED'] = 'N';
			$arResult['SHOW_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_PROPERTY_TEMPLATE']);
		}
		$arResult['ALL_PROPERTIES'] = $arResult['SHOW_PROPERTIES'];
	}
	unset($arProp, $propCode);
	if ($existDelete)
	{
		foreach ($arResult['DELETED_PROPERTIES'] as $propCode => $arProp)
		{
			$arResult['DELETED_PROPERTIES'][$propCode]['IS_DELETED'] = 'Y';
			$arResult['DELETED_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_PROPERTY_TEMPLATE']);
			$arResult['ALL_PROPERTIES'][$propCode] = $arResult['DELETED_PROPERTIES'][$propCode];
		}
		unset($arProp, $propCode, $arResult['DELETED_PROPERTIES']);
	}
	Collection::sortByColumn($arResult["ALL_PROPERTIES"], array('SORT' => SORT_ASC, 'ID' => SORT_ASC));
}

$arResult["ALL_OFFER_FIELDS"] = array();
$existShow = !empty($arResult["SHOW_OFFER_FIELDS"]);
$existDelete = !empty($arResult["DELETED_OFFER_FIELDS"]);
if ($existShow || $existDelete)
{
	if ($existShow)
	{
		foreach ($arResult["SHOW_OFFER_FIELDS"] as $propCode)
		{
			$arResult["SHOW_OFFER_FIELDS"][$propCode] = array(
				"CODE" => $propCode,
				"IS_DELETED" => "N",
				"ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_FIELD_TEMPLATE']),
				'SORT' => $arResult['FIELDS_SORT'][$propCode]
			);
		}
		unset($propCode);
		$arResult['ALL_OFFER_FIELDS'] = $arResult['SHOW_OFFER_FIELDS'];
	}
	if ($existDelete)
	{
		foreach ($arResult['DELETED_OFFER_FIELDS'] as $propCode)
		{
			$arResult['ALL_OFFER_FIELDS'][$propCode] = array(
				"CODE" => $propCode,
				"IS_DELETED" => "Y",
				"ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_FIELD_TEMPLATE']),
				'SORT' => $arResult['FIELDS_SORT'][$propCode]
			);
		}
		unset($propCode, $arResult['DELETED_OFFER_FIELDS']);
	}
	Collection::sortByColumn($arResult['ALL_OFFER_FIELDS'], array('SORT' => SORT_ASC));
}

$arResult['ALL_OFFER_PROPERTIES'] = array();
$existShow = !empty($arResult["SHOW_OFFER_PROPERTIES"]);
$existDelete = !empty($arResult["DELETED_OFFER_PROPERTIES"]);
if ($existShow || $existDelete)
{
	if ($existShow)
	{
		foreach ($arResult['SHOW_OFFER_PROPERTIES'] as $propCode => $arProp)
		{
			$arResult["SHOW_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "N";
			$arResult["SHOW_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_PROPERTY_TEMPLATE']);
		}
		unset($arProp, $propCode);
		$arResult['ALL_OFFER_PROPERTIES'] = $arResult['SHOW_OFFER_PROPERTIES'];
	}
	if ($existDelete)
	{
		foreach ($arResult['DELETED_OFFER_PROPERTIES'] as $propCode => $arProp)
		{
			$arResult["DELETED_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "Y";
			$arResult["DELETED_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_PROPERTY_TEMPLATE']);
			$arResult['ALL_OFFER_PROPERTIES'][$propCode] = $arResult["DELETED_OFFER_PROPERTIES"][$propCode];
		}
		unset($arProp, $propCode, $arResult['DELETED_OFFER_PROPERTIES']);
	}
	Collection::sortByColumn($arResult['ALL_OFFER_PROPERTIES'], array('SORT' => SORT_ASC, 'ID' => SORT_ASC));
}

foreach ($arResult['ITEMS'] as $key => $arElement)
{
    unset($arResult['ITEMS'][$key]);
    $arResult['ITEMS'][$arElement['ID']] = $arElement;
}

$arResultTable = Array ();

if (!empty($arResult["SHOW_FIELDS"]))
{
    foreach ($arResult["SHOW_FIELDS"] as $code => $arProp)
    {
        $showRow = true;
        if ((!isset($arResult['FIELDS_REQUIRED'][$code]) || $arResult['DIFFERENT']) && count($arResult["ITEMS"]) > 1)
        {
            $arCompare = array();
            foreach ($arResult["ITEMS"] as $arElement)
            {
                $arPropertyValue = $arElement["FIELDS"][$code];
                if (is_array($arPropertyValue))
                {
                    sort($arPropertyValue);
                    $arPropertyValue = implode(" / ", $arPropertyValue);
                }
                $arCompare[] = $arPropertyValue;
            }
            unset($arElement);
            $showRow = (count(array_unique($arCompare)) > 1);
        }
        if ($showRow)
        {
            $arRowItemsProps = Array ();
            
            foreach ($arResult["ITEMS"] as $arElement)
            {
                $arRowItemsProps[$arElement['ID']] = $arElement["FIELDS"][$code];
            }
            
            $arRow = Array (
                'TITLE' => GetMessage("IBLOCK_FIELD_".$code),
                'ITEMS' => $arRowItemsProps
            );
        }
        
        $arResultTable[$code] = $arRow;
    }
}

if (!empty($arResult["SHOW_OFFER_FIELDS"]))
{
	foreach ($arResult["SHOW_OFFER_FIELDS"] as $code => $arProp)
	{
            if ($code != "PREVIEW_PICTURE" || $code != "DETAIL_PICTURE")
                continue;
            
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as $arElement)
			{
				$Value = $arElement["OFFER_FIELDS"][$code];
				if(is_array($Value))
				{
					sort($Value);
					$Value = implode(" / ", $Value);
				}
				$arCompare[] = $Value;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}
		if ($showRow)
		{
                    $arRowItemsProps = Array ();
            
                    foreach ($arResult["ITEMS"] as $arElement)
                    {
                        $arRowItemsProps[$arElement['ID']] = (is_array($arElement["OFFER_FIELDS"][$code])? implode("/ ", $arElement["OFFER_FIELDS"][$code]): $arElement["OFFER_FIELDS"][$code]);
                    }

                    $arRow = Array (
                        'TITLE' => GetMessage("IBLOCK_OFFER_FIELD_".$code),
                        'ITEMS' => $arRowItemsProps
                    );
                    
                    $arResultTable['OFFER_FIELDS_' . $code] = $arRow;
                }
        }
}


// show prices

$arRowItemsProps = Array();

$useLocations = \Nextype\Magnet\CSolution::getInstance(SITE_ID)::$options['LOCATIONS_ENABLED'] == 'Y';
if ($useLocations)
{
    $locationPrices = \Nextype\Magnet\CLocations::getCurrentRegion()['PROPERTIES']['PRICE']['VALUE'];
    if (is_array($locationPrices) && in_array('COMPONENT', $locationPrices))
            $locationPrices = $arParams['PRICE_CODE'];
    
    $arPrices = array();
    foreach ($locationPrices as $code)
    {
        if (isset($arResult['PRICES'][$code]) && $arResult['PRICES'][$code]['CAN_BUY'])
            $arPrices[] = $arResult['PRICES'][$code]['ID'];
    }
}

foreach ($arResult["ITEMS"] as $arElement)
{        
    if (isset($arElement['MIN_PRICE']) && is_array($arElement['MIN_PRICE']) && !$useLocations)
    {
        $arRowItemsProps[$arElement['ID']] = $arElement['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
    }
    elseif (!empty($arElement['PRICE_MATRIX']) && is_array($arElement['PRICE_MATRIX']))
    {
        $strMatrix = '';
        $matrix = $arElement['PRICE_MATRIX'];
        $rows = $matrix['ROWS'];
        $rowsCount = count($rows);
        if ($rowsCount > 0)
        {
            if (count($rows) > 1)
            {
                $strMatrix = '<table class="compare-price">';
                
                $currentPrice = false;
                if ($useLocations && !empty($arPrices))
                {
                    foreach ($arPrices as $priceId)
                    {
                        if (!$currentPrice || $currentPrice['DISCOUNT_PRICE'] > current($arElement['PRICE_MATRIX']['MATRIX'][$priceId])['DISCOUNT_PRICE'])
                            $currentPrice = current($arElement['PRICE_MATRIX']['MATRIX'][$priceId]);
                    }
                }
                
                foreach ($rows as $index => $rowData)
                {
                    if (empty($matrix['MIN_PRICES'][$index]))
                        continue;
                    if ($rowData['QUANTITY_FROM'] == 0)
                        $rowTitle = GetMessage('CP_TPL_CCR_RANGE_TO', array('#TO#' => $rowData['QUANTITY_TO']));
                    elseif ($rowData['QUANTITY_TO'] == 0)
                        $rowTitle = GetMessage('CP_TPL_CCR_RANGE_FROM', array('#FROM#' => $rowData['QUANTITY_FROM']));
                    else
                        $rowTitle = GetMessage(
                                'CP_TPL_CCR_RANGE_FULL', array('#FROM#' => $rowData['QUANTITY_FROM'], '#TO#' => $rowData['QUANTITY_TO'])
                        );
                    
                    $strMatrix .= '<tr><td>' . $rowTitle . '</td><td>';
                    $strMatrix .= \CCurrencyLang::CurrencyFormat($matrix['MIN_PRICES'][$index]['PRICE'], $matrix['MIN_PRICES'][$index]['CURRENCY']);
                    $strMatrix .= '</td></tr>';
                    unset($rowTitle);
                }
                unset($index, $rowData);
                $strMatrix .= '</table>';
            }
            else
            {
                $currentPrice = false;
                if ($useLocations && !empty($arPrices))
                {
                    foreach ($arPrices as $priceId)
                    {
                        if (!$currentPrice || $currentPrice['DISCOUNT_PRICE'] > current($arElement['PRICE_MATRIX']['MATRIX'][$priceId])['DISCOUNT_PRICE'])
                            $currentPrice = current($arElement['PRICE_MATRIX']['MATRIX'][$priceId]);
                    }
                }
                else
                {
                    $currentPrice = current($matrix['MIN_PRICES']);
                }
                
                $strMatrix = \CCurrencyLang::CurrencyFormat($currentPrice['PRICE'], $currentPrice['CURRENCY']);
                unset($currentPrice);
            }
            
        }
        unset($rowsCount, $rows, $matrix);
        
        $arRowItemsProps[$arElement['ID']] = $strMatrix;
    }
    else
    {
        $arRowItemsProps[$arElement['ID']] = "";
    }

}

$arRow = Array(
    'TITLE' => GetMessage('CATALOG_COMPARE_PRICE'),
    'ITEMS' => $arRowItemsProps
);

$arResultTable['PRICE'] = $arRow;

// end show prices

if (!empty($arResult["SHOW_PROPERTIES"]))
{
	foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
	{
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as $arElement)
			{
				$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
				if (is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}

		if ($showRow)
		{
                    $arRowItemsProps = Array ();
            
                    foreach ($arResult["ITEMS"] as $arElement)
                    {
                        $arRowItemsProps[$arElement['ID']] = (is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
                    }

                    $arRow = Array (
                        'TITLE' => $arProperty["NAME"],
                        'ITEMS' => $arRowItemsProps
                    );
                    
                    $arResultTable['PROPERTIES_' . $code] = $arRow;
                }
        }
}

if (!empty($arResult["SHOW_OFFER_PROPERTIES"]))
{
	foreach($arResult["SHOW_OFFER_PROPERTIES"] as $code=>$arProperty)
	{
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as $arElement)
			{
				$arPropertyValue = $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["VALUE"];
				if(is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}
		if ($showRow)
		{
                    $arRowItemsProps = Array ();
            
                    foreach ($arResult["ITEMS"] as $arElement)
                    {
                        $arRowItemsProps[$arElement['ID']] = (is_array($arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
                    }

                    $arRow = Array (
                        'TITLE' => $arProperty["NAME"],
                        'ITEMS' => $arRowItemsProps
                    );
                    
                    $arResultTable['OFFER_PROPERTIES_' . $code] = $arRow;
                }
        }
}


$arResult['TABLE'] = $arResultTable;

if ($arParams['COMPARE_SHOW_DELAY_BTN'] == 'Y')
{
    $arResult['DELAYED_ITEMS'] = array_keys(\Nextype\Magnet\CSolution::getWishList());
}

