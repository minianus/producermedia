(function() {
	'use strict';

	if (!!window.JCSaleProductsGiftComponent)
		return;

	window.JCSaleProductsGiftComponent = function(params) {
		this.formPosting = false;
		this.siteId = params.siteId || '';
		this.template = params.template || '';
		this.componentPath = params.componentPath || '';
		this.parameters = params.parameters || '';
                this.responsiveOwlParams = params.responsiveOwlParams || [],
		this.container = document.querySelector('[data-entity="' + params.container + '"]');
		this.currentProductId = params.currentProductId;

		if (params.deferredLoad)
		{
			BX.ready(BX.delegate(this.deferredLoad, this));
		}
                

		BX.addCustomEvent(
			'onCatalogStoreProductChange',
			BX.delegate(function(offerId){
				offerId = parseInt(offerId);

				if (this.currentProductId === offerId)
				{
					return;
				}

				this.currentProductId = offerId;
				this.offerChangedEvent();
			}, this)
		);
	};

	window.JCSaleProductsGiftComponent.prototype =
	{
		offerChangedEvent: function()
		{
			this.sendRequest({action: 'deferredLoad', offerId: this.currentProductId});
		},

		deferredLoad: function()
		{
			this.sendRequest({action: 'deferredLoad'});
		},

		sendRequest: function(data)
		{
			var defaultData = {
				siteId: this.siteId,
				template: this.template,
				parameters: this.parameters
			};
                        
			BX.ajax({
				url: this.componentPath + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
				method: 'POST',
				timeout: 60,
                                dataType: 'json',
				data: BX.merge(defaultData, data),
				onsuccess: BX.delegate(function(result){
                                    if (result) {
                                        
                                        if (!result || !result.JS)
					{
						BX.cleanNode(this.container);
						return;
					}

					BX.ajax.processScripts(
						BX.processHTML(result.JS).SCRIPT,
						false,
						BX.delegate(function(){this.showAction(result, data);}, this)
					);
                                        
                                        
                                    }

					
				}, this)
			});
		},
                
                showAction: function(result, data)
		{
			if (!data)
				return;

			switch (data.action)
			{
				case 'deferredLoad':
					if (result.items !== undefined)
					{
						$("#product-gifts-tip").addClass('visible');
						var processed = BX.processHTML(result.items, false);
						BX(this.container).innerHTML = processed.HTML;
								
						
						var obContainer = $(this.container);
						obContainer.find('.swiper-container .slider-element').css('max-width', 'none');
						let giftsSwiper = new Swiper(obContainer.find('.items.swiper-container'), {
							spaceBetween: 24,
							simulateTouch: false,
							breakpoints: this.responsiveOwlParams,
							navigation: {
								navigation: {
									nextEl: '.swiper-button-next',
									prevEl: '.swiper-button-prev',
								},
							},
							on: {
								init: function(){
									obContainer.find('.owl-container').css('height', obContainer.find('.items.swiper-container').height());
									obContainer.find('.swiper-container').addClass('show');
								}
							}
						});
						
						window.CSolution.setProductsHeight(obContainer.find('.swiper-container .slider-element'));
						
						BX.ajax.processScripts(processed.SCRIPT);
					}
				break;
			}
		}

	}
})();