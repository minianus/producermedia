<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

if(isset($APPLICATION->arAuthResult))
    $arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;


$CSolution = (\Bitrix\Main\Loader::includeModule('nextype.magnet')) ? \Nextype\Magnet\CSolution::getInstance(SITE_ID) : false;

?>


<div class="auth">
	<div class="reg">
		<div class="text"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></div>
		
		<div class="form">
                        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR'): ?>
                        <div class="message-errors"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></div>
                        <? endif; ?>

                        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'OK'): ?>
                        <div class="message-success"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></div>
                        <? endif; ?>
                        
			<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                            <? endif; ?>
                            <input type="hidden" name="AUTH_FORM" value="Y">
                            <input type="hidden" name="TYPE" value="SEND_PWD">
                            
                                <input type="hidden" name="USER_LOGIN" value="" />
                                
                                <?if($arResult["PHONE_REGISTRATION"]):?>
                                <div class="input-container">
					<div class="label"><?=GetMessage("AUTH_PHONE")?></div>
					<input type="text" name="USER_PHONE_NUMBER" phone-mask value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
				</div>
                                <div style="margin-bottom: 18px;text-align: center;"><small><?=GetMessage('OTHER_LABEL')?></small></div>
                                <? endif; ?>
				<div class="input-container">
					<div class="label"><?=GetMessage("AUTH_EMAIL")?></div>
					<input type="email" name="USER_EMAIL" />
				</div>
                                <?if($arResult["USE_CAPTCHA"]):?>
                                <div class="input-container <?=(\Nextype\Magnet\CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?>">
					<div class="label"><?=GetMessage("system_auth_captcha")?>:</div>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                        <br/>
                                        <input type="text" name="captcha_word" maxlength="50" value="" />
				</div>
                                    <?if ($CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                                        <input type="hidden" name="g-recaptcha-response" />
                                    <?endif?>
                                <?endif?>
				<div class="buttons">
					<button type="submit" name="send_account_info" value="Y" class="btn submit"
                    <?if($CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && $CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox' && $arResult["USE_CAPTCHA"]):?>
                        onclick="window.CSolution.onSubmitReCaptcha('bform')"
                    <?endif?>
                                                >
                                            <?=GetMessage("AUTH_SEND")?>
                                        </button>
					<a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="auth-link"><?=GetMessage("AUTH_AUTH")?></a>
				</div>
			</form>
		</div>
	</div>
</div>
