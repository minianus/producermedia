<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */



//echo "<pre>";
//print_r($arResult["JS_DATA"]);
//echo "</pre>";
// добавляем бонусы в заказ
if (!empty($arResult["ORDER_ID"])){
$order = \Bitrix\Sale\Order::load($arResult["ORDER_ID"]);

$collection = $order->getPropertyCollection();

    $propertyCollection = $order->getPropertyCollection();
    foreach ($propertyCollection as $property) {
        if ($property->getField('CODE') == "BONUS") $property->setValue($_COOKIE["bonus"]);
    }

$order->save();
}

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);