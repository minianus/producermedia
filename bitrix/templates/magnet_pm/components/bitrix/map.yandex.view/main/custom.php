<?php
$arParams['INIT_MAP_LON'] = floatval($arParams['INIT_MAP_LON']);
$arParams['INIT_MAP_LON'] = $arParams['INIT_MAP_LON'] ? $arParams['INIT_MAP_LON'] : 37.64;
$arParams['INIT_MAP_LAT'] = floatval($arParams['INIT_MAP_LAT']);
$arParams['INIT_MAP_LAT'] = $arParams['INIT_MAP_LAT'] ? $arParams['INIT_MAP_LAT'] : 55.76;
$arParams['INIT_MAP_SCALE'] = intval($arParams['INIT_MAP_SCALE']);
$arParams['INIT_MAP_SCALE'] = $arParams['INIT_MAP_SCALE'] ? $arParams['INIT_MAP_SCALE'] : 10;

$arResult['ALL_MAP_TYPES'] = array('MAP' => 'map', 'SATELLITE' => 'satellite', 'HYBRID' => 'hybrid', 'PUBLIC' => 'publicMap', 'PUBLIC_HYBRID' => 'publicMapHybrid');

$arResult['ALL_MAP_OPTIONS'] = array('ENABLE_SCROLL_ZOOM' => 'scrollZoom', 'ENABLE_DBLCLICK_ZOOM' => 'dblClickZoom', 'ENABLE_DRAGGING' => 'drag', /*'ENABLE_RULER' => 'ruler', */'ENABLE_RIGHT_MAGNIFIER' => 'rightMouseButtonMagnifier'/*, 'ENABLE_LEFT_MAGNIFIER' => 'leftMouseButtonMagnifier'*/);
$arResult['ALL_MAP_CONTROLS'] = array(
    'ZOOM' => 'zoomControl',
    'SMALLZOOM' => 'smallZoomControl',
    //'MINIMAP' => 'miniMap',
    'TYPECONTROL' => 'typeSelector',
    //'SCALELINE' => 'scaleLine',
    'SEARCH' => 'searchControl'
);

if (!$arParams['INIT_MAP_TYPE'] || !array_key_exists($arParams['INIT_MAP_TYPE'], $arResult['ALL_MAP_TYPES']))
	$arParams['INIT_MAP_TYPE'] = 'MAP';

if (!is_array($arParams['OPTIONS']))
	$arParams['OPTIONS'] = array('ENABLE_SCROLL_ZOOM', 'ENABLE_DBLCLICK_ZOOM', 'ENABLE_DRAGGING');
else
{
	foreach ($arParams['OPTIONS'] as $key => $option)
	{
		if (!$arResult['ALL_MAP_OPTIONS'][$option])
			unset($arParams['OPTIONS'][$key]);
	}

	$arParams['OPTIONS'] = array_values($arParams['OPTIONS']);
}

if (!is_array($arParams['CONTROLS']))
	$arParams['CONTROLS'] = array('TOOLBAR', 'ZOOM', 'MINIMAP', 'TYPECONTROL', 'SCALELINE');
else
{
	foreach ($arParams['CONTROLS'] as $key => $control)
	{
		if (!$arResult['ALL_MAP_CONTROLS'][$control])
			unset($arParams['CONTROLS'][$key]);
	}

	$arParams['CONTROLS'] = array_values($arParams['CONTROLS']);
}

$arJsData = Array ();
foreach ($arResult['ALL_MAP_CONTROLS'] as $control => $method)
{
    if (in_array($control, $arParams['CONTROLS']))
    {
        $arJsData['CONTROLS'][] = $method;
    }
}

$arJsData['PLACEMARKS'] = Array (
    "type" => "FeatureCollection",
    "features" => Array ()
);
if (!empty($arParams['~MAP_DATA']))
{
    $arData = unserialize($arParams['~MAP_DATA']);
    if (is_array($arData['PLACEMARKS']) && !empty($arData['PLACEMARKS']))
    {
        foreach ($arData['PLACEMARKS'] as $key => $arItem)
        {
            if (!empty($arItem['LAT']) && !empty($arItem['LON']))
            {
                $arPlacemark = Array (
                    "type" => "Feature",
                    "id" => $key,
                    "geometry" => Array (
                        "type" => "Point",
                        "coordinates" => Array ($arItem['LAT'], $arItem['LON'])
                    )
                );

                if (!empty($arItem['TEXT']))
                {
                    $arPlacemark['properties'] = Array (
                        "balloonContentBody" => str_replace("###RN###", "<br>", $arItem['TEXT'])
                    );
                }

                $arJsData['PLACEMARKS']['features'][] = $arPlacemark;
            }
        }
    }
}
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div style="width:<?=$arTransParams['MAP_WIDTH']?>; height: <?=$arTransParams['MAP_HEIGHT']?>" id="<?=$arParams['MAP_ID']?>"></div>

<script>
ymaps.ready(function () {
    var myMap = new ymaps.Map('<?=$arParams['MAP_ID']?>', {
            center: [<?=$arTransParams['INIT_MAP_LAT']?>, <?=$arTransParams['INIT_MAP_LON']?>],
            zoom: <?=$arTransParams['INIT_MAP_SCALE']?>,
            controls: <?=CUtil::PhpToJSObject($arJsData['CONTROLS'])?>,
        }, {
            searchControlProvider: 'yandex#search'
    });
    
    var objectManager = new ymaps.ObjectManager();
    myMap.geoObjects.add(objectManager);
    objectManager.add(<?=CUtil::PhpToJSObject($arJsData['PLACEMARKS'])?>);
       
    <? if (!in_array("ENABLE_SCROLL_ZOOM", $arParams['OPTIONS'])): ?>
    myMap.behaviors.disable('scrollZoom');
    <? endif; ?>
    <?if (!empty($arCoords)):?>
            var i = 0, officesPlaceMarks = [];
                <?foreach ($arCoords as $arCoord):?>
                    officesPlaceMarks[i] = new ymaps.Placemark(
                            [<?=$arCoord['LON']?>, <?=$arCoord['LAT']?>], {
                            content: '',
                            balloonContent: '<?=$arCoord["TEXT"]?>'
                            }
                    );
                    i++;
                <?endforeach?>

            var mapClusterer = new ymaps.Clusterer();
            mapClusterer.add(officesPlaceMarks);
            myMap.geoObjects.add(mapClusterer);
            myMap.setBounds(myMap.geoObjects.getBounds());
    <? endif; ?>
});
</script>
