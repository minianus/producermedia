<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Nextype\Magnet\CSolution;

if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
$arPlacemarks = array();
$gpsN = '';
$gpsS = '';
?>
<?if(is_array($arResult["STORES"]) && !empty($arResult["STORES"])): ?>
<div class="items">
    <? foreach($arResult["STORES"] as $arItem):
        $showLink = (!empty($arParams["PATH_TO_ELEMENT"]) && $arItem["URL"]); ?>
    <div class="item">
        <? if (!empty($arItem["STORE_TITLE"])): ?>
			<div class="name">
                <?if ($showLink):?>
                    <a class="link" href="<?=$arItem["URL"]?>">
                <?endif?>
                <?=$arItem["STORE_TITLE"]?>
                <?if ($showLink):?>
                    </a>
                <?endif?>
            </div>
        <? endif; ?>
        <div class="office">
    		<div class="img">
    		    <? $arItem['DETAIL_IMG'] = empty($arItem['DETAIL_IMG']['SRC']) ? CSolution::getEmptyImage() : $arItem['DETAIL_IMG']; ?>
    		    <img src="<?=$arItem['DETAIL_IMG']['SRC']?>" alt="<?=$arItem["STORE_TITLE"]?>">
    		</div>
    		<div class="content">
    			<div class="office-info">
    			    <div class="address"><?=$arItem["ADDRESS"]?></div>

    			    <? if (!empty($arItem["SCHEDULE"])): ?>
    			    	<div class="time">
                            <?=$arItem["SCHEDULE"]?>
                        </div>
    			    <? endif; ?>
    			</div>
                
                <div class="contact-info">                
                    <? if (!empty($arItem['PHONE'])): ?>
                    <a href="tel:<?=CSolution::phone2int($arItem['PHONE'])?>" class="phone"><?=$arItem['PHONE']?></a>
                    <? endif; ?>

                    <? if (!empty($arItem['EMAIL'])): ?>
                    <a href="mailto:<?=($arItem['EMAIL'])?>" class="phone"><?=$arItem['EMAIL']?></a>
                    <? endif; ?>
                </div>
               
    		</div>
        </div>
    </div>
    <? endforeach; ?>
</div>
<? endif; ?>
