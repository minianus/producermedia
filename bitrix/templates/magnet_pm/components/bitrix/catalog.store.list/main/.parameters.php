<?php

$arTemplateParameters["SHOW_EMAIL"] = array(
    "PARENT" => "ADDITIONAL_SETTINGS",
    "NAME" => GetMessage('CMP_SHOW_EMAIL'),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

