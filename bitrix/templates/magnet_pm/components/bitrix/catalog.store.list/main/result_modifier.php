<?php

if (!empty($arParams['STORES']))
{
    foreach ($arResult["STORES"] as $key => $arItem)
    {
        if (!in_array($arItem['ID'], $arParams['STORES']))
        {
                unset($arResult["STORES"][$key]);
                continue;
        }
    }
}

$arStoresIds = array();
foreach ($arResult["STORES"] as $key => $arItem)
{
    $arStoresIds[] = $arItem["ID"];
    
    if (!empty($arItem['DETAIL_IMG']['SRC']))
    {
        $resize = CFile::ResizeImageGet($arItem['DETAIL_IMG']['ID'], array('width'=>500, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL);
        if ($resize)
            $arResult["STORES"][$key]['DETAIL_IMG']['SRC'] = $resize['src'];
    }
}

$needEmail = ($arParams["SHOW_EMAIL"] == "Y" && \Bitrix\Main\Loader::includeModule("catalog"));
if ($needEmail && !empty($arStoresIds))
{
    $arEmails = array();
    $rsStores = \Bitrix\Catalog\StoreTable::getList(array(
        "filter"=> array("ID" => $arStoresIds),
        "select" => array("EMAIL", "ID")
    ));
    while ($arStore = $rsStores->fetch())
    {
        $arEmails[$arStore["ID"]] = $arStore["EMAIL"];
    }

    foreach ($arResult["STORES"] as &$arItem)
    {
        $arItem["EMAIL"] = $arEmails[$arItem["ID"]];
    }
}

