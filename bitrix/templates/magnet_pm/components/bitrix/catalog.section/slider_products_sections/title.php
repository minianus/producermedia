<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="categories-block">
    <div class="top-block">
        <div class="text">
            <h2 class="title"><?= $arParams['SECTION']['NAME'] ?></h2>
            <? if (!empty($arParams['SECTION']['SECTION_PAGE_URL'])): ?>
                <a href="<?= $arParams['SECTION']['SECTION_PAGE_URL'] ?>" class="link"><?= GetMessage('SECTION_DETAIL_LINK') ?></a>
            <? endif; ?>
        </div>
    </div>
    <? if (!empty($arParams['SECTION']['PICTURE'])): ?>
        <div class="bottom-block">
            <div class="image">
                <? $arPreview = CFile::ResizeImageGet($arParams['SECTION']['PICTURE'], Array('width' => 400, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT); ?>
                <img src="<?= $arPreview['src'] ?>" alt="<?= $arParams['SECTION']['NAME'] ?>" title="<?= $arParams['SECTION']['NAME'] ?>">
            </div>
        </div>
    <? endif; ?>
</div>