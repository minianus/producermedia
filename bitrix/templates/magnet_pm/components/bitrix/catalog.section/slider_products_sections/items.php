<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="owl-container" id="<?=$strCarouselID?>">
	<div class="items-carousel swiper-container">
		<div class="swiper-wrapper">
			<? foreach ($arResult['ITEMS'] as $item): ?>
				<? $uniqueId = $item['ID'].'_'.md5(randString(6).$component->getAction());
					$areaId = $this->GetEditAreaId($uniqueId);
					$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
					$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
				?>
				<div class="item-wrap swiper-slide">
				<?
				$APPLICATION->IncludeComponent(
						'bitrix:catalog.item', 'main', array(
						'RESULT' => array(
							'ITEM' => $item,
							'AREA_ID' => $areaId,
							'TYPE' => 'CARD',
							'BIG_LABEL' => 'N',
							'BIG_DISCOUNT_PERCENT' => 'N',
							'BIG_BUTTONS' => 'N',
							'SCALABLE' => 'N'
						),
						'PARAMS' => $generalParams + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
							), $component, array('HIDE_ICONS' => 'Y')
				);
                                $itemName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
                                $templateData['ITEMS'][$itemName] = $item;
				?>
				</div>
			<? endforeach; ?>
		</div>
		
		
	</div>
	<div class="swiper-pagination bullets-<?=$strCarouselID?>"></div>
	<div class="swiper-button-prev arrow-<?=$strCarouselID?> icon-custom"></div>
	<div class="swiper-button-next arrow-<?=$strCarouselID?> icon-custom"></div>
</div>