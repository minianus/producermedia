<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"SLIDER_FULL_WIDTH" => Array(
                "PARENT" => "VISUAL",
		"NAME" => GetMessage("T_SLIDER_FULL_WIDTH"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	)
    );
?>
