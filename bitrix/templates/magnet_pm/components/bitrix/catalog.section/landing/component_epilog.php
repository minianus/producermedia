<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;


if (!empty($templateData['CURRENCIES']))
{
	$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	CJSCore::Init(Array('currency'));
        
	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

\Nextype\Magnet\CSolution::getInstance(SITE_ID);
if (Nextype\Magnet\CSolution::$options['CATALOG_SHOW_FAST_VIEW'] == "Y")          
{ ?>
    <script>
        BX.loadScript([
            '<?=SITE_TEMPLATE_PATH?>/components/bitrix/catalog.element/main/script.js',
            '<?=SITE_TEMPLATE_PATH?>/components/bitrix/catalog.product.subscribe/main/script.js'
        ]);
    </script>
<? }

// lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad'))
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- items-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}

	$component::sendJsonAnswer(array(
		'items' => $itemsContainer,
		'pagination' => $paginationContainer
	));
}