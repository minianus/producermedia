<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;


if (!empty($templateData['CURRENCIES']))
{
	$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	CJSCore::Init(Array('currency'));
        
	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

// check wish state
if ($arParams['DISPLAY_WISH_LIST'])
{
    if (\Bitrix\Main\Loader::includeModule('nextype.magnet') )
    {
        $arWishList = \Nextype\Magnet\CSolution::getWishList();
        $items = $templateData['ITEMS'];
        if (count($arWishList) > 0): ?>
            <script> if (typeof wishType === 'undefined') wishType = '';</script>
        <?
            foreach($items as $itemName => $item):
                $inwishlist = false;
                $wishedIds = array();
            
                if (!empty($item['JS_OFFERS']))
		{
                    foreach ($item['JS_OFFERS'] as $key => $offer)
                    {
                        if (isset($arWishList[$offer['ID']]))
			{
                            $inwishlist = true;
                            $wishedIds[] = $offer['ID'];
			}
                    }
                    $currentOffer = (isset($item['JS_OFFERS'][$item['OFFERS_SELECTED']])) ? $item['JS_OFFERS'][$item['OFFERS_SELECTED']] : reset($item['JS_OFFERS']);
                    $offerInWishlist = (in_array($currentOffer['ID'], $wishedIds)) ? 'Y' : 'N';
		}
                elseif (isset($arWishList[$item['ID']]))
		{
                    $inwishlist = true;
                }
                
                if ($inwishlist)
                { ?>
                    <script>
			BX.ready(BX.defer(function(){
                            wishType = "<?=(!empty($item['JS_OFFERS'])) ? 'offer' : 'product';?>";
				if (!!window.<?=$itemName?> && wishType == 'product')
				{
					window.<?=$itemName?>.setWishList(true);
				}
                                <? if (!empty($wishedIds)): ?>
                                    window.<?=$itemName?>.setWishInfo(<?=CUtil::PhpToJSObject($wishedIds, false, true)?>);
                                    if (!!window.<?=$itemName?> && wishType == 'offer' && "<?=$offerInWishlist?>" === "Y")
                                    {
                                        window.<?=$itemName?>.setWishList(true)
                                    }
				<? endif ?>
			}));
                    </script>
            <?  }
               else
                { ?>
                    <script>
			BX.ready(BX.defer(function(){
                            wishType = "<?=(!empty($item['JS_OFFERS'])) ? 'offer' : 'product';?>";
				if (!!window.<?=$itemName?> && wishType == 'product')
				{
					window.<?=$itemName?>.setWishList(false);
				}
                                <? if (!empty($wishedIds)): ?>
                                    window.<?=$itemName?>.setWishInfo(<?=CUtil::PhpToJSObject($wishedIds, false, true)?>);
                                    if (!!window.<?=$itemName?> && wishType == 'offer' && "<?=$offerInWishlist?>" === "N")
                                    {
                                        window.<?=$itemName?>.setWishList(false)
                                    }
				<? endif ?>
			}));
                    </script>
            <?  }
            endforeach;
        endif;
    }
}

\Nextype\Magnet\CSolution::getInstance(SITE_ID);
if (Nextype\Magnet\CSolution::$options['CATALOG_SHOW_FAST_VIEW'] == "Y")
{
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/components/bitrix/catalog.element/main/script.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/components/bitrix/catalog.product.subscribe/main/script.js");
}

//    lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad'))
{
    $content = ob_get_contents();
    ob_end_clean();

    list(, $mainContent) = explode('<!-- carousel content -->', $content);
    list(, $epilogue) = explode('<!-- component-end -->', $content);
    list(, $swiperScript) = explode('<!-- swiper script -->', $content);

    if ($arParams['AJAX_MODE'] === 'Y')
    {
        $component->prepareLinks($paginationContainer);
    }

    $component::sendJsonAnswer(array(
        'content' => $mainContent,
        'epilogue' => $epilogue,
        'swiper' => $swiperScript
    ));
}

