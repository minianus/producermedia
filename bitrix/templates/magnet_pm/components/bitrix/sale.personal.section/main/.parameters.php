<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters['SHOW_EXIT_LINK'] = array(
	'PARENT' => 'BASE',
	'NAME' => GetMessage('T_SHOW_EXIT_LINK'),
	'TYPE' => 'CHECKBOX',
        'REFRESH' => 'Y',
	'DEFAULT' => 'Y'
);