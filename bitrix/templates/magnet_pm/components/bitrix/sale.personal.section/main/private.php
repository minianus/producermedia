<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if ($arParams['SHOW_PRIVATE_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_PRIVATE"));
if ($arParams['SET_TITLE'] == 'Y')
{
	$APPLICATION->SetTitle(Loc::getMessage("SPS_TITLE_PRIVATE"));
}

?>
<div class="personal">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", array(
		"ROOT_MENU_TYPE" => "personal",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_TYPE" => "A",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(),
	),
		false
	);?>
	<div class="personal-profile">
        <h2>Личные данные<?php// $APPLICATION->ShowTitle();?></h2>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.profile",
			"",
			Array(
				"SET_TITLE" =>$arParams["SET_TITLE"],
				"AJAX_MODE" => $arParams['AJAX_MODE_PRIVATE'],
				"SEND_INFO" => $arParams["SEND_INFO_PRIVATE"],
				"CHECK_RIGHTS" => $arParams['CHECK_RIGHTS_PRIVATE']
			),
			$component
		);?>
	</div>
</div>