<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	
if ($arParams['SHOW_ACCOUNT_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

use Bitrix\Main\Localization\Loc;
if ($arParams['SET_TITLE'] == 'Y')
{
	$APPLICATION->SetTitle(Loc::getMessage("SPS_TITLE_ACCOUNT"));
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ACCOUNT"));?>
<div class="personal">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", array(
		"ROOT_MENU_TYPE" => "personal",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_TYPE" => "A",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(),
	),
		false
		);?>
	<div class="personal-refill">
		<? if ($arParams['SHOW_ACCOUNT_COMPONENT'] !== 'N')
		{
			$APPLICATION->IncludeComponent(
				"bitrix:sale.personal.account",
				"",
				Array(
					"SET_TITLE" => "N"
				),
				$component
			);
		}
		if ($arParams['SHOW_ACCOUNT_PAY_COMPONENT'] !== 'N' && $USER->IsAuthorized())
		{
			?>
			<div class="info">

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:sale.account.pay",
					"main",
					Array(
						"COMPONENT_TEMPLATE" => "main",
						"REFRESHED_COMPONENT_MODE" => "Y",
						"ELIMINATED_PAY_SYSTEMS" => $arParams['ACCOUNT_PAYMENT_ELIMINATED_PAY_SYSTEMS'],
						"PATH_TO_BASKET" => $arParams['PATH_TO_BASKET'],
						"PATH_TO_PAYMENT" => $arParams['PATH_TO_PAYMENT'],
						"PERSON_TYPE" => $arParams['ACCOUNT_PAYMENT_PERSON_TYPE'],
						"REDIRECT_TO_CURRENT_PAGE" => "N",
						"SELL_AMOUNT" => $arParams['ACCOUNT_PAYMENT_SELL_TOTAL'],
						"SELL_CURRENCY" => $arParams['ACCOUNT_PAYMENT_SELL_CURRENCY'],
						"SELL_SHOW_FIXED_VALUES" => $arParams['ACCOUNT_PAYMENT_SELL_SHOW_FIXED_VALUES'],
						"SELL_SHOW_RESULT_SUM" =>  $arParams['ACCOUNT_PAYMENT_SELL_SHOW_RESULT_SUM'],
						"SELL_TOTAL" => $arParams['ACCOUNT_PAYMENT_SELL_TOTAL'],
						"SELL_USER_INPUT" => $arParams['ACCOUNT_PAYMENT_SELL_USER_INPUT'],
						"SELL_VALUES_FROM_VAR" => "N",
						"SELL_VAR_PRICE_VALUE" => "",
						"SET_TITLE" => "N",
					),
					$component
				); ?>
			</div>
		<? } ?>
	</div>
</div>