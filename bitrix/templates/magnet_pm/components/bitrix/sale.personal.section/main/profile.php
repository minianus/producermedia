<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if ($arParams['SHOW_PROFILE_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_PROFILE"));?>

<div class="personal">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", array(
		"ROOT_MENU_TYPE" => "personal",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_TYPE" => "A",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(),
	),
		false
	);?>
	<div class="personal-profile-list">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:sale.personal.profile.list",
			"",
			array(
				"PATH_TO_DETAIL" => $arResult['PATH_TO_PROFILE_DETAIL'],
				"PATH_TO_DELETE" => $arResult['PATH_TO_PROFILE_DELETE'],
				"PER_PAGE" => $arParams["PROFILES_PER_PAGE"],
				"SET_TITLE" =>$arParams["SET_TITLE"],
			),
			$component
		);
		?>
	</div>
</div>