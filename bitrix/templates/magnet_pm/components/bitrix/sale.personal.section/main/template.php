<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$theme = Bitrix\Main\Config\Option::get("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);

$availablePages = array();

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'],
		"name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
		"icon" => '<i class="fa fa-calculator"></i>'
	);
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ACCOUNT'],
		"name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
		"icon" => '<i class="fa fa-credit-card"></i>'
	);
}

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PRIVATE'],
		"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
		"icon" => '<i class="fa fa-user-secret"></i>'
	);
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{

	$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'].$delimeter."filter_history=Y",
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"icon" => '<i class="fa fa-list-alt"></i>'
	);
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PROFILE'],
		"name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
		"icon" => '<i class="fa fa-list-ol"></i>'
	);
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_BASKET'],
		"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
		"icon" => '<i class="fa fa-shopping-cart"></i>'
	);
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_SUBSCRIBE'],
		"name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
		"icon" => '<i class="fa fa-envelope"></i>'
	);
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_CONTACT'],
		"name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
		"icon" => '<i class="fa fa-info-circle"></i>'
	);
}

if ($arParams['SHOW_EXIT_LINK'] === 'Y')
{
	$availablePages[] = array(
		"path" => '?logout=yes&' . bitrix_sessid_get(),
		"name" => Loc::getMessage("SPS_LOGOUT_PAGE_NAME"),
                "icon" => @file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/img/icons/personal-logout.svg')
	);
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
	foreach ($customPagesList as $page)
	{
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"icon" => (strlen($page[2])) ? '<i class="fa '.htmlspecialcharsbx($page[2]).'"></i>' : ""
		);
	}
}

if (empty($availablePages))
{
	ShowError(Loc::getMessage("SPS_ERROR_NOT_CHOSEN_ELEMENT"));
}
else
{
	?>

        <style>
            @media (max-width: 1024px) {
                .margin-top__none {
                    margin-top: unset;
                }
            }

            @media (max-width: 767px) {
                .margin-top__none {
                    margin-top: unset;
                }
            }
        </style>

	<div class="personal margin-top__none">
    	<?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", array(
			"ROOT_MENU_TYPE" => "personal",
			"MAX_LEVEL" => "1",
			"MENU_CACHE_TYPE" => "A",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_TIME" => "36000000",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(),
		),
			false
		);?>
        <?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu_mobile", array(
            "ROOT_MENU_TYPE" => "personal",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_TYPE" => "A",
            "CACHE_SELECTED_ITEMS" => "N",
            "MENU_CACHE_TIME" => "36000000",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
        ),
            false
        );?>



        <div class="personal-menu">
            <div class="style-data">
                <h2>Мои данные</h2>
            </div>
            <div class="personal-menu_items">

                <div class="em ui-a">
                    <div>
                        <div class="em0">
                            <p class="me">Станьте миллионером!</p>
                            <p class="m0e">
                                <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg" class="q7e">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M11.132 2.504a1 1 0 011.736 0l3.35 5.863 5.075-5.074a1 1 0 011.697.845l-1.666 12a1 1 0 01-.99.862H10.5a1 1 0 010-2h8.963l1.136-8.185-3.892 3.892a1 1 0 01-1.575-.21L12 5.015l-3.132 5.48a1 1 0 01-1.575.211L3.4 6.815 4.537 15H6.5a1 1 0 110 2H3.667a1 1 0 01-.99-.862l-1.668-12a1 1 0 011.698-.845l5.074 5.074 3.35-5.863zM4 20a1 1 0 100 2h16a1 1 0 100-2H4z"
                                          fill="currentColor"></path>
                                </svg>
                                <a href="/" class="personal-menu_link"><span>Подробнее</span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="em ui-a">
                    <div>
                        <div class="em0">
                            <p class="me">Premium бонусы</p>
                            <p class="m0e">
                                <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg" class="q7e">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M11.132 2.504a1 1 0 011.736 0l3.35 5.863 5.075-5.074a1 1 0 011.697.845l-1.666 12a1 1 0 01-.99.862H10.5a1 1 0 010-2h8.963l1.136-8.185-3.892 3.892a1 1 0 01-1.575-.21L12 5.015l-3.132 5.48a1 1 0 01-1.575.211L3.4 6.815 4.537 15H6.5a1 1 0 110 2H3.667a1 1 0 01-.99-.862l-1.668-12a1 1 0 011.698-.845l5.074 5.074 3.35-5.863zM4 20a1 1 0 100 2h16a1 1 0 100-2H4z"
                                          fill="currentColor"></path>
                                </svg>
                                <span>0 Б</span>
                            </p>
                        </div>
                    </div>
                </div>


                <div class="style-data">
                    <h2>Условия и помощь</h2>
                </div>
                    <div class="em ui-a">
                        <div>
                            <div class="em1">
                                <a href="/company/payment-and-delivery/" class="personal-menu_link"><p>Условия оплаты</p></a>
                            </div>
                        </div>
                    </div>
                    <div class="em ui-a">
                        <div>
                            <div class="em1">
                                <a href="/company/payment-and-delivery/" class="personal-menu_link"><p>Условия возврата</p></a>
                            </div>
                        </div>
                    </div>



            </div>

<!--            <div class="personal-menu">-->
<!--                --><?// foreach ($availablePages as $blockElement): ?>
<!--                    <a class="item" href="--><?//=htmlspecialcharsbx($blockElement['path'])?><!--">-->
<!--                        --><?//=$blockElement['icon']?>
<!--                        <span class="name">--><?//=htmlspecialcharsbx($blockElement['name'])?><!--</span>-->
<!--                    </a>-->
<!--                --><?// endforeach; ?>
<!--            </div>-->
		</div>
	</div>
	<?
}
?>
