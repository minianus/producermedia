<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$itemSize = count($arResult);

if ($itemSize > 0)
    $strReturn .= '<span class="item"><a href="'.SITE_DIR.'">'.GetMessage('HOMEPAGE_LINK').'</a></span>';

for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if($arResult[$index]["LINK"] <> "" && $arResult[$index]["LINK"] != $APPLICATION->GetCurPage())
    {
        $strReturn .= '
            <span class="item"><a href="'.$arResult[$index]["LINK"].'">'.$title.'</a></span>';
    }
    else
    {
        $strReturn .= '
            <span class="item active">'.$title.'</span>';
    }
}

return $strReturn;
