<?
$MESS["AUTH_PLEASE_AUTH"] = "Please login";
$MESS["AUTH_LOGIN"] = "Login";
$MESS['AUTH_PHONE'] = "Phone number";
$MESS["AUTH_PASSWORD"] = "Password";
$MESS["AUTH_REMEMBER_ME"] = "Remember me";
$MESS["AUTH_AUTHORIZE"] = "Login";
$MESS["AUTH_REGISTER"] = "Sign In";
$MESS["AUTH_REGISTER_TITLE"] = "Sign In";
$MESS["AUTH_FIRST_ONE"] = "If this is your first time on the site, and you want us to remember you, and all your orders are saved, then sign up.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Forgot your password?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Enter the word in the picture";
$MESS["AUTH_TITLE"] = "Login";
$MESS["AUTH_SECURE_NOTE"] = "Before sending the authorization form, the password will be encrypted in the browser. This will avoid passing the password in the clear.";
$MESS["AUTH_NONSECURE_NOTE"] = "Password will be sent in clear text. Enable JavaScript in your browser to encrypt the password before sending.";
