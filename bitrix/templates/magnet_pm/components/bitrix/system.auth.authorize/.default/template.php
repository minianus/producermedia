<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CRecaptcha;
$CSolution = CSolution::getInstance(SITE_ID);
?>


<div class="auth">
	<div class="left-column">
		<div class="text"><?=GetMessage('AUTH_PLEASE_AUTH')?></div>
		<div class="form">
                    <?
                    if(!empty($arParams["~AUTH_RESULT"])):
                            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
                    ?>
                            <div class="message-errors"><?=nl2br(htmlspecialcharsbx($text))?></div>
                    <?endif?>

                    <?
                    if($arResult['ERROR_MESSAGE'] <> ''):
                            $text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
                    ?>
                            <div class="message-errors"><?=nl2br(htmlspecialcharsbx($text))?></div>
                    <?endif?>
			<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                            <input type="hidden" name="AUTH_FORM" value="Y" />
                            <input type="hidden" name="TYPE" value="AUTH" />
                            <?if (strlen($arResult["BACKURL"]) > 0):?>
                            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                            <?endif?>
                            <?foreach ($arResult["POST"] as $key => $value):?>
                            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                            <?endforeach?>
                            
				<div class="input-container">
                                    <? if (CSolution::$options['USER_USE_PHONE_AS_LOGIN'] != "Y"): ?>
					<div class="label"><?=GetMessage("AUTH_LOGIN")?></div>
					<input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>">
                                    <? else: ?>
                                        <div class="label"><?=GetMessage("AUTH_PHONE")?></div>
					<input type="text" name="USER_LOGIN" phone-mask maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>">
                                    <? endif; ?>
				</div>
				<div class="input-container">
					<div class="label">
                                            <?=GetMessage("AUTH_PASSWORD")?>
                                            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                                            <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="forgot-password" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                                            <? endif; ?>
                                        </div>
					<input type="password" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off">
				</div>
                                <?if($arResult["SECURE_AUTH"]):?>
                                    <span class="input-container" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                            <div class="bx-auth-secure-icon"></div>
                                    </span>
                                    <noscript>
                                    <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                                            <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                    </span>
                                    </noscript>
                                    <script type="text/javascript">
                                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                    </script>
                                <?endif?>
                                    
                                <?if($arResult["CAPTCHA_CODE"]):?>
                                    <div class="input-container form-captcha <?=(CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?>">
                                        <div class="label control-label">
                                            <? if ($CSolution::$options['RECAPTCHA_TYPE'] != 'scoring')
                                                echo GetMessage("AUTH_CAPTCHA_PROMT");
                                            ?>
                                        </div>
                                        <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                        <br/>
                                        <input type="text" name="captcha_word" maxlength="50" value="" size="15">
                                    </div>
                                    <?if ($CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                                        <input type="hidden" name="g-recaptcha-response" />
                                    <?endif?>
                                <?endif;?>
                                    
                                <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
                                <div class="checkbox">
					<label class="auth-label">
						<input type="checkbox" name="USER_REMEMBER" value="Y">
						<span class="auth-label-text"><?=GetMessage("AUTH_REMEMBER_ME")?></span>
					</label>
				</div>
                                <?endif?>
                                    
				<button type="submit" name="Login" value="Y" class="btn submit"
                    <?if($CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && $CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox' && $arResult["CAPTCHA_CODE"]):?>
                        onclick="window.CSolution.onSubmitReCaptcha('form_auth')"
                    <?endif?>
                ><?=GetMessage("AUTH_AUTHORIZE")?></button>
			</form>
                        
                        
                        <?if($arResult["AUTH_SERVICES"]):?>
                            <?
                            $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                                    array(
                                            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                                            "CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
                                            "AUTH_URL" => $arResult["AUTH_URL"],
                                            "POST" => $arResult["POST"],
                                            "SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
                                            "FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
                                            "AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
                                    ),
                                    $component,
                                    array("HIDE_ICONS"=>"Y")
                            );
                            ?>
                        <?endif?>
		</div>
	</div>
        <?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
	<div class="right-column">
		<div class="name"><?=GetMessage('AUTH_REGISTER_TITLE')?></div>
		<div class="text"><?=GetMessage('AUTH_FIRST_ONE')?></div>
                <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="btn white reg-link" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
	</div>
        <? endif; ?>
</div>

