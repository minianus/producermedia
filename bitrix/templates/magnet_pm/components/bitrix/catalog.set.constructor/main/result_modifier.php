<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["ELEMENT"]['DETAIL_PICTURE'] || $arResult["ELEMENT"]['PREVIEW_PICTURE'])
{
	$arFileTmp = CFile::ResizeImageGet(
		$arResult["ELEMENT"]['DETAIL_PICTURE'] ? $arResult["ELEMENT"]['DETAIL_PICTURE'] : $arResult["ELEMENT"]['PREVIEW_PICTURE'],
		array("width" => "250", "height" => "250"),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);
        $arFileTmp['SRC'] = $arFileTmp['src'];
        unset($arFileTmp['src']);
	$arResult["ELEMENT"]['DETAIL_PICTURE'] = $arFileTmp;
}
else
{
    $arResult["ELEMENT"]['DETAIL_PICTURE'] = \Nextype\Magnet\CSolution::getEmptyImage();
}

$arDefaultSetIDs = array($arResult["ELEMENT"]["ID"]);
foreach (array("DEFAULT", "OTHER") as $type)
{
	foreach ($arResult["SET_ITEMS"][$type] as $key=>$arItem)
	{
		$arElement = array(
			"ID"=>$arItem["ID"],
			"NAME" =>$arItem["NAME"],
			"DETAIL_PAGE_URL"=>$arItem["DETAIL_PAGE_URL"],
			"DETAIL_PICTURE"=>$arItem["DETAIL_PICTURE"],
			"PREVIEW_PICTURE"=> $arItem["PREVIEW_PICTURE"],
			"PRICE_CURRENCY" => $arItem["PRICE_CURRENCY"],
			"PRICE_DISCOUNT_VALUE" => $arItem["PRICE_DISCOUNT_VALUE"],
			"PRICE_PRINT_DISCOUNT_VALUE" => $arItem["PRICE_PRINT_DISCOUNT_VALUE"],
			"PRICE_VALUE" => $arItem["PRICE_VALUE"],
			"PRICE_PRINT_VALUE" => $arItem["PRICE_PRINT_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE_VALUE" => $arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE" => $arItem["PRICE_DISCOUNT_DIFFERENCE"],
			"CAN_BUY" => $arItem['CAN_BUY'],
			"SET_QUANTITY" => $arItem['SET_QUANTITY'],
			"MEASURE_RATIO" => $arItem['MEASURE_RATIO'],
			"BASKET_QUANTITY" => $arItem['BASKET_QUANTITY'],
			"MEASURE" => $arItem['MEASURE']
		);
		if ($arItem["PRICE_CONVERT_DISCOUNT_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_VALUE"];
		if ($arItem["PRICE_CONVERT_VALUE"])
			$arElement["PRICE_CONVERT_VALUE"] = $arItem["PRICE_CONVERT_VALUE"];
		if ($arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"];

		if ($type == "DEFAULT")
			$arDefaultSetIDs[] = $arItem["ID"];
                
		if ($arItem['DETAIL_PICTURE'] || $arItem['PREVIEW_PICTURE'])
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arItem['DETAIL_PICTURE'] ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'],
				array("width" => "100", "height" => "100"),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);
                        $arFileTmp['SRC'] = $arFileTmp['src'];
                        unset($arFileTmp['src']);
			$arElement['DETAIL_PICTURE'] = $arFileTmp;
		}
                else
                {
                    $arElement['DETAIL_PICTURE'] = \Nextype\Magnet\CSolution::getEmptyImage(true);
                }
                
                $arElement['JS_DATA'] = Array (
                    'data-id="'.$arElement['ID'].'"',
                    'data-price="'.floatval($arElement['PRICE_DISCOUNT_VALUE']).'"',
                    'data-quantity="'.$arItem['BASKET_QUANTITY'].'"',
                    'data-can-buy="'.$arItem['CAN_BUY'].'"',
                    'data-diff-price="'.floatval($arElement['PRICE_DISCOUNT_DIFFERENCE_VALUE']).'"',
                    'data-measure="'.$arItem["MEASURE"]["SYMBOL"].'"',
                    'data-old-price="'.$arItem["PRICE_VALUE"].'"',
                );

		$arResult["SET_ITEMS"][$type][$key] = $arElement;
	}
}
$arResult["DEFAULT_SET_IDS"] = $arDefaultSetIDs;
