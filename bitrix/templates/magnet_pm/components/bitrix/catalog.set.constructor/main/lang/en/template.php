<?
$MESS["CATALOG_SET_TITLE"] = "Buy in a set with other products";
$MESS["CATALOG_SET_BUY"] = "Buy";
$MESS["CATALOG_SET_DISCOUNT_DIFF"] = "(discount #PRICE#)";
$MESS["CATALOG_SET_BUY_SET"] = "Buy a set";
$MESS["CATALOG_SET_BUY_SET_COMPLETE"] = "Basket set";
$MESS["CATALOG_SET_CONSTRUCT"] = "Make your own set";
$MESS["CATALOG_SET_POPUP_LOADER"] = "Loading...";
$MESS["CATALOG_SET_POPUP_TITLE_BAR"] = "Build your own set";
$MESS["CATALOG_SET_POPUP_TITLE"] = "Add accessory to the set";
$MESS["CATALOG_SET_POPUP_DESC"] = "Drag any item with the mouse into the empty field or just click on it";
$MESS["CATALOG_SET_SUM"] = "Total";
$MESS["CATALOG_SET_DISCOUNT"] = "Discount";
$MESS["CATALOG_SET_WITHOUT_DISCOUNT"] = "Price without discount";
$MESS["CATALOG_SET_ADDED2BASKET"] = "The kit has been added to the cart";
$MESS["CATALOG_SET_BUTTON_BUY"] = "Go to cart";
$MESS["CATALOG_SET_BUTTON_ADD"] = "Add";
$MESS["CATALOG_SET_MESS_NOT_AVAILABLE"] = "Not available";
$MESS["CT_BCE_CATALOG_MESS_EMPTY_SET"] = "There are no products in the set, select from the list provided";
$MESS["CATALOG_SET_PRODUCTS_PRICE"] = "The cost of goods separately";
$MESS["CATALOG_SET_SET_PRICE"] = "Set cost";
$MESS["CATALOG_SET_ECONOMY_PRICE"] = "Economy: #VALUE#";
$MESS['CATALOG_SET_PRICE_MEASURE_FULL'] = "for #RATIO# #MEASURE#.";
$MESS['CATALOG_SET_PRICE_MEASURE'] = "* #RATIO# #MEASURE#.";
