<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$curJsId = "set_constructor_" . $this->randString();
?>
<div class="set-constructor" id="<?=$curJsId?>" data-helper="product::constructor">
	<!-- noindex -->
	<div class="mobile-title"><?=GetMessage('CATALOG_SET_TITLE')?></div>
	<!--/noindex-->
    <div class="base-product">
        <div class="img">
            <img src="<?=$arResult["ELEMENT"]['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult["ELEMENT"]['NAME']?>" title="<?=$arResult["ELEMENT"]['NAME']?>" />
        </div>
        <div class="base-info">
    		<div class="name">
    		    <?=$arResult["ELEMENT"]['NAME']?>
    		</div>
    		<div class="price">
    		    <?=$arResult["ELEMENT"]['PRICE_PRINT_DISCOUNT_VALUE']?>
    		    <?=$arResult["ELEMENT"]['MEASURE']['SYMBOL'] && $arResult["ELEMENT"]["MEASURE_RATIO"] ? GetMessage("CATALOG_SET_PRICE_MEASURE", Array (
    		                "#MEASURE#" => $arResult["ELEMENT"]['MEASURE']['SYMBOL'],
    		                "#RATIO#" => $arResult["ELEMENT"]["MEASURE_RATIO"],
    		               )) : ''?>
    		</div>
    		<? if ($arResult["ELEMENT"]['PRICE_VALUE'] > $arResult["ELEMENT"]['PRICE_DISCOUNT_VALUE']): ?>
    		<div class="old-price">
    		    <?=$arResult["ELEMENT"]['PRICE_PRINT_VALUE']?>
    		</div>
        	<? endif; ?>
        </div>
    </div>
    <div class="sets-list">
        <div class="title"><?=GetMessage('CATALOG_SET_TITLE')?></div>
        <table class="list">
            <?foreach(Array ('DEFAULT', 'OTHER') as $type):?>
            <? if (is_array($arResult["SET_ITEMS"][$type])): ?>
            <? foreach ($arResult["SET_ITEMS"][$type] as $arItem): ?>
            <?
            $bInSet = in_array($arItem['ID'], $arResult["DEFAULT_SET_IDS"]) ? true : false;
            ?>
            <tr <?=implode(" ", $arItem['JS_DATA'])?> class="<?=$bInSet ? 'added' : ''?><?=!$arItem['CAN_BUY'] ? ' disabled' : ''?>">
                <td class="img">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" /></a>
                </td>
                <td class="name">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><?=$arItem['NAME']?></a>
                </td>
                <td class="price">
                    <? if ($arItem['PRICE_VALUE'] > $arItem['PRICE_DISCOUNT_VALUE']): ?>
                    <div class="old"><?=$arItem['PRICE_VALUE'] * $arItem["MEASURE_RATIO"] * $arItem['BASKET_QUANTITY']?></div>
                    <? endif; ?>
                    <div class="current">
                    <?=CurrencyFormat($arItem['PRICE_DISCOUNT_VALUE'] * $arItem["MEASURE_RATIO"] * $arItem['BASKET_QUANTITY'], $arItem['PRICE_CURRENCY'])?>
                    <span class="measure">
                    	<?=$arItem['MEASURE']['SYMBOL'] && $arItem["MEASURE_RATIO"] ? GetMessage("CATALOG_SET_PRICE_MEASURE_FULL", Array (
                        "#MEASURE#" => $arItem['MEASURE']['SYMBOL'],
                        "#RATIO#" => $arItem["MEASURE_RATIO"] * $arItem['BASKET_QUANTITY'],
                       )) : ''?>
                    </span>
                    </div>
                </td>
                <td class="action">
                    <a href="javascript:void(0);" class="change-set<?=$bInSet ? ' remove' : ' add'?><?=!$arItem['CAN_BUY'] ? ' disabled' : ''?>" data-add="<?=GetMessage('CATALOG_SET_TEXT_ADD')?>" data-remove="<?=GetMessage('CATALOG_SET_TEXT_REMOVE')?>"></a>                    
                </td>
            </tr>
            <? endforeach; ?>
            <? endif; ?>
            <? endforeach; ?>
        </table>
    </div>
    <div class="set-result">
		<div class="set-price-container">
    		<? if ($arResult['SHOW_DEFAULT_SET_DISCOUNT'] && !empty($arResult["SET_ITEMS"]["OLD_PRICE"])): ?>
    		<div class="old-price" data-role="sum-old-price">
    		    <?=$arResult["SET_ITEMS"]["OLD_PRICE"]?>
    		</div>
    		<? endif; ?>
    		<div class="price" data-role="sum-price">
    		    <?=$arResult["SET_ITEMS"]["PRICE"]?>
    		</div>
    		<? if ($arResult['SHOW_DEFAULT_SET_DISCOUNT'] && !empty($arResult["SET_ITEMS"]["OLD_PRICE"])): ?>
    		<div class="economy" data-role="sum-economy">
    		    <?=GetMessage("CATALOG_SET_ECONOMY_PRICE", Array ("#VALUE#" => $arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE"]))?>
    		</div>
    		<? endif; ?>
        </div>
        <a href="<?=$arParams["BASKET_URL"]?>" data-role="set-buy-btn" <?=($arResult["ELEMENT"]["CAN_BUY"] ? '' : 'style="display: none;"')?> class="btn"><?=GetMessage('CATALOG_SET_BUY_SET')?></a>
    </div>
</div>

<?
$arJsParams = array(
	"jsId" => $curJsId,
	"parentContId" => "bx-set-const-".$curJsId,
	"ajaxPath" => $this->GetFolder().'/ajax.php',
	"canBuy" => $arResult["ELEMENT"]["CAN_BUY"],
	"currency" => $arResult["ELEMENT"]["PRICE_CURRENCY"],
	"mainElementPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"],
	"mainElementOldPrice" => $arResult["ELEMENT"]["PRICE_VALUE"],
	"mainElementDiffPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
	"mainElementBasketQuantity" => $arResult["ELEMENT"]["BASKET_QUANTITY"],
	"lid" => SITE_ID,
	"iblockId" => $arParams["IBLOCK_ID"],
	"setIds" => $arResult["DEFAULT_SET_IDS"],
	"offersCartProps" => $arParams["OFFERS_CART_PROPERTIES"],
	"itemsRatio" => $arResult["BASKET_QUANTITY"],
	"messages" => array(
		"EMPTY_SET" => GetMessage('CT_BCE_CATALOG_MESS_EMPTY_SET'),
		"ADD_BUTTON" => GetMessage("CATALOG_SET_BUTTON_ADD"),
                "SET_BUY_SET_COMPLETE" => GetMessage("CATALOG_SET_BUY_SET_COMPLETE")
	)
);
?>
<script type="text/javascript">
	BX.ready(function(){
            new BX.Catalog.SetConstructor(<?=CUtil::PhpToJSObject($arJsParams, false, true, true)?>);
	});
</script>
