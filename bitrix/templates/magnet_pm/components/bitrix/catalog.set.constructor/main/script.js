BX.namespace("BX.Catalog.SetConstructor");

BX.Catalog.SetConstructor = (function()
{
	var SetConstructor = function(params)
	{
		this.numSliderItems = params.numSliderItems || 0;
		this.numSetItems = params.numSetItems || 0;
		this.jsId = params.jsId || "";
		this.ajaxPath = params.ajaxPath || "";
		this.currency = params.currency || "";
		this.lid = params.lid || "";
		this.iblockId = params.iblockId || "";
		this.basketUrl = params.basketUrl || "";
		this.setIds = params.setIds || null;
		this.offersCartProps = params.offersCartProps || null;
		this.itemsRatio = params.itemsRatio || null;
		this.messages = params.messages;

		this.canBuy = params.canBuy;
		this.mainElementPrice = params.mainElementPrice || 0;
		this.mainElementOldPrice = params.mainElementOldPrice || 0;
		this.mainElementDiffPrice = params.mainElementDiffPrice || 0;
		this.mainElementBasketQuantity = params.mainElementBasketQuantity || 1;

		this.parentCont = BX(params.parentContId) || null;
		
                
                var container = $("#" + this.jsId);
                var self = this;
                
                container.find('.list tr:not(.disabled)').on('click', function (event) {

                    var tr = $(this),
                        button = tr.find('.change-set'),
                        bInSet = tr.hasClass('added'),
                        id = parseInt(tr.data('id'));
                    
                    if (bInSet) {
                        tr.removeClass('added');
                        button.removeClass('remove').addClass('add');
                    } else {
                        tr.addClass('added');
                        button.addClass('remove').removeClass('add');
                    }
                    
                    if (typeof(self.setIds) != 'object')
                        self.setIds = [];
                    
                    if (self.setIds.indexOf(id) !== -1)
                        self.setIds.splice(self.setIds.indexOf(id), 1);
                    else
                        self.setIds.push(id);
                    
                    self.recountPrice();
                });
                
                
                
                container.find('[data-role="set-buy-btn"]').on('click', function (event) {
                    if (!$(this).hasClass('in-basket')) {
                        event.preventDefault();
                        $(this).addClass('loaded');
                        BX.ajax.post(
                            self.ajaxPath,
                            {
                                    sessid: BX.bitrix_sessid(),
                                    action: 'catalogSetAdd2Basket',
                                    set_ids: self.setIds,
                                    lid: self.lid,
                                    iblockId: self.iblockId,
                                    setOffersCartProps: self.offersCartProps,
                                    itemsRatio: self.itemsRatio
                            },
                            BX.proxy(function(result)
                            {
                                $(this).text(self.messages.SET_BUY_SET_COMPLETE);
                                $(this).removeClass('loaded').addClass('in-basket');

                                BX.onCustomEvent('OnBasketChange');
                                
                            }, this)
                        );
                        return false;
                    }
                });

	};
        
        SetConstructor.prototype.recountPrice = function() {

            var sumPrice = this.mainElementPrice*this.mainElementBasketQuantity,
		sumOldPrice = this.mainElementOldPrice*this.mainElementBasketQuantity,
		sumDiffDiscountPrice = this.mainElementDiffPrice*this.mainElementBasketQuantity,
                self = this,
                container = $("#" + this.jsId);

            container.find('.list tr').each(function () {
                var id = parseInt($(this).data('id'));

                if (self.setIds.indexOf(id) !== -1) {
                    
                    var ratio = Number($(this).data("quantity")) || 1;
                    sumPrice += Number($(this).data("price"))*ratio,
                    sumOldPrice += Number($(this).data("old-price"))*ratio,
                    sumDiffDiscountPrice += Number($(this).data("diff-price"))*ratio;
                }
            });

            container.find('[data-role="sum-price"]').html(BX.Currency.currencyFormat(sumPrice, this.currency, true));
            
            if (Math.floor(sumDiffDiscountPrice * 100) > 0)
            {
                container.find('[data-role="sum-old-price"]').html(BX.Currency.currencyFormat(sumOldPrice, this.currency, true)).show();
                container.find('[data-role="sum-economy"]').show().find('span').html(BX.Currency.currencyFormat(sumDiffDiscountPrice, this.currency, true));
            } else
            {
                container.find('[data-role="sum-old-price"], [data-role="sum-economy"]').hide();
            }
        };
        
        

	return SetConstructor;
})();