<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution,
   Nextype\Magnet\CRecaptcha;
CSolution::getInstance(SITE_ID);

if(isset($APPLICATION->arAuthResult))
    $arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;

if($arResult["SHOW_SMS_FIELD"] == true)
{
    CJSCore::Init('phone_auth');
}
?>

<div class="auth">
	<div class="reg">

                <?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
                <div class="text"><?=GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></div>
                <? else: ?>
                <div class="text"><?=GetMessage("AUTH_REG_TITLE")?></div>
                <?endif?>
                
                <?if($arResult["SHOW_SMS_FIELD"] == true):?>
                
                <div class="form">
                    <div class="message-errors" id="bx_register_error" style="display:none"><?ShowError("error")?></div>
                    
                    <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="regform" enctype="multipart/form-data">
                        <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
                        
                        <div class="input-container">
                                <div class="label"><?=GetMessage("main_register_sms_code")?> <span class="required">*</span></div>
                                <input size="30" type="text" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" />
                        </div>
                        
                        <div class="buttons">
                            <button type="submit" name="code_submit_button" value="Y" class="btn submit"><?=GetMessage("main_register_sms_send")?></button>
                            <div id="bx_register_resend"></div>
			</div>
                    </form>
                </div>
                
                <script>
                new BX.PhoneAuth({
                    containerId: 'bx_register_resend',
                    errorContainerId: 'bx_register_error',
                    interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                    data:
                        <?=CUtil::PhpToJSObject([
                            'signedData' => $arResult["SIGNED_DATA"],
                        ])?>,
                    onError:
                        function(response)
                        {
                            var errorDiv = BX('bx_register_error');
                            var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                            errorNode.innerHTML = '';
                            for(var i = 0; i < response.errors.length; i++)
                            {
                                errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                            }
                            errorDiv.style.display = '';
                        }
                });
                </script>
                
		<?elseif(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>
                
		<div class="form">
                        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR'): ?>
                        <div class="message-errors"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></div>
                        <? endif; ?>

                        <? if ($arResult['ERROR_MESSAGE']['TYPE'] == 'OK'): ?>
                        <div class="message-success">
                            <?
                            echo $arResult['ERROR_MESSAGE']['MESSAGE'] . "<br/>";
                            
                            if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK")
                                echo GetMessage("AUTH_EMAIL_SENT");
                            ?>
                        </div>
                        <? endif; ?>
                        
			<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
                            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                            <? endif; ?>
                            <input type="hidden" name="AUTH_FORM" value="Y" />
                            <input type="hidden" name="TYPE" value="REGISTRATION" />
        
                            <div class="input-container">
                                <div class="label"><?=GetMessage("AUTH_NAME")?></div>
                                <input type="text" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" />
                            </div>
                            
                            <div class="input-container">
                                <div class="label"><?=GetMessage("AUTH_LAST_NAME")?></div>
                                <input type="text" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" />
                            </div>
                            
                            <? if (CSolution::$options['USER_USE_PHONE_AS_LOGIN'] != "Y"): ?>
                            <div class="input-container">
                                <div class="label"><?=GetMessage("AUTH_LOGIN_MIN")?> <span class="required">*</span></div>
                                <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" />
                            </div>
                            <? else: ?>
                                <?if($arResult["PHONE_REGISTRATION"]):?>
                                <div class="input-container">
                                    <div class="label"><?=GetMessage("AUTH_PHONE")?> <span class="required">*</span></div>
                                    <input type="text" onkeyup="$(this).parent().find('input[name=PERSONAL_PHONE]').val($(this).val())" name="USER_PHONE_NUMBER" phone-mask maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
                                    <input type="hidden" name="PERSONAL_PHONE" value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
                                </div>
                                <? else: ?>
                                <div class="input-container">
                                    <div class="label"><?=GetMessage("AUTH_PHONE")?> <span class="required">*</span></div>
                                    <input type="text" name="PERSONAL_PHONE" phone-mask maxlength="50" value="<?=!empty($arResult["PERSONAL_PHONE"]) ? $arResult["PERSONAL_PHONE"] : trim(strip_tags($_REQUEST['PERSONAL_PHONE']))?>" />
                                </div>
                                <?endif?>

                            <? endif; ?>
                            
                            <div class="input-container">
				<div class="label"><?=GetMessage('AUTH_PASSWORD_REQ')?> <span class="required">*</span></div>
				<input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
                                <? if (!empty($arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"])): ?>
				<div class="tip"><?=$arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]?></div>
                                <? endif; ?>
                            </div>
                            
                            <div class="input-container">
				<div class="label"><?=GetMessage('AUTH_CONFIRM')?> <span class="required">*</span></div>
                                <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
                            </div>
				
                            <div class="input-container">
				<div class="label"><?=GetMessage('AUTH_EMAIL')?><?if($arResult["EMAIL_REQUIRED"]):?> <span class="required">*</span><?endif?></div>
                                <input type="email" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" />
                            </div>
                            
                            <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                            <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
                                if (CSolution::$options['USER_USE_PHONE_AS_LOGIN'] == "Y" && $FIELD_NAME == "PERSONAL_PHONE") continue;
                            ?>
                            <div class="input-container">
                                    <div class="label">
                                        <?=$arUserField["EDIT_FORM_LABEL"]?> <?if ($arUserField["MANDATORY"]=="Y"):?><span class="required">*</span><?endif;?>
                                    </div>
                                    <?$APPLICATION->IncludeComponent(
                                                            "bitrix:system.field.edit",
                                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?>
                            </div>
                            
                            <?endforeach;?>
                            <?endif;?>
                            
                            <? if ($arResult["USE_CAPTCHA"] == "Y"): ?>
                            <div class="input-container <?=(CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?>">
                                <div class="label">
                                    <?=GetMessage("CAPTCHA_REGF_PROMT")?> <span class="required">*</span>
                                </div>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                <br/>
                                <input type="text" name="captcha_word" maxlength="50" value="" />
                                
                    <?if (CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                        <input type="hidden" name="g-recaptcha-response" />
                    <?endif?>
                            </div>
                            <? endif; ?>
                            
                            
                            
                            <?if (CSolution::$options['FORM_POLICY'] == 'Y')
                            {
                                $APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            "",
                                            array(
                                                    "PATH" => SITE_DIR . "include/form_policy_checkbox.php",
                                                    "AREA_FILE_SHOW" => "file",
                                                    "AREA_FILE_SUFFIX" => "",
                                                    "AREA_FILE_RECURSIVE" => "Y",
                                                    "EDIT_TEMPLATE" => "standard.php",
                                                    "FORM_ID" => $sID,
                                                    "CHECKED" => CSolution::$options['FORM_POLICY_CHECKED'],
                                                    "DETAIL_PAGE_URL" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PROCESSING_POLICY']),
                                            ),
                                            false
                                    );

                            }
                            ?>
		

                            

				
				<div class="buttons">
					<button type="submit" name="Register" value="Y" class="btn submit"
                            <?if(CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                                onclick="window.CSolution.onSubmitReCaptcha('bform')"
                            <?endif?>
                                                >
                                            <?=GetMessage("AUTH_REGISTER")?>
                                        </button>
					<a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="auth-link"><?=GetMessage("AUTH_AUTH")?></a>
				</div>
			</form>
		</div>
                <? endif; ?>
	</div>
</div>