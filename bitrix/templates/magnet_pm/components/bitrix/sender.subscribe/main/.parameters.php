<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters['USE_RECAPTCHA'] = array(
    'PARENT' => 'BASE',
    'NAME' => GetMessage('TP_USE_RECAPTCHA'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);

$arTemplateParameters['RECAPTCHA_HIDE'] = array(
    'PARENT' => 'BASE',
    'NAME' => GetMessage('TP_RECAPTCHA_HIDE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);

?>

