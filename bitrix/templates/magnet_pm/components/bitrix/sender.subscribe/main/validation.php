<?
//verify google recaptcha response
define("NO_KEEP_STATISTIC", true);
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (!\Bitrix\Main\Loader::includeModule('nextype.magnet'))
    die('Nextype magnet module not included');

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && !empty($response = $request->get('response')))
{
    $arValidation = array(
        'checkkey' => '',
        'success' => false
    );
    
    $siteId = (empty($request->get('siteid'))) ? SITE_ID : $request->get('siteid');
    $data = http_build_query(array(
            'response' => $response,
            'secret' => \Nextype\Magnet\CSolution::getInstance($siteId)::$options['RECAPTCHA_SECRET_KEY'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
            )
    );
    $options = array('http' => array(
                'method' => 'POST',
                'header' => 'application/x-www-form-urlencoded',
                'content' => $data
            )
    );
    $query = stream_context_create($options);
    
    $googleResponse = @file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $query);
    if (!$googleResponse)
    {
        exit('Failed get response from Google');
    }
    
    $result = json_decode($googleResponse);
    if ($result->success)
    {
        $arValidation['checkkey'] = substr($response, 0, 7);
        $arValidation['success'] = true;
    }
    
    echo \Bitrix\Main\Web\Json::encode($arValidation);
    die;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");