<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::includeModule('form'))
	return;

$arAutocompleteFields = Array (
    '' => GetMessage('FORM_AUTOCOMPLETE_NO_SELECT'),
);

if (!empty($arCurrentValues['WEB_FORM_ID']))
{
    $rsQuestions = CFormField::GetList($arCurrentValues['WEB_FORM_ID'], "N");
    while ($arQuestion = $rsQuestions->Fetch())
    {
        $arAutocompleteFields[$arQuestion['SID']] = "[" . $arQuestion['SID'] . "] " .$arQuestion['TITLE'];
    }
}

$arTemplateParameters['AUTOCOMPLETE_FIELD_NAME'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('FORM_AUTOCOMPLETE_FIELD_NAME'),
	'TYPE' => 'LIST',
	'VALUES' => $arAutocompleteFields,
	'DEFAULT' => '',
);

$arTemplateParameters['AUTOCOMPLETE_FIELD_VALUE'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('FORM_AUTOCOMPLETE_FIELD_VALUE'),
	'TYPE' => 'TEXT',
	'DEFAULT' => '',
);