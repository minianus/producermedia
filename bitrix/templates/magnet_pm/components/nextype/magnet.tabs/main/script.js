(function(window){
	'use strict';

	if (window.JMagnetTabs)
		return;
            
        window.JMagnetTabs = function(arParams)
	{
            this.container = false;
            
            if (typeof arParams === 'object')
            {
                this.params = arParams;
                
                if (this.params.CONTAINER_ID === undefined || this.params.CONTAINER_ID == "")
                    return;
                

                BX.ready(BX.delegate(this.init, this));
            }
            
        }
        
        window.JMagnetTabs.prototype = {
            
            init: function () {
                
                this.container = $("#" + this.params.CONTAINER_ID);
                var self = this;
                
                if (typeof(this.container) == 'object') {
                    
                    self.renderTab(self.container.find('.tab-content.active'));
                    
                    this.container.find('.tabs-container > a').on('click', function (event) {
                        event.preventDefault();
                        var tab = self.container.find('.tab-content:eq(' + $(this).index() + ')');
                        if (tab.length > 0) {
                            $(this).parent().find('> a').removeClass('active');
                            self.container.find('.tab-content').removeClass('active');
                            tab.addClass('active');
                            $(this).addClass('active');
                            self.loadAjaxTab(tab);
                            
                            
                        }
                        return false;
                    });
                    
                    window.addEventListener("resize", function () {
                        clearTimeout(window.resizedFinished);
                        window.resizedFinished = setTimeout(function(){
                            self.renderTab(self.container.find('.tab-content'));
                        }, 500);

                    });
                    
                    
                }
                
            },
            
            waitResize: function (func, wait, immediate) {
                var timeout;
                return function() {
                  var context = this, args = arguments;
                  var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                  };
                  var callNow = immediate && !timeout;
                  clearTimeout(timeout);
                  timeout = setTimeout(later, wait);
                  if (callNow) func.apply(context, args);
                };
            },
            
            initTabSlider: function () {
                var tab = this.container.find('.tab-content:eq(0)');
                if (tab.length > 0 && !tab.hasClass('loaded')) {
                    this.loadAjaxTab(tab);
                }
            },
            
            loadAjaxTab: function (tab) {
                if (typeof(tab) == 'object') {
                    if (!tab.hasClass('loaded'))
                    {
                        var self = this;
                        tab.addClass('loaded');
                        
                        $.ajax({
                            url: self.params.AJAX,
                            type: 'post',
                            data: {
                                is_ajax_mode: 'y',
                                ajax_component_params: self.params.COMPONENT_PARAMS,
                                ajax_component_template: self.params.TEMPLATE,
                                ajax_component_filter: tab.data('comp-filter')
                            },
                            success: function (result) {
                                tab.html(result);
                                //window.CSolution.setProductsHeight(tab.find('.swiper-slide'));
                                tab.find('.owl-container').css('height', 'auto');
                                self.renderTab(tab);

                            }
                        });

                    }
                }
            },
            
            renderTab: function (tab) {
                window.CSolution.setProductsHeight(tab.find('.swiper-slide'));
                tab.each(function(i, elem) {
                    
                        
                    var slideWidth = $(this).width(),
                        items = $(this).find('.product-wrap'),
                            productMaxWidth = $('.product-wrap').width(),				
                            itemsOnSlide = Math.floor(slideWidth / productMaxWidth) * 2;

                    //$(this).find('.product-wrap').css('max-width', productMaxWidth);
                    if (window.innerWidth < 768) {
						var owlContainer = $(this).find('.owl-container'),
						    productContainer = owlContainer.find('.products-container');
						    productContainer.addClass('owl-carousel');
                        productContainer.owlCarousel({
                            loop: false,
                            dots: true,
                            margin: 24,
                            mouseDrag: false,
                            info: true,
                            onTranslate: function (event) {
                                $(event.currentTarget).find('[data-lazy="default"]').lazy();
                            },
                            onInitialize: function (event) {
                                $(event.currentTarget).find('[data-lazy="default"]').lazy();
                            },
                            responsive:{
                                0:{
                                    items: 1
                                },
                                461: {
                                	items: 2
                                },
                                701:{
                                	items: 3
                                }
                            }
                        });
                        
                    } else {
                    	if (items.length > itemsOnSlide) {
                            var slideCount = 0, itemsCount = itemsOnSlide;			
                            var owlContainer = $(this).find('.owl-container');
                            if (owlContainer.find('.owl-loaded').length > 0) {
                                items = items.clone().css('height', '');
                                var nItems = $('<div class="products-container"></div>').append(items);
                                owlContainer.empty().append(nItems);
                            }
                            
                            var productContainer = owlContainer.find('.products-container');
                    
                            productContainer.append('<div class="products-list slide-' + slideCount + '"></div>');
                            items.each(function(keyItem, elem) {
                                if (keyItem != itemsCount) {
                                    $(this).siblings('.slide-' + slideCount).append(elem);
                                }
                                else {
                                    slideCount++;
                                    itemsCount = itemsCount + itemsOnSlide;
                                    $(this).parents('.products-container').append('<div class="products-list slide-' + slideCount + '"></div>');
                                    $(this).siblings('.slide-' + slideCount).append(elem);
                                }

                            });
                            
                            productContainer.removeClass('products-container').addClass('owl-carousel');
                            productContainer.owlCarousel({
                                        loop: false,
                                        items: 1,
                                        margin: 24,
                                        mouseDrag: false,
                                        info: true,
                                        navText: ["<div class='prev icon-custom'></div>", "<div class='next icon-custom'></div>"],                                    
                                        responsive:{
                                            0:{
                                                nav: false,
                                                dots: true
                                            },
                                            768:{
                                                nav: true,
                                                dots: false
                                            }
                                        },
                                        onInitialized: function (event) {
                                                var slideHeight = $(event.target).find('.owl-item.active').height();
                                                $(event.target).parent().height(slideHeight);
                                                $(event.target).find('[data-lazy="default"]').lazy();
                                                
                                        },
                                        onTranslate: function (event) {
                                                var currentSlide = $(event.target).find('.owl-item')[event.item.index],
                                                    slideHeight = $(currentSlide).height();
                                                $(event.target).parent().height(slideHeight);

                                                var lazyImg = $(event.target).find('[data-lazy="default"]');
                                                if (lazyImg.length > 0) {
                                                    lazyImg.each(function () {
                                                        $(this).attr('src', $(this).data('src')).removeAttr('data-src data-lazy');
                                                    });
                                                }
                                        }
                            });   
                    	} else {
                            $(this).find('[data-lazy="default"]').lazy();
			}
                    }
                    
                    
                    
                });
            }
            
            
        }
})(window);