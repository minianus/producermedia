<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['TABS'])):
    CJSCore::Init(Array('currency'));

    \Nextype\Magnet\CSolution::getInstance(SITE_ID);
    $mainId = $this->GetEditAreaId('tabs');
    $obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
    $arResult['arComponentSectionParams']['IMAGES_LAZY_LOAD'] = \Nextype\Magnet\CSolution::$options['IMAGES_LAZY_LOAD'];

?>
<div class="products main-nav" id="<?=$mainId?>" data-helper="homepage::product_tabs">
	<div class="tabs-container">
            <? foreach ($arResult['TABS'] as $key => $arTab): ?>
		<a class="tab<?=$key == 0 ? ' active' : ''?>" href="javascript:void(0);"><?=$arTab['NAME']?></a>
            <? endforeach; ?>

	</div>
        <? foreach ($arResult['TABS'] as $key => $arTab):
            $arComponentParamsTmp = array_merge($arResult['arComponentSectionParams'], Array("CACHE_FILTER" => "Y", "TAB_CODE" => $arTab['CODE'], 'IMAGES_LAZY_LOAD' => \Nextype\Magnet\CSolution::$options['IMAGES_LAZY_LOAD']));            
            $GLOBALS[$arResult['arComponentSectionParams']['FILTER_NAME']] = $arTab['FILTER'];
            $sComponentTemplateTmp = 'tabs_slider';
        ?>
        <div class="tab-content<?=$key == 0 ? ' active' : ''?>" data-comp-filter="<?=urlencode(serialize($arTab['FILTER']))?>">
            <? if ($key == 0) {
                include($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/components/catalog_section.php");
            } else {
            ?>
            <div class="preloader">
                <svg width="200px" height="200px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-dual-ring" style="background: none;"><circle cx="50" cy="50" fill="none" stroke-linecap="round" r="40" stroke-width="4" stroke="#333" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(161.792 50 50)"></circle></svg>
            </div>
            <? } ?>
        </div>
        <? endforeach; ?>
        <script>
            var <?=$obName?> = new JMagnetTabs(<?=CUtil::PhpToJSObject(Array (
                'CONTAINER_ID' => $mainId,
                'AJAX' => SITE_DIR . "include/components/catalog_section.php",
                'COMPONENT_PARAMS' => urlencode(serialize($arResult['arComponentSectionParams'])),
                'TEMPLATE' => $sComponentTemplateTmp,
            ), false, true)?>);
           
	</script>
</div>
<? endif; ?>