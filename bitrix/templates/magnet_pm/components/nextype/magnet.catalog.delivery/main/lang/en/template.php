<?php

$MESS['NT_MAGNET_CDELIVERY_TITLE_W_CITY'] = "Read more about delivery to the <span>#CITY#</span>";
$MESS['NT_MAGNET_CDELIVERY_TITLE_WO_CITY'] = "Delivery details";
$MESS['NT_MAGNET_CDELIVERY_DESCR'] = "To calculate delivery to another city, enter its name in the field below.";
$MESS['NT_MAGNET_CDELIVERY_QTY_SELECTOR'] = "Qty:";
$MESS['NT_MAGNET_CDELIVERY_USE_BASKET'] = "Add items to cart";
$MESS['NT_MAGNET_CDELIVERY_CITY_PLACEHOLDER'] = "City name";
$MESS['NT_MAGNET_CDELIVERY_EMPTY_RESULT'] = "No shipping methods available.";
$MESS['NT_MAGNET_CDELIVERY_FREE_PRICE'] = "Is free";