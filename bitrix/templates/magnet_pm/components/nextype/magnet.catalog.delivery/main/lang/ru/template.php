<?php

$MESS['NT_MAGNET_CDELIVERY_TITLE_W_CITY'] = "Подробнее о доставке в <span>#CITY#</span>";
$MESS['NT_MAGNET_CDELIVERY_TITLE_WO_CITY'] = "Укажите город для расчета доставки";
$MESS['NT_MAGNET_CDELIVERY_DESCR'] = "Чтобы расчитать доставку в другой город, введите его название в поле ниже.";
$MESS['NT_MAGNET_CDELIVERY_QTY_SELECTOR'] = "Кол-во:";
$MESS['NT_MAGNET_CDELIVERY_USE_BASKET'] = "Учитывать товары в корзине";
$MESS['NT_MAGNET_CDELIVERY_CITY_PLACEHOLDER'] = "Название города";
$MESS['NT_MAGNET_CDELIVERY_EMPTY_RESULT'] = "Нет доступных способов доставки";
$MESS['NT_MAGNET_CDELIVERY_FREE_PRICE'] = "Бесплатно";
$MESS['NT_MAGNET_CDELIVERY_UNDEFINED'] = "Произошла ошибка при расчете цены";