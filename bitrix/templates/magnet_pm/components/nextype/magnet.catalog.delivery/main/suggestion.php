<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define("NOT_CHECK_PERMISSIONS", true);

use Bitrix\Main;
use Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');

Loader::includeModule('sale');

require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/components/bitrix/sale.location.selector.search/class.php');

$result = true;
$errors = array();
$data = array();

try
{
	CUtil::JSPostUnescape();

	$request = Main\Context::getCurrent()->getRequest()->getPostList();
	if($request['version'] == '2')
		$data = CBitrixLocationSelectorSearchComponent::processSearchRequestV2($_REQUEST);
	else
		$data = CBitrixLocationSelectorSearchComponent::processSearchRequest();
}
catch(Main\SystemException $e)
{
	$result = false;
	$errors[] = $e->getMessage();
}

if ($data["ITEMS"] && Loader::includeModule('nextype.magnet'))
{
    foreach ($data["ITEMS"] as &$item)
    {
        $item["DISPLAY_GEN"] = \Nextype\Magnet\CLocations::getCityGenitive($item["DISPLAY"]);
    }
}

print(\Bitrix\Main\Web\Json::encode(array(
	'result' => $result,
	'errors' => $errors,
	'data' => $data
)));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");