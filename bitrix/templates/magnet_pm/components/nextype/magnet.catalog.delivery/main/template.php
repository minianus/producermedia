<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$containerId = randString() . "_catalog_delivery";
?>
<div class="delivery-popup" id="<?=$containerId?>">
	<div class="popup-header">
		<a href="javascript:void(0);" onclick="$('.popup').jqmHide(); return false;" class="close-popup-icon icon-custom"></a>
		<div class="title" id="<?=$containerId?>_location_name">
                    <?=!empty($arResult['LOCATION']['NAME_RU']) ? 
                        GetMessage('NT_MAGNET_CDELIVERY_TITLE_W_CITY', array("#CITY#" => $arResult['LOCATION']['NAME_RU'])) : 
                        GetMessage('NT_MAGNET_CDELIVERY_TITLE_WO_CITY')?>
                </div>
		<div class="desc"><?=GetMessage('NT_MAGNET_CDELIVERY_DESCR')?></div>		
	</div>
    
	<form class="delivery-search" onsubmit="return false;">
		<div class="search-filed">
			<input type="text" placeholder="<?=GetMessage('NT_MAGNET_CDELIVERY_CITY_PLACEHOLDER')?>">
                        <div class="suggestions"></div>
                        <input type="hidden" name="location" value="<?=$arResult['LOCATION']['ID']?>" id="<?=$containerId?>_location" />
		</div>	
            
		<div class="counter-container">
			<div class="label"><?=GetMessage('NT_MAGNET_CDELIVERY_QTY_SELECTOR')?></div>
			<div class="counter" id="<?=$containerId?>_qty_container">
				<a href="javascript:void(0)" class="minus icon-custom"></a>
				<input class="count" name="qty" type="number" id="<?=$containerId?>_qty" value="1">
				<a href="javascript:void(0)" class="plus icon-custom"></a>
			</div>
		</div>
		<div class="checkbox">
			<input type="checkbox" value="y" name="include_basket" id="<?=$containerId?>_use_basket">
			<label for="<?=$containerId?>_use_basket"><?=GetMessage('NT_MAGNET_CDELIVERY_USE_BASKET')?></label>
		</div>
	</form>
        <div id="<?=$containerId?>_ajax_data">
        <? if ($_REQUEST['refresh'] == 'y') $APPLICATION->RestartBuffer(); ?>
            <? if (!empty($arResult['DELIVERY'])): ?>
            <div class="list">
                <? foreach ($arResult['DELIVERY'] as $arDelivery): ?>
                    <? if (empty($arResult['LOCATION']['NAME_RU']))
                        continue; ?>
                    <div class="item">
                        <div class="img">
                            <?
                            if (empty($arDelivery['LOGOTIP']))
                                $arDelivery['LOGOTIP'] = \Nextype\Magnet\CSolution::getEmptyImage()["SRC"];
                            ?>
                            <img src="<?=$arDelivery['LOGOTIP']?>" alt="<?=$arDelivery['NAME']?>" />
                        </div>
                        
                        <div class="info">
                            <div class="name"><?=$arDelivery['NAME']?></div>
                                <? if (!empty($arDelivery['DESCRIPTION'])): ?>
                                    <div class="text"><?=$arDelivery['DESCRIPTION']?></div>
                                <? endif; ?>
                        </div>
                        
                        <div class="price">
                            <? if ($arDelivery['PRICE'] === 0.00)
                                echo GetMessage('NT_MAGNET_CDELIVERY_FREE_PRICE');
                               elseif (!isset($arDelivery['PRICE']))
                                echo GetMessage('NT_MAGNET_CDELIVERY_UNDEFINED)PRICE');
                               else
                                echo $arDelivery['PRICE_FORMATED']; ?>
                        </div>
                    </div>
            <? endforeach; ?>
        </div>
        <? else: ?>
            <p><?=GetMessage('NT_MAGNET_CDELIVERY_EMPTY_RESULT')?></p>
        <? endif; ?>
            
            <? if ($_REQUEST['refresh'] == 'y') exit(); ?>
            
        </div>
</div>
<script>

    function fnRefresh<?=$containerId?>() {
        window.CSolution.setAjaxLoader($("#<?=$containerId?>_ajax_data"));
        $.ajax({
            type: 'post',
            url: '<?=$APPLICATION->GetCurPageParam('refresh=y', Array ('qty', 'include_basket', 'location'))?>',
            data: $("#<?=$containerId?> form").serialize(),
            success: function (data) {
                $("#<?=$containerId?>_ajax_data").html(data);
            }
        });
    }
    
    function fnSuggestion<?=$containerId?>(event) {
        var input = $(event.target);
        if (input.val().length > 0) {
            $.ajax({
                type: 'post',
                dataType: 'json',
                cache: false,
                url: '<?=$templateFolder?>/suggestion.php',
                data: {
                    select: {
                        1: 'CODE',
                        2: 'TYPE_ID',
                        'DISPLAY': 'NAME.NAME',
                        'VALUE': 'ID'
                    },
                    filter: {
                        '=PHRASE': input.val(),
                        '=NAME.LANGUAGE_ID': '<?=LANGUAGE_ID?>',
                        '=SITE_ID': '<?=SITE_ID?>'
                    },
                    version: 2,
                    PAGE_SIZE: 10,
                    PAGE: 0
                },
                success: function (json) {
                    if (json !== undefined && typeof(json.data.ITEMS) == 'object') {
                        var sContainer = $("#<?=$containerId?> .search-filed .suggestions");
                        sContainer.empty();
                        $.each(json.data.ITEMS, function (index, ob) {
                            var item = $('<a href="javascript:void(0)" data-value="'+ob.VALUE+'" class="item">'+ob.DISPLAY+'</a>');
                            item.on('click', function () {
                                if ($(this).data('value') != "") {
                                    if ($("#<?=$containerId?>_location_name span").length == 0)
                                        $("#<?=$containerId?>_location_name").html("<?=GetMessage('NT_MAGNET_CDELIVERY_TITLE_W_CITY', array('#CITY#' => ''));?>");
                                        
                                    $("#<?=$containerId?>_location").val($(this).data('value'));
                                    fnRefresh<?=$containerId?>();
                                    $("#<?=$containerId?> .search-filed .suggestions").removeClass('open');
                                    $("#<?=$containerId?> .search-filed > input[type=text]").val(ob.DISPLAY);
                                    $("#<?=$containerId?>_location_name span").text(ob.DISPLAY_GEN);
                                }
                            });
                            sContainer.append(item)
                        });
                        sContainer.addClass('open');
                    }
                
                }
            });
        }
    }
    
    
    $("#<?=$containerId?> form").on('refresh', $.debounce(fnRefresh<?=$containerId?>, 500));
    $("#<?=$containerId?> .search-filed > input").on('keyup', $.debounce(fnSuggestion<?=$containerId?>, 500));
    
    $("#<?=$containerId?>_use_basket, #<?=$containerId?>_qty").on('change', function (event) {
        $("#<?=$containerId?> form").trigger('refresh');
    });
    
    
    $(document).click(function(event) {
        if ($(event.target).closest("#<?=$containerId?> .search-filed .suggestions").length) return;
        $("#<?=$containerId?> .search-filed .suggestions").removeClass("open");
        event.stopPropagation();
    });
    
    $("#<?=$containerId?>_qty_container a").on('click', function () {
        var input = $(this).parent().find('input'), num = parseFloat(input.val());
        if ($(this).hasClass('minus') && num - 1 > 0)
            input.val(num - 1);
        
        if ($(this).hasClass('plus'))
            input.val(num + 1);
        
        $("#<?=$containerId?> form").trigger('refresh');
    });
</script>