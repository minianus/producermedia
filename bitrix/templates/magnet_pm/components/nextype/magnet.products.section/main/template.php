<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(Array('currency'));
$cntSections = 0;
?>
<div class="products-new main-nav">
    <? foreach ($arResult['SECTIONS'] as $arSection)
    {
        $arComponentParamsTmp = array_merge($arParams, Array (
                'SECTION_ID' => $arSection['ID'],
                'SECTION' => Array (
                    'ID' => $arSection['ID'],
                    'NAME' => $arSection['NAME'],
                    'PICTURE' => $arSection[$arParams["SECTION_PICTURE_SOURCE"]],
                    'SECTION_PAGE_URL' => $arSection['SECTION_PAGE_URL']
                ),
                'TITLE_POSITION' => $cntSections % 2 ? 'even' : 'odd',
                'SET_TITLE' => 'N',
                'SET_BROWSER_TITLE' => 'N',
                'SET_META_KEYWORDS' => 'N',
                'SET_META_DESCRIPTION' => 'N',
                'SET_LAST_MODIFIED' => 'N',
        ));
        
        if (is_array($arComponentParamsTmp['CUSTOM_FILTER']))
        {
            if (!isset($GLOBALS[$arComponentParamsTmp['FILTER_NAME']]) || !is_array($GLOBALS[$arComponentParamsTmp['FILTER_NAME']]))
                $GLOBALS[$arComponentParamsTmp['FILTER_NAME']] = Array ();
            
            $GLOBALS[$arComponentParamsTmp['FILTER_NAME']] = array_merge($GLOBALS[$arComponentParamsTmp['FILTER_NAME']], $arComponentParamsTmp['CUSTOM_FILTER']);
        }
        
        $sComponentTemplateTmp = 'slider_products_sections';
        
        include($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/components/catalog_section.php");
                
        $cntSections++;
    }
    ?>

</div>