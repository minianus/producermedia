<div class="popup-header">
    <div class="popup-title">
        <?= GetMessage('HEADER_TITLE') ?>
    </div>
    <a href="#" onclick="$('.popup').jqmHide(); return false;" class="close-popup-icon pull-right icon-custom">
        <i class="zmdi zmdi-close"></i>
    </a>
</div>
<div class="popup-body">
    <div class="text"><?= GetMessage('CONFIRM_MESSAGE', Array("#ORDER_ID#" => $arResult['ORDER']['ACCOUNT_NUMBER'])) ?></div>
    <br/>
    <?if ($arResult["ORDER"]["USER_EMAIL"]):?>
        <div class="text"><?= GetMessage('CONFIRM_SEND_EMAIL', Array("#EMAIL#" => $arResult['ORDER']['USER_EMAIL'])) ?></div>
    <?endif?>
        
    <?if (!empty($arResult["extMessages"])):?>
        <?foreach ($arResult["extMessages"] as $arMessage):?>
            <br/>
            <div class="text"><?=GetMessage($arMessage["CODE"], $arMessage["REPLACE"])?></div>
        <?endforeach?>
    <?endif?>
</div>