<?
$MESS['HEADER_TITLE'] = "Buy in 1 click";
$MESS['SUBMIT_BUTTON'] = "Checkout";
$MESS['CONFIRM_MESSAGE'] = "You have successfully placed an order in 1 click. Order number ##ORDER_ID#";
$MESS["CONFIRM_SEND_EMAIL"] = "Information sended to email #EMAIL#";
$MESS['TEXTAREA_LABEL'] = "Comment";
$MESS['CAPTCHA_LABEL'] = "Enter symbols from captcha";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Robot check fail. Please, try again later";
