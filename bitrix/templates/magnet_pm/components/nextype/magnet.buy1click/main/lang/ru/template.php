<?
$MESS['HEADER_TITLE'] = "Купить в 1 клик";
$MESS['SUBMIT_BUTTON'] = "Оформить заказ";
$MESS['CONFIRM_MESSAGE'] = "Вы успешно оформили заказ в 1 клик. Номер заказа ##ORDER_ID#";
$MESS["CONFIRM_SEND_EMAIL"] = "Информация отправлена на почту #EMAIL#";
$MESS['TEXTAREA_LABEL'] = "Комментарий";
$MESS['CAPTCHA_LABEL'] = "Подтвердите, что вы не робот";
$MESS['RECAPTCHA_SCORE_FAIL'] = "Вы были распознаны как робот. Повторите попытку позже";
$MESS["TPL_NEED_CONFIRM_PHONE_NUMBER"] = "Для входа в <a href='#PERSONAL#' target='_blank'>личный кабинет</a> необходимо подтвердить номер телефона. Для входа используйте логин <i>#PHONE#</i>";
$MESS["TPL_NEED_CONFIRM_EMAIL_ADDRESS"] = "Для входа в <a href='#PERSONAL#' target='_blank'>личный кабинет</a> необходимо подтвердить email-адрес. Для входа используйте логин <i>#EMAIL#</i>";
