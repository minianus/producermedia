<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;
CSolution::getInstance(SITE_ID);
$sID = "buy1click_" . randString(5);

if (!empty($arResult['ORDER']))
{
    include_once __DIR__ . '/confirm.php';
    return;
}
?>
<div class="form popup-dialog">
    <div class="popup-content" id="main_buy1click_form">
        <?
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['is_ajax'] == 'y')
            $APPLICATION->RestartBuffer();
        ?>
        <form method="post" name="<?=$sID?>" id="<?=$sID?>" action="<?=$APPLICATION->GetCurPageParam()?>">
            <?=bitrix_sessid_post()?>
            <div class="popup-header">
                <div class="popup-title">
                    <?=GetMessage('HEADER_TITLE')?>
                </div>
                <a href="#" onclick="$('.popup').jqmHide(); return false;" class="close-popup-icon pull-right icon-custom">
                    <i class="zmdi zmdi-close"></i>
                </a>
            </div>
            
            <div class="popup-body">
                
                <? if (!empty($arResult['ERRORS'])): ?>
                <div class="popup-errors">
                    <? foreach ($arResult['ERRORS'] as $error): ?>
                    <?=$error?><br/>
                    <? endforeach; ?>
                </div>
                <? endif; ?>
                
                
                <? foreach ($arResult['DISPLAY_PROPS'] as $arField): ?>
                <div class="form-group">
                    <div class="control-label">
                        <?=$arField['NAME']?>
                        <? if ($arField['REQUIED'] == 'Y'): ?>
                        <span class="danger-color">*</span>
                        <? endif; ?>
                    </div>
                    <? if ($arField['TYPE'] == 'TEXT'): ?>
                    <input type="text" class="<?=$arField['ERROR']=='Y' ? ' error' : ''?><?=($arField['IS_PHONE'] == "Y" || $arField['CODE'] == "PHONE") ? ' is-phone' : ''?>" value="<?=$arField['VALUE']?>" name="BUY1CLICK[<?=$arField['CODE']?>]" <?=$arField['REQUIED'] == 'Y' ? 'required="required"' : '' ?> />
                    <? endif; ?>
                </div>
                <? endforeach; ?>
                
                <?if (($commentType = $arResult["SHOW_" . $component::COMMENT_FIELD_NAME]) != "N"):?>
                    <div class="form-group">
                        <div class="control-label"><?=GetMessage('TEXTAREA_LABEL')?>
                            <?if ($commentType == "R"):?>
                                <span class="danger-color">*</span>
                            <?endif?>
                        </div>
                        <textarea <?=($commentType == "R") ? "required" : ""?> name="BUY1CLICK[COMMENT]"><?=htmlspecialchars($_REQUEST['BUY1CLICK']['COMMENT'])?></textarea>
                    </div>
                <?endif?>
                
                <?if($arResult["USE_CAPTCHA"]):?>
                    <div class="form-captcha <?=(\Nextype\Magnet\CRecaptcha::isInvisible()) ? 'grecaptcha-invisible' : ''?> <?=($GLOBALS['NT_MAGNET_CRECAPTCHA_CHECK_ERROR'] === "Y") ? ' error' : ''?>">
                        <div class="field">
                            <div class="control-label">
                            <? if (CSolution::$options['RECAPTCHA_ENABLED'] != 'Y' || (CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] == 'checkbox'))
                                    echo GetMessage('CAPTCHA_LABEL') . '<span class="danger-color">*</span>';
                            ?>
                            </div>
                            <div class="image"><img src="/bitrix/tools/captcha.php?captcha_code=<?=$arResult["CAPTCHA_CODE"]?>"></div>
                            <input name="BUY1CLICK[captcha_code]" value="<?=$arResult["CAPTCHA_CODE"]?>" type="hidden">
                            <input id="BUY1CLICK[captcha_word]" name="BUY1CLICK[captcha_word]" type="text">
                            <input name="captcha_code" value="<?=$arResult["CAPTCHA_CODE"]?>" type="hidden">
                            <input id="captcha_word" name="captcha_word" type="hidden">
                        </div>
                    </div>
                    <?if (CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                        <input type="hidden" name="g-recaptcha-response" />
                    <?endif?>
                <?endif?>
                
                <?if (CSolution::$options['FORM_POLICY'] == 'Y')
                {
                    $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                        "PATH" => SITE_DIR . "include/form_policy_checkbox.php",
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "",
                                        "AREA_FILE_RECURSIVE" => "Y",
                                        "EDIT_TEMPLATE" => "standard.php",
                                        "FORM_ID" => $sID,
                                        "CHECKED" => CSolution::$options['FORM_POLICY_CHECKED'],
                                        "DETAIL_PAGE_URL" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PROCESSING_POLICY']),
                                ),
                                false
                        );

                }
                ?>
            </div>
            <div class="popup-footer">
                <button class="btn" type="submit"
                    <?if($arResult["USE_CAPTCHA"] && CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'checkbox'):?>
                        onclick="window.CSolution.onSubmitReCaptcha('<?=CUtil::JSEscape($sID)?>')"
                    <?endif?>
                ><?=GetMessage('SUBMIT_BUTTON')?></button>
            </div>
        </form>
        <?
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['is_ajax'] == 'y'):
            if((!$arResult["USE_CAPTCHA"])
                || ($arResult["USE_CAPTCHA"] && CSolution::$options['RECAPTCHA_ENABLED'] != 'Y')
                || ($arResult["USE_CAPTCHA"] && CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'scoring')):?>
            <script>buy1click_bindSubmit()</script>
            <?endif;
            exit();
        endif?>
    </div>
</div>
<script>
$('#<?=$sID?> .is-phone').mask(window.arOptions['phone_mask']);

<?if((!$arResult["USE_CAPTCHA"])
        || ($arResult["USE_CAPTCHA"] && CSolution::$options['RECAPTCHA_ENABLED'] != 'Y')
        || ($arResult["USE_CAPTCHA"] && CSolution::$options['RECAPTCHA_ENABLED'] == 'Y' && CSolution::$options['RECAPTCHA_TYPE'] != 'scoring')):?>

    function buy1click_bindSubmit() {
        $("#main_buy1click_form form").on('submit', function (event) {
            event.preventDefault();
            $(this).find("[type='submit']").attr('disabled', 'disabled').addClass('loading');
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize() + '&is_ajax=y',
                type: 'post',
                success: function (data) {
                    $("#main_buy1click_form").html(data);
                    $('#main_buy1click_form .is-phone').mask(window.arOptions['phone_mask']);
                }
            });
            return false;
        });
    }
    buy1click_bindSubmit();
<?endif?>
</script>