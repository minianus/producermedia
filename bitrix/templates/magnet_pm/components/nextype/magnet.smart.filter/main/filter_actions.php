<? if ($arParams['FILTER_VIEW_MODE'] == 'TOP'): ?>
<div class="bx_filter_actions">
    <button type="submit" class="btn bx_filter_search_button" type="submit" id="set_filter" name="set_filter"><?= GetMessage("CT_BCSF_SET_FILTER") ?></button>
    <button type="submit" class="btn transparent bx_filter_search_reset" type="submit" id="del_filter" name="del_filter"><?= GetMessage("CT_BCSF_DEL_FILTER") ?></button>
</div>
<? endif; ?>