<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if (!empty($GLOBALS['arPromoItem']['BACKGROUND']['SRC'])): ?>

    <div class="promo-main-banner position-<?= strtolower($GLOBALS['arPromoItem']['TEXTPOSITION']['VALUE_XML_ID']) ?>"
         style="background-image: url('<?= $GLOBALS['arPromoItem']['BACKGROUND']['SRC'] ?>')">
        <div class="promo-main-banner-info">
            <div class="promo-main-banner-img-mobile">
                <img src="<?= $GLOBALS['arPromoItem']['IMAGE']['SRC'] ?>"
                     alt="<?= strip_tags($GLOBALS['arPromoItem']['TITLE']) ?>"
                     title="<?= strip_tags($GLOBALS['arPromoItem']['TITLE']) ?>">
            </div>
            <h1 class="title"><?= $GLOBALS['arPromoItem']['TITLE'] ?></h1>
            <? if (!empty($GLOBALS['arPromoItem']['DESCRIPTION'])): ?>
                <div class="desc"><?= $GLOBALS['arPromoItem']['DESCRIPTION'] ?></div>
            <? endif; ?>
        </div>
        <? if (!empty($GLOBALS['arPromoItem']['IMAGE']['SRC'])): ?>
            <div class="promo-main-banner-img">
                <img src="<?= $GLOBALS['arPromoItem']['IMAGE']['SRC'] ?>"
                     alt="<?= strip_tags($GLOBALS['arPromoItem']['TITLE']) ?>"
                     title="<?= strip_tags($GLOBALS['arPromoItem']['TITLE']) ?>">
            </div>
        <? endif; ?>
    </div>
<? endif; ?>