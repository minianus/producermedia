<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="promo-text">
		<div class="content">
                    <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
			<h2 class="subtitle"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
                    <? endif; ?>
                    <? if (!empty($GLOBALS['arPromoItem']['DESCRIPTION'])): ?>
			<div class="main-text">
                            <?=$GLOBALS['arPromoItem']['DESCRIPTION']?>
			</div>
                    <? endif; ?>
                    <? if (!empty($GLOBALS['arPromoItem']['VIDEO'])): ?>
			<div class="video-section">
                            <? foreach ($GLOBALS['arPromoItem']['VIDEO'] as $arVideo): ?>
				<div class="item lazy-video" data-loader="videoLoader" data-video='<?=$arVideo['VALUE']?>'></div>
                            <? endforeach; ?>
			</div>
                    <? endif; ?>
		</div>
                <? if (!empty($GLOBALS['arPromoItem']['FILESTITLE'])): ?>
                <?
                if (!empty($GLOBALS['arPromoItem']['FILES']))
                {
                    foreach ($GLOBALS['arPromoItem']['FILES'] as $arDoc)
                    {
                        if (!empty($arDoc['VALUE']))
                        {
                            if ($arFile = CFile::GetFileArray($arDoc['VALUE']))
                            {
                                $fileinfo = explode(".", $arFile['ORIGINAL_NAME']);

                                $arFile['EXTENSION'] = trim(strtolower(end($fileinfo)));
                                $arFile['NAME'] = str_replace("." . end($fileinfo), "", $arFile['ORIGINAL_NAME']);
                                $arFile['PATH'] = CFile::GetPath($arFile['ID']);

                                $arFile['FILE_SIZE'] = $arFile['FILE_SIZE'] / 1000;

                                if ($arFile['FILE_SIZE'] < 1024)
                                    $arFile['FILE_SIZE'] = round($arFile['FILE_SIZE'], 0) . " " . GetMessage('BLOCK_TEXT_FILE_SIZE_KB');
                                else
                                    $arFile['FILE_SIZE'] = round($arFile['FILE_SIZE'] / 1024, 2) . " " . GetMessage('BLOCK_TEXT_FILE_SIZE_MB');

                                $arFiles[] = $arFile;
                            }
                        }
                    }
                }
                ?>
                <? if (!empty($arFiles)): ?>
		<div class="promo-docs">
			<h3 class="subtitle"><?=$GLOBALS['arPromoItem']['FILESTITLE']?></h3>
                        <? if (!empty($arFiles)): ?>
			<div class="documents">
				<div class="items">
                                    <? foreach ($arFiles as $arFile): ?>
					<div class="item <?=$arFile['EXTENSION']?>">
						<a href="<?=$arFile['PATH']?>" target="_blank" class="file-name" rel="nofollow"><?=$arFile['NAME']?></a>
						<div class="size"><?=$arFile['FILE_SIZE']?></div>
					</div>
                                    <? endforeach; ?>
				</div>
			</div>
                        <? endif; ?>
		</div>
            <? endif; ?>
            <? endif; ?>
</div>
<script>
$(document).ready(function() {
            $('.lazy-video').lazy({
    	    customLoaderName: function(element) {
    	        element.load();
    	    },
    	    videoLoader: function(element) {
    	        element.html(element.data("video"));
    	    },
    	    asyncLoader: function(element) {
    	        setTimeout(function() {
    	            element.load()
    	        }, 5000);
    	    },
    	    errorLoader: function(element) {
    	        element.error();
    	    }
            });
	});
</script>