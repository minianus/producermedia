<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($GLOBALS['arPromoItem']['ITEMS']))
{
    $arReviewsIDs = Array ();
    foreach ($GLOBALS['arPromoItem']['ITEMS'] as $arReview)
        if (!empty($arReview['VALUE']))
            $arReviewsIDs[] = $arReview['VALUE'];
    
    if (!empty($arReviewsIDs))
    {
        $arReviews = \Nextype\Magnet\CCache::CIBlockElement_GetList(Array (), Array ('ID' => $arReviewsIDs), true);
        if (!empty($arReviews))
        {
            foreach ($arReviews as $key => $arItem)
            {
                if (!empty($arItem['PROPERTIES']['PRODUCT']['VALUE']))
                {
                    $arReviews[$key]['PROPERTIES']['PRODUCT'] = \Nextype\Magnet\CCache::CIBlockElement_GetByID($arItem['PROPERTIES']['PRODUCT']['VALUE']);
                }
            }
        }
    }
    $carouselID = "carousel_" . randString(6);
}
?>
<? if (!empty($arReviews)): ?>
<section class="promo-reviews">
    <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
    <h2 class="title"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
    <? endif; ?>
    <div class="items swiper-container" id="<?=$carouselID?>">
        <div class="swiper-wrapper">
            <? foreach ($arReviews as $arItem): ?>
            <div class="item swiper-slide">
                <div class="review-info">
                    <div class="author-img-container">
                        <div class="author-img">
                            <?
                            if (!empty($arItem['PREVIEW_PICTURE']))
                            {
                                $preview = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_EXACT);
                                $preview['SRC'] = $preview['src'];
                            }
                            else
                            {
                                $preview = \Nextype\Magnet\CSolution::getEmptyImage(true);
                            }
                            
                            ?>
                            <img src="<?=$preview['SRC']?>" alt="<?=$arItem['NAME']?>">
                        </div>
                    </div>
                    <div class="author-info">
                        <div class="name"><?=$arItem['NAME']?></div>
                        <? if (!empty($arItem['PROPERTIES']['PRODUCT'])): ?>
                        <div class="review-product"><?=GetMessage('BLOCK_REVIEWS_LABEL_FROM_PRODUCT')?> <a href="<?=$arItem['PROPERTIES']['PRODUCT']['DETAIL_PAGE_URL']?>" target="_blank"><?=$arItem['PROPERTIES']['PRODUCT']['NAME']?></a></div>
                        <? endif; ?>
                    </div>
                </div>
                <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                <div class="review-text">
                    <?=$arItem['PREVIEW_TEXT']?>
                </div>
                <? endif; ?>
            </div>
            <? endforeach; ?>
        </div>
        <div class="swiper-button-prev icon-custom"></div>
		<div class="swiper-button-next icon-custom"></div>
		<div class="swiper-pagination"></div>
    </div>
</section>
    <script>
        let reviewsSwiper = new Swiper('.promo-reviews #<?=$carouselID?>', {
            loop: true,
            simulateTouch: false,
            spaceBetween: 24,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            breakpoints : {
                0:{
                    slidesPerView: 1,
                },
                920:{
                    slidesPerView: 2,
                }
            }
        });
    </script>
<? endif; ?>
