<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="promo-banner" <? if (!empty($GLOBALS['arPromoItem']['BACKGROUND'])): ?>style="background-image: url('<?=$GLOBALS['arPromoItem']['BACKGROUND']['SRC']?>')"<?endif;?>>
    <div class="promo-banner-info">
        <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
        <h2 class="title"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
        <? endif; ?>
        <? if (!empty($GLOBALS['arPromoItem']['DESCRIPTION'])): ?>
        <div class="desc"><?=$GLOBALS['arPromoItem']['DESCRIPTION']?></div>
        <? endif; ?>
        <? if (!empty($GLOBALS['arPromoItem']['BTNTEXT'])):
            $url = (!empty($GLOBALS['arPromoItem']['BTNFORMID'])) ? 'javascript:void(0)' : $GLOBALS['arPromoItem']['BTNLINK'];
            $btnId = "consult_btn_" . randString(6);
        ?>
        <a href="<?=$url?>" id="<?=$btnId?>" class="btn"><?=$GLOBALS['arPromoItem']['BTNTEXT']?></a>
        <? if (!empty($GLOBALS['arPromoItem']['BTNFORMID'])): ?>
        <script>
        $("body").on('click', '#<?=$btnId?>', function () {
            jqmPopup('promoConsultPopup', 'include/ajax/form.php?id=<?=$GLOBALS['arPromoItem']['BTNFORMID']?>', true);
        });
        </script>
        <? endif; ?>
        <? endif; ?>
    </div>
</div>