<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($GLOBALS['arPromoItem']['ITEMS']))
{
    $arTizersIDs = Array ();
    foreach ($GLOBALS['arPromoItem']['ITEMS'] as $arTizer)
        if (!empty($arTizer['VALUE']))
            $arTizersIDs[] = $arTizer['VALUE'];
    
    
    if (!empty($arTizersIDs))
        $arTizers = \Nextype\Magnet\CCache::CIBlockElement_GetList(Array (), Array ('ID' => $arTizersIDs));
    
}
?>
<? if (!empty($arTizers)): ?>
<div class="advantages">
    <? foreach ($arTizers as $arTizer):
        $arTizer['PREVIEW_PICTURE'] = CFile::GetFileArray($arTizer['PREVIEW_PICTURE']);
    ?>
		<div class="item">
                    <? if (!empty($arTizer['PREVIEW_PICTURE']['SRC'])): ?>
			<span class="icon" style="background-image: url('<?=$arTizer['PREVIEW_PICTURE']['SRC']?>');"></span>
                    <? endif; ?>
			<span><?=$arTizer['NAME']?></span>
		</div>
    <? endforeach; ?>
</div>
<? endif; ?>