<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($GLOBALS['arPromoItem']['ITEMS']))
{
    $arFaqIDs = Array ();
    foreach ($GLOBALS['arPromoItem']['ITEMS'] as $arFaq)
        if (!empty($arFaq['VALUE']))
            $arFaqIDs[] = $arFaq['VALUE'];
    
    $arNewFaqs = Array ();
    if (!empty($arFaqIDs))
        $arFaqs = \Nextype\Magnet\CCache::CIBlockElement_GetList(Array (), Array ('ID' => $arFaqIDs), true);
    
    if (!empty($arFaqs))
    {
        for($i = 0; $i < count($arFaqs); $i++)
        {
            $arNewFaqs[$i%2][] = $arFaqs[$i];
        }
        
        $arFaqs = $arNewFaqs;
    }
    
}
?>
<? if (!empty($arFaqs)): ?>
    <section class="promo-faq">
        <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
        <h2 class="title"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
        <? endif; ?>
        <? if (!empty($GLOBALS['arPromoItem']['DESCRIPTION'])): ?>
        <div class="desc"><?=$GLOBALS['arPromoItem']['DESCRIPTION']?></div>
        <? endif; ?>
        
        
        <div class="columns">
            <? foreach ($arFaqs as $col => $arItems): ?>
            <div class="column">
                <? foreach ($arItems as $arItem): ?>
                <div class="accordeon">
                    <a href="javascript:void(0)" class="name"><?=$arItem['NAME']?></a>
                    <div class="text"><?=$arItem['PREVIEW_TEXT']?></div>
                </div>
                <? endforeach; ?>
            </div>
            <? endforeach; ?>
        </div>
    </section>
<? endif; ?>
