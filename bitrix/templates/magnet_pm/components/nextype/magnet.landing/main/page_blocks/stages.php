<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($GLOBALS['arPromoItem']['ITEMS']))
{
    $arReviewsIDs = Array ();
    foreach ($GLOBALS['arPromoItem']['ITEMS'] as $arStage)
        if (!empty($arStage['VALUE']))
            $arStagesIDs[] = $arStage['VALUE'];
    
    if (!empty($arStagesIDs))
        $arStages = \Nextype\Magnet\CCache::CIBlockElement_GetList(Array (), Array ('ID' => $arStagesIDs), true);
}
?>
<? if (!empty($arStages)): ?>
<section class="promo-steps">
    <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
    <h2 class="title"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
    <? endif; ?>
    
    <div class="promo-steps-content">
        <ol class="items">
            <? foreach ($arStages as $arItem):
                
                    if (!empty($arItem['PREVIEW_PICTURE']))
                    {
                        $preview = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT);
                        $preview['SRC'] = $preview['src'];
                    }
                    else
                    {
                        $preview = \Nextype\Magnet\CSolution::getEmptyImage(true);
                    }
                    ?>
            <li class="item">
                <div class="icon" style="background-image: url('<?=$preview['SRC']?>')"></div>
                <div class="info">
                    <div class="name"><?=$arItem['NAME']?></div>
                    <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                    <div class="desc"><?=$arItem['PREVIEW_TEXT']?></div>
                    <? endif; ?>
                </div>
            </li>
            <? endforeach; ?>
        </ol>
    </div>
</section>
<? endif; ?>