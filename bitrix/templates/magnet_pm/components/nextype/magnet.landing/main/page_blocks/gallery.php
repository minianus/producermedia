<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($GLOBALS['arPromoItem']['ITEMS']))
{
    $arGalleryIDs = Array ();
    foreach ($GLOBALS['arPromoItem']['ITEMS'] as $arTizer)
        if (!empty($arTizer['VALUE']))
            $arGalleryIDs[] = $arTizer['VALUE'];
    
    if (!empty($arGalleryIDs))
        $arGallery = \Nextype\Magnet\CCache::CIBlockElement_GetList(Array (), Array ('ID' => $arGalleryIDs));
    
    $carouselID = "carousel_" . randString(6);
}
?>
<? if (!empty($arGallery)): ?>
<section class="gallery promo-gallery">
    <? if (!empty($GLOBALS['arPromoItem']['TITLE'])): ?>
    <h2 class="title"><?=$GLOBALS['arPromoItem']['TITLE']?></h2>
    <? endif; ?>
    
	<div class="items swiper-container" id="<?=$carouselID?>">
		<div class="swiper-wrapper">
			<? foreach ($arGallery as $arItem):
				if (empty($arItem['PREVIEW_PICTURE']))
					continue;
				
				$preview = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array ('width' => 200, 'height' => 200), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
			?>
			<div class="item swiper-slide">
				<a href="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>" data-lightbox="<?=$GLOBALS['arPromoItem']['CODE']?>" data-title="<?=$arItem['NAME']?>" class="img">
					<img src="<?=$preview['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>">
				</a>
				<div class="name"><?=$arItem['NAME']?></div>
				<? if (!empty($arItem['PREVIEW_TEXT'])): ?>
				<div class="description"><?=$arItem['PREVIEW_TEXT']?></div>
				<? endif; ?>
			</div>
			<? endforeach; ?>
		</div>
		<div class="swiper-button-prev icon-custom"></div>
		<div class="swiper-button-next icon-custom"></div>
		<div class="swiper-pagination"></div>
    </div>
</section>
<script>
	let gallerySwiper = new Swiper('.promo-gallery #<?=$carouselID?>', {
		loop: true,
		simulateTouch: false,
		spaceBetween: 24,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
		},
		breakpoints : {
			0:{
				slidesPerView:1,
				dots: true,
				nav: false,
			},
			500:{
				slidesPerView:2,
				dots: true,
				nav: false,
			},
			920: {
				slidesPerView: 2,
				dots: false,
				nav: true,
			},
			1140:{
				slidesPerView:3,
			},
			1450:{
				slidesPerView: 4,
			}
		}
	});
</script>
<? endif; ?>