<?
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
use Nextype\Magnet\CCache;


if (is_array($GLOBALS['arPromoItem']['ITEMS']['CONDITIONS']))
{
        $arFilterItemsIDS = $arFilterSectionsIDS = Array ();
        $obSolution = new CSolution(SITE_ID);
        $arParams['USE_FILTER'] = "N";
        $arParams['IBLOCK_ID'] = CSolution::$options['CATALOG_IBLOCKID'];
        $arParams['FILTER_NAME'] = 'arrFilter' . $GLOBALS['arPromoItem']['ID'];

        foreach($GLOBALS['arPromoItem']['ITEMS']['CONDITIONS']['CHILDREN'] as $key=>$prop)
        {
            $arPropData = explode(':', $prop['CLASS_ID']);
            $rsProperty = CIBlock::GetProperties($arPropData[1], Array(), Array("ID"=>$arPropData[2]));
            if($arProp = $rsProperty->Fetch())
            {
                $GLOBALS['arPromoItem']['ITEMS']['CONDITIONS']['CHILDREN'][$key]['TYPE'] = $arProp['PROPERTY_TYPE'];
            }
        }

        $GLOBALS[$arParams['FILTER_NAME']] = $obSolution->getFilterFromConditions($GLOBALS['arPromoItem']['ITEMS']['CONDITIONS']);
        CLocations::setFilter($arParams);
        
        if ($GLOBALS['arPromoItem']['FILTER']['VALUE_XML_ID'] == "Y")
        {
            $arParams['USE_FILTER'] = 'Y';

            $arSearchElements = CCache::CIBlockElement_GetList(Array (), array_merge($GLOBALS[$arParams['FILTER_NAME']], Array (
                "IBLOCK_ID" => CSolution::$options['CATALOG_IBLOCKID']
            )));
            
            foreach ($arSearchElements as $arItem)
            {
                $arFilterItemsIDS[] = $arItem['ID'];
                if (!in_array($arItem['IBLOCK_SECTION_ID'], $arFilterSectionsIDS))
                    $arFilterSectionsIDS[] = $arItem['IBLOCK_SECTION_ID'];
            }
            
            $GLOBALS[$arParams['FILTER_NAME']]['=ID'] = $arFilterItemsIDS;
            
           
        }
        
        
        $arParams['DISPLAY_LIST_SORT'] = !is_array($arParams['DISPLAY_LIST_SORT']) ? Array () : $arParams['DISPLAY_LIST_SORT'];
        $arBasePriceType = Array ();
        if (\Bitrix\Main\Loader::includeModule("sale") && \Bitrix\Main\Loader::includeModule("catalog") && in_array("PRICE", $arParams['DISPLAY_LIST_SORT']))
        {
            $arBasePriceType = CCatalogGroup::GetList(Array(), Array("BASE" => "Y"))->fetch();
        }

        $arSortValues = Array (
            'POPULAR' => Array (
                'FIELD' => 'shows',
                'LABEL' => GetMessage('CATALOG_SORT_POPULAR')
            ),
            'NAME' => Array (
                'FIELD' => 'name',
                'LABEL' => GetMessage('CATALOG_SORT_NAME')
            ),
            'PRICE' => Array (
                'FIELD' => 'catalog_PRICE_' . $arBasePriceType['ID'],
                'LABEL' => GetMessage('CATALOG_SORT_PRICE')
            )
        );
        $arCurrentSorts = Array ();
        if (is_array($arParams['DISPLAY_LIST_SORT']))
        {

            foreach ($arParams['DISPLAY_LIST_SORT'] as $key)
            {
                if (isset($arSortValues[$key]))
                {
                    $arSort = array_merge($arSortValues[$key], Array (
                        'SORT_ORDER' => 'ASC',
                        'INVERT_SORT_ORDER' => 'DESC'
                    ));


                    if (!empty($_REQUEST['sort_by']) && $_REQUEST['sort_by'] == strtolower($key))
                    {
                        $arSort['ACTIVE'] = true;
                        if ($_REQUEST['sort_order'] == "desc")
                        {
                            $arSort['SORT_ORDER'] = 'DESC';
                            $arSort['INVERT_SORT_ORDER'] = 'ASC';
                        }

                        $arParams["ELEMENT_SORT_FIELD"] = $arSort['FIELD'];
                        $arParams["ELEMENT_SORT_ORDER"] = $arSort['SORT_ORDER'];
                    }

                    $arCurrentSorts[$key] = $arSort;
                }
            }
        }

        ?>
        <main class="catalog">
            <div class="sort">
                <? if($arParams['USE_FILTER'] == 'Y'): ?>
                <a href="javascript:void(0);" class="filter-btn <?=(strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER']) != 'top' ? 'desktop-hide' : '')?> <?=(isset($arParams['FILTER_HIDE_ON_MOBILE']) && $arParams['FILTER_HIDE_ON_MOBILE'] === 'Y' ? 'mobile-hidden' : '')?>"><?=GetMessage('FILTER_BUTTON_NAME')?></a>
                <? endif; ?>
                <div class="sort-filter">
                    <? if (!empty($arCurrentSorts)): ?>
                        <? foreach ($arCurrentSorts as $key => $arSort): ?>
                        <a href="<?=$APPLICATION->GetCurPageParam('sort_by='. strtolower($key).'&sort_order=' . strtolower($arSort['INVERT_SORT_ORDER']), Array ('sort_by', 'sort_order'))?>#products" rel="nofollow" class="item<?=$arSort['ACTIVE']?' active':''?><?=$arSort['SORT_ORDER'] == 'ASC' ? ' asc' :' desc'?>"><?=$arSort['LABEL']?></a>
                        <? endforeach; ?>
                    <? endif; ?>

                </div>
                
            </div>
            
            <div class="products-container<?=($arParams['USE_FILTER'] == 'Y') ? ' has-filter' : ''?> <?= strtolower(CSolution::$options['CATALOG_VIEW_MODE_FILTER'])?>" id="products">
                <? if ($arParams['USE_FILTER'] == 'Y'): ?>
                    <div class="filter">
                        
                            <?
                            
                            $APPLICATION->IncludeComponent(
                                    "nextype:magnet.smart.filter", "main", array(
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "FILTER_VIEW_MODE" => CSolution::$options['CATALOG_VIEW_MODE_FILTER'],
                                "ITEM_IDS" => $arFilterItemsIDS,
                                "SECTION_IDS" => $arFilterSectionsIDS,
                                "FILTER_NAME" => $arParams["FILTER_NAME"],
                                "PRICE_CODE" => $arParams["~PRICE_CODE"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "SAVE_IN_SESSION" => "N",
                                "XML_EXPORT" => "N",
                                "SECTION_TITLE" => "NAME",
                                "SECTION_DESCRIPTION" => "DESCRIPTION",
                                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                "SEF_MODE" => "N",
                                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                                    ), $component, array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                    </div>
                <? endif; ?>
                <div class="products">
        <?

        $APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"PATH" => SITE_DIR . "include/landing_catalog_main.php",
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => "standard.php",
                                "FILTER_NAME" => $arParams['FILTER_NAME'],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"STORES" => $arParams["STORES"],
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"ELEMENT_COUNT" => !empty($GLOBALS['arPromoItem']['COUNT']) ? intval($GLOBALS['arPromoItem']['COUNT']) : $arParams["LINK_PRODUCTS_ELEMENT_COUNT"],
                                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                                "VIEW_MODE" => "CARD"
			),
			false
		);
        ?>
                </div>
            </div>
        </main>
            <?
}