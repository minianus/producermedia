<?php

$arResult['DISPLAY_BLOCKS'] = Array ();

if (!empty($arResult['BLOCKS']))
{
    foreach ($arResult['BLOCKS'] as $code => $arBlocks)
    {
        if (is_array($arBlocks) && file_exists(__DIR__ . "/page_blocks/" . strtolower($code) . ".php"))
        {
            foreach ($arBlocks as $key => $arBlock)
            {
                if (isset($arBlock['ACTIVE']) && empty($arBlock['ACTIVE']['VALUE']))
                    continue;
                
                $arBlock['CODE'] = $code;
                $arBlock['ID'] = $code . "_" . $key;
                $arBlock['SORT'] = isset($arBlock['SORT']) ? intval($arBlock['SORT']) : 0;
                
                $blockCode = $code . "_" . $key;
                $arResult['DISPLAY_BLOCKS'][$blockCode] = $arBlock;
                $arResult['DISPLAY_BLOCKS'][$blockCode]['INCLUDE_FILE'] = __DIR__ . "/page_blocks/" . strtolower($code) . ".php";
            }
        }
    }
}

if (!empty($arResult['DISPLAY_BLOCKS']))
{
    usort($arResult['DISPLAY_BLOCKS'], function ($a, $b) {
        if ($a['SORT'] == $b['SORT']) {
            return 0;
        }
        return ($a['SORT'] < $b['SORT']) ? -1 : 1;
    });
}
