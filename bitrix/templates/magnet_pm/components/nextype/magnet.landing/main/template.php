<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Magnet\CSolution;

$arMetaProperties = $arResult['SEO_DATA'];

CSolution::setMetaStrings(Array (
    "og:type" => "website",
    "og:title" => !empty($arMetaProperties["ELEMENT_META_TITLE"]) ? $arMetaProperties["ELEMENT_META_TITLE"] : $arResult['DISPLAY_BLOCKS'][0]['TITLE'],
    "og:url" => CSolution::getCurrentHost() . $arResult['PROPERTIES']['URL']['VALUE'],
    "og:image" => !empty($arResult['DISPLAY_BLOCKS'][0]["IMAGE"]['SRC']) ? CSolution::getCurrentHost() . $arResult['DISPLAY_BLOCKS'][0]["IMAGE"]['SRC'] : CSolution::getCurrentHost() . \Nextype\Magnet\CSolution::getSiteLogo()['SRC'],
    "og:description" => !empty($arMetaProperties["ELEMENT_META_DESCRIPTION"]) ? $arMetaProperties["ELEMENT_META_DESCRIPTION"] : $arResult['DISPLAY_BLOCKS'][0]['DESCRIPTION']
));
?>
<div class="promo-page" data-helper="landings">
        <? if (!empty($arResult['DISPLAY_BLOCKS']))
        {
            foreach ($arResult['DISPLAY_BLOCKS'] as $arItem)
            {
                $GLOBALS['arPromoItem'] = $arItem;
                include($arItem['INCLUDE_FILE']);
                unset($GLOBALS['arPromoItem']);
            }
        }
        ?>
</div>
