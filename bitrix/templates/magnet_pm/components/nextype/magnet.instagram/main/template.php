<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Nextype\Magnet\CSolution;

CSolution::getInstance(SITE_ID);
$ajaxContainer = "instagram_" . randString(5);
?>

<section class="instagram" data-helper="homepage::instagram">
	<h2 class="title"><?=GetMessage('INSTAGRAM_TITLE')?></h2>
        <? if (!empty(CSolution::$options['INSTAGRAM_LINK_MORE'])): ?>
	<a target="_blank" href="<?=CSolution::$options['INSTAGRAM_LINK_MORE']?>" class="more"><?=GetMessage('INSTAGRAM_TEXT_MORE')?></a>
        <? endif; ?>
        
        <div class="instagram-wrapper" id="<?=$ajaxContainer?>">
        <? if (!empty($arResult['ERRORS'])): ?>
            <p><?=$arResult['ERRORS']?></p>
        <? else: ?>
            <? if ($arParams['LAZY_LOAD'] == 'Y'): ?>
            <div class="loader">
                <svg width="50px" height="50px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-dual-ring" style="background: none;"><circle cx="50" cy="50" fill="none" stroke-linecap="round" r="40" stroke-width="4" stroke="#333" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(161.792 50 50)"></circle></svg>
            </div>
            <? endif; ?>
        <? if ($arParams['LAZY_LOAD'] == 'Y' && is_array($arResult['ITEMS'])) $APPLICATION->RestartBuffer(); ?>
        <? if (!empty($arResult['ITEMS'])): ?>
	<div class="items swiper-container">
		<div class="swiper-wrapper">

		
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
			<div class="item swiper-slide <?=($arItem["TYPE"] == "VIDEO") ? 'video' : ''?>">
				<a href="<?=$arItem['DETAIL_URL']?>" target="_blank" class="link">
                    <?if ($arItem["TYPE"] == "VIDEO"):?>
                        <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0)">
                                <path class="shadow" d="M7 9.92962C7 5.4014 11.7971 2.48244 15.8188 4.56357L44.5598 19.4366C48.9269 21.6964 48.9096 27.9482 44.5301 30.1839L15.7891 44.8559C11.769 46.9081 7 43.9882 7 39.4746V9.92962Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="50" height="50" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    <?endif?>
				<img data-lazy="default" data-src="<?=($arItem["TYPE"] == "VIDEO") ? $arItem['PREVIEW_PICTURE'] : $arItem['DETAIL_PICTURE']?>" src="<?= SITE_TEMPLATE_PATH?>/img/instagram-placeholder.svg" alt="">
                                <? if (!empty($arItem['TEXT']) && $arParams['SHOW_TEXT'] == 'Y'): ?>
                                <span class="desc">
                                    <span>
                                    <?=$arItem['TEXT']?>
                                    </span>
                                </span>
                                <? endif; ?>
				</a>
			</div>
			<? endforeach; ?>
		</div>
		
	</div>
	<div class="swiper-button-prev swiper-button-instagram icon-custom"></div>
	<div class="swiper-button-next swiper-button-instagram icon-custom"></div>
        
        <? endif; ?>
            <? endif; ?>
        <? if ($arParams['LAZY_LOAD'] == 'Y' && is_array($arResult['ITEMS'])) exit(); ?>
        </div>
</section>
<?
if ($arParams['LAZY_LOAD'] == "Y" && !isset($arResult['ERRORS']))
{
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedTemplate = $signer->sign($templateName, 'magnet.instagram');
    $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'magnet.instagram');
    ?>
    <script>
        $(document).ready(function() {
            $.ajax({
                method: 'post',
                url: '<?= CUtil::JSEscape($componentPath) ?>/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
                data: {
                    get_result: 'y',
                    siteId: '<?= $component->getSiteId() ?>',
                    template: '<?= CUtil::JSEscape($signedTemplate) ?>',
                    parameters: '<?= CUtil::JSEscape($signedParams) ?>',
                },
                success: function (result) {
					$("#<?=$ajaxContainer?>").html(result);
					var instagramSwiper = new Swiper('#<?=$ajaxContainer?> .swiper-container', {
						loop: true,
						spaceBetween: 0,
						navigation: {
							nextEl: '.swiper-button-next.swiper-button-instagram',
							prevEl: '.swiper-button-prev.swiper-button-instagram',
						},
						on: {
							init: function(event){
								$(event.target).find('[data-lazy="default"]').lazy();
							},
							slideChange: function(event) {
								var lazyImg = $(event.target).find('[data-lazy="default"]');
								if (lazyImg.length > 0) {
									lazyImg.each(function () {
										$(this).attr('src', $(this).data('src')).removeAttr('data-src data-lazy');
									});
								}
							},
						},
						breakpoints: {
							0:{
								slidesPerView:1,
								spaveBetween:0
							},
							420:{
								slidesPerView:2
							},
							590:{
								slidesPerView:3
							},
							840:{
								slidesPerView: 4
							},
							1120:{
								slidesPerView: 5
							},
							1370:{
								slidesPerView: 6
							},
							1580:{
								slidesPerView:7
							},
							1770:{
								slidesPerView: 8
							}
						}
					});
                    $('#<?=$ajaxContainer?> .items .item').height($('#<?=$ajaxContainer?> .items .item').width());
                }
            });
        });
    </script>
    <?
}
elseif (!isset($arResult['ERRORS']))
{
?>
    <script>
    var instagramSwiper = new Swiper('#<?=$ajaxContainer?> .swiper-container', {
			loop: true,
			spaceBetween: 0,
			navigation: {
				nextEl: '.swiper-button-next.swiper-button-instagram',
				prevEl: '.swiper-button-prev.swiper-button-instagram',
			},
			on: {
				init: function(event){
					$('#<?=$ajaxContainer?> .swiper-container').find('[data-lazy="default"]').lazy();
				},
				slideChange: function(event) {
					var lazyImg = $('#<?=$ajaxContainer?> .swiper-container').find('[data-lazy="default"]');
					if (lazyImg.length > 0) {
						lazyImg.each(function () {
							$(this).attr('src', $(this).data('src')).removeAttr('data-src data-lazy');
						});
					}
				},
			},
			breakpoints: {
				0:{
					slidesPerView:1,
					spaveBetween:0
				},
				420:{
					slidesPerView:2
				},
				590:{
					slidesPerView:3
				},
				840:{
					slidesPerView: 4
				},
				1120:{
					slidesPerView: 5
				},
				1370:{
					slidesPerView: 6
				},
				1580:{
					slidesPerView:7
				},
				1770:{
					slidesPerView: 8
				}
			}
		});
                $('#<?=$ajaxContainer?> .items .item').height($('#<?=$ajaxContainer?> .items .item').width());
    </script>
<? } ?>
