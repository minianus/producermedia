<?
use Bitrix\Main\Grid\Declension;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$reviewsDeclension = new Declension(GetMessage('CP_REVIEWS_DECLENSION_1'), GetMessage('CP_REVIEWS_DECLENSION_4'), GetMessage('CP_REVIEWS_DECLENSION_5'));
?>

<div class="component-reviews<?=empty($arResult['VALUES']) ? ' open' : ''?>">
    
    <? if (!empty($arResult['ITEMS'])): ?>
    <div class="top-line">
        <div class="total-values">
            <div class="total-value"><?=$arResult['TOTAL']['AVG_RATING']?></div>
            <div class="stars big">
                <? for ($i = 1; $i <= 5; $i++): ?>
                <i class="st<?=$arResult['TOTAL']['AVG_RATING'] >= $i ? ' f' : ''?><?=$arResult['TOTAL']['AVG_RATING'] < $i && $arResult['TOTAL']['AVG_RATING'] > $i - 1 ? ' h' : ''?>"></i>
                <? endfor; ?>
            </div>
            <div class="total-label">
            <?=GetMessage('CP_REVIEWS_TOTAL_RATING_LABEL', Array ("#COUNT#" => $arResult['TOTAL']['ITEMS'], "#DECLENSION#" => $reviewsDeclension->get($arResult['TOTAL']['ITEMS'])))?>
            </div>
            <a href="javascript:void(0)" class="btn btn-primary add-review"><?=GetMessage('CP_REVIEWS_ADD_REVIEW')?></a>
        </div>
        <div class="transcript">
            <? for ($i = 5; $i >= 1; $i--): ?>
            <div class="line">
                <div class="rating">
                    <i class="st"></i>
                    <?=$i?>
                </div>
                <div class="progress"><span style="width: <?=$arResult['TOTAL']['AVG_BY_STARS'][$i]['PERCENT']?>%"></span></div>
                <div class="link">
                    <? if (!empty($arResult['TOTAL']['AVG_BY_STARS'][$i]['COUNT'])): ?>
                        <? if ($arResult['TOTAL']['ITEMS'] > 1): ?>
                        <a href="javascript:void(0)" data-rating-show="<?=$i?>">
                        <?=GetMessage('CP_REVIEWS_COUNT_REVIEWS', Array ('#COUNT#' => $arResult['TOTAL']['AVG_BY_STARS'][$i]['COUNT'], "#DECLENSION#" => $reviewsDeclension->get($arResult['TOTAL']['AVG_BY_STARS'][$i]['COUNT'])))?>
                        </a>
                        <? else: ?>
                        <span><?=GetMessage('CP_REVIEWS_COUNT_REVIEWS', Array ('#COUNT#' => $arResult['TOTAL']['AVG_BY_STARS'][$i]['COUNT'], "#DECLENSION#" => $reviewsDeclension->get($arResult['TOTAL']['AVG_BY_STARS'][$i]['COUNT'])))?></span>
                        <? endif; ?>
                    <? else: ?>
                    <span><?=GetMessage('CP_REVIEWS_ZERO_REVIEWS')?></span>
                    <? endif; ?>
                </div>
                
            </div>
            <? endfor; ?>
        </div>
    </div>
    
    <div class="items">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <div class="item<?=$arItem['HAS_VOTE'] == "Y" ? ' has-vote' : ''?>" data-review-id="<?=intval($arItem['ID'])?>" data-rating="<?=intval($arItem['UF_RATING'])?>">
            <? if ($USER->IsAdmin() && !$arItem['UF_IS_MODERATED']): ?>
            <div class="need-moderate">
                <span>
                    <?=GetMessage('CP_REVIEWS_NEED_MODERATED', Array ('#LINK#' => '/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=' . $arResult['TABLE_ID'] . '&ID=' . $arItem['ID']))?>
                </span>
            </div>
            <? endif; ?>
            <div class="vote">
                <a href="javascript:void(0)" data-role="vote" data-value="1" class="up<?=$arItem['HAS_VOTE'] == "Y" && $arItem['VOTE'] > 0 ? ' active' : ''?>"><?=$arItem['UF_LIKE']?></a>
                <a href="javascript:void(0)" data-role="vote" data-value="-1" class="down<?=$arItem['HAS_VOTE'] == "Y" && $arItem['VOTE'] < 0 ? ' active' : ''?>"><?=$arItem['UF_DISLIKE']?></a>
            </div>
            <? if (!empty($arItem['UF_USER_NAME'])): ?>
            <div class="name font-big-bold"><?=$arItem['UF_USER_NAME']?></div>
            <? endif; ?>
            <div class="short-info">
                <div class="row">
                    <div class="col-auto stars">
                        <? for ($i = 1; $i <= 5; $i++): ?>
                        <i class="st<?=intval($arItem['UF_RATING']) >= $i ? ' f' : ''?><?=intval($arItem['UF_RATING']) < $i && intval($arItem['UF_RATING']) > $i - 1 ? ' h' : ''?>"></i>
                        <? endfor; ?>
                    </div>
                    <? if (!empty($arItem['UF_CREATED'])): ?>
                    <div class="col-auto created font-small">
                        <?=$arItem['UF_CREATED']?>
                    </div>
                    <? endif; ?>
                </div>
                <div class="row">
                    <? if (!empty($arItem['UF_USE_EXPERIENCE'])): ?>
                    <div class="col-auto font-small"><?=GetMessage('CP_REVIEWS_USE_EXPERIENCE', Array ("#VALUE#" => $arItem['UF_USE_EXPERIENCE']))?></div>
                    <? endif; ?>
                    <? if (!empty($arItem['UF_RECOMMEND'])): ?>
                    <div class="col-auto font-small">
                        <?=$arItem['UF_RECOMMEND'] == "N" ? GetMessage('CP_REVIEWS_RECOMMEND_N') : GetMessage('CP_REVIEWS_RECOMMEND_Y')?>
                    </div>
                    <? endif; ?>
                </div>
            </div>
            
            <? if (!empty($arItem['UF_POSITIVE'])): ?>
            <div class="text">
                <div class="title font-bold-p"><?=GetMessage('CP_REVIEWS_POSITIVE')?></div>
                <?=$arItem['UF_POSITIVE']?>
            </div>
            <? endif; ?>
            
            <? if (!empty($arItem['UF_NEGATIVE'])): ?>
            <div class="text">
                <div class="title font-bold-p"><?=GetMessage('CP_REVIEWS_NEGATIVE')?></div>
                <?=$arItem['UF_NEGATIVE']?>
            </div>
            <? endif; ?>
            
            <? if (!empty($arItem['UF_COMMENT'])): ?>
            <div class="text">
                <div class="title font-bold-p"><?=GetMessage('CP_REVIEWS_COMMENT')?></div>
                <?=$arItem['UF_COMMENT']?>
            </div>
            <? endif; ?>
            
            <? if ($USER->IsAdmin()): ?>
            <div class="admin-tools">
                <a class="font-small" href="/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=<?=$arResult['TABLE_ID']?>&ID=<?=$arItem['ID']?>" target="_blank"><?=GetMessage('CP_REVIEWS_EDIT')?></a>
            </div>
            <? endif; ?>
        </div>
        <? endforeach; ?>
    </div>
    <? else: ?>
    <div class="empty-reviews">
        <div class="info">
            <div class="h3 title"><?=GetMessage('CP_REVIEWS_EMPTY_TITLE')?></div>
            <div class="text"><?=GetMessage('CP_REVIEWS_EMPTY_TEXT')?></div>
            <a href="javascript:void(0)" class="btn btn-primary add-review"><?=GetMessage('CP_REVIEWS_ADD_REVIEW')?></a>
        </div>
    </div>
    <? endif; ?>
</div>

<div class="component-reviews-form<?=!empty($arResult['VALUES']) ? ' open' : ''?>">
    
    <form method="post">
        <?
        if (isset($arParams['IS_AJAX']) && $arParams['IS_AJAX'] == 'Y')
            $APPLICATION->RestartBuffer();
        ?>
    <? if ($arResult['SUCCESS'] == "Y"): ?>
    <div class="success-message">
        <div class="title"><?=GetMessage('CP_REVIEWS_FORM_SUCCESS_TITLE')?></div>
        <div class="text"><?= GetMessage('CP_REVIEWS_FORM_SUCCESS_TEXT') ?></div>
    </div>
    <? else: ?>
    <div class="h3 title"><?=GetMessage('CP_REVIEWS_FORM_TITLE')?></div>
    <blockquote>
        <?=GetMessage('CP_REVIEWS_FORM_HELP_TEXT')?>
    </blockquote>
    
    
        
        <? if (!empty($arResult['ERRORS'])): ?>
        <div class="errors-message">
            <?=implode("<br>", $arResult['ERRORS']); ?>
        </div>
        <? endif; ?>
        
    <div class="form-group custom">
        <label>
            <?=GetMessage('CP_REVIEWS_FORM_RATING')?>
        </label>
        <? $current = intval($arResult['VALUES']['RATING']) > 0 ? intval($arResult['VALUES']['RATING']) : 1; ?>
        <div class="stars">
            <? for ($i=1; $i <= 5; $i++): ?>
            <a href="javascript:void(0)" data-value="<?=$i?>" class="st<?=$i <= $current ? ' f' : ''?>"></a>
            <? endfor; ?>
            <input type="hidden" name="REVIEW[RATING]" value="<?=$current?>" />
        </div>
    </div>
    
    <div class="form-group custom">
        <label>
            <?=GetMessage('CP_REVIEWS_FORM_RECOMMEND')?>
        </label>
        <div class="radio-line">
            <div class="form-check form-group">
                <label class="form-check-label">
                    <input class="form-check-input" <?=empty($arResult['VALUES']['RECOMMEND']) || $arResult['VALUES']['RECOMMEND'] == "Y" ? 'checked="checked"' : ''?> type="radio" name="REVIEW[RECOMMEND]" value="Y">
                    <span>
                        <?=GetMessage('CP_REVIEWS_FORM_RECOMMEND_1')?>
                    </span>
                </label>
            </div>
            <div class="form-check form-group">
                <label class="form-check-label">
                    <input class="form-check-input" <?=$arResult['VALUES']['RECOMMEND'] == "N" ? 'checked="checked"' : ''?> type="radio" name="REVIEW[RECOMMEND]" value="N">
                    <span>
                        <?=GetMessage('CP_REVIEWS_FORM_RECOMMEND_2')?>
                    </span>
                </label>
            </div>
        </div>
          
    </div>
    
    <div class="form-group custom">
        <label>
            <?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE')?>
        </label>
        <div class="radio-line">
            <div class="form-check form-group">
                <label class="form-check-label">
                    <input class="form-check-input" <?=empty($arResult['VALUES']['USE_EXPERIENCE']) || $arResult['VALUES']['USE_EXPERIENCE'] == GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_1') ? 'checked="checked"' : ''?> type="radio" name="REVIEW[USE_EXPERIENCE]" value="<?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_1')?>">
                    <span><?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_1')?></span>
                </label>
            </div>
            <div class="form-check form-group">
                <label class="form-check-label">
                    <input class="form-check-input" <?=$arResult['VALUES']['USE_EXPERIENCE'] == GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_2') ? 'checked="checked"' : ''?> type="radio" name="REVIEW[USE_EXPERIENCE]" value="<?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_2')?>">
                    <span><?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_2')?></span>
                </label>
            </div>
            <div class="form-check form-group">
                <label class="form-check-label">
                    <input class="form-check-input" <?=$arResult['VALUES']['USE_EXPERIENCE'] == GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_3') ? 'checked="checked"' : ''?> type="radio" name="REVIEW[USE_EXPERIENCE]" value="<?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_3')?>">
                    <span><?=GetMessage('CP_REVIEWS_FORM_USE_EXPERIENCE_3')?></span>
                </label>
            </div>
        </div>
          
    </div>
    
    <div class="form-group">
        <label class="control-label">
            <?=GetMessage('CP_REVIEWS_FORM_POSITIVE')?> <span class="danger-color">*</span>
        </label>
        <div class="field">
            <textarea rows="5" class="form-control<?=!empty($arResult['VALUES']['POSITIVE']) ? ' filled' : ''?>" name="REVIEW[POSITIVE]" required="required"><?=$arResult['VALUES']['POSITIVE']?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label">
            <?=GetMessage('CP_REVIEWS_FORM_NEGATIVE')?> <span class="danger-color">*</span>
        </label>
        <div class="field">
            <textarea rows="5" class="form-control<?=!empty($arResult['VALUES']['NEGATIVE']) ? ' filled' : ''?>" required="required" name="REVIEW[NEGATIVE]"><?=$arResult['VALUES']['NEGATIVE']?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label">
            <?=GetMessage('CP_REVIEWS_FORM_COMMENT')?>
        </label>
        <div class="field">
            <textarea rows="5" class="form-control<?=!empty($arResult['VALUES']['COMMENT']) ? ' filled' : ''?>" name="REVIEW[COMMENT]"><?=$arResult['VALUES']['COMMENT']?></textarea>
        </div>
    </div>
        
        <div class="form-group">
            <label class="control-label">
                <?=GetMessage('CP_REVIEWS_FORM_NAME')?> <span class="danger-color">*</span>
            </label>
            <div class="field">
                <input type="text" class="form-control<?=!empty($arResult['VALUES']['NAME']) ? ' filled' : ''?>" value="<?=$arResult['VALUES']['NAME']?>" name="REVIEW[NAME]" />
            </div>
        </div>
        
        <div class="form-actions d-flex">
            <button type="submit" class="btn"><?=GetMessage('CP_REVIEWS_FORM_SEND')?></button>
            <a href="javascript:void(0)" class="btn transparent cancel-add-review"><?=GetMessage('CP_REVIEWS_FORM_CANCEL')?></a>
        </div>
    <input type="hidden" name="ajax_component_params" value="<?=urlencode(serialize($arParams))?>" />
    <input type="hidden" name="ajax_component_template" value="<?=$templateName?>" />
    <?
    if (isset($arParams['IS_AJAX']) && $arParams['IS_AJAX'] == 'Y')
        die;
    ?>
    </form>
    <? endif; ?>
</div>