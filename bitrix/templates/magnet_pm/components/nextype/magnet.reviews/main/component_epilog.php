<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arResult
 * @var array $arParams
 * @var array $templateData
 */
?>
<script>
$(document).ready(function () {
    var showLabel = "";
    
    $(".component-reviews [data-rating-show]").on('click', function () {
        if ($(this).hasClass('open')) {
            $(".component-reviews .items > .item").show();
            $(this).removeClass('open');
            $(this).text(showLabel);
        } else {
            $(this).addClass('open');
            $(".component-reviews .items > .item").hide();
            $(".component-reviews .items > .item[data-rating='"+$(this).data('rating-show')+"']").show();
            showLabel = $(this).text();
            $(this).text('<?=GetMessage('CP_REVIEWS_SHOW_ALL_REVIEWS')?>');
        }
    });
    
    $(".component-reviews [data-role='vote']").on('click', function () {
        if (!$(this).closest('.item').hasClass('has-vote')) {
            var value = parseInt($(this).data('value')), id = parseInt($(this).closest('.item').data('review-id'));
            if (!!value) {
                $(this).closest('.item').addClass('has-vote');
                
                $.ajax({
                    url: '<?=SITE_DIR?>include/components/catalog_reviews.php?is_ajax_mode=y',
                    data: {
                        review_id: id,
                        review_vote: value,
                        ajax_component_params: '<?=urlencode(serialize($arParams))?>',
                        ajax_component_template: '<?=$templateName?>'
                    }
                });
                
                $(this).addClass('active').text(parseInt($(this).text()) + 1);
                $(this).closest('.item').addClass('has-vote');
            }
        }
        
    });
    
    $(".component-reviews .add-review").on('click', function () {
        $(".component-reviews").removeClass('open');
        $(".component-reviews-form").addClass('open');
    });
    
    $(".component-reviews-form .stars > a").hover(function () {
        var value = parseInt($(this).data('value'));
        $(this).parent().find('[data-value]').removeClass('f');
        for (var i = 1; i <= value; i++)
            $(this).parent().find('[data-value="'+i+'"]').addClass('f');
    }, function () {
        var value = parseInt($(this).parent().find('input').val());
        $(this).parent().find('[data-value]').removeClass('f');
        for (var i = 1; i <= value; i++)
            $(this).parent().find('[data-value="'+i+'"]').addClass('f');
    });
    
    $(".component-reviews-form .stars > a").on('click', function () {
        var value = parseInt($(this).data('value'));
        $(this).parent().find('input').val(value);
    });
    
    $(".component-reviews-form .cancel-add-review").on('click', function () {
        $(".component-reviews").addClass('open');
        $(".component-reviews-form").removeClass('open');
    });
    
    $(".component-reviews-form form").on('submit', function (event) {
        event.preventDefault();

        $.ajax({
            type: 'post',
            url: '<?=SITE_DIR?>include/components/catalog_reviews.php?is_ajax_mode=y',
            data: $(this).serialize(),
            success: function (response) {
                $(".component-reviews-form form").html(response);
            }
        });
        return false;
    });
    
    
});
</script>