<?php

$MESS['CP_REVIEWS_TOTAL_RATING_LABEL'] = 'Общий рейтинг основан на #COUNT# #DECLENSION#';
$MESS['CP_REVIEWS_ADD_REVIEW'] = "Оставить отзыв";
$MESS['CP_REVIEWS_ZERO_REVIEWS'] = "0 отзывов";
$MESS['CP_REVIEWS_COUNT_REVIEWS'] = "#COUNT# #DECLENSION#";
$MESS['CP_REVIEWS_USE_EXPERIENCE'] = "Опыт использования: #VALUE#";
$MESS['CP_REVIEWS_POSITIVE'] = "Достоинства";
$MESS['CP_REVIEWS_NEGATIVE'] = "Недостатки";
$MESS['CP_REVIEWS_COMMENT'] = "Комментарий";
$MESS['CP_REVIEWS_EMPTY_TITLE'] = "Отзывов пока что нет";
$MESS['CP_REVIEWS_EMPTY_TEXT'] = "Но вы можете исправить ситуацию. Расскажите другим покупателям о преимуществах и недостатках товара. Ваш отзыв поможет им сделать выбор.";
$MESS['CP_REVIEWS_RECOMMEND_N'] = "Рекомендует товар: <span style='color:#FF4422'>нет</span>";
$MESS['CP_REVIEWS_RECOMMEND_Y'] = "Рекомендует товар: <span style='color:#22AA33'>да</a>";
$MESS['CP_REVIEWS_EDIT'] = "Изменить отзыв";
$MESS['CP_REVIEWS_FORM_SUCCESS_TITLE'] = "Спасибо за Ваш отзыв!";
$MESS['CP_REVIEWS_FORM_SUCCESS_TEXT'] = "Ваш отзыв успешно отправлен. Как только он пройдет модерацию, мы разместим его на этой странице.";
$MESS['CP_REVIEWS_SHOW_ALL_REVIEWS'] = "Вернуть";
$MESS['CP_REVIEWS_DECLENSION_1'] = "отзыв";
$MESS['CP_REVIEWS_DECLENSION_4'] = "отзыва";
$MESS['CP_REVIEWS_DECLENSION_5'] = "отзывов";
$MESS['CP_REVIEWS_NEED_MODERATED'] = "Отзыв не виден пользователям сайта. Требуется модерация. <br> <a target='_blank' href='#LINK#'>Промодерировать и опубликовать</a>";
        
$MESS['CP_REVIEWS_FORM_TITLE'] = "Оставить отзыв";
$MESS['CP_REVIEWS_FORM_POSITIVE'] = "Достоинства";
$MESS['CP_REVIEWS_FORM_NEGATIVE'] = "Недостатки";
$MESS['CP_REVIEWS_FORM_COMMENT'] = "Комментарий";
$MESS['CP_REVIEWS_FORM_NAME'] = "Ваше имя";
$MESS['CP_REVIEWS_FORM_USE_EXPERIENCE'] = "Опыт использования";
$MESS['CP_REVIEWS_FORM_USE_EXPERIENCE_1'] = "Менее месяца";
$MESS['CP_REVIEWS_FORM_USE_EXPERIENCE_2'] = "Не более года";
$MESS['CP_REVIEWS_FORM_USE_EXPERIENCE_3'] = "Более года";
$MESS['CP_REVIEWS_FORM_RECOMMEND'] = "Рекомендуете ли вы данный товар другим?";
$MESS['CP_REVIEWS_FORM_RECOMMEND_1'] = "Рекомендую";
$MESS['CP_REVIEWS_FORM_RECOMMEND_2'] = "Не рекомендую";
$MESS['CP_REVIEWS_FORM_RATING'] = "Общая оценка";
$MESS['CP_REVIEWS_FORM_SEND'] = "Отправить";
$MESS['CP_REVIEWS_FORM_CANCEL'] = "Отмена";
$MESS['CP_REVIEWS_FORM_HELP_TEXT'] = "Мы ожидаем от вас честный и корректный отзыв. Поэтому не публикуем отзывы не по теме, отзывы с нецензурными выражениями и оскорблениями, а также с сылками на другие сайты.";