<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('nextype.magnet'))
    die(GetMessage('NOT_MODULE_INSTALLED'));

CSolution::getInstance(SITE_ID);
CSolution::start();

CJSCore::Init(array('ajax', 'window'));

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= LANGUAGE_ID ?>">
<head>
    <? $APPLICATION->ShowMeta("viewport"); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="format-detection" content="telephone=no">

    <? $APPLICATION->ShowHead(); ?>
    <? $APPLICATION->AddHeadString('<script>BX.message(' . CUtil::PhpToJSObject($MESS, false) . ')</script>', true); ?>
    <? CSolution::ShowJsScripts('in_head'); ?>
    <style>
        @keyframes placeHolderShimmer {
            0% {
                -webkit-transform: translateZ(0);
                transform: translateZ(0);
                background-position: -468px 0
            }
            to {
                -webkit-transform: translateZ(0);
                transform: translateZ(0);
                background-position: 468px 0
            }
        }

        .skeleton-card {
            transition: all .3s ease-in-out;
            -webkit-backface-visibility: hidden;
        }

        .skeleton-card.hidden {
            transition: all .3s ease-in-out;
            opacity: 0;
            height: 0;
            padding: 0
        }

        .skeleton-bg {
            will-change: transform;
            animation: placeHolderShimmer 1s linear infinite forwards;
            -webkit-backface-visibility: hidden;
            background: #fff;
            background: linear-gradient(90deg, #fff 8%, #f5f5f5 18%, #fff 33%);
            background-size: 800px 104px;
            height: 100%;
            position: relative;
            box-sizing: border-box;
        }
    </style>
</head>
<body<?= CSolution::ShowBodyAttributes(); ?>>
<? CSolution::ShowJsScripts('after_open_body'); ?>

<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div class="wrapper<?= CSolution::$options['LOCATIONS_ENABLED'] == "Y" ? ' enabled-locations ' : '' ?>">
    <? CSolution::ShowBanners('header_before'); ?>


    <header class="<?= CSolution::$options['HEADER_TYPE']; ?> <? if ($APPLICATION->GetCurPage() == "/personal/"): ?>none-mobile<? endif; ?> ">
        <? CSolution::ShowPageBlock('header'); ?>

        <?
        if (CSolution::$options['MOBILE_VERSION_2_0'] == "Y")
            CSolution::ShowPageBlock('mobile.menu_2_0');
        else
            CSolution::ShowPageBlock('mobile.menu');
        ?>
    </header>


    <? CSolution::ShowBanners('header_after'); ?>

    <? CSolution::ShowPageBlock('aside.catalog.menu'); ?>


<script>
    let num = localStorage.getItem("numberOfTovar");

    function createElementsInside() {

        var colorArray2 = document.body.getElementsByClassName("items-counter");
        for (var i = 0; i < colorArray2.length; i++) {
            var innerHtml = "";

                innerHtml += num;

            colorArray2[i].innerHTML = innerHtml;
        }
    }
    document.addEventListener("DOMContentLoaded", createElementsInside);
</script>
    <? if ($APPLICATION->GetCurPage() == "/"): ?>
    <div class="wrapper">
        <div class="navbar-mobile hide-desktop">
            <a class="navbar-mobile__link" href="/">
                <span class="navbar-mobile__icon navbar-mobile__icon--home navbar-mobile__icon--active"></span>
                <span class="navbar-mobile__text navbar-mobile__text--active">Главная</span>
            </a>

            <a class="navbar-mobile__link" href="/catalog/">
                <span class="navbar-mobile__icon navbar-mobile__icon--catalog"></span>
                <span class="navbar-mobile__text">Каталог</span>
            </a>

            <a class="navbar-mobile__link" href="/personal/cart/" style="position: relative;">

                <span class="navbar-mobile__icon navbar-mobile__icon--basket">
                    <span class="items-counter"></span>
                </span>
                <span class="navbar-mobile__text">Корзина</span>
            </a>

            <a class="navbar-mobile__link" href="/personal/cart/?filter=delayed">
                <span class="navbar-mobile__icon navbar-mobile__icon--favorite"></span>
                <span class="navbar-mobile__text">Избранное</span>
            </a>

            <a class="navbar-mobile__link" href="/personal/">
                <span class="navbar-mobile__icon navbar-mobile__icon--profile"></span>
                <span class="navbar-mobile__text">Профиль</span>
            </a>
        </div>
    </div>
    <? endif; ?>


    <? if (CSite::InDir("/catalog/")): ?>
        <div class="wrapper">
            <div class="navbar-mobile hide-desktop">
                <a class="navbar-mobile__link" href="/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--home"></span>
                    <span class="navbar-mobile__text">Главная</span>
                </a>

                <a class="navbar-mobile__link" href="/catalog/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--catalog navbar-mobile__icon--active"></span>
                    <span class="navbar-mobile__text navbar-mobile__text--active">Каталог</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/" style="position: relative;">

                <span class="navbar-mobile__icon navbar-mobile__icon--basket">
                    <span class="items-counter"></span>
                </span>
                    <span class="navbar-mobile__text">Корзина</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/?filter=delayed">
                    <span class="navbar-mobile__icon navbar-mobile__icon--favorite"></span>
                    <span class="navbar-mobile__text">Избранное</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--profile"></span>
                    <span class="navbar-mobile__text">Профиль</span>
                </a>
            </div>
        </div>
    <? endif; ?>


    <? if ($_GET["filter"]=="delayed"): ?>
        <div class="wrapper">
            <div class="navbar-mobile hide-desktop">
                <a class="navbar-mobile__link" href="/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--home"></span>
                    <span class="navbar-mobile__text">Главная</span>
                </a>

                <a class="navbar-mobile__link" href="/catalog/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--catalog"></span>
                    <span class="navbar-mobile__text">Каталог</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/" style="position: relative;">

                <span class="navbar-mobile__icon navbar-mobile__icon--basket">
                    <span class="items-counter"></span>
                </span>
                    <span class="navbar-mobile__text">Корзина</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/?filter=delayed">
                    <span class="navbar-mobile__icon navbar-mobile__icon--favorite navbar-mobile__icon--active"></span>
                    <span class="navbar-mobile__text navbar-mobile__text--active">Избранное</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--profile"></span>
                    <span class="navbar-mobile__text">Профиль</span>
                </a>
            </div>
        </div>
    <? endif; ?>

    <? if (CSite::InDir("/personal/")): ?>
        <div class="wrapper">
            <div class="navbar-mobile hide-desktop">
                <a class="navbar-mobile__link" href="/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--home"></span>
                    <span class="navbar-mobile__text">Главная</span>
                </a>

                <a class="navbar-mobile__link" href="/catalog/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--catalog"></span>
                    <span class="navbar-mobile__text">Каталог</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/" style="position: relative;">

                <span class="navbar-mobile__icon navbar-mobile__icon--basket">
                    <span class="items-counter"></span>
                </span>
                    <span class="navbar-mobile__text">Корзина</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/cart/?filter=delayed">
                    <span class="navbar-mobile__icon navbar-mobile__icon--favorite"></span>
                    <span class="navbar-mobile__text">Избранное</span>
                </a>

                <a class="navbar-mobile__link" href="/personal/">
                    <span class="navbar-mobile__icon navbar-mobile__icon--profile navbar-mobile__icon--active"></span>
                    <span class="navbar-mobile__text navbar-mobile__text--active">Профиль</span>
                </a>
            </div>
        </div>
    <? endif; ?>


    <? if (CSolution::isCustomPage()): ?>
    <main>
        <div class="container">
            <? else: ?>
            <div class="container">
                <? if ($APPLICATION->GetCurPage() != "/personal/") : ?>
                    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/breadcrumbs.php'); ?>
                <? endif; ?>

                <? if ($APPLICATION->GetCurPage() != "/personal/" && $APPLICATION->GetCurPage() != "/personal/private/"): ?>
                    <h1 class="main-title" id="pagetitle"><? $APPLICATION->ShowTitle(false) ?></h1>
                <? endif; ?>

                <? endif; ?>
                <div class="content-wrapper">
                    <div class="content-column<?= $APPLICATION->GetDirProperty("bContentPage") == "Y" || (defined('bContentPage') && bContentPage == true) ? ' content-page' : '' ?>">