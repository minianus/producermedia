<?php
$MESS['HEADER_CALLBACK_BUTTON'] = "Заказать звонок";
$MESS['LINK_COMPARE'] = "Сравнение";
$MESS['LINK_WISH'] = "Избранное";
$MESS['LINK_SIGNIN'] = "Войти";
$MESS['HEADER_CITY_TITLE'] = "Ваш город:";
$MESS['SHARE_BUTTON'] = "Поделиться";
$MESS['NOT_MODULE_INSTALLED'] = "Модуль nextype.magnet не установлен";
$MESS['LOCATIONS_GEO_TITLE'] = "Ваш город";
$MESS['LOCATIONS_GEO_CHANGE_BUTTON'] = "Выбрать другой";
$MESS['LOCATIONS_GEO_CONFIRM_BUTTON'] = "Да";
$MESS['BACK_BUTTON'] = "Назад";
