<?php
$MESS['FOOTER_PROCESSING_POLICY'] = "Политика конфиденциальности";
$MESS['FOOTER_CONTACTS_TITLE'] = "Контакты";
$MESS['COOKIES_AGREEMENT_TEXT'] = "Этот сайт собирает cookie-файлы, данные об IP-адресе и местоположении пользователей. Дальнейшее использование сайта означает ваше согласие на обработку таких данных.";
$MESS['COOKIES_AGREEMENT_BTN'] = "Я согласен";