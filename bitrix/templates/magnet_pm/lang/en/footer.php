<?php
$MESS['FOOTER_PROCESSING_POLICY'] = "Privacy policy";
$MESS['FOOTER_CONTACTS_TITLE'] = "Contacts";
$MESS['COOKIES_AGREEMENT_TEXT'] = "This site collects cookies, IP address and user usage data. Further use of the site constitutes your consent to the processing of such data.";
$MESS['COOKIES_AGREEMENT_BTN'] = "I agree";