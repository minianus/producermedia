<div class="cookie type-2 white">
    <div class="text"><?=GetMessage('COOKIES_AGREEMENT_TEXT')?></div>
    <a href="javascript:void(0)" class="btn agree"><span><?=GetMessage('COOKIES_AGREEMENT_BTN')?></span></a>
</div>