<div class="cookie type-2 black">
    <div class="text"><?=GetMessage('COOKIES_AGREEMENT_TEXT')?></div>
    <a href="javascript:void(0)" class="btn agree"><span><?=GetMessage('COOKIES_AGREEMENT_BTN')?></span></a>
</div>