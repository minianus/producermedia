<div class="header-mobile-menus">
    <div class="container">
    <?
        $APPLICATION->IncludeComponent(
                "bitrix:menu", "top_mobile", Array(
            "ROOT_MENU_TYPE" => "top",
            "MAX_LEVEL" => "3",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "ALLOW_MULTI_SELECT" => "N",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => ""
                )
        );
        ?>
    </div>
    
    <div class="catalog-mobile-menu">
        <div class="content">
            <nav>
                <div class="head">
                    <a href="javascript:void(0);" class="burger-button">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                    <div class="title">
                        <? $APPLICATION->IncludeFile(SITE_DIR . 'include/aside_catalog_link.php'); ?>
                    </div>
                </div>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:menu", "catalog_mobile", Array(
                    "ROOT_MENU_TYPE" => "catalog_left",
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "catalog_left",
                    "USE_EXT" => "Y",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                        )
                );
                ?>
            </nav>
        </div>


    </div>
</div>