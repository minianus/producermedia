<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
use Nextype\Magnet\CGeo;
use Nextype\Magnet\CCache;

CSolution::getInstance(SITE_ID);
if (CSolution::$options['LOCATIONS_GEO_ENABLED'] == "Y" && !CGeo::isSetLocation())
    $arGeo = CGeo::getCurrentLocation();
?>

<div class="header-mobile-menus">
    <div class="container">
    <?
        $APPLICATION->IncludeComponent(
                "bitrix:menu", "top_mobile", Array(
            "ROOT_MENU_TYPE" => "top",
            "MAX_LEVEL" => "4",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "ALLOW_MULTI_SELECT" => "N",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => ""
                )
        );
        ?>
    </div>
    
    <div class="catalog-mobile-menu">
        <div class="content">
            <nav>
                <div class="head">
                    <a href="javascript:void(0);" class="burger-button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                    <div class="title">
                        <? $APPLICATION->IncludeFile(SITE_DIR . 'include/aside_catalog_link.php'); ?>
                    </div>
                </div>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:menu", "catalog_mobile", Array(
                    "ROOT_MENU_TYPE" => "catalog_left",
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "catalog_left",
                    "USE_EXT" => "Y",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                        )
                );
                ?>
            </nav>
        </div>


    </div>
</div>

<div class="header-mobile-menus menu-2-0">
    <div class="catalog-mobile-menu open">
        <div class="container">
            <div class="content">
                <nav class="menu-container">
                    <ul class="list">
                        <li class="item has-sub">
                            <a href="<?=SITE_DIR?>catalog/">
                                <span class="text"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/inline_catalog_text.php'); ?></span>
                            </a>
                            
                            <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu", "mobile_20_left", Array(
                                "ROOT_MENU_TYPE" => "catalog_left",
                                "MAX_LEVEL" => "3",
                                "ROOT_ITEM_TEXT" => @file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/inline_catalog_text.php'),
                                "ROOT_ITEM_LINK" => SITE_DIR . "catalog/",
                                "CHILD_MENU_TYPE" => "catalog_left",
                                "USE_EXT" => "Y",
                                "ALLOW_MULTI_SELECT" => "N",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600000",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "MENU_CACHE_GET_VARS" => ""
                                    )
                            );
                            ?>
                        </li>
                    
                        <? $APPLICATION->IncludeComponent(
                                "bitrix:menu", "mobile_20_left", Array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "Y",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600000",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_CACHE_GET_VARS" => ""
                                )
                        );
                    ?>
                    </ul>
                    
                    
                    <ul class="list">
                        <? if (CSolution::$options['LOCATIONS_ENABLED'] == "Y"):
                            $arRegions = CCache::CIBlockElement_GetList(Array ('SORT' => 'ASC'), Array ('IBLOCK_ID' => CSolution::$options['LOCATIONS_IBLOCKID'], 'ACTIVE' => "Y"), true);
                        ?>
                        <li class="item has-sub">
                            <a href="#" class="">
                                <span class="text">
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 1C4.04427 1 2.00348 2.63556 2 4.97872C1.99566 7.78266 6 12 6 12C6 12 10.0043 7.78906 10 4.97872C9.99652 2.63921 7.95573 1 6 1ZM6 6.85106C5.01823 6.85106 4.22222 6.01272 4.22222 4.97872C4.22222 3.94473 5.01823 3.10638 6 3.10638C6.98177 3.10638 7.77778 3.94473 7.77778 4.97872C7.77778 6.01272 6.98177 6.85106 6 6.85106Z" fill="#424242"/>
                                    </svg>

                                    <span><?= $GLOBALS['arCurrentRegion']['NAME'] ?></span>
                                </span>
                            </a>
                            <? if (!empty($arRegions)): ?>
                            <div class="sub">
                                <div class="items">
                                    <div class="item back">
                                        <a href="javascript:void(0);"><?=GetMessage('BACK_BUTTON')?></a>
                                    </div>
                                    <? foreach ($arRegions as $arRegion):
                                        $link = SITE_DIR . "include/components/set_region.php?id=" . $arRegion['ID'] . '&r=' . $APPLICATION->GetCurPageParam();
                                        if (CSolution::$options['LOCATIONS_ON_SUBDOMAIN'] == 'Y')
                                            $link = CLocations::getSubdomainLink($arRegion['PROPERTIES']['SUBDOMAIN']['VALUE']) . $APPLICATION->GetCurPageParam();
                                    ?>
                                    <div class="item">
                                        <a href="<?=$link?>">                                                
                                            <span class="text"><?=$arRegion['NAME']?></span>                                         
                                       </a>
                                    </div>
                                    <? endforeach; ?>
                                </div>
                                
                            </div>
                            <? endif; ?>
                        </li>
                        <? endif; ?>
                        
                        <? if (CSolution::$options['CATALOG_SHOW_WISH_LIST'] == "Y"): ?>
                        <li class="item">
                           <a href="<?=str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_CART'])?>?filter=delayed" class="">
                               <span class="text">
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.00141 1C7.31927 1 6 2.62016 6 2.62016C6 2.62016 4.68073 1 2.99859 1C1.31927 1 0 2.27337 0 3.89082C0 7.822 6 11 6 11C6 11 12 7.822 12 3.89082C12 2.27337 10.6807 1 9.00141 1Z" fill="#424242"/>
                                    </svg>

                                   <span><?=GetMessage('LINK_WISH')?></span>
                               </span>
                                
                            </a> 
                        </li>
                        <? endif; ?>
                        
                        <? if (CSolution::$options['CATALOG_SHOW_COMPARE_LIST'] == "Y"): ?>
                        <li class="item">
                           <a href="<?=SITE_DIR?>catalog/compare.php" class="">
                                <span class="text">
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.6 3H12V10H9.6V3Z" fill="#424242"/>
                                        <path d="M4.8 0H7.2V12H4.8V0Z" fill="#424242"/>
                                        <path d="M0 2H2.4V11H0V2Z" fill="#424242"/>
                                    </svg>

                                   <span><?=GetMessage('LINK_COMPARE')?></span>
                               </span>
                            </a> 
                        </li>
                        <? endif; ?>
                        
                        <? if ($USER->IsAuthorized()): ?>
                        <li class="item">
                           <a href="<?=\Nextype\Magnet\CSolution::ShowPageUrl('personal')?>" class="">
                                <span class="text">
                                   <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 0C4.33789 0 3 1.4016 3 3.14286V3.66667C2.44922 3.66667 2 4.13728 2 4.71429V9.95238C2 10.5294 2.44922 11 3 11H9C9.55078 11 10 10.5294 10 9.95238V4.71429C10 4.13728 9.55078 3.66667 9 3.66667V3.14286C9 1.4016 7.66211 0 6 0ZM6 1.04762C7.13867 1.04762 8 1.94996 8 3.14286V3.66667H4V3.14286C4 1.94996 4.86133 1.04762 6 1.04762ZM6 6.28571C6.55078 6.28571 7 6.75632 7 7.33333C7 7.91034 6.55078 8.38095 6 8.38095C5.44922 8.38095 5 7.91034 5 7.33333C5 6.75632 5.44922 6.28571 6 6.28571Z" fill="#424242"/>
                                    </svg>

                                   <span><?=$USER->GetFullName()?></span>
                               </span>
                            </a> 
                        </li>
                        
                        <? else: ?>
                        <li class="item">
                           <a href="<?=\Nextype\Magnet\CSolution::ShowPageUrl('auth')?>" class="">
                                <span class="text">
                                   <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 0C4.33789 0 3 1.4016 3 3.14286V3.66667C2.44922 3.66667 2 4.13728 2 4.71429V9.95238C2 10.5294 2.44922 11 3 11H9C9.55078 11 10 10.5294 10 9.95238V4.71429C10 4.13728 9.55078 3.66667 9 3.66667V3.14286C9 1.4016 7.66211 0 6 0ZM6 1.04762C7.13867 1.04762 8 1.94996 8 3.14286V3.66667H4V3.14286C4 1.94996 4.86133 1.04762 6 1.04762ZM6 6.28571C6.55078 6.28571 7 6.75632 7 7.33333C7 7.91034 6.55078 8.38095 6 8.38095C5.44922 8.38095 5 7.91034 5 7.33333C5 6.75632 5.44922 6.28571 6 6.28571Z" fill="#424242"/>
                                    </svg>

                                   <span><?=GetMessage('LINK_SIGNIN')?></span>
                               </span>
                            </a> 
                        </li>
                        
                        <? endif; ?>
                        
                        
                        
                        <?
                        if (CSolution::$options['LOCATIONS_ENABLED'] == "Y")
                            $arPhones = $GLOBALS['arCurrentRegion']['PROPERTIES']['ALL_PHONES']['VALUE'];
                        else
                            $arPhones = !empty(CSolution::$options['CONTACT_PHONES']) ? unserialize(CSolution::$options['CONTACT_PHONES']) : Array();
                        ?>
                        <li class="item has-sub phone">
                            <a href="tel:<?= CSolution::phone2int($arPhones[0]) ?>" class="">
                                <span class="text">
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.9994 8.45514V10.4195C10.9995 10.5601 10.9462 10.6956 10.8503 10.7985C10.7545 10.9014 10.6231 10.964 10.4828 10.9739C10.24 10.9906 10.0417 10.9994 9.8884 10.9994C4.97922 10.9994 1 7.02022 1 2.11105C1 1.95772 1.00833 1.7594 1.02555 1.51664C1.0354 1.37634 1.0981 1.24499 1.20099 1.14911C1.30389 1.05322 1.43932 0.999936 1.57997 1H3.5443C3.61321 0.99993 3.67968 1.02548 3.73081 1.07167C3.78193 1.11787 3.81406 1.18142 3.82095 1.24999C3.83373 1.37776 3.8454 1.47942 3.85651 1.55664C3.96691 2.32711 4.19316 3.07646 4.52758 3.77929C4.58036 3.8904 4.54591 4.02317 4.44592 4.09427L3.2471 4.95089C3.98009 6.65883 5.34117 8.01991 7.04911 8.7529L7.90462 7.5563C7.93958 7.50742 7.9906 7.47235 8.04877 7.45723C8.10694 7.4421 8.16858 7.44787 8.22293 7.47353C8.92568 7.80732 9.67484 8.03301 10.445 8.14294C10.5222 8.15405 10.6239 8.16627 10.7506 8.17849C10.819 8.18551 10.8824 8.21769 10.9285 8.26881C10.9746 8.31992 11.0001 8.38632 11 8.45514H10.9994Z" fill="#424242"/>
                                    </svg>


                                    <span><?= $arPhones[0] ?></span>
                                </span>
                            </a>
                            <? if (count($arPhones) > 1): ?>
                            <div class="sub">
                                <div class="items">
                                    <div class="item back">
                                        <a href="javascript:void(0);"><?=GetMessage('BACK_BUTTON')?></a>
                                    </div>
                                    
                                    <? foreach ($arPhones as $phone): ?>
                                    <div class="item">
                                        <a href="tel:<?= CSolution::phone2int($phone) ?>">                                                
                                            <span class="text"><?=$phone?></span>                                         
                                       </a>
                                    </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <? endif; ?>
                        </li>
                    </ul>
                    
                    <div class="contacts-block">
                        
                        <?
                        $address = CSolution::ShowAddress();
                        if (!empty($address)):
                        ?>
                        <div class="address"> 
                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 1C4.04427 1 2.00348 2.63556 2 4.97872C1.99566 7.78266 6 12 6 12C6 12 10.0043 7.78906 10 4.97872C9.99652 2.63921 7.95573 1 6 1ZM6 6.85106C5.01823 6.85106 4.22222 6.01272 4.22222 4.97872C4.22222 3.94473 5.01823 3.10638 6 3.10638C6.98177 3.10638 7.77778 3.94473 7.77778 4.97872C7.77778 6.01272 6.98177 6.85106 6 6.85106Z" fill="#5855F3"/>
                            </svg>
                            <span><?=$address?></span>
                        </div>
                        <? endif; ?>
                        
                        <?php
                        $CSolution = \Nextype\Magnet\CSolution::getInstance();
                        $arSocials = Array (
                            'SOCIALS_VK', 'SOCIALS_FACEBOOK', 'SOCIALS_TWITTER', 'SOCIALS_YOUTUBE', 'SOCIALS_INSTAGRAM', 'SOCIALS_OK'
                        );
                        $arSocialsResult = Array ();
                        foreach ($arSocials as $key)
                        {
                            if (isset($CSolution::$options[$key]) && !empty($CSolution::$options[$key]))
                            {
                                $arSocialsResult[] = Array (
                                    'CLASS' => strtolower(str_replace("SOCIALS_", "", $key)),
                                    'LINK' => $CSolution::$options[$key]
                                );
                            }
                        }
                        ?>

                        <? if (!empty($arSocialsResult)): ?>
                        <div class="socials">
                            <? foreach ($arSocialsResult as $arSocial): ?>
                            <a href="<?=$arSocial['LINK']?>" class="elem <?=$arSocial['CLASS']?>-icon"></a>
                            <? endforeach; ?>
                        </div>
                        <? endif; ?>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>