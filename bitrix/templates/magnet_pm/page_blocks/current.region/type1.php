<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
use Nextype\Magnet\CGeo;

CSolution::getInstance(SITE_ID);
if (CSolution::$options['LOCATIONS_GEO_ENABLED'] == "Y" && !CGeo::isSetLocation())
    $arGeo = CGeo::getCurrentLocation();

?>

<? if (CSolution::$options['LOCATIONS_ENABLED'] == "Y"): ?>

    <div class="city" data-helper="header::region">
        <div class="current"><span><?= GetMessage('HEADER_CITY_TITLE') ?></span> <a href="javascript:void(0);" class="locations-list-popup-btn"><?= $GLOBALS['arCurrentRegion']['NAME'] ?></a></div>
        
        <? if ($arGeo): ?>
        <? $confirmBtnId = "btn_set_region" . randString(6); ?>
        <div class="confirm-your-region-overlay open"></div>
        <div class="confirm-your-region open">
            <a href="javascript:void(0)" onclick="silenceSetRegion(true);" class="close close-popup-icon icon-custom"></a>
            <div class="selected-region-label"><?=GetMessage('LOCATIONS_GEO_TITLE')?></div>
            <div class="selected-region"><?=$arGeo['NAME']?></div>
            <div class="buttons">
                <a id="<?=$confirmBtnId?>" data-href="<?=SITE_DIR?>include/components/set_region.php?id=<?=$arGeo['ID']?>&r=<?=$APPLICATION->GetCurDir()?>" class="btn" data-reload="<?=($arGeo["NAME"] == $GLOBALS['arCurrentRegion']['NAME']) ? "N" : "Y"?>"><?=GetMessage('LOCATIONS_GEO_CONFIRM_BUTTON')?></a>
                <a href="javascript:void(0);" onclick="$(this).closest('.confirm-your-region').removeClass('open').parent().find('.confirm-your-region-overlay').removeClass('open');" class="btn transparent locations-list-popup-btn"><?=GetMessage('LOCATIONS_GEO_CHANGE_BUTTON')?></a>
            </div>
        </div>
        <? include("confirm_script.php"); ?>
        <? endif; ?>
    </div>
<? endif; ?>

<script>
    $(document).ready(function() {
        jqmPopup('locations-list', 'include/components/locations_list.php?is_ajax_mode=y');
    });
</script>