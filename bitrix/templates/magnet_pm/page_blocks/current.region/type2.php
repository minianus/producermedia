<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
use Nextype\Magnet\CCache;
use Nextype\Magnet\CGeo;

CSolution::getInstance(SITE_ID);
if (CSolution::$options['LOCATIONS_GEO_ENABLED'] == "Y" && !CGeo::isSetLocation())
    $arGeo = CGeo::getCurrentLocation();
?>

<? if (CSolution::$options['LOCATIONS_ENABLED'] == "Y"): ?>
    <?
        $arRegions = CCache::CIBlockElement_GetList(Array ('SORT' => 'ASC'), Array ('IBLOCK_ID' => CSolution::$options['LOCATIONS_IBLOCKID'], 'ACTIVE' => "Y"), true);
    ?>
    <div class="city" data-helper="header::region">
        <div class="current"><span><?= GetMessage('HEADER_CITY_TITLE') ?></span> <a href="javascript:void(0);" class="locations-list-popup-btn"><?= $GLOBALS['arCurrentRegion']['NAME'] ?></a></div>
        <? if ($arRegions): ?>
        <ul class="list">
            
            <? foreach ($arRegions as $arRegion):
                $link = SITE_DIR . "include/components/set_region.php?id=" . $arRegion['ID'] . '&r=' . $APPLICATION->GetCurPageParam();
                if (CSolution::$options['LOCATIONS_ON_SUBDOMAIN'] == 'Y')
                    $link = CLocations::getSubdomainLink($arRegion['PROPERTIES']['SUBDOMAIN']['VALUE']) . $APPLICATION->GetCurPageParam();
            ?>
            <li><a href="<?=$link?>"><?=$arRegion['NAME']?></a></li>
            <? endforeach; ?>
        </ul>
        <? endif; ?>
        
        <? if ($arGeo): ?>
        <? $confirmBtnId = "btn_set_region" . randString(6); ?>
        <div class="confirm-your-region-overlay open"></div>
        <div class="confirm-your-region open">
            <a href="javascript:void(0)" onclick="silenceSetRegion(true);" class="close close-popup-icon icon-custom"></a>
            <div class="selected-region-label"><?=GetMessage('LOCATIONS_GEO_TITLE')?></div>
            <div class="selected-region"><?=$arGeo['NAME']?></div>
            <div class="buttons">
                <a id="<?=$confirmBtnId?>" data-href="<?=SITE_DIR?>include/components/set_region.php?id=<?=$arGeo['ID']?>&r=<?=$APPLICATION->GetCurDir()?>" class="btn" data-reload="<?=($arGeo["NAME"] == $GLOBALS['arCurrentRegion']['NAME']) ? "N" : "Y"?>"><?=GetMessage('LOCATIONS_GEO_CONFIRM_BUTTON')?></a>
                <a href="javascript:void(0);" onclick="$(this).closest('.confirm-your-region').removeClass('open').parent().find('.confirm-your-region-overlay').removeClass('open');" class="btn transparent locations-list-popup-btn"><?=GetMessage('LOCATIONS_GEO_CHANGE_BUTTON')?></a>
            </div>
        </div>
        <? include("confirm_script.php"); ?>
        <? endif; ?>
    </div>
<? endif; ?>

<script>
    $(document).ready(function () {
        $("body").on('click', '.locations-list-popup-btn', function (event) {
                event.preventDefault();
                $(this).parents('.city').toggleClass('open');
                return false;
        });
    });
    
</script>