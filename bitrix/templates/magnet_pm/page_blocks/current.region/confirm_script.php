<script>
    function silenceSetRegion(ignoreReload = false)
    {
        let btn = $("#<?=$confirmBtnId?>") || false;
        if (!btn)
            return;
        let link = $(btn).data("href") || "",
            needReload = $(btn).data("reload") == "Y";
        if (!link)
            return;
        $(".confirm-your-region").removeClass('open').parent().find('.confirm-your-region-overlay').removeClass('open');
        
        if (!needReload || ignoreReload) {
            link = link.replace("?id=<?=$arGeo['ID']?>", "?id=<?=$GLOBALS['arCurrentRegion']['ID']?>");
            $.ajax(link);
        }
        else if (!ignoreReload) {
            window.location.href = link;
        }
    }
    $(document).ready(() => {
        $("div.confirm-your-region-overlay.open").on("click", (e) => {
            if ($(e.target).closest(".confirm-your-region.open").length)
                return;
            silenceSetRegion(true);
        });

        $("#<?=$confirmBtnId?>").on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            silenceSetRegion();
        });
        
        $(document).keyup(function(e) {
             if (e.key === "Escape" && $(".confirm-your-region.open").length) {
                 silenceSetRegion(true);
            }
        });
    })
</script>