<div class="basket-container">
    <?
    $APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"PATH" => SITE_DIR . "include/components/basket.php",
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => "standard.php",
                                "TEMPLATE" => "inline"
			),
			false
		);
    ?>
</div>