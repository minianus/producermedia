<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

?>
<div class="top" data-helper="header::top">
    <div class="container">
        <? CSolution::ShowPageBlock('current.region'); ?>
        
        <?
        $address = CSolution::ShowAddress();
        if (!empty($address)): ?>
        <div class="address icon-custom">
            <?=$address; ?>
        </div>
        <? endif; ?>
        <div class="links">
            <? @include(__DIR__ . "/include/tools.php") ?>
            
            <? @include(__DIR__ . "/include/personal.php") ?>
        </div>
    </div>
</div>
<div class="middle" data-helper="header::middle">
    <div class="container">

        <a href="javascript:void(0);" class="burger">
            <svg viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5 6.6665H35V9.99984H5V6.6665ZM5 18.3332H25V21.6665H5V18.3332ZM5 29.9998H35V33.3332H5V29.9998Z" fill="#5855F3"/>
            </svg>

        </a>

        <div class="logo" data-helper="header::logo">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/logo.php'); ?>
        </div>
        <div class="slogan" data-helper="header::slogan">
            <? CSolution::IncludeFile(SITE_DIR . "include/header_slogan.php", Array(), Array("MODE" => "html")); ?>

        </div>

        <? $APPLICATION->IncludeFile(SITE_DIR . 'include/search_line.php', array(), array('SHOW_BORDER' => false)); ?>
        
        <? @include(__DIR__ . "/include/phones.php") ?>

        <? CSolution::ShowPageBlock('basket.inline'); ?>
        
    </div>
</div>
<div class="end-block"></div>
<div class="bottom">
    <div class="container">
        
        <? CSolution::ShowPageBlock('top.catalog.menu'); ?>
        
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:menu", "top", Array(
            "ROOT_MENU_TYPE" => "top",
            "MAX_LEVEL" => "3",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "ALLOW_MULTI_SELECT" => "N",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => ""
                )
        );
        ?>

    </div>
</div>

<div class="header-mobile searchbar">
    <div class="middle" data-helper="header::middle">
        <div class="container">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/search_line.php', array(), array('SHOW_BORDER' => false)); ?>
        </div>
    </div>
</div>

