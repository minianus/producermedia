<? if ($USER->IsAuthorized()): ?>
<a href="<?=\Nextype\Magnet\CSolution::ShowPageUrl('personal')?>" class="personal-link icon-custom"><span><?=$USER->GetFullName()?></span></a>
<? else: ?>
<a href="<?=\Nextype\Magnet\CSolution::ShowPageUrl('auth')?>" class="auth-popup-btn login-link icon-custom"><span><?=GetMessage('LINK_SIGNIN')?></span></a>
<? endif; ?>