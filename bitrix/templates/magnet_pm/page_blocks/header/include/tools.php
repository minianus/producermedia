<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;
?>
<? if (CSolution::$options['CATALOG_SHOW_WISH_LIST'] == "Y"): ?>
<a href="<?=str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_CART'])?>?filter=delayed" class="wishlist-link icon-custom"><span><?=GetMessage('LINK_WISH')?></span></a>
<? endif; ?>
<? if (CSolution::$options['CATALOG_SHOW_COMPARE_LIST'] == "Y"): ?>
<a href="<?=SITE_DIR?>catalog/compare.php" class="compare-link icon-custom"><span><?=GetMessage('LINK_COMPARE')?></span> <span id="header-compare-result">(0)</span></a>
<? endif; ?>

