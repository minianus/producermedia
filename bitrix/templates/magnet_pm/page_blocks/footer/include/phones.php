<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;


if (CSolution::$options['LOCATIONS_ENABLED'] == "Y")
    $arPhones = $GLOBALS['arCurrentRegion']['PROPERTIES']['ALL_PHONES']['VALUE'];
else
    $arPhones = !empty(CSolution::$options['CONTACT_PHONES']) ? unserialize(CSolution::$options['CONTACT_PHONES']) : Array();
?>

<div class="tel-container<?= count($arPhones) > 1 ? ' has-content' : '' ?>">
    <div class="tel">
        <a href="tel:<?= CSolution::phone2int($arPhones[0]) ?>"><?= $arPhones[0] ?></a>
        <? if (count($arPhones) > 1): ?>
            <div class="content">
                <? for ($i = 1; $i < count($arPhones); $i++): ?>
                    <a href="tel:<?= CSolution::phone2int($arPhones[$i]) ?>"><?= $arPhones[$i] ?></a>
                <? endfor; ?>
            </div>
        <? endif; ?>
    </div>
    <div class="call-me">
        <a href="javascript:void(0)" class="callback-popup-btn"><?= GetMessage('HEADER_CALLBACK_BUTTON') ?></a>
    </div>
</div>