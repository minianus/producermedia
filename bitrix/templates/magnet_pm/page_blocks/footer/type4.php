<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

?>
<div class="subscribe-block">
	<div class="container" data-helper="subscribe">
		<div class="name"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/subscribe_long_name.php'); ?></div>
		<noindex>
		<div class="name mobile"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/subscribe_name.php'); ?></div>
		</noindex>
		<div class="form-container"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/subscribe.php'); ?></div>
		<div class="desc"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/subscribe_desc.php'); ?></div>
	</div>
</div>
<div class="top">
    <div class="container" data-helper="footer">
        <div class="footer-menu">
            <div class="column">
                <div class="name"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/menu_title_1.php'); ?></div>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:menu", "footer", Array(
                    "ROOT_MENU_TYPE" => "footer_1",
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                        )
                );
                ?>

            </div>
            <div class="column">
                <div class="name"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/menu_title_2.php'); ?></div>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:menu", "footer", Array(
                    "ROOT_MENU_TYPE" => "footer_2",
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                        )
                );
                ?>
            </div>
            <div class="column">
                <div class="name"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/menu_title_3.php'); ?></div>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:menu", "footer", Array(
                    "ROOT_MENU_TYPE" => "footer_3",
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                        )
                );
                ?>
            </div>
    		<div class="contacts-container">
    		    <div class="name"><?= GetMessage('FOOTER_CONTACTS_TITLE') ?></div>
    		    <div class="address icon-custom">
    		        <?=CSolution::ShowAddress(); ?>
    		    </div>            
    		    <? @include(__DIR__ . "/include/phones.php") ?>
    			<div class="social">
    			    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/socials.php'); ?>
    			</div>
    		</div>
        </div>
    </div>
</div>
<div class="bottom">
    <div class="container">
        <div class="copyright">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/copyright.php'); ?>
        </div>
        <div class="policy">
            <? if (!empty(CSolution::$options['PAGES_PROCESSING_POLICY'])): ?>
                <a href="<?= str_replace('#SITE_DIR#', SITE_DIR, CSolution::$options['PAGES_PROCESSING_POLICY']) ?>"><?= GetMessage('FOOTER_PROCESSING_POLICY') ?></a>
            <? endif; ?>
        </div>
        <div class="payments">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/paymethods.php'); ?>
        </div>
    </div>
</div>