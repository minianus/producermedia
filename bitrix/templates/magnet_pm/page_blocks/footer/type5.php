<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

?>
<div class="container" data-helper="footer">
	<? @include(__DIR__ . "/include/phones.php") ?>
	<div class="address icon-custom">
		<?=CSolution::ShowAddress(); ?>
	</div>
	<div class="copyright">
		<? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/copyright.php'); ?>
	</div>
	<div class="policy">
            <? if (!empty(CSolution::$options['PAGES_PROCESSING_POLICY'])): ?>
                <a href="<?= str_replace('#SITE_DIR#', SITE_DIR, CSolution::$options['PAGES_PROCESSING_POLICY']) ?>"><?= GetMessage('FOOTER_PROCESSING_POLICY') ?></a>
            <? endif; ?>
	</div>
	<div class="social">
		<? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/socials.php'); ?>
	</div>	
</div>