<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

CSolution::getInstance(SITE_ID);
?>

<nav class="header-catalog-menu<?=CSolution::$options['SIDEBAR_MENU_OPEN_ON_HOVER'] == "Y" ? ' open-on-hover' : ''?>">
    <a href="javascript:void(0);" class="head" data-helper="aside::catalog::menu">
        <span class="title">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/inline_catalog_text.php'); ?>
        </span>
        <span class="burger-button">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </span>
    </a>

    <?
    $APPLICATION->IncludeComponent(
        "bitrix:menu", "catalog2", Array(
        "ROOT_MENU_TYPE" => "catalog_left",
        "MAX_LEVEL" => "4",
        "CHILD_MENU_TYPE" => "catalog_left",
        "USE_EXT" => "Y",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => ""
            )
    );
    ?>
</nav>