<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

?>

<aside class="catalog-menu open">
    <div class="content" data-helper="aside::catalog::menu">
        <nav>
            <div class="head">
                <div class="title">
                    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/aside_catalog_link.php'); ?>
                </div>
            </div>
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:menu", "catalog1", Array(
                "ROOT_MENU_TYPE" => "catalog_left",
                "MAX_LEVEL" => "4",
                "CHILD_MENU_TYPE" => "catalog_left",
                "USE_EXT" => "Y",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600000",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => ""
                    )
            );
            ?>


        </nav>
    </div>
</aside>   