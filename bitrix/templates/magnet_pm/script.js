window.CSolution = {
    
    construct: function () {
        var self = this;
        
		$('.product-wrap:not(.slider-element)').each(function() {
			self.setProductsHeight($(this));
		});
		$('.offers.swiper-container .offer-wrap').each(function() {
			self.setOffersHeight($(this));
		});
		$('.offers:not(.swiper-container) .offer-wrap').each(function () {
			self.setProductsHeight($(this));
		});
		$('.personal-subscribe-list .bx_item_list_slide .bx_catalog_item').each(function() {
			self.setProductsHeight($(this));
		});
      
        $('.instagram:not(a) .items .item').height($('.instagram:not(a) .items .item').width());
        this.refreshHeaderCompare();
        
        if (window.arOptions['phone_mask'] != "") {
            $('[phone-mask]').mask(window.arOptions['phone_mask']);
        }

    },
    
    initScrollbar: function () {
        if (!$("body").hasClass('firefox-macos'))
            $(".scrollbar-inner").scrollbar();
    },
    
    initEvents: function () {
        
        var bTouch = !!('ontouchstart' in window);
        
        if (typeof(navigator) == 'object' && navigator.userAgent.indexOf('Mac OS') != -1 && navigator.userAgent.indexOf('Firefox') != -1)
        {
            $("body").addClass('firefox-macos');
        }
        
        $(document).click(function(event) {
            if ($(event.target).closest(".city").length) return;
            $('.city').removeClass('open');
            event.stopPropagation();
        });

        $(document).click(function(event) {
            if ($(event.target).closest(".header-catalog-menu").length) return;
            if ($(event.target).closest(".sidebar-type1 aside.catalog-menu").length) return;
            if ($(event.target).closest(".catalog-mobile-menu").length) return;
            
            $(".header-catalog-menu, .sidebar-type1 aside.catalog-menu, .sidebar-type5 aside.catalog-menu, aside.catalog-menu .burger-button").removeClass("open");

            event.stopPropagation();
        });
        
	$('body').on('click', '.product-detail .left > .tabs-container .tab', function() {
		if ($(this).hasClass('active')) {
			$(this).parents('.tabs').toggleClass('open');
		} else {
			$(this).parents('.tabs').removeClass('open');
			$(this).addClass('active').siblings().removeClass('active')
			$(this).closest('.tabs-container').find('.tabs-content .tab-content').removeClass('active').eq($(this).index()).addClass('active');
		}
	});

	$('body').on('click', '.product-tabs-container .tabs-container .tab', function() {
		if ($(this).hasClass('active')) {
			$(this).parents('.tabs').toggleClass('open');
		} else {
			$(this).parents('.tabs').removeClass('open');
			$(this).addClass('active').siblings().removeClass('active')
			$(this).closest('.tabs-container').find('.tabs-content .tab-content').removeClass('active').eq($(this).index()).addClass('active');
			$('.product-tabs-container .tabs-content .tab-content.active .owl-container').css('height', 'auto');
			window.CSolution.setProductsHeight($('.product-tabs-container .tabs-content .tab-content.active .product-wrap'));
		}
	});
        
        $("body").on('click', '#scroll-top-button', function () {
            $("html, body").stop().animate({scrollTop:0}, 500, 'swing');
        });
        
        var resizeFn = this.waitResize(function() {
			$('.product-wrap:not(.slider-element)').each(function() {
				window.CSolution.setProductsHeight($(this));
			});
			$('.offers.owl-carousel .offer-wrap').each(function() {
				window.CSolution.setOffersHeight($(this));
			});
			$('.offers:not(.owl-carousel) .offer-wrap').each(function() {
				window.CSolution.setProductsHeight($(this));
			});
			$('.personal-subscribe-list .bx_item_list_slide .bx_catalog_item').each(function() {
				window.CSolution.setProductsHeight($(this));
            });
            $('.light-categories.more-list .items').each(function () {
                window.CSolution.setLightCategoriesHeight($(this));
            });

        }, 300);
        window.addEventListener("resize", resizeFn);
        
         window.CSolution.initScrollbar();

         // $("body").on('mouseover', '.catalog-menu li.item', function (event) {
         //     $(this).parent().find('li.item').removeClass('open');
         //     $(this).addClass('open');
         //     if ($(this).find("> .sub").length > 0) {
         //         $(this).find("> .sub").css({
         //             maxHeight: (parseInt($('.catalog-menu .content').outerHeight())) + 'px'
         //         });
         //     }
         // });



         let timer;
         $("body").on('mouseenter', '.catalog-menu li.item.has-sub', function () {
             var that = this;
             timer = setTimeout(function () {
                 $(that).parent().find('li.item').removeClass('open');
                 $(that).addClass('open');
                 if ($(that).find("> .sub").length > 0) {
                     $(that).find("> .sub").css({
                         maxHeight: (parseInt($('.catalog-menu .content').outerHeight())) + 'px'
                     });
                 }
             },100);
         });
         $("body").on('mouseleave', '.catalog-menu li.item', function () {
             clearTimeout(timer);
         })
         $("body").on('mouseleave', '.catalog-menu li.item.has-sub', function () {
            $(this).removeClass('open');
         })
        
        $("body").on('mouseover', '.header-catalog-menu li.item.has-sub', function (event) {
            if ($(this).find("> .sub").length > 0) {
                $(this).find("> .sub").css({
                    minHeight: (parseInt($(this).parents('ul.list').height())) + 'px'
                });
            }
        });
        
        $("body").on('mouseover', function (event) {
            if ($(event.target).closest(".catalog-menu").length) return;
            
            $('.catalog-menu li.item').removeClass('open');
        });
        
        $("body").on('click', '.catalog-menu:not(.open-on-hover) .burger-button', function (event) {
		event.preventDefault();
		$(this).toggleClass('open')
			   .parents('.catalog-menu').toggleClass('open');		
		return false;
	});

        $("body").on('click', 'header .inline-search .search-icon', function (event) {
		event.preventDefault();
		$(this).parents('.inline-search').toggleClass('open');
		if ($(this).parents('.inline-search').hasClass('open')) {
			$(this).parents('.inline-search').find('input[type="text"]').focus();
		}
		return false;
	});

        $("body").on('click', 'header .header-catalog-menu .head', function (event) {
		event.preventDefault();
		$(this).parents('.header-catalog-menu').toggleClass('open');
		return false;
	});        

        $("body").on('click', '.brands-page .brands-list .search-panel .mobile-link', function (event) {
		event.preventDefault();
		$(this).parent().toggleClass('open');
		return false;
	});

        $("body").on('click', '.jobs-list .items .item .name', function (event) {
		event.preventDefault();
		$(this).parent().toggleClass('open');
		return false;
    });
    $('body').on('click', function (e) {
        if (!($(e.target).is('#title-search-input') || $(e.target).parents('#title-search-input').length || $(e.target).is('#title-search-input') || $(e.target).parents('title-search-result').length)) {
            $('.title-search-result').removeClass('opened');
            $('body').removeClass('search-opened')
        }
    })
    $('#title-search-input').on('input click', function(){
        if ($('#title-search-input').val() != '') {
            $('.title-search-result').addClass('opened')
            $('body').addClass('search-opened');
        }
        else {
            $('.title-search-result').removeClass('opened');
            $('body').removeClass('search-opened')
        }
    })
        

        $("body").on('click', '.accordeon .name', function (event) {
		event.preventDefault();
		$(this).parent().toggleClass('open');
		return false;
	});
        
        $("body").on('click', '.sort .filter-btn', function (event) {
		event.preventDefault();
		$(this).toggleClass('open');
		$('.filter').toggleClass('open');
		return false;
    });

    // $(window).on('scroll', function(){
    //     if ($(this).scrollTop() > 1) {
    //         $('.header-mobile.floating').addClass('moving');
    //     }
    //     else {
    //         $('.header-mobile.floating').removeClass('moving');
    //     }
    // });

    var headerMobile = $('header'),
        scrollPrev = 0;

    $(window).scroll(function () {
        var scrolled = $(window).scrollTop();

        if ($(this).width() < 481) {
            if (scrolled > 137 && scrolled > scrollPrev) {
                headerMobile.removeClass('up');
                headerMobile.addClass('down');
                // headerMobile.removeClass('up');
            } else if (scrolled < 138) {
                headerMobile.removeClass('up');
                headerMobile.removeClass('down');
            } else if (scrolled < scrollPrev) {
                headerMobile.removeClass('down');
                headerMobile.addClass('up'); 
            }
            scrollPrev = scrolled;

        }

    });


    $(window).on('scroll', function(){
        if ($(this).scrollTop() > 137 && $(this).width() < 481) {
            $('header').addClass('moving');
        }
        else {
            $('header').removeClass('moving');
        }

        // if ($(this).scrollTop() > 60 && $(this).width() < 481) {
        //     $('header .middle .search').css('display', 'none');
        // }
        // else {
        //     $('header .middle .search').css('display', 'block');
        // }
    });

    $('.header-mobile-menus.menu-2-0 .close-area').on('click', function(){
        $(this).parent().removeClass('open');
        $('html').removeClass('block-scroll');
    });

    $(".header-mobile-menus.menu-2-0").swipe({
        swipeLeft: function (event, direction, distance, duration, fingerCount, fingerData) {
           $(this).removeClass('open');
           $('html').removeClass('block-scroll');
        }
    });

    // $("body").swipe({
        
    //     swipeRight: function (event, direction, distance, duration, fingerCount, fingerData) {
    //         if (distance > 250) {
    //             $('.header-mobile-menus.menu-2-0').addClass('open');
    //             $(this).addClass('block-scroll');
    //         }
            
    //     }
    // });

    $('.product-detail .feedback .text').on('click', function(){
        $(this).parent().toggleClass('open');
    });

    // $('.header-mobile .burger').on('click', function(){
    //     $('body').addClass('block-scroll');
    //     $(this).parents('header').find('.header-mobile-menus.menu-2-0').addClass('open');
    // });
    $('header .burger').on('click', function(){
        $('html').addClass('block-scroll');
        $(this).parents('header').find('.header-mobile-menus.menu-2-0').addClass('open');
    });
    $('.header-mobile-menus.menu-2-0 .list .item.has-sub a').on('click', function(){
        if (!$(this).parents('.list').hasClass('open-sub'))
            $(this).parents('nav').addClass('moved');
        else
            $(this).parents('nav').removeClass('moved');
    });
	$('.catalog-mobile-menu .content nav .head').swipe( {
		longTap:function(event, target) {
			$(target).find('.burger-button').toggleClass('open');
			$(target).parents('.catalog-mobile-menu').toggleClass('open');
			$(target).parents('.catalog-mobile-menu').find('nav .list>.item.has-sub').removeClass('open');
			$(target).parents('.catalog-mobile-menu').find('.list').removeClass('open-sub');
		},
		longTapThreshold: 20
	});

    $('body').on('click', '.header-mobile-menus:not(".menu-2-0") .catalog-mobile-menu .content nav .list>.item.has-sub>a', function (event) {
        event.preventDefault();
        $(this).parent().addClass('open');
        $(this).parents(".list").addClass('open-sub');
        return false;
    });

    $('body').on('click', '.header-mobile-menus:not(".menu-2-0") .catalog-mobile-menu.open nav .sub .items .item.back a', function (event) {
        event.preventDefault();
        $(this).parents('.item.has-sub').removeClass('open');
        $(this).parents(".list").removeClass('open-sub');
        return false;
    });
        
        $("body").on('click', 'header .content-mobile-menu a', function (event) {
            var sub = $(this).parent().find('>.sub-menu');
            if (sub.length > 0 && !$(this).parent().hasClass('mobile-open')) {
                event.preventDefault();
                $(this).parent().addClass('mobile-open');
                return false;
            }

        });
        
        BX.addCustomEvent(window, 'OnCompareChange', BX.proxy(this.refreshHeaderCompare));
        
        if (window.arOptions['buy1click_enabled'] == 'Y') {
            $("body").on('click', '[buy1click]', function (event) {
                
                    event.preventDefault();	
                    $('.popup').jqmHide();
                    jqmPopup('buy1click', 'include/ajax/buy_1_click.php?id=' + $(this).data('id') + '&qty=' + $(this).data('qty'), true);
                    return false;
                
            });
        }
        
        if (window.arOptions['fastview_enabled'] == 'Y') {
            $('body').on('click', '[data-role="product-fastview"]', function (event) {
                if (document.body.clientWidth > 480) {
                    
                    event.preventDefault();
                    var url = $(this).data('url');
                    if (url !== undefined) {
                        url = url.indexOf('?') != -1  ? url + '&is_fast_view=Y' : url + '?is_fast_view=Y';
                        url = (document.location.href.indexOf('clear_cache=Y') !== -1 ? url + '&clear_cache=Y' : url)
                        $('.popup').jqmHide();
                        jqmPopup('fastview', url, true, true);
                    }
                    return false;
                }
            });
        }
        
        if (window.arOptions['cookies_agreement_enabled'] == 'Y')
        {
            if (BX.getCookie('cookies_agreement') != "Y") {
                $('body').on('click', '.cookie .agree', function(event){
                    $('.cookie').removeClass('show').addClass('hide');
                    BX.setCookie('cookies_agreement', 'Y', {expires: 86400, path: BX.message('SITE_DIR')});
                });

                $(document).ready(function() {
                        $('.cookie').addClass('show');
                });
            }
        }
        
        if (window.arOptions['scroll_top_button'] == "Y")
        {
            $(window).on('mousewheel DOMMouseScroll', function (e) {
                var direction = (function () {

                    var delta = (e.type === 'DOMMouseScroll' ?
                                 e.originalEvent.detail * -40 :
                                 e.originalEvent.wheelDelta);

                    return delta > 0 ? 0 : 1;
                }());

                if(direction === 1) {
                   // scrolling down
                   $("#scroll-top-button").addClass('show');
                } else {
                    // scrolling up
                    $("#scroll-top-button").removeClass('show');
                }
                
            });
        }
        
        if (window.arOptions['ya_counter_id'] != '') {
            this.initYandexReachGoal();
        }
        
        jqmPopup('callback', 'include/ajax/form.php?id=callback');
        jqmPopup('question', 'include/ajax/form.php?id=question');
        
    },
    
    initYandexReachGoal: function () {
        var clickEvents = [
            {
                selector: '.call-me a',
                event: 'header_callback_open' 
            },
            {
                selector: '.question-popup-btn',
                event: 'question_open' 
            },
            
            {
                selector: '.product-item-button-container .btn',
                event: 'catalog_to_basket' 
            },
            {
                selector: '.additional-links .compare',
                event: 'catalog_compare' 
            },
            {
                selector: '.additional-links .wishlist',
                event: 'catalog_wishlist' 
            }
            
        ],
        formSubmitEvents = [
            {
                selector: 'form[name="NT_MAGNET_CALLBACK"]',
                event: 'header_callback_submit' 
            },
            {
                selector: 'form[name="NT_MAGNET_PRODUCT_QUESTION"]',
                event: 'product_question_submit' 
            },
            {
                selector: 'form[name="NT_MAGNET_QUESTION"]',
                event: 'question_submit' 
            }
        ];
        
        $.each (clickEvents, function (index, arEvent) {
            $(arEvent['selector']).on('click', function (event) {
                if (window.arOptions['ya_counter_id'] != '') {
                    var obYacounter = window['yaCounter' + window.arOptions['ya_counter_id']];
                    if (typeof(obYacounter) == 'object' && typeof(obYacounter['reachGoal']) == 'function' && arEvent['event'] != '') {
                        obYacounter.reachGoal(arEvent['event']);
                        console.log('ReachGoal: ' + arEvent['event']);
                    }
                    
                }
            });
        });
        
        $.each (formSubmitEvents, function (index, arEvent) {
            $("body").on('submit', arEvent['selector'], function (event) {
                if (window.arOptions['ya_counter_id'] != '') {
                    var obYacounter = window['yaCounter' + window.arOptions['ya_counter_id']];
                    if (typeof(obYacounter) == 'object' && typeof(obYacounter['reachGoal']) == 'function' && arEvent['event'] != '') {
                        obYacounter.reachGoal(arEvent['event']);
                        console.log('ReachGoal: ' + arEvent['event']);
                    }
                }
            });
        });
        
        
    },
    
    initBasket: function (options) {
        var self = this, options = (options) || false;
        BX.addCustomEvent(window, 'OnBasketChange', BX.proxy(this.refreshHeaderBasket, options));
        
        if (window.arOptions['basket_show_popup'] == 'Y') {
            BX.addCustomEvent(window, 'OnBasketSuccessAdd', BX.proxy(this.showBasketPopup, options));
        }
    },
    
    resizeEvent: function () {
        $('.instagram:not(a) .items .item').height($('.instagram:not(a) .items .item').width());
        if(document.documentElement.clientWidth > 1024) {
            $('[data-lazy="mobile-off"]').lazy({});
	}  
    },
    
    setAjaxLoader: function (obContainer) {
        if (typeof(obContainer) == 'object') {
            obContainer.css('position', 'relative');
            obContainer.append('<div class="ajax-loader"></div>');
        }
    },
    
    deleteAjaxLoader: function (obContainer) {
        
    },
    
    refreshHeaderBasket: function () {
        var self = this;
        if (typeof (self) == 'object') {
            var obCart = $("#" + self.cartId);
            
            if (obCart.hasClass('fly-basket-container') && obCart.hasClass('open')) {
                window.CSolution.setAjaxLoader(obCart.find(".tabs-content"));
            }
            
            BX.ajax({
                url: self.ajaxPath,
                method: 'POST',
                dataType: 'html',
                data: {
                    sessid: BX.bitrix_sessid(),
                    siteId: self.siteId,
                    templateName: self.templateName,
                    arParams: self.arParams
                },
                onsuccess: function (data) {
                    if (data.length > 0)
                         BX(self.cartId).innerHTML = data;
                }
            });
        }
        
    },
    
    refreshHeaderCompare: function () {
        BX.ajax({
            url: BX.message('SITE_DIR') + 'catalog/compare.php?is_ajax_compare_count=y',
            method: 'GET',
            dataType: 'html',
            onsuccess: function (data) {
                if (data.length > 0)
                     BX('header-compare-result').innerHTML = '(' + $.trim(data) + ')';
            }
        });
    },
    
    showBasketPopup: function () {
        var self = this;
        $('.popup').jqmHide();
        var popup = $($("#popup-basket-add").html());
        popup.find("[data-close]").on('click', function () {$(this).parents('.popup-basket-add').remove();});
        popup.find('.overflow').on('click', function () {$(this).parents('.popup-basket-add').remove();});
        $("body").append(popup);
    },
    
    ajaxLoadBlocks: function () {
        
        if (document.location.href.indexOf('show_skeleton=y') === -1)
        {
            $('.ajax-block').lazy({

                ajaxBlock: function (element) {
                    var url = element.data('ajax-url');

                    if (url !== undefined) {
                        BX.ajax({
                            url: url + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '&clear_cache=Y' : ''),
                            method: 'GET',
                            timeout: 30,
                            onsuccess: BX.delegate(function (result) {
                                if (!result)
                                    return;

                                var ob = BX.processHTML(result);
                                this.classList.add('loaded');
                                BX(this).innerHTML = ob.HTML;
                                BX.ajax.processScripts(ob.SCRIPT, true);
                                BX.onCustomEvent("onAjaxSuccess");

                            }, element.get(0))
                        });
                    }
                }
            });
        }
    },
    
    showMessage: function (message) {
        alert(message);
    },
    
    setProductsHeight: function(items) {
        if (document.body.clientWidth < 768)
            return;
        if (items.length > 0) {
            var maxHeight = 0,
                maxHeightStatic = 0;
            items.each(function() {
                $(this).find('.product-card').removeClass('ready');
                $(this).height('auto');
                $(this).removeClass('swiper-slide-ready');
                $(this).find('.static').height('100%');

                var itemHeight = parseInt($(this).height());
                var itemHeightStatic = parseInt($(this).find('.static').height());
                if (itemHeight > maxHeight) {
                    maxHeight = itemHeight;
                }
                if (itemHeightStatic > maxHeightStatic) {
                    maxHeightStatic = itemHeightStatic;
                }
            });
            items.each(function() {
                $(this).height(maxHeight);
                $(this).find('.static').height(maxHeightStatic);
                $(this).find('.product-card').addClass('ready');
                if ($(this).hasClass('swiper-slide')){
                    $(this).addClass('swiper-slide-ready');
                }
            });
        }
    },
    setProductsHeight1: function(items) {
        if (items.length > 0) {
    	var maxHeight = 0,
    	    maxHeightStatic = 0;
		items.each(function() {
       		$(this).height('auto');
       		$(this).find('.static').height('100%');

			var itemHeight = parseInt($(this).height());
			var itemHeightStatic = parseInt($(this).find('.static').height());
			if (itemHeight > maxHeight) {
				maxHeight = itemHeight;
			}
			if (itemHeightStatic > maxHeightStatic) {
				maxHeightStatic = itemHeightStatic;
            }
        });
       
		items.each(function() {
			$(this).height(maxHeight);
            $(this).find('.static').height(maxHeightStatic);
            $(this).closest('.swiper-container').height(maxHeight);
            $(this).closest('.items').find('.categories-block').height(maxHeight);
		});
            }
    },
    
    setContainerHeight: function(items) {
        let maxHeight = 0;
        $(items).closest('.swiper-container').height('auto');
        items.each(function () {
            let itemHeight = parseInt($(this).height());
            if (itemHeight > maxHeight) {
                maxHeight = itemHeight;
            }
        });
        items.each(function () {
            $(this).closest('.swiper-container').height(maxHeight * 2 + 40);
        });
    },

    setOffersHeight: function(items) {
        if (items.length > 0) {
    	var maxHeight = 0,
    	    maxHeightStatic = 0;
		items.each(function() {                   
			var itemHeight = parseInt($(this).height());
			var itemHeightStatic = parseInt($(this).find('.static').height());
			if (itemHeight > maxHeight) {
				maxHeight = itemHeight;
			}
			if (itemHeightStatic > maxHeightStatic) {
				maxHeightStatic = itemHeightStatic;
			}
		});
		items.each(function() {
			$(this).height(maxHeight);
			$(this).find('.static').height(maxHeightStatic);
		});
            }
    },

    setProductsStaticHeight: function(items) {
		items.each(function() {
			var itemHeight = $(this).height();
			$(this).height(itemHeight);
		});
    },
    setLightCategoriesHeight: function(items) {
		items.each(function() {
            var itemsHeight = $(this).height();
            if (itemsHeight > 43) {
                $('.catalog .light-categories.more-list .show-more-cats').css('display', 'flex');
            } else {
                $('.catalog .light-categories.more-list .show-more-cats').css('display', 'none');
            }
		});
    },
    
    waitResize: function (func, wait, immediate) {
        var timeout;
        return function() {
          var context = this, args = arguments;
          var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
    },
    
    getTimeRemaining: function (endtime) {
        var exEndtime = endtime.split(/[^0-9]/);
        var t = Date.parse(new Date(exEndtime[0],exEndtime[1]-1,exEndtime[2])) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        return {
            'total': t,
            'days': days < 10 ? '0' + days : days,
            'hours': hours < 10 ? '0' + hours : hours,
            'minutes': minutes < 10 ? '0' + minutes : minutes,
            'seconds': seconds < 10 ? '0' + seconds : seconds
        };
    },
    
    onSubmitReCaptcha: function(form = false, callback = false) {
        if (!form)
            return;
        if (typeof form == 'string')
            form = $("#"+form);
            
        let formName = form.name, hasEmptyFields = false;
        if (!formName)
            formName = $(form).attr("name");
        
        $(form).find("[required='required']").each((index, item) => {
            if (!$(item).val())
                hasEmptyFields = true;
        })
        
        if (hasEmptyFields) {
            return false;
        }

        if (!!grecaptcha && !!window.arOptions.grecaptcha.active)
        {
            if (!!event)
                event.preventDefault();
            this.sendedWebFormName = formName;
            if (window.arOptions.grecaptcha.type == 'scoring')
            {
                let inputNode = $(form).find('input[name="g-recaptcha-response"]');
                if (inputNode && !$(inputNode).val()) {
                    grecaptcha.execute(window.arOptions.grecaptcha.key.toString(), {action: 'submit'}).then((token) => {
                        inputNode.val(token);
                        if (window.arOptions.grecaptcha.send == 0) {
                            window.arOptions.grecaptcha.send = 1;
                            if (formName.indexOf("buy1click_") !== -1) {
                                $(form).find("[type='submit']").attr('disabled', 'disabled').addClass('loading');
                                $.ajax({
                                    url: $(form).attr('action'),
                                    data: $(form).serialize() + '&is_ajax=y',
                                    type: 'post',
                                    success: function (data) {
                                        $("#main_buy1click_form").html(data);
                                        $("#main_buy1click_form").find(".is-phone").mask(window.arOptions['phone_mask']);
                                    }
                                });
                            }
                            else {
                                $(form).find("button[type=\"submit\"]").trigger("click");
                            }
                        }
                        setTimeout(() => {
                            window.arOptions.grecaptcha.send = 0;
                        }, 3000);
                    });
                }
            }
            else
            {
                grecaptcha.execute();
            }

            if (typeof(callback) == "function")
            {
                this.grecaptchaWaiterCallback = callback;
            }
            return false;
        }
    },
    
    sendedWebFormName: '',
    grecaptchaWaiterCallback: ''

};

$(document).ready(function() {

        BX.addCustomEvent('onAjaxSuccess', function (data) {
            if (!!data)
                return;
            $("[id*='_recaptcha']").each(function () {
                var sitekey = $(this).data('sitekey');
                if (sitekey !== undefined && $(this).empty() && typeof(window.grecaptcha) == 'object') {
                    try {
                        window.grecaptcha.render($(this).attr('id'));
                    } catch (e) {
                        window.grecaptcha.reset();
                    }
                }

            });
            $('[data-lazy="default"]').lazy({});
            
            window.onpopstate = function(event) {
                setTimeout(function () {

                    $('[data-lazy-loaded="true"]').css('opacity', '1');

                    var instance = $('[data-lazy="default"]').Lazy({
                        chainable: false,
                        bind: "event"
                    });

                    instance.loadAll();
                }, 500);

            };
            
            window.CSolution.initScrollbar();

            // bugfix for ajax mode and location replace
            if (typeof(window.arOptions['region_variables']) == "object") {
                $.each(window.arOptions['region_variables'], function (code, value) {
                    if ($("#navigation").length > 0) {
                        $("#navigation").html($("#navigation").html().replace(new RegExp("#" + code + "#",'g'), value));
                    }
                    
                    if ($("#pagetitle").length > 0) {
                        $("#pagetitle").html($("#pagetitle").html().replace(new RegExp("#" + code + "#",'g'), value));
                    }
                });
            }
        });

        lightbox.option({
            'showImageNumberLabel': false
        });

    $('[data-lazy="default"]').lazy({});

    if(document.documentElement.clientWidth > 1024) {
    	$('[data-lazy="mobile-off"]').lazy({});
    }    
    
    $('.product-img .thumbnails .video').YouTubePopUp();

    $('.tel-container.has-content .tel a').off('click');
 
    window.CSolution.construct();
    window.CSolution.initEvents();
    window.CSolution.ajaxLoadBlocks();
});

$(window).resize(function() {
    window.CSolution.resizeEvent();
    window.CSolution.setContainerHeight($('.products .swiper-slide'));
    window.CSolution.setProductsHeight($('.products .swiper-slide'));
    $('.offer-wrap.swiper-slide .static').css('height', $('.offer-wrap.swiper-slide .item').height());
});

var onResponseReCaptcha = function(token) {
    if (!token)
        return;

    if (window.CSolution.sendedWebFormName.length) {
        if (window.arOptions.grecaptcha.type == 'invisible') {
            $('form[name="'+window.CSolution.sendedWebFormName+'"]').find('input[name="g-recaptcha-response"]').val(token);
        }
        $('form[name="'+window.CSolution.sendedWebFormName+'"]').find('button[type="submit"]').trigger('click');
        window.CSolution.sendedWebFormName = '';
        
        if (!!window.CSolution.grecaptchaWaiterCallback) {
            window.CSolution.grecaptchaWaiterCallback(token);
        }
        return;
    }

    if (!!BX.Sale.OrderAjaxComponent) {
        BX.Sale.OrderAjaxComponent.onResponseReCaptcha(token);
    }
    
}