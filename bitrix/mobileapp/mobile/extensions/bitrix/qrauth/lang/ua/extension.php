<?php
$MESS["AUTH_WAIT"] = "Авторизуємо...";
$MESS["BROWSERS"] = "#1# Google Chrome, Microsoft Edge, Safari,
Mozilla Firefox, Opera та інші
  ";
$MESS["GET_MORE"] = "Отримайте ще більше можливостей для вашого бізнесу
в повній версії Бітрікс24.";
$MESS["OPEN_BROWSER"] = "Відкрийте [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] на комп'ютері
";
$MESS["SCAN_QR"] = "Наведіть камеру телефону на [B]QR-код[/B]
";
$MESS["WRONG_QR"] = "Неправильний QR-код";
