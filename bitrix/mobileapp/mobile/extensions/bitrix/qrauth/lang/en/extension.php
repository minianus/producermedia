<?php
$MESS["AUTH_WAIT"] = "Logging you in...";
$MESS["BROWSERS"] = "#1# Google Chrome, Microsoft Edge, Safari, Mozilla Firefox, Opera and other browsers
  ";
$MESS["GET_MORE"] = "Get even more business tools in the full version of Bitrix24.";
$MESS["OPEN_BROWSER"] = "Open [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] on your computer";
$MESS["SCAN_QR"] = "Scan the [B]QR code[/B] with your mobile camera";
$MESS["WRONG_QR"] = "QR code is incorrect";
