<?php
$MESS["AUTH_WAIT"] = "Sie werden eingeloggt";
$MESS["GET_MORE"] = "Genießen Sie noch mehr Business Tools in der Vollversion von Bitrix24.";
$MESS["OPEN_BROWSER"] = "Öffnen Sie [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] auf Ihrem Computer
";
$MESS["SCAN_QR"] = "Scannen Sie den [B]QR-Code[/B] mit Ihrer mobilen Kamera ein
";
$MESS["WRONG_QR"] = "QR-Code ist nicht korrekt";
