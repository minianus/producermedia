<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;


$aMenuLinksExt = $APPLICATION->IncludeComponent(
   "nextype:magnet.menu.sections",
   "",
   Array(
       "IS_SEF" => "Y",
       "DEPTH_LEVEL" => "3",
        "SEF_BASE_URL" => "",
        "SECTION_PAGE_URL" => "/catalog/#SECTION_CODE_PATH#/",
        "DETAIL_PAGE_URL" => "/catalog/#SECTION_CODE_PATH#/#ELEMENT_ID#",
      "IBLOCK_TYPE" => "nt_magnet_catalog", 
      "IBLOCK_ID" => "19", 
      "CACHE_TIME" => "3600000" 
   )
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);