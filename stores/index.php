<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.store", 
	"main", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"MAP_TYPE" => "0",
		"PHONE" => "Y",
		"SCHEDULE" => "Y",
		"SEF_FOLDER" => "/",
		"SEF_MODE" => "Y",
		"SET_TITLE" => "Y",
		"TITLE" => "",
		"COMPONENT_TEMPLATE" => "main",
		"SEF_URL_TEMPLATES" => array(
			"liststores" => "stores/",
			"element" => "stores/#store_id#/",
		),
        "MORE_PHOTO" => "0"
	),
	false
);?><br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>