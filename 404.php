<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
CHTTP::SetStatus('404 Not Found');
$APPLICATION->SetTitle("Страница не найдена");
?>

<div class="error-404">
	<div class="content">
		<div class="img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/404.svg" alt="Страница не найдена"></div>
		<div class="name">Упс... Ошибка</div>
		<div class="text">
			<p>Такой страницы не существует, либо она была удалена.</p>
			<p>Чтобы найти нужную страницу воспользуйтесь <a href="<?=SITE_DIR?>catalog/">поиском</a> или перейдите в каталог.</p></div>
		<div class="buttons">
			<a href="<?=SITE_DIR?>catalog/" class="btn">Перейти в каталог</a>
			<a href="<?=SITE_DIR?>" class="btn transparent">Перейти на главную</a>
		</div>
	</div>
</div>

<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>