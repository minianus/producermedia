<?
define('bAsideRight', true);
define('bContentPage', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Возможности");
?>
<p>На данной странице представлены способы оформления контента сайта.</p>
<hr>
<h3>Заголовки</h3>
<div class="code-content">
<code>&#60;h2&#62;Заголовок h2&#60;/h2&#62;</code>
<h2>Заголовок h2</h2>
</div>
<div class="code-content">
<code>&#60;h3&#62;Заголовок h3&#60;/h3&#62;</code>
<h3>Заголовок h3</h3>
</div>
<div class="code-content">
<code>&#60;h4&#62;Заголовок h4&#60;/h4&#62;</code>
<h4>Заголовок h4</h4>
</div>
<div class="code-content">
<code>&#60;h5&#62;Заголовок h5&#60;/h5&#62;</code>
<h5>Заголовок h5</h5>
</div>
<div class="code-content">
<code>&#60;h6&#62;Заголовок h6&#60;/h6&#62;</code>
<h6>Заголовок h6</h6>
</div>
<hr>
<h3>Списки</h3>
<h4 class="code-subtitle">Маркрированный список</h4>
<div class="code-content">
<code>
	&#60;ul&#62;<br>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	&#60;/ul&#62;
</code>
<ul>
	<li>Элемент</li>
	<li>Элемент</li>
	<li>Элемент</li>
</ul>
</div>
<h4 class="code-subtitle">Нумерованный список</h4>
<div class="code-content">
<code>
	&#60;ol&#62;<br>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	<pre>  &#60;li&#62;Элемент&#60;/li&#62;<br></pre>
	&#60;/ol&#62;
</code>
<ol>
	<li>Элемент</li>
	<li>Элемент</li>
	<li>Элемент</li>
</ol>
</div>
<hr>
<h3>Таблица</h3>
<div class="code-content">
	<code>
		&#60;table&#62;<br>
		<pre>  &#60;tbody&#62;<br></pre>
		<pre>    &#60;tr&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>    &#60;/tr&#62;<br></pre>
		<pre>    &#60;tr&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>    &#60;/tr&#62;<br></pre>
		<pre>    &#60;tr&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>      &#60;td&#62;Ячейка&#60;/td&#62;<br></pre>
		<pre>    &#60;/tr&#62;<br></pre>
		<pre>  &#60;/tbody&#62;<br></pre>
		&#60;/table&#62;<br>
	</code>
	<table>
		<tbody>
			<tr>
				<td>Ячейка</td>
				<td>Ячейка</td>
				<td>Ячейка</td>
			</tr>
			<tr>
				<td>Ячейка</td>
				<td>Ячейка</td>
				<td>Ячейка</td>
			</tr>
			<tr>
				<td>Ячейка</td>
				<td>Ячейка</td>
				<td>Ячейка</td>
			</tr>
		</tbody>
	</table>
</div>
<hr>
<h3>Оформление текста</h3>
<div class="code-content">
	<code>&#60;b&#62;Полужирный текст&#60;/b&#62;</code>
	<p><b>Полужирный текст</b></p>
</div>
<div class="code-content">
	<code>&#60;i&#62;Курсивный текст&#60;/i&#62;</code>
	<p><i>Курсивный текст</i></p>
</div>
<div class="code-content">
	<code>&#60;a href="/здесь-ссылка/"&#62;Ссылка&#60;/a&#62;</code>
	<p><a href="#">Ссылка</a></p>
</div>
<hr>
<h3>Цитата</h3>
<code>&#60;blockquote&#62;Тут размещается длинный текст цитаты&#60;/blockquote&#62;</code>
<blockquote>Тут размещается длинный текст цитаты</blockquote>
<hr>
<div class="bootstrap">
<h3>Сетка Bootstrap</h3>
<p class="desc">Для удобства доработки шаблона мы добавили поддержку сетки Bootstrap. Наименование классов аналогично официальным, описанным в <!-- noindex --><a href="http://bootstrap-4.ru/docs/4.2.1/layout/grid/" rel="nofollow" target="_blank">документации</a><!-- /noindex -->. Но для всех классов добавляется префикс <code>bs-</code>. Например, <code>bs-container</code> вместо <code>container</code> или <code>bs-row</code> вместо <code>row</code>.</p>
<code>
	&#60;div class="bs-container"&#62;<br>
	<pre>  &#60;div class="bs-row"&#62;<br></pre>
	<pre>    &#60;div class="bs-col"&#62;bs-col&#60;/div&#62;<br></pre>
	<pre>    &#60;div class="bs-col"&#62;bs-col&#60;/div&#62;<br></pre>
	<pre>    &#60;div class="bs-col"&#62;bs-col&#60;/div&#62;<br></pre>
	<pre>    &#60;div class="bs-col"&#62;bs-col&#60;/div&#62;<br></pre>
	<pre>  &#60;/div&#62;</pre>
	<pre>  &#60;div class="bs-row"&#62;<br></pre>
	<pre>    &#60;div class="bs-col-8"&#62;bs-col-8&#60;/div&#62;<br></pre>
	<pre>    &#60;div class="bs-col-4"&#62;bs-col-4&#60;/div&#62;<br></pre>
	<pre>  &#60;/div&#62;</pre>
	&#60;/div&#62;
</code>
<div class="bs-container">
  <div class="bs-row">
    <div class="bs-col">bs-col</div>
    <div class="bs-col">bs-col</div>
    <div class="bs-col">bs-col</div>
    <div class="bs-col">bs-col</div>
  </div>
  <div class="bs-row">
    <div class="bs-col-9">bs-col-9</div>
    <div class="bs-col-3">bs-col-3</div>
  </div>
</div>
</div>
<hr>
<h3>Аккордеон</h3>
<code>
	&#60;div class="accordeon"&#62;<br>
	<pre>  &#60;a href="#" class="name"&#62;Заголовок аккордеона&#60;/a&#62;<br></pre>
	<pre>  &#60;div class="text"&#62;Большое описание, но скрытое.&#60;/div&#62;<br></pre>
	&#60;/div&#62;<br>
	&#60;div class="accordeon"&#62;<br>
	<pre>  &#60;a href="#" class="name"&#62;Заголовок аккордеона&#60;/a&#62;<br></pre>
	<pre>  &#60;div class="text"&#62;Большое описание, но скрытое.&#60;/div&#62;<br></pre>
	&#60;/div&#62;
</code>
<div class="accordeon">
	<a href="#" class="name">Заголовок аккордеона</a>
	<div class="text">Большое описание, но скрытое.</div>
</div>
<div class="accordeon">
	<a href="#" class="name">Заголовок аккордеона</a>
	<div class="text">Большое описание, но скрытое.</div>
</div>
<hr>
<h3>Горизонтальная линия</h3>
<hr>
<code>&#60;hr&#62;</code>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>