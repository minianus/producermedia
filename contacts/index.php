<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Контакты");
?>
<main class="contacts">
    <?
    $APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"PATH" => "page_blocks/map.php",
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => "standard.php",
			),
			false
		);
    ?>
    <? @include(__DIR__ . "/page_blocks/contacts.php") ?>
    
    <? @include(__DIR__ . "/page_blocks/offices.php") ?>
    
    
    
</main>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>