<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

if (CSolution::$options['LOCATIONS_ENABLED'] == "Y")
    $arPhones = $GLOBALS['arCurrentRegion']['PROPERTIES']['ALL_PHONES']['VALUE'];
else
    $arPhones = !empty(CSolution::$options['CONTACT_PHONES']) ? unserialize(CSolution::$options['CONTACT_PHONES']) : Array();
?>

<div itemscope itemtype="http://schema.org/Organization" data-helper="contacts::info">
    <meta itemprop="name" content="<?=strip_tags(CSolution::$options['SITE_NAME'])?>" />
    <div class="info" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

        <div class="item icon-custom main-address">
            <div class="text">
                <div class="name">Наш адрес</div>
                <div class="content" itemprop="streetAddress"><?=CSolution::ShowAddress(); ?></div>
            </div>
        </div>
        <div class="item icon-custom phone">
            <div class="text">
                <div class="name">Контактный телефон </div>
                <div class="content">
                    <? foreach ($arPhones as $phone): ?>
                    <a href="tel:<?=CSolution::phone2int($phone)?>" itemprop="telephone"><?=$phone?></a><br/>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <div class="item icon-custom email">
            <div class="text">
                <div class="name">E-mail</div>
                <div class="content"><a href="mailto:<?=CSolution::ShowEmail() ?>" itemprop="email"><?=CSolution::ShowEmail() ?></a></div>
            </div>
        </div>
        <a href="javascript:void(0);" class="question-popup-btn btn">Задать вопрос</a>
    </div>
</div>