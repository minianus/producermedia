<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CLocations;

$arRegionStores = false;
CSolution::getInstance(SITE_ID);
if (CSolution::$options['LOCATIONS_ENABLED'] == "Y")
{
    
    $arRegion = CLocations::getCurrentRegion();
    if (!empty($arRegion['PROPERTIES']['STORE']['VALUE']) && !in_array("COMPONENT", $arRegion['PROPERTIES']['STORE']['VALUE']))
          $arRegionStores = $arRegion['PROPERTIES']['STORE']['VALUE'];
    
}

if (CSolution::$options['STORES_IN_CONTACTS'] == "Y"): ?>
<div class="offices" data-helper="contacts::offices">
		<h2 class="title">
                    
                    <?
                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                                "PATH" => "page_blocks/offices_title.php",
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "",
                                                "AREA_FILE_RECURSIVE" => "Y",
                                                "EDIT_TEMPLATE" => "standard.php",
                                        ),
                                        false
                                );
                    ?>
                </h2>
		<div class="desc">
                    <?
                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                                "PATH" => "page_blocks/offices_description.php",
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "",
                                                "AREA_FILE_RECURSIVE" => "Y",
                                                "EDIT_TEMPLATE" => "standard.php",
                                        ),
                                        false
                                );
                    ?>
                    
                </div>

                <?
                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                                "PATH" => "page_blocks/offices_list.php",
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "",
                                                "AREA_FILE_RECURSIVE" => "Y",
                                                "EDIT_TEMPLATE" => "standard.php",
                                                "STORES" => $arRegionStores
                                        ),
                                        false
                                );
                    ?>
    
                
    
		
	</div>
<? endif; ?>