<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? if(!CModule::IncludeModule("nextype.magnet"))
    die('Error include nextype.magnet module'); 

$APPLICATION->IncludeComponent("nextype:magnet.buy1click","main",Array(
    "PRODUCT_ID" => intval($_REQUEST['id']),
    "QUANTITY" => intval($_REQUEST['qty']) > 1 ? intval($_REQUEST['qty']) : 1
) );

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>