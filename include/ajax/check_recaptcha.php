<?php
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
define('NOT_CHECK_PERMISSIONS', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$token = $request->get('token');
$sid = $request->get('sid');
if (\Bitrix\Main\Loader::includeModule('nextype.magnet') && $request->isAjaxRequest())
{
    $token = preg_replace("/[^a-zA-Z0-9-_=]/", "", $token);
    if (empty($token))
        die('Empty token');
    
    $code = \Nextype\Magnet\CRecaptcha::checkCode(array(
        "captcha_sid" => $sid,
        "g-recaptcha-response" => $token
    ));
    echo \Bitrix\Main\Web\Json::encode(array(
        'valid' => !!$code,
        'code' => $code
    ));
    die();
}
