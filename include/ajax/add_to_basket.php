<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$id = (int)$request->get('id');
$res = array('success' => false);

if ($request->isAjaxRequest() && $id > 0
        && \Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('catalog'))
{
    $minQty = \Bitrix\Catalog\MeasureRatioTable::getList([
	'limit' => 1,
	'select' => ['RATIO'],
	'filter' => ['PRODUCT_ID' => $id]
    ])->fetch()['RATIO'];
    
    $minQty = ($minQty) ? $minQty : 1;
    
    $siteId = $context->getSite();
    $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), $siteId);
    $item = $basket->createItem('catalog', $id);
    $item->setFields(array(
        'QUANTITY' => $minQty,
        'LID' => $siteId,
        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
    ));
    $basket->save();
    $res['success'] = true;
}
echo \Bitrix\Main\Web\Json::encode($res);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");