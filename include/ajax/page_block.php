<?
use Nextype\Magnet\CSolution;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("nextype.magnet"))
    die('Error include nextype.magnet module');

CSolution::getInstance(SITE_ID);

if (!empty($_REQUEST['action']))
    $path = SITE_DIR."/include/page_blocks/homepage/products.php";
else
    $path = trim(strip_tags($_REQUEST['path']));

if (!$path)
    die('Empty block path');

$params = !empty($_REQUEST['params']) && @unserialize($_REQUEST['params']) ? @unserialize($_REQUEST['params']) : Array ();

$APPLICATION->ShowAjaxHead();

CSolution::IncludeFile($path, $params, Array ("SHOW_BORDER" => false));



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");



