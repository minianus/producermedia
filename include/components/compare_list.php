<?
$bAjaxMode = ($_REQUEST['is_ajax_mode'] == 'y') ? true : false;
if ($bAjaxMode)
{
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}

if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
    die();

\Nextype\Magnet\CSolution::getInstance(SITE_ID);

$arComponentParams = (!empty($_REQUEST['ajax_component_params'])) ? unserialize(urldecode($_REQUEST['ajax_component_params'])) : $arComponentParamsTmp;
$sComponentTemplate = (!empty($sComponentTemplateTmp)) ? $sComponentTemplateTmp : 'main';
$sComponentTemplate = (!empty($_REQUEST['ajax_component_template'])) ? urldecode($_REQUEST['ajax_component_template']) : $sComponentTemplate;

if (!empty($_REQUEST['ajax_component_filter']))
    $GLOBALS[$arComponentParams['FILTER_NAME']] = (!empty($_REQUEST['ajax_component_filter'])) ? unserialize(urldecode($_REQUEST['ajax_component_filter'])) : Array ();

$APPLICATION->IncludeComponent(
        "bitrix:catalog.compare.list", $sComponentTemplate, $arComponentParams, false, array("HIDE_ICONS" => "Y")
);
if ($bAjaxMode)
{
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>