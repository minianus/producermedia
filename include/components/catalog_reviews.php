<?
use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;

$bAjaxMode = ($_REQUEST['is_ajax_mode'] == 'y') ? true : false;
if ($bAjaxMode)
{
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    
    if (!CModule::IncludeModule('nextype.magnet'))
        die('Module nextype.magnet not installed');
}

$arComponentParamsTmp = Array (
    'IS_AJAX' => $bAjaxMode ? 'Y' : 'N'
);

$arComponentParams = (!empty($_REQUEST['ajax_component_params'])) ? array_merge($arComponentParamsTmp, unserialize(urldecode($_REQUEST['ajax_component_params']))) : $arComponentParamsTmp;
$sComponentTemplate = (!empty($sComponentTemplateTmp)) ? $sComponentTemplateTmp : 'main';
$sComponentTemplate = (!empty($_REQUEST['ajax_component_template'])) ? urldecode($_REQUEST['ajax_component_template']) : $sComponentTemplate;

CSolution::getInstance(SITE_ID);

$APPLICATION->IncludeComponent(
        "nextype:magnet.reviews", $sComponentTemplate, $arComponentParams, false, array("HIDE_ICONS" => "Y")
);
if ($bAjaxMode)
{
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>