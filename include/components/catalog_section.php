<?
$bAjaxMode = ($_REQUEST['is_ajax_mode'] == 'y') ? true : false;
if ($bAjaxMode)
{
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}

if (!defined('B_PROLOG_INCLUDED') && empty($arComponentParamsTmp))
{
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    header("Location: " . SITE_DIR);
    exit();
}

use Nextype\Magnet\CSolution;
use Nextype\Magnet\CCache;
use Nextype\Magnet\CLocations;

$arComponentParams = (!empty($_REQUEST['ajax_component_params'])) ? unserialize(urldecode($_REQUEST['ajax_component_params'])) : $arComponentParamsTmp;
$sComponentTemplate = (!empty($sComponentTemplateTmp)) ? $sComponentTemplateTmp : 'main';
$sComponentTemplate = (!empty($_REQUEST['ajax_component_template'])) ? urldecode($_REQUEST['ajax_component_template']) : $sComponentTemplate;

if (!empty($_REQUEST['ajax_component_filter']))
    $GLOBALS[$arComponentParams['FILTER_NAME']] = (!empty($_REQUEST['ajax_component_filter'])) ? unserialize(urldecode($_REQUEST['ajax_component_filter'])) : Array ();

CSolution::getInstance(SITE_ID);
CLocations::setFilter($arComponentParams);
$APPLICATION->IncludeComponent(
        "bitrix:catalog.section", $sComponentTemplate, $arComponentParams, false, array("HIDE_ICONS" => "Y")
);
if ($bAjaxMode)
{
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>