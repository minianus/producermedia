<?php

$module_id = 'nextype.magnet';
$isLocalModuleDir = false;

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$module_id."/include.php"))
        $isLocalModuleDir = true;

if ($isLocalModuleDir)
            include($_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $module_id . "/tools/robots.php");
        else
            include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/tools/robots.php");
