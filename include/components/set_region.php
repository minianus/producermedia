<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? if(!CModule::IncludeModule("nextype.magnet"))
    die('Error include nextype.magnet module'); 

$pathUrl = !empty($_REQUEST['r']) ? urldecode($_REQUEST['r']) : SITE_DIR;

if (intval($_REQUEST['id']) > 0)
{
    $result = \Nextype\Magnet\CLocations::setRegion(intval($_REQUEST['id']));
    if (is_string($result))
    {
        $protocol = (CMain::IsHTTPS()) ? "https://" : "http://";
        LocalRedirect($protocol . $result . $pathUrl);
    }
}

LocalRedirect($pathUrl);

$APPLICATION->FinalActions();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>