<?
$bAjaxMode = ($_REQUEST['is_ajax_mode'] == 'y') ? true : false;
if ($bAjaxMode)
{
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    
    if (! \Bitrix\Main\Loader::includeModule('nextype.magnet') )
        die(GetMessage('NOT_MODULE_INSTALLED'));
}

use Nextype\Magnet\CSolution;

CSolution::getInstance(SITE_ID);

$arParams['TEMPLATE'] = empty($arParams['TEMPLATE']) ? 'inline' : $arParams['TEMPLATE'];

$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", $arParams['TEMPLATE'], Array(
        "HIDE_ON_BASKET_PAGES" => "N",
        "PATH_TO_BASKET" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_CART']),
        "PATH_TO_ORDER" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_ORDER']),
        "PATH_TO_PERSONAL" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PERSONAL']),
        "PATH_TO_PROFILE" => str_replace("#SITE_DIR#", SITE_DIR, CSolution::$options['LINK_PERSONAL']),
        "PATH_TO_REGISTER" => SITE_DIR."auth/",
        "SHOW_PRODUCTS" => $arParams['SHOW_PRODUCTS'] ? $arParams['SHOW_PRODUCTS'] : "N",
        "PATH_TO_CATALOG" => $arParams['PATH_TO_CATALOG']
    )
);

if ($bAjaxMode)
{
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>

