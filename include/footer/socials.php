<?php
$CSolution = \Nextype\Magnet\CSolution::getInstance();
$arSocials = Array (
    'SOCIALS_VK', 'SOCIALS_FACEBOOK', 'SOCIALS_TWITTER', 'SOCIALS_YOUTUBE', 'SOCIALS_INSTAGRAM', 'SOCIALS_OK'
);
$arResult = Array ();
foreach ($arSocials as $key)
{
    if (isset($CSolution::$options[$key]) && !empty($CSolution::$options[$key]))
    {
        $arResult[] = Array (
            'CLASS' => strtolower(str_replace("SOCIALS_", "", $key)),
            'LINK' => $CSolution::$options[$key]
        );
    }
}
?>
<? if (count($arResult) > 0): ?>
<div class="name">Мы в социальных сетях</div>
    <div class="links">
        <? foreach ($arResult as $arItem): ?>
        <a href="<?=$arItem['LINK']?>" target="_blank" class="item icon-custom <?=$arItem['CLASS']?>"></a>
        <? endforeach; ?>
    </div>
<? endif; ?>




