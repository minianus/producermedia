<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"main",
	Array(
		"CATEGORY_0" => array("iblock_nt_magnet_catalog"),
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0_iblock_nt_magnet_catalog" => array('19'),
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "title-search",
		"INPUT_ID" => "title-search-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => "/catalog/",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "Y"
	)
);?>