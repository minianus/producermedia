<a href="<?=SITE_DIR?>">
    <? $arLogo = \Nextype\Magnet\CSolution::getSiteLogo(); ?>
    <? if (!empty($arLogo['SRC'])): ?>
        <img src="<?=$arLogo['SRC']?>" alt="<?=$arLogo['ALT']?>" title="<?=$arLogo['TITLE']?>">
    <? else: ?>
        <? include($arLogo['DEFAULT']) ?>
    <? endif; ?>
</a>