<div class="policy">
    <input id="agreement_checkbox_<?=$arParams['FORM_ID']?>" required="required" type="checkbox" value="Y" <?=$arParams['CHECKED'] == "Y" ? 'checked="checked"' : ''?>>
    <label for="agreement_checkbox_<?=$arParams['FORM_ID']?>">Я согласен с <a target="_blank" href="<?=$arParams['DETAIL_PAGE_URL']?>">условиями обработки</a> персональных данных</label>
</div>
<script>
$("#agreement_checkbox_<?=$arParams['FORM_ID']?>").on('change', function () {
    if ($(this).is(':checked'))
        $(this).parents('form').find('[type="submit"]').removeAttr('disabled');
    else
        $(this).parents('form').find('[type="submit"]').attr('disabled', 'disabled');
});
</script>