<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"homepage", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"BLOCK_MORE_LINK" => "/catalog/",
		"BLOCK_MORE_TITLE" => "Весь каталог",
		"BLOCK_TITLE" => "Популярные категории",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "nt_magnet_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "DESCRIPTION",
			1 => "PICTURE",
			2 => "UF_HIDE_HOMEPAGE",
			3 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "homepage",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

