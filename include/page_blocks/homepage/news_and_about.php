<div class="about-and-news">
    
    <section class="about" data-helper="homepage::about">
        <div class="img">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/page_blocks/homepage/about_image.php'); ?>
            
        </div>
        <div class="content">
            <? $APPLICATION->IncludeFile(SITE_DIR . 'include/page_blocks/homepage/about_text.php'); ?>
            
        </div>
    </section>
    
    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/page_blocks/homepage/news.php'); ?>
</div>