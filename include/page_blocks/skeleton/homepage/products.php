<div class="skeleton-products-title"></div>
<div class="skeleton-products">
    <? for ($i = 0; $i < 12; $i++): ?>
    <div class="skeleton-card">
        <div class="skeleton-bg">
            <div class="card-skeleton-img"></div>
            <div class="card-skeleton-title"></div>
            <div class="card-skeleton-price"></div>
            <div class="card-skeleton-qty"></div>
        </div>
    </div>
    <? endfor; ?>
</div>

<style>
    .skeleton-products-title {
        max-width: 530px;
        background: #fff;
        height: 40px;
        margin-bottom: 20px;
    }
    
    .skeleton-products {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px;
    }
    
    .skeleton-products .skeleton-card {
        position: relative;
        margin: 0 12px 25px;
        width: calc(( 100% - 145px ) / 6);
        background: #fff;
        height: 440px;
        position: relative;
    }
    
    .skeleton-products .skeleton-bg {
        padding: 20px 20px 16px;
    }
        
    .skeleton-products .card-skeleton-img {
        width: 100%;
        height: 224px;
        background: #f5f5f5;
        margin-bottom: 16px;
    }
    
    .skeleton-products .card-skeleton-title {
        width: 90%;
        height: 30px;
        background: #f5f5f5;
        margin-bottom: 50px;
    }
    
    .skeleton-products .card-skeleton-price {
        width: 60%;
        height: 40px;
        background: #f5f5f5;
        margin-bottom: 10px;
    }
    
    .skeleton-products .card-skeleton-qty {
        width: 70%;
        height: 20px;
        background: #f5f5f5;
    }

    @media (max-width: 1680px) {
        .skeleton-products .skeleton-card {
            width: calc(( 100% - 121px ) / 5);
        }
        .skeleton-products .skeleton-card:nth-of-type(12), .skeleton-products .skeleton-card:nth-of-type(11) {
            display: none;
        }
    }

    @media (max-width: 1330px) {
        .skeleton-products .skeleton-card {
            width: calc(( 100% - 97px ) / 4);
        }
        .skeleton-products .skeleton-card:nth-of-type(9), .skeleton-products .skeleton-card:nth-of-type(10) {
            display: none;
        }
    }

    @media (max-width: 1150px) {
        .skeleton-products .skeleton-card {
            width: calc(( 100% - 73px ) / 3);
        }
        .skeleton-products .skeleton-card:nth-of-type(8), .skeleton-products .skeleton-card:nth-of-type(7) {
            display: none;
        }
    }

    @media (max-width: 700px) {
        .skeleton-products .skeleton-card {
            width: calc(( 100% - 49px ) / 2)
        }
        .skeleton-products .skeleton-card:nth-of-type(6), .skeleton-products .skeleton-card:nth-of-type(5), .skeleton-products .skeleton-card:nth-of-type(4), .skeleton-products .skeleton-card:nth-of-type(3) {
            display: none;
        }
        .skeleton-products .card-skeleton-img {
            height: 164px;
        }
    }
    @media (max-width: 460px) {
        /* .skeleton-products .skeleton-card {
            width: 100%;
        } */
        /* .skeleton-products .skeleton-card:nth-of-type(2) {
            display: none;
        } */
    }
    
</style>