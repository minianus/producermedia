<div class="skeleton-categories-title-wrap">
    <div class="skeleton-categories-title"></div>
    <div class="skeleton-categories-subtitle"></div>
</div>
<div class="skeleton-categories">
    <? for ($i = 0; $i < 6; $i++): ?>
    <div class="skeleton-card">
        <div class="skeleton-bg">
            <div class="card-skeleton-img"></div>
            <div class="card-skeleton-info-wrap">
                <div class="card-skeleton-title"></div>

                <div class="card-skeleton-subcategory"></div>
                <div class="card-skeleton-subcategory"></div>
                <div class="card-skeleton-subcategory"></div>
                <div class="card-skeleton-subcategory"></div>
                <div class="card-skeleton-subcategory"></div>

            </div>
            
        </div>
    </div>
    <? endfor; ?>
</div>

<style>
    .skeleton-categories-title-wrap {
        display: flex
    }

    .skeleton-categories-title {
        width: 260px;
        background: #fff;
        height: 30px;
        margin-bottom: 20px;
        margin-right: 18px
    }
    .skeleton-categories-subtitle {
        width: 90px;
        background: #fff;
        height: 24px;
        margin-bottom: 20px;
        margin-right: 18px;
        margin-top: auto;
    }
    
    .card-skeleton-subcategory {
        width: calc(100% / 3 - 16px);
        display: inline-block;
        background: #f5f5f5;
        height: 16px;
        margin: 0 4px 4px 0;
    }
    
    .skeleton-categories {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px 23px;
    }
    .card-skeleton-info-wrap {
        width: calc(100% - 98px);
    }
    .skeleton-categories .skeleton-card {
        position: relative;
        margin: 0 12px 25px;
        width: calc(( 100% - 72px ) / 3);
        background: #fff;
        position: relative;
        height: 130px;
    }
    
    .skeleton-categories .skeleton-bg {
        padding: 20px 20px 16px;
        display: flex;
    }
        
    .skeleton-categories .card-skeleton-img {
        width: 80px;
        height: 80px;
        background: #f5f5f5;
        margin-right: 18px;
    }
    
    .skeleton-categories .card-skeleton-title {
        width: 60%;
        height: 20px;
        background: #f5f5f5;
        margin-bottom: 16px;
    }
    
</style>