
<div class="skeleton-brands">
    <div class="skeleton-bg">
        <? for ($i = 0; $i < 8; $i++): ?>
        <div class="skeleton-card">
            
        </div>
        <? endfor; ?> 
    </div>
    
</div>

<style>
    .skeleton-brands {
        display: flex;
        flex-wrap: wrap;
        margin: 16px 0 0;
    }
    .skeleton-brands .skeleton-bg {
        display: flex;
        width: 100%;
        padding: 15px 72px;
        position: relative;
    }
    .skeleton-brands .skeleton-bg:after, .skeleton-brands .skeleton-bg:before {
        content: '';
        width: 42px;
        height: 42px;
        position: absolute;
        background: #f5f5f5;
        top: 50%;
        transform: translateY(-50%);
    }
    .skeleton-brands .skeleton-bg:after {
        right: 16px;
    }
    .skeleton-brands .skeleton-bg:before {
        left: 16px;
    }
    .skeleton-brands .skeleton-card {
        position: relative;
        margin: 0 8px 0 8px;
        width: calc(( 100% - 128px ) / 8);
        background: #f5f5f5;
        height: 100px;
        position: relative;
    }
    
</style>