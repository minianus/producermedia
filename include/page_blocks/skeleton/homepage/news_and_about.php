
<div class="skeleton-about-and-news">
    <div class="skeleton-about">
        <div class="skeleton-bg">
            <div class="skeleton-about-image"></div>
            <div class="skeleton-about-content">
                <div class="skeleton-about-title"></div>
                <div class="skeleton-about-text"></div>
                <div class="skeleton-about-button"></div>
            </div>
        </div>
    </div>
    <div class="skeleton-news">
        <div class="skeleton-bg">
            <div class="skeleton-news-title-row">
                <div class="skeleton-news-title"></div>
                <div class="skeleton-news-subtitle"></div>
            </div>
            <div class="skeleton-news-list">
                <? for ($i = 0; $i < 4; $i++): ?>
                    <div class="skeleton-card">
                        <div class="skeleton-news-item-image"></div>
                        <div class="skeleton-news-item-title"></div>
                        <div class="skeleton-news-item-date"></div>
                    </div>
                <? endfor; ?>
            </div>
        </div>
    </div>
</div>



<style>
    .skeleton-about-and-news {
        display: flex;
        flex-wrap: wrap;
    }
    .skeleton-about{
        width: 49.35%;
        margin: 0 1.3% 0 0;
        height: 350px;
    }
    .skeleton-about-image {
        width: 33%;
        max-width: 251px;
        height: 100%;
        background: #f5f5f5;
    }
    .skeleton-about .skeleton-bg {
        padding: 40px;
        display: flex;
    }
    .skeleton-news .skeleton-bg {
        padding: 38px 40px 24px;
    }
    .skeleton-about-content {
        width: 62%;
        margin-left: 5.25%;
        padding-top: 8px
    }
    .skeleton-about-title {
        width: 40%;
        height: 40px;
        background: #f5f5f5;
        margin-bottom: 20px;
    }
    .skeleton-about-text {
        width: 100%;
        background: #f5f5f5;
        height: 50%;
        margin-bottom: 16px;
    }
    .skeleton-about-button {
        width: 35%;
        height: 50px;
        background: #f5f5f5;
    }
    .skeleton-news {
        width: 49.35%;
        margin: 0 0 0 auto;
        height: 350px;
    }
    .skeleton-news-title-row {
        display: flex;
        width: 100%;
        margin-bottom: 32px;
    }
    .skeleton-news-title {
        width: 20%;
        height: 30px;
        background: #f5f5f5;
    }
    .skeleton-news-subtitle {
        width: 10%;
        height: 26px;
        background: #f5f5f5;
        margin-left: auto;
    }
    .skeleton-news-list {
        display: flex;
        margin: 23px -12px 0;
        width: 100%;
    }
    .skeleton-card {
        width: calc((100% - 96px)/4);
        margin: 0 12px 15px;
    }
    .skeleton-news-item-image {
        width: 100%;
        height: 125px;
        margin-bottom: 8px;
        background: #f5f5f5;
    }
    .skeleton-news-item-title {
        width: 100%;
        margin-bottom: 8px;
        background: #f5f5f5;
        height: 36px;
    }
    .skeleton-news-item-date {
        width: 50%;
        background: #f5f5f5;
        height: 23px;
    }
</style>