
<div class="skeleton-promo">
    <? for ($i = 0; $i < 3; $i++): ?>
    <div class="skeleton-card">
        <div class="skeleton-bg">
            <div class="card-skeleton-label"></div>
            <div class="card-skeleton-title"></div>
        </div>
    </div>
    <? endfor; ?>
</div>

<style>
    .skeleton-promo {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px;
    }

    .skeleton-promo .skeleton-card {
        position: relative;
        margin: 0 12px 25px;
        width: calc(( 100% - 72px ) / 3);
        background: #fff;
        position: relative;
        height: 200px;
    }
    
    .skeleton-promo .skeleton-bg {
        padding: 30px;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
    }
        
    .skeleton-promo .card-skeleton-img {
        width: 80px;
        height: 80px;
        background: #f5f5f5;
        margin-right: 18px;
    }
    
    .skeleton-promo .card-skeleton-label {
        width: 40%;
        height: 20px;
        background: #f5f5f5;
        margin-bottom: 16px;
    }
    
    .skeleton-promo .card-skeleton-title {
        width: 100%;
        height: 40px;
        background: #f5f5f5;
        margin-bottom: 10px;
    }
    
</style>