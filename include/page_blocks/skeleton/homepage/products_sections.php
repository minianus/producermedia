<div class="skeleton-items odd">
    <div class="skeleton-categories-block">
        <div class="skeleton-bg">
            <div class="skeleton-categories-block-title"></div>
            <div class="skeleton-categories-block-link"></div>
            <div class="skeleton-categories-block-img"></div>
        </div>
    </div>
    <div class="skeleton-products-new">
        <? for ($i = 0; $i < 4; $i++): ?>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="card-skeleton-img"></div>
                <div class="card-skeleton-title"></div>
                <div class="card-skeleton-price"></div>
                <div class="card-skeleton-qty"></div>
            </div>
        </div>
        <? endfor; ?>
    </div>
</div>

<div class="skeleton-items even">
    <div class="skeleton-products-new">
        <? for ($i = 0; $i < 4; $i++): ?>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="card-skeleton-img"></div>
                <div class="card-skeleton-title"></div>
                <div class="card-skeleton-price"></div>
                <div class="card-skeleton-qty"></div>
            </div>
        </div>
        <? endfor; ?>
    </div>
    <div class="skeleton-categories-block">
        <div class="skeleton-bg">
            <div class="skeleton-categories-block-title"></div>
            <div class="skeleton-categories-block-link"></div>
            <div class="skeleton-categories-block-img"></div>
        </div>
    </div>
</div>

<div class="skeleton-items odd">
    <div class="skeleton-categories-block">
        <div class="skeleton-bg">
            <div class="skeleton-categories-block-title"></div>
            <div class="skeleton-categories-block-link"></div>
            <div class="skeleton-categories-block-img"></div>
        </div>
    </div>
    <div class="skeleton-products-new">
        <? for ($i = 0; $i < 4; $i++): ?>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="card-skeleton-img"></div>
                <div class="card-skeleton-title"></div>
                <div class="card-skeleton-price"></div>
                <div class="card-skeleton-qty"></div>
            </div>
        </div>
        <? endfor; ?>
    </div>
</div>


<style>

    .skeleton-items {
        display: flex;
        margin-bottom: 50px;
    }
    .skeleton-items.odd .skeleton-categories-block {
        margin-right: 24px;
    }
    
    .skeleton-items.even .skeleton-categories-block {
        margin-left: 24px;
    }
    .skeleton-items.even .skeleton-categories-block-title {
        margin-left: auto;
    }
    .skeleton-items.even .skeleton-categories-block-link {
        margin-left: auto;
    }

    .skeleton-categories-block {
        width: 552px;
        height: 440px;
        background: #fff;
    }
    .skeleton-bg {
        padding: 32px;
    }
    .skeleton-categories-block-title {
        width: 75%;
        height: 30px;
        margin-bottom: 12px;
        background: #f5f5f5;
    }
    .skeleton-categories-block-link {
        width: 30%;
        height: 19px;
        margin-bottom: 32px;
        background: #f5f5f5;
    }
    .skeleton-categories-block-img {
        width: 100%;
        height: 282px;
        background: #f5f5f5;
    }

    .skeleton-products-new {
        display: flex;
        flex-wrap: wrap;
        width: calc(100% - 552px);
        margin: 0 -12px;
    }
    
    .skeleton-products-new .skeleton-card {
        position: relative;
        margin: 0 12px;
        width: calc(( 100% - 96px ) / 4);
        background: #fff;
        height: 440px;
    }
    
    .skeleton-products-new .skeleton-bg {
        padding: 20px 20px 16px;
    }
        
    .skeleton-products-new .card-skeleton-img {
        width: 100%;
        height: 224px;
        background: #f5f5f5;
        margin-bottom: 16px;
    }
    
    .skeleton-products-new .card-skeleton-title {
        width: 90%;
        height: 30px;
        background: #f5f5f5;
        margin-bottom: 50px;
    }
    
    .skeleton-products-new .card-skeleton-price {
        width: 60%;
        height: 40px;
        background: #f5f5f5;
        margin-bottom: 10px;
    }
    
    .skeleton-products-new .card-skeleton-qty {
        width: 70%;
        height: 20px;
        background: #f5f5f5;
    }

    @media (max-width: 1700px) {
        .skeleton-products-new .skeleton-card {
            width: calc(( 100% - 72px ) / 3);
        }
        .skeleton-products-new .skeleton-card:last-of-type {
            display: none;
        }
    }
    @media (max-width: 1439px) {
        .skeleton-categories-block {
            width: 400px;
            height: 440px;
            background: #fff;
        }
        .skeleton-products-new {
            width: calc(100% - 400px);
        }
        .skeleton-products-new .skeleton-card {
            width: calc(( 100% - 48px ) / 2);
        }
        .skeleton-products-new .skeleton-card:nth-of-type(3) {
            display: none;
        }
    }
    @media (max-width: 1025px) {
        .skeleton-items {
            flex-direction: column;
        }
        .skeleton-categories-block {
            width: 100%;
            height: 129px;
        }

        .skeleton-items.odd .skeleton-categories-block {
            margin: 0 0 24px 0;
        }
    
        .skeleton-items.even .skeleton-categories-block {
            margin: 0 0 24px 0;
        }
        .skeleton-items.even .skeleton-categories-block-title {
            margin-left: 0;
        }
        .skeleton-items.even .skeleton-categories-block-link {
            margin-left: 0;
        }

        .skeleton-categories-block-img {
            display: none;
        }
        .skeleton-products-new {
            width: calc(100% + 24px);
        }

        .skeleton-items.even .skeleton-products-new {
            order: 1;
        }

        .skeleton-products-new .skeleton-card {
            width: calc(( 100% - 96px ) / 4);
        }
        .skeleton-products-new .skeleton-card:last-of-type {
            display: block;
        }
        .skeleton-products-new .skeleton-card:nth-of-type(3) {
            display: block;
        }
    }

    @media (max-width: 950px) {
        .skeleton-products-new .skeleton-card {
            width: calc(( 100% - 72px ) / 3);
        }
        .skeleton-products-new .skeleton-card:last-of-type {
            display: none;
        }
    }
    @media (max-width: 767px) {
        .skeleton-products-new .skeleton-card {
            width: calc(( 100% - 48px ) / 2);
        }
        .skeleton-products-new .skeleton-card:nth-of-type(3) {
            display: none;
        }
    }
    @media (max-width: 480px) {
        .skeleton-products-new .skeleton-card {
            width: calc( 100% - 24px );
        }
        .skeleton-products-new .skeleton-card:nth-of-type(2) {
            display: none;
        }
    }

</style>