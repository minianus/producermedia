
<div class="skeleton-actions">
    <? for ($i = 0; $i < 6; $i++): ?>
    <div class="skeleton-card">
        <div class="skeleton-bg">
            <div class="card-skeleton-img"></div>
            <div class="card-skeleton-title"></div>
            <div class="card-skeleton-label"></div>
            <div class="card-skeleton-text"></div>
        </div>
    </div>
    <? endfor; ?>
</div>

<style>
    .skeleton-actions {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px;
    }
    
    .skeleton-actions .skeleton-card {
        position: relative;
        margin: 0 12px 25px;
        width: calc(( 100% - 144px ) / 6);
        background: #fff;
        height: 400px;
        position: relative;
    }
    
    .skeleton-actions .skeleton-bg {
        padding: 20px 20px;
    }
        
    .skeleton-actions .card-skeleton-img {
        width: 100%;
        height: 150px;
        background: #f5f5f5;
        margin-bottom: 24px;
    }
    
    .skeleton-actions .card-skeleton-title {
        width: 90%;
        height: 40px;
        background: #f5f5f5;
        margin-bottom: 14px;
    }
    .skeleton-actions .card-skeleton-label {
        width: 50%;
        height: 24px;
        background: #f5f5f5;
        margin-bottom: 18px;
    }
    
    .skeleton-actions .card-skeleton-text {
        width: 100%;
        height: 64px;
        background: #f5f5f5;
    }
    
</style>