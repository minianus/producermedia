
<div class="skeleton-instagram">
    <div class="skeleton-bg">
        <div class="skeleton-categories-title-wrap">
            <div class="skeleton-categories-title"></div>
            <div class="skeleton-categories-subtitle"></div>
        </div>
        <div class="skeleton-cards-list">
            <? for ($i = 0; $i < 8; $i++): ?>
                <div class="skeleton-card"> 
                </div>
            <? endfor; ?> 
        </div>
    </div>
    
</div>

<style>
    .skeleton-instagram {
        display: flex;
        flex-wrap: wrap;
        margin: 24px 0 24px;
    }
    .skeleton-instagram .skeleton-bg {
        width: 100%;
        padding: 2% 38px 30px;
    }
    .skeleton-instagram .skeleton-cards-list {
        display: flex;
        padding: 15px 0;
        margin: 0 -8px;
        position: relative;
    }
    .skeleton-instagram .skeleton-cards-list:after, .skeleton-instagram .skeleton-cards-list:before {
        content: '';
        width: 42px;
        height: 42px;
        position: absolute;
        background: #f5f5f5;
        top: 50%;
        transform: translateY(-50%);
        background-color: #fff;
        z-index: 1;
    }
    .skeleton-instagram .skeleton-cards-list:after {
        right: -12px;
    }
    .skeleton-instagram .skeleton-cards-list:before {
        left: -12px;
    }
    .skeleton-instagram .skeleton-categories-title,  .skeleton-instagram .skeleton-categories-subtitle{
        background-color: #f5f5f5;
    }
    .skeleton-instagram .skeleton-card {
        position: relative;
        margin: 0 8px 0 8px;
        width: calc(( 100% - 32px ) / 8);
        background: #f5f5f5;
        height: 200px;
        position: relative;
    }
    @media (max-width: 1769px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 28px ) / 7);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(8) {
            display: none;
        }
    }
    @media (max-width: 1579px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 24px ) / 6);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(7) {
            display: none;
        }
    }
    @media (max-width: 1369px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 20px ) / 5);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(6) {
            display: none;
        }
    }
    @media (max-width: 1119px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 16px ) / 4);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(5) {
            display: none;
        }
    }
    @media (max-width: 839px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 12px ) / 3);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(4) {
            display: none;
        }
    }
    @media (max-width: 589px) {
        .skeleton-instagram .skeleton-card {
            width: calc(( 100% - 8px ) / 2);
        }
        .skeleton-instagram .skeleton-card:nth-of-type(3) {
            display: none;
        }
    }
    @media (max-width: 419px) {
        .skeleton-instagram .skeleton-card {
            width: 100%;
        }
        .skeleton-instagram .skeleton-card:nth-of-type(2) {
            display: none;
        }
    }
</style>