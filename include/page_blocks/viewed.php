<?
use Nextype\Magnet\CSolution;
CSolution::getInstance(SITE_ID);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.viewed.products", 
	"main", 
	array(
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"SHOW_PRICE_COUNT" => "1",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PRICE_VAT_INCLUDE" => "Y",
		"USE_PRODUCT_QUANTITY" => "Y",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"PAGE_ELEMENT_COUNT" => "30",
		"SHOW_FROM_SECTION" => "N",
		"IBLOCK_ID" => CSolution::$options['CATALOG_IBLOCKID'],
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SHOW_PRODUCTS_37" => "Y",
		"ADDITIONAL_PICT_PROP_37" => "MORE_PHOTO",
		"LABEL_PROP_37" => "-",
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"LINE_ELEMENT_COUNT" => "3",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "nt_magnet_catalog",
		"SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
		"SECTION_ELEMENT_CODE" => "",
		"DEPTH" => "2",
		"SHOW_PRODUCTS_3" => "Y",
		"PROPERTY_CODE_3" => array(
			0 => "",
			1 => ",",
			2 => "",
		),
		"CART_PROPERTIES_3" => array(
			0 => "",
			1 => ",",
			2 => "",
		),
		"ADDITIONAL_PICT_PROP_3" => "PHOTOS",
		"LABEL_PROP_3" => "-",
		"PROPERTY_CODE_4" => array(
			0 => "",
			1 => "",
		),
		"CART_PROPERTIES_4" => array(
			0 => "",
			1 => "",
		),
		"ADDITIONAL_PICT_PROP_4" => "",
		"OFFER_TREE_PROPS_4" => array(
		)
	),
	false
);?>